-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 27, 2015 at 10:36 PM
-- Server version: 5.5.40
-- PHP Version: 5.6.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `orangehrm32`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `dashboard_get_subunit_parent_id`(
                  id INT
                ) RETURNS int(11)
    READS SQL DATA
    DETERMINISTIC
BEGIN
                SELECT (SELECT t2.id 
                               FROM ohrm_subunit t2 
                               WHERE t2.lft < t1.lft AND t2.rgt > t1.rgt    
                               ORDER BY t2.rgt-t1.rgt ASC LIMIT 1) INTO @parent
                FROM ohrm_subunit t1 WHERE t1.id = id;

                RETURN @parent;

                END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `allowance`
--

CREATE TABLE IF NOT EXISTS `allowance` (
`allowance_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `perce_basic` float DEFAULT NULL,
  `is_taxable` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `allowance`
--

INSERT INTO `allowance` (`allowance_id`, `title`, `amount`, `perce_basic`, `is_taxable`) VALUES
(1, 'Internet', 25000, 0, 0),
(22, 'Medical Allowance', 100000, 0, 0),
(24, 'Maternity Allowance', 120000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `deduction`
--

CREATE TABLE IF NOT EXISTS `deduction` (
`deduction_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `description` text,
  `date_created` date DEFAULT NULL,
  `emp_number` int(7) NOT NULL,
  `balance` float DEFAULT NULL,
  `is_recurring` tinyint(1) DEFAULT '0',
  `status` tinyint(4) DEFAULT NULL,
  `completed_date` date DEFAULT NULL,
  `payment_start` date DEFAULT NULL,
  `repayment_amount` float DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deduction`
--

INSERT INTO `deduction` (`deduction_id`, `title`, `amount`, `description`, `date_created`, `emp_number`, `balance`, `is_recurring`, `status`, `completed_date`, `payment_start`, `repayment_amount`) VALUES
(10, 'Advance Salary', 200000, '', '2015-01-21', 2, 120000, 1, 1, NULL, '2015-01-21', 10000),
(11, 'lost phone', 50000, '', '2015-01-24', 1, 10000, 0, 2, '2015-02-06', '2015-01-23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deduction_line`
--

CREATE TABLE IF NOT EXISTS `deduction_line` (
`line_id` int(11) NOT NULL,
  `amount` float DEFAULT NULL,
  `deduction_date` date DEFAULT NULL,
  `deduction_id` int(11) NOT NULL,
  `emp_number` int(7) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `balance` float DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deduction_line`
--

INSERT INTO `deduction_line` (`line_id`, `amount`, `deduction_date`, `deduction_id`, `emp_number`, `payroll_id`, `balance`) VALUES
(18, 50000, '2015-02-06', 11, 1, 59, 10000),
(19, 10000, '2015-02-06', 10, 2, 59, 130000),
(20, 10000, '2015-03-06', 10, 2, 60, 120000);

-- --------------------------------------------------------

--
-- Table structure for table `employee_loan`
--

CREATE TABLE IF NOT EXISTS `employee_loan` (
  `employee_loan_id` int(11) NOT NULL,
  `date_joined` date DEFAULT NULL,
  `emp_number` int(7) NOT NULL,
  `status` int(11) DEFAULT '0' COMMENT 'status ',
  `extra_info` text,
  `repayment_start` date DEFAULT NULL,
  `month_repayment` double DEFAULT NULL,
  `loan_id` int(11) NOT NULL,
  `balance` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_loan`
--

INSERT INTO `employee_loan` (`employee_loan_id`, `date_joined`, `emp_number`, `status`, `extra_info`, `repayment_start`, `month_repayment`, `loan_id`, `balance`) VALUES
(26, '2015-01-30', 2, 1, '', '2015-01-23', 208333.33, 31, 2916700),
(27, '2015-01-23', 1, 1, '', '2015-01-23', 208333.33, 31, 2708370);

-- --------------------------------------------------------

--
-- Table structure for table `financial_organization`
--

CREATE TABLE IF NOT EXISTS `financial_organization` (
`organization_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `extra_info` text
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `financial_organization`
--

INSERT INTO `financial_organization` (`organization_id`, `name`, `address`, `contact_number`, `extra_info`) VALUES
(1, 'NBC', 'Dar', '223344556', 'NBC Bank Loan'),
(3, 'CRDB', 'Dar', '0747474', 'Bank Loan Services'),
(6, 'The Company', 'Addres', '3030303', 'Loan by company itself');

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_config`
--

CREATE TABLE IF NOT EXISTS `hs_hr_config` (
  `key` varchar(100) NOT NULL DEFAULT '',
  `value` varchar(512) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_config`
--

INSERT INTO `hs_hr_config` (`key`, `value`) VALUES
('admin.default_workshift_end_time', '17:00'),
('admin.default_workshift_start_time', '09:00'),
('admin.localization.default_date_format', 'Y-m-d'),
('admin.localization.default_language', 'en_US'),
('admin.localization.use_browser_language', 'No'),
('admin.product_type', 'os'),
('attendanceEmpEditSubmitted', 'No'),
('attendanceSupEditSubmitted', 'No'),
('authorize_user_role_manager_class', 'BasicUserRoleManager'),
('beacon.activation_acceptance_status', 'off'),
('beacon.activiation_status', 'off'),
('beacon.company_name', ''),
('beacon.flash_period', '120'),
('beacon.lock', 'locked'),
('beacon.next_flash_time', '0000-00-00'),
('beacon.uuid', '0'),
('csrf_secret', '1o91j9i1v95aue1ger4bo1p4cdhp1532id1s2rnvk9dcu50bfi0cv'),
('hsp_accrued_last_updated', '0000-00-00'),
('hsp_current_plan', '0'),
('hsp_used_last_updated', '0000-00-00'),
('include_supervisor_chain', 'No'),
('ldap_domain_name', ''),
('ldap_port', ''),
('ldap_server', ''),
('ldap_status', ''),
('leave.entitlement_consumption_algorithm', 'FIFOEntitlementConsumptionStrategy'),
('leave.include_pending_leave_in_balance', '1'),
('leave.leavePeriodStatus', '1'),
('leave.work_schedule_implementation', 'BasicWorkSchedule'),
('leave_period_defined', 'Yes'),
('pim_show_deprecated_fields', '0'),
('report.mysql_group_concat_max_len', '2048'),
('showSIN', '0'),
('showSSN', '0'),
('showTaxExemptions', '0'),
('themeName', 'default'),
('timesheet_period_and_start_date', '<TimesheetPeriod><PeriodType>Weekly</PeriodType><ClassName>WeeklyTimesheetPeriod</ClassName><StartDate>1</StartDate><Heading>Week</Heading></TimesheetPeriod>'),
('timesheet_period_set', 'Yes'),
('timesheet_time_format', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_country`
--

CREATE TABLE IF NOT EXISTS `hs_hr_country` (
  `cou_code` char(2) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL DEFAULT '',
  `cou_name` varchar(80) NOT NULL DEFAULT '',
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_country`
--

INSERT INTO `hs_hr_country` (`cou_code`, `name`, `cou_name`, `iso3`, `numcode`) VALUES
('AD', 'ANDORRA', 'Andorra', 'AND', 20),
('AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784),
('AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4),
('AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28),
('AI', 'ANGUILLA', 'Anguilla', 'AIA', 660),
('AL', 'ALBANIA', 'Albania', 'ALB', 8),
('AM', 'ARMENIA', 'Armenia', 'ARM', 51),
('AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530),
('AO', 'ANGOLA', 'Angola', 'AGO', 24),
('AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL),
('AR', 'ARGENTINA', 'Argentina', 'ARG', 32),
('AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16),
('AT', 'AUSTRIA', 'Austria', 'AUT', 40),
('AU', 'AUSTRALIA', 'Australia', 'AUS', 36),
('AW', 'ARUBA', 'Aruba', 'ABW', 533),
('AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31),
('BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70),
('BB', 'BARBADOS', 'Barbados', 'BRB', 52),
('BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50),
('BE', 'BELGIUM', 'Belgium', 'BEL', 56),
('BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854),
('BG', 'BULGARIA', 'Bulgaria', 'BGR', 100),
('BH', 'BAHRAIN', 'Bahrain', 'BHR', 48),
('BI', 'BURUNDI', 'Burundi', 'BDI', 108),
('BJ', 'BENIN', 'Benin', 'BEN', 204),
('BM', 'BERMUDA', 'Bermuda', 'BMU', 60),
('BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96),
('BO', 'BOLIVIA', 'Bolivia', 'BOL', 68),
('BR', 'BRAZIL', 'Brazil', 'BRA', 76),
('BS', 'BAHAMAS', 'Bahamas', 'BHS', 44),
('BT', 'BHUTAN', 'Bhutan', 'BTN', 64),
('BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL),
('BW', 'BOTSWANA', 'Botswana', 'BWA', 72),
('BY', 'BELARUS', 'Belarus', 'BLR', 112),
('BZ', 'BELIZE', 'Belize', 'BLZ', 84),
('CA', 'CANADA', 'Canada', 'CAN', 124),
('CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL),
('CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180),
('CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140),
('CG', 'CONGO', 'Congo', 'COG', 178),
('CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756),
('CI', 'COTE D''IVOIRE', 'Cote D''Ivoire', 'CIV', 384),
('CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184),
('CL', 'CHILE', 'Chile', 'CHL', 152),
('CM', 'CAMEROON', 'Cameroon', 'CMR', 120),
('CN', 'CHINA', 'China', 'CHN', 156),
('CO', 'COLOMBIA', 'Colombia', 'COL', 170),
('CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188),
('CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL),
('CU', 'CUBA', 'Cuba', 'CUB', 192),
('CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132),
('CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL),
('CY', 'CYPRUS', 'Cyprus', 'CYP', 196),
('CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203),
('DE', 'GERMANY', 'Germany', 'DEU', 276),
('DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262),
('DK', 'DENMARK', 'Denmark', 'DNK', 208),
('DM', 'DOMINICA', 'Dominica', 'DMA', 212),
('DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214),
('DZ', 'ALGERIA', 'Algeria', 'DZA', 12),
('EC', 'ECUADOR', 'Ecuador', 'ECU', 218),
('EE', 'ESTONIA', 'Estonia', 'EST', 233),
('EG', 'EGYPT', 'Egypt', 'EGY', 818),
('EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732),
('ER', 'ERITREA', 'Eritrea', 'ERI', 232),
('ES', 'SPAIN', 'Spain', 'ESP', 724),
('ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231),
('FI', 'FINLAND', 'Finland', 'FIN', 246),
('FJ', 'FIJI', 'Fiji', 'FJI', 242),
('FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238),
('FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583),
('FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234),
('FR', 'FRANCE', 'France', 'FRA', 250),
('GA', 'GABON', 'Gabon', 'GAB', 266),
('GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826),
('GD', 'GRENADA', 'Grenada', 'GRD', 308),
('GE', 'GEORGIA', 'Georgia', 'GEO', 268),
('GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254),
('GH', 'GHANA', 'Ghana', 'GHA', 288),
('GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292),
('GL', 'GREENLAND', 'Greenland', 'GRL', 304),
('GM', 'GAMBIA', 'Gambia', 'GMB', 270),
('GN', 'GUINEA', 'Guinea', 'GIN', 324),
('GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312),
('GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226),
('GR', 'GREECE', 'Greece', 'GRC', 300),
('GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL),
('GT', 'GUATEMALA', 'Guatemala', 'GTM', 320),
('GU', 'GUAM', 'Guam', 'GUM', 316),
('GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624),
('GY', 'GUYANA', 'Guyana', 'GUY', 328),
('HK', 'HONG KONG', 'Hong Kong', 'HKG', 344),
('HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL),
('HN', 'HONDURAS', 'Honduras', 'HND', 340),
('HR', 'CROATIA', 'Croatia', 'HRV', 191),
('HT', 'HAITI', 'Haiti', 'HTI', 332),
('HU', 'HUNGARY', 'Hungary', 'HUN', 348),
('ID', 'INDONESIA', 'Indonesia', 'IDN', 360),
('IE', 'IRELAND', 'Ireland', 'IRL', 372),
('IL', 'ISRAEL', 'Israel', 'ISR', 376),
('IN', 'INDIA', 'India', 'IND', 356),
('IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL),
('IQ', 'IRAQ', 'Iraq', 'IRQ', 368),
('IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364),
('IS', 'ICELAND', 'Iceland', 'ISL', 352),
('IT', 'ITALY', 'Italy', 'ITA', 380),
('JM', 'JAMAICA', 'Jamaica', 'JAM', 388),
('JO', 'JORDAN', 'Jordan', 'JOR', 400),
('JP', 'JAPAN', 'Japan', 'JPN', 392),
('KE', 'KENYA', 'Kenya', 'KEN', 404),
('KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417),
('KH', 'CAMBODIA', 'Cambodia', 'KHM', 116),
('KI', 'KIRIBATI', 'Kiribati', 'KIR', 296),
('KM', 'COMOROS', 'Comoros', 'COM', 174),
('KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659),
('KP', 'KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF', 'Korea, Democratic People''s Republic of', 'PRK', 408),
('KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410),
('KW', 'KUWAIT', 'Kuwait', 'KWT', 414),
('KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136),
('KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398),
('LA', 'LAO PEOPLE''S DEMOCRATIC REPUBLIC', 'Lao People''s Democratic Republic', 'LAO', 418),
('LB', 'LEBANON', 'Lebanon', 'LBN', 422),
('LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662),
('LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438),
('LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144),
('LR', 'LIBERIA', 'Liberia', 'LBR', 430),
('LS', 'LESOTHO', 'Lesotho', 'LSO', 426),
('LT', 'LITHUANIA', 'Lithuania', 'LTU', 440),
('LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442),
('LV', 'LATVIA', 'Latvia', 'LVA', 428),
('LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434),
('MA', 'MOROCCO', 'Morocco', 'MAR', 504),
('MC', 'MONACO', 'Monaco', 'MCO', 492),
('MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498),
('MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450),
('MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584),
('MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807),
('ML', 'MALI', 'Mali', 'MLI', 466),
('MM', 'MYANMAR', 'Myanmar', 'MMR', 104),
('MN', 'MONGOLIA', 'Mongolia', 'MNG', 496),
('MO', 'MACAO', 'Macao', 'MAC', 446),
('MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580),
('MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474),
('MR', 'MAURITANIA', 'Mauritania', 'MRT', 478),
('MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500),
('MT', 'MALTA', 'Malta', 'MLT', 470),
('MU', 'MAURITIUS', 'Mauritius', 'MUS', 480),
('MV', 'MALDIVES', 'Maldives', 'MDV', 462),
('MW', 'MALAWI', 'Malawi', 'MWI', 454),
('MX', 'MEXICO', 'Mexico', 'MEX', 484),
('MY', 'MALAYSIA', 'Malaysia', 'MYS', 458),
('MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508),
('NA', 'NAMIBIA', 'Namibia', 'NAM', 516),
('NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540),
('NE', 'NIGER', 'Niger', 'NER', 562),
('NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574),
('NG', 'NIGERIA', 'Nigeria', 'NGA', 566),
('NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558),
('NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528),
('NO', 'NORWAY', 'Norway', 'NOR', 578),
('NP', 'NEPAL', 'Nepal', 'NPL', 524),
('NR', 'NAURU', 'Nauru', 'NRU', 520),
('NU', 'NIUE', 'Niue', 'NIU', 570),
('NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554),
('OM', 'OMAN', 'Oman', 'OMN', 512),
('PA', 'PANAMA', 'Panama', 'PAN', 591),
('PE', 'PERU', 'Peru', 'PER', 604),
('PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258),
('PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598),
('PH', 'PHILIPPINES', 'Philippines', 'PHL', 608),
('PK', 'PAKISTAN', 'Pakistan', 'PAK', 586),
('PL', 'POLAND', 'Poland', 'POL', 616),
('PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666),
('PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612),
('PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630),
('PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL),
('PT', 'PORTUGAL', 'Portugal', 'PRT', 620),
('PW', 'PALAU', 'Palau', 'PLW', 585),
('PY', 'PARAGUAY', 'Paraguay', 'PRY', 600),
('QA', 'QATAR', 'Qatar', 'QAT', 634),
('RE', 'REUNION', 'Reunion', 'REU', 638),
('RO', 'ROMANIA', 'Romania', 'ROM', 642),
('RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643),
('RW', 'RWANDA', 'Rwanda', 'RWA', 646),
('SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682),
('SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90),
('SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690),
('SD', 'SUDAN', 'Sudan', 'SDN', 736),
('SE', 'SWEDEN', 'Sweden', 'SWE', 752),
('SG', 'SINGAPORE', 'Singapore', 'SGP', 702),
('SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654),
('SI', 'SLOVENIA', 'Slovenia', 'SVN', 705),
('SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744),
('SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703),
('SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694),
('SM', 'SAN MARINO', 'San Marino', 'SMR', 674),
('SN', 'SENEGAL', 'Senegal', 'SEN', 686),
('SO', 'SOMALIA', 'Somalia', 'SOM', 706),
('SR', 'SURINAME', 'Suriname', 'SUR', 740),
('ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678),
('SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222),
('SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760),
('SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748),
('TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796),
('TD', 'CHAD', 'Chad', 'TCD', 148),
('TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL),
('TG', 'TOGO', 'Togo', 'TGO', 768),
('TH', 'THAILAND', 'Thailand', 'THA', 764),
('TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762),
('TK', 'TOKELAU', 'Tokelau', 'TKL', 772),
('TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL),
('TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795),
('TN', 'TUNISIA', 'Tunisia', 'TUN', 788),
('TO', 'TONGA', 'Tonga', 'TON', 776),
('TR', 'TURKEY', 'Turkey', 'TUR', 792),
('TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780),
('TV', 'TUVALU', 'Tuvalu', 'TUV', 798),
('TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan', 'TWN', 158),
('TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834),
('UA', 'UKRAINE', 'Ukraine', 'UKR', 804),
('UG', 'UGANDA', 'Uganda', 'UGA', 800),
('UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL),
('US', 'UNITED STATES', 'United States', 'USA', 840),
('UY', 'URUGUAY', 'Uruguay', 'URY', 858),
('UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860),
('VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336),
('VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670),
('VE', 'VENEZUELA', 'Venezuela', 'VEN', 862),
('VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92),
('VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850),
('VN', 'VIET NAM', 'Viet Nam', 'VNM', 704),
('VU', 'VANUATU', 'Vanuatu', 'VUT', 548),
('WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876),
('WS', 'SAMOA', 'Samoa', 'WSM', 882),
('YE', 'YEMEN', 'Yemen', 'YEM', 887),
('YT', 'MAYOTTE', 'Mayotte', NULL, NULL),
('ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710),
('ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894),
('ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716);

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_currency_type`
--

CREATE TABLE IF NOT EXISTS `hs_hr_currency_type` (
  `code` int(11) NOT NULL DEFAULT '0',
  `currency_id` char(3) NOT NULL DEFAULT '',
  `currency_name` varchar(70) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_currency_type`
--

INSERT INTO `hs_hr_currency_type` (`code`, `currency_id`, `currency_name`) VALUES
(3, 'AED', 'Utd. Arab Emir. Dirham'),
(4, 'AFN', 'Afghanistan Afghani'),
(5, 'ALL', 'Albanian Lek'),
(6, 'ANG', 'NL Antillian Guilder'),
(7, 'AOR', 'Angolan New Kwanza'),
(177, 'ARP', 'Argentina Pesos'),
(8, 'ARS', 'Argentine Peso'),
(10, 'AUD', 'Australian Dollar'),
(11, 'AWG', 'Aruban Florin'),
(12, 'BBD', 'Barbados Dollar'),
(13, 'BDT', 'Bangladeshi Taka'),
(15, 'BGL', 'Bulgarian Lev'),
(16, 'BHD', 'Bahraini Dinar'),
(17, 'BIF', 'Burundi Franc'),
(18, 'BMD', 'Bermudian Dollar'),
(19, 'BND', 'Brunei Dollar'),
(20, 'BOB', 'Bolivian Boliviano'),
(21, 'BRL', 'Brazilian Real'),
(22, 'BSD', 'Bahamian Dollar'),
(23, 'BTN', 'Bhutan Ngultrum'),
(24, 'BWP', 'Botswana Pula'),
(25, 'BZD', 'Belize Dollar'),
(26, 'CAD', 'Canadian Dollar'),
(27, 'CHF', 'Swiss Franc'),
(28, 'CLP', 'Chilean Peso'),
(29, 'CNY', 'Chinese Yuan Renminbi'),
(30, 'COP', 'Colombian Peso'),
(31, 'CRC', 'Costa Rican Colon'),
(32, 'CUP', 'Cuban Peso'),
(33, 'CVE', 'Cape Verde Escudo'),
(34, 'CYP', 'Cyprus Pound'),
(171, 'CZK', 'Czech Koruna'),
(37, 'DJF', 'Djibouti Franc'),
(38, 'DKK', 'Danish Krona'),
(39, 'DOP', 'Dominican Peso'),
(40, 'DZD', 'Algerian Dinar'),
(41, 'ECS', 'Ecuador Sucre'),
(43, 'EEK', 'Estonian Krona'),
(44, 'EGP', 'Egyptian Pound'),
(46, 'ETB', 'Ethiopian Birr'),
(42, 'EUR', 'Euro'),
(48, 'FJD', 'Fiji Dollar'),
(49, 'FKP', 'Falkland Islands Pound'),
(51, 'GBP', 'Pound Sterling'),
(52, 'GHC', 'Ghanaian Cedi'),
(53, 'GIP', 'Gibraltar Pound'),
(54, 'GMD', 'Gambian Dalasi'),
(55, 'GNF', 'Guinea Franc'),
(57, 'GTQ', 'Guatemalan Quetzal'),
(58, 'GYD', 'Guyanan Dollar'),
(59, 'HKD', 'Hong Kong Dollar'),
(60, 'HNL', 'Honduran Lempira'),
(61, 'HRK', 'Croatian Kuna'),
(62, 'HTG', 'Haitian Gourde'),
(63, 'HUF', 'Hungarian Forint'),
(64, 'IDR', 'Indonesian Rupiah'),
(66, 'ILS', 'Israeli New Shekel'),
(67, 'INR', 'Indian Rupee'),
(68, 'IQD', 'Iraqi Dinar'),
(69, 'IRR', 'Iranian Rial'),
(70, 'ISK', 'Iceland Krona'),
(72, 'JMD', 'Jamaican Dollar'),
(73, 'JOD', 'Jordanian Dinar'),
(74, 'JPY', 'Japanese Yen'),
(75, 'KES', 'Kenyan Shilling'),
(76, 'KHR', 'Kampuchean Riel'),
(77, 'KMF', 'Comoros Franc'),
(78, 'KPW', 'North Korean Won'),
(79, 'KRW', 'Korean Won'),
(80, 'KWD', 'Kuwaiti Dinar'),
(81, 'KYD', 'Cayman Islands Dollar'),
(82, 'KZT', 'Kazakhstan Tenge'),
(83, 'LAK', 'Lao Kip'),
(84, 'LBP', 'Lebanese Pound'),
(85, 'LKR', 'Sri Lanka Rupee'),
(86, 'LRD', 'Liberian Dollar'),
(87, 'LSL', 'Lesotho Loti'),
(88, 'LTL', 'Lithuanian Litas'),
(90, 'LVL', 'Latvian Lats'),
(91, 'LYD', 'Libyan Dinar'),
(92, 'MAD', 'Moroccan Dirham'),
(93, 'MGF', 'Malagasy Franc'),
(94, 'MMK', 'Myanmar Kyat'),
(95, 'MNT', 'Mongolian Tugrik'),
(96, 'MOP', 'Macau Pataca'),
(97, 'MRO', 'Mauritanian Ouguiya'),
(98, 'MTL', 'Maltese Lira'),
(99, 'MUR', 'Mauritius Rupee'),
(100, 'MVR', 'Maldive Rufiyaa'),
(101, 'MWK', 'Malawi Kwacha'),
(102, 'MXN', 'Mexican New Peso'),
(172, 'MXP', 'Mexican Peso'),
(103, 'MYR', 'Malaysian Ringgit'),
(104, 'MZM', 'Mozambique Metical'),
(105, 'NAD', 'Namibia Dollar'),
(106, 'NGN', 'Nigerian Naira'),
(107, 'NIO', 'Nicaraguan Cordoba Oro'),
(109, 'NOK', 'Norwegian Krona'),
(110, 'NPR', 'Nepalese Rupee'),
(111, 'NZD', 'New Zealand Dollar'),
(112, 'OMR', 'Omani Rial'),
(113, 'PAB', 'Panamanian Balboa'),
(114, 'PEN', 'Peruvian Nuevo Sol'),
(115, 'PGK', 'Papua New Guinea Kina'),
(116, 'PHP', 'Philippine Peso'),
(117, 'PKR', 'Pakistan Rupee'),
(118, 'PLN', 'Polish Zloty'),
(120, 'PYG', 'Paraguay Guarani'),
(121, 'QAR', 'Qatari Rial'),
(122, 'ROL', 'Romanian Leu'),
(123, 'RUB', 'Russian Rouble'),
(180, 'RUR', 'Russia Rubles'),
(173, 'SAR', 'Saudi Arabia Riyal'),
(125, 'SBD', 'Solomon Islands Dollar'),
(126, 'SCR', 'Seychelles Rupee'),
(127, 'SDD', 'Sudanese Dinar'),
(128, 'SDP', 'Sudanese Pound'),
(129, 'SEK', 'Swedish Krona'),
(131, 'SGD', 'Singapore Dollar'),
(132, 'SHP', 'St. Helena Pound'),
(130, 'SKK', 'Slovak Koruna'),
(135, 'SLL', 'Sierra Leone Leone'),
(136, 'SOS', 'Somali Shilling'),
(137, 'SRD', 'Surinamese Dollar'),
(138, 'STD', 'Sao Tome/Principe Dobra'),
(139, 'SVC', 'El Salvador Colon'),
(140, 'SYP', 'Syrian Pound'),
(141, 'SZL', 'Swaziland Lilangeni'),
(142, 'THB', 'Thai Baht'),
(143, 'TND', 'Tunisian Dinar'),
(144, 'TOP', 'Tongan Pa''anga'),
(145, 'TRL', 'Turkish Lira'),
(146, 'TTD', 'Trinidad/Tobago Dollar'),
(147, 'TWD', 'Taiwan Dollar'),
(148, 'TZS', 'Tanzanian Shilling'),
(149, 'UAH', 'Ukraine Hryvnia'),
(150, 'UGX', 'Uganda Shilling'),
(151, 'USD', 'United States Dollar'),
(152, 'UYP', 'Uruguayan Peso'),
(153, 'VEB', 'Venezuelan Bolivar'),
(154, 'VND', 'Vietnamese Dong'),
(155, 'VUV', 'Vanuatu Vatu'),
(156, 'WST', 'Samoan Tala'),
(158, 'XAF', 'CFA Franc BEAC'),
(159, 'XAG', 'Silver (oz.)'),
(160, 'XAU', 'Gold (oz.)'),
(161, 'XCD', 'Eastern Caribbean Dollars'),
(179, 'XDR', 'IMF Special Drawing Right'),
(162, 'XOF', 'CFA Franc BCEAO'),
(163, 'XPD', 'Palladium (oz.)'),
(164, 'XPF', 'CFP Franc'),
(165, 'XPT', 'Platinum (oz.)'),
(166, 'YER', 'Yemeni Riyal'),
(167, 'YUM', 'Yugoslavian Dinar'),
(175, 'YUN', 'Yugoslav Dinar'),
(168, 'ZAR', 'South African Rand'),
(176, 'ZMK', 'Zambian Kwacha'),
(169, 'ZRN', 'New Zaire'),
(170, 'ZWD', 'Zimbabwe Dollar');

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_custom_export`
--

CREATE TABLE IF NOT EXISTS `hs_hr_custom_export` (
  `export_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `fields` text,
  `headings` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_custom_fields`
--

CREATE TABLE IF NOT EXISTS `hs_hr_custom_fields` (
  `field_num` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  `screen` varchar(100) DEFAULT NULL,
  `extra_data` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_custom_import`
--

CREATE TABLE IF NOT EXISTS `hs_hr_custom_import` (
  `import_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `fields` text,
  `has_heading` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_district`
--

CREATE TABLE IF NOT EXISTS `hs_hr_district` (
  `district_code` varchar(13) NOT NULL DEFAULT '',
  `district_name` varchar(50) DEFAULT NULL,
  `province_code` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_employee`
--

CREATE TABLE IF NOT EXISTS `hs_hr_employee` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `employee_id` varchar(50) DEFAULT NULL,
  `emp_lastname` varchar(100) NOT NULL DEFAULT '',
  `emp_firstname` varchar(100) NOT NULL DEFAULT '',
  `emp_middle_name` varchar(100) NOT NULL DEFAULT '',
  `emp_nick_name` varchar(100) DEFAULT '',
  `emp_smoker` smallint(6) DEFAULT '0',
  `ethnic_race_code` varchar(13) DEFAULT NULL,
  `emp_birthday` date DEFAULT NULL,
  `nation_code` int(4) DEFAULT NULL,
  `emp_gender` smallint(6) DEFAULT NULL,
  `emp_marital_status` varchar(20) DEFAULT NULL,
  `emp_ssn_num` varchar(100) CHARACTER SET latin1 DEFAULT '',
  `emp_sin_num` varchar(100) DEFAULT '',
  `emp_other_id` varchar(100) DEFAULT '',
  `emp_dri_lice_num` varchar(100) DEFAULT '',
  `emp_dri_lice_exp_date` date DEFAULT NULL,
  `emp_military_service` varchar(100) DEFAULT '',
  `emp_status` int(13) DEFAULT NULL,
  `job_title_code` int(7) DEFAULT NULL,
  `eeo_cat_code` int(11) DEFAULT NULL,
  `work_station` int(6) DEFAULT NULL,
  `emp_street1` varchar(100) DEFAULT '',
  `emp_street2` varchar(100) DEFAULT '',
  `city_code` varchar(100) DEFAULT '',
  `coun_code` varchar(100) DEFAULT '',
  `provin_code` varchar(100) DEFAULT '',
  `emp_zipcode` varchar(20) DEFAULT NULL,
  `emp_hm_telephone` varchar(50) DEFAULT NULL,
  `emp_mobile` varchar(50) DEFAULT NULL,
  `emp_work_telephone` varchar(50) DEFAULT NULL,
  `emp_work_email` varchar(50) DEFAULT NULL,
  `sal_grd_code` varchar(13) DEFAULT NULL,
  `joined_date` date DEFAULT NULL,
  `emp_oth_email` varchar(50) DEFAULT NULL,
  `termination_id` int(4) DEFAULT NULL,
  `custom1` varchar(250) DEFAULT NULL,
  `custom2` varchar(250) DEFAULT NULL,
  `custom3` varchar(250) DEFAULT NULL,
  `custom4` varchar(250) DEFAULT NULL,
  `custom5` varchar(250) DEFAULT NULL,
  `custom6` varchar(250) DEFAULT NULL,
  `custom7` varchar(250) DEFAULT NULL,
  `custom8` varchar(250) DEFAULT NULL,
  `custom9` varchar(250) DEFAULT NULL,
  `custom10` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_employee`
--

INSERT INTO `hs_hr_employee` (`emp_number`, `employee_id`, `emp_lastname`, `emp_firstname`, `emp_middle_name`, `emp_nick_name`, `emp_smoker`, `ethnic_race_code`, `emp_birthday`, `nation_code`, `emp_gender`, `emp_marital_status`, `emp_ssn_num`, `emp_sin_num`, `emp_other_id`, `emp_dri_lice_num`, `emp_dri_lice_exp_date`, `emp_military_service`, `emp_status`, `job_title_code`, `eeo_cat_code`, `work_station`, `emp_street1`, `emp_street2`, `city_code`, `coun_code`, `provin_code`, `emp_zipcode`, `emp_hm_telephone`, `emp_mobile`, `emp_work_telephone`, `emp_work_email`, `sal_grd_code`, `joined_date`, `emp_oth_email`, `termination_id`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `custom6`, `custom7`, `custom8`, `custom9`, `custom10`) VALUES
(1, '0001', 'Doe', 'John', '', '', 0, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', NULL, 1, NULL, 2, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '0002', 'Doe', 'Jane', '', '', 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, '', NULL, 2, NULL, 5, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '0004', 'Smith', 'Josh', '', '', 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, '', NULL, 3, NULL, 3, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_attachment`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_attachment` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `eattach_id` int(11) NOT NULL DEFAULT '0',
  `eattach_desc` varchar(200) DEFAULT NULL,
  `eattach_filename` varchar(100) DEFAULT NULL,
  `eattach_size` int(11) DEFAULT '0',
  `eattach_attachment` mediumblob,
  `eattach_type` varchar(200) DEFAULT NULL,
  `screen` varchar(100) DEFAULT '',
  `attached_by` int(11) DEFAULT NULL,
  `attached_by_name` varchar(200) DEFAULT NULL,
  `attached_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_basicsalary`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_basicsalary` (
`id` int(11) NOT NULL,
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `sal_grd_code` int(11) DEFAULT NULL,
  `currency_id` varchar(6) NOT NULL DEFAULT '',
  `ebsal_basic_salary` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `payperiod_code` varchar(13) DEFAULT NULL,
  `salary_component` varchar(100) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_emp_basicsalary`
--

INSERT INTO `hs_hr_emp_basicsalary` (`id`, `emp_number`, `sal_grd_code`, `currency_id`, `ebsal_basic_salary`, `payperiod_code`, `salary_component`, `comments`) VALUES
(1, 1, 1, 'TZS', '1500000', '4', 'Basic', ''),
(4, 2, 1, 'TZS', '650000', '4', 'Basic', ''),
(5, 4, 1, 'TZS', '1200000', '4', 'Salary', '');

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_children`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_children` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `ec_seqno` decimal(2,0) NOT NULL DEFAULT '0',
  `ec_name` varchar(100) DEFAULT '',
  `ec_date_of_birth` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_contract_extend`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_contract_extend` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `econ_extend_id` decimal(10,0) NOT NULL DEFAULT '0',
  `econ_extend_start_date` datetime DEFAULT NULL,
  `econ_extend_end_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_emp_contract_extend`
--

INSERT INTO `hs_hr_emp_contract_extend` (`emp_number`, `econ_extend_id`, `econ_extend_start_date`, `econ_extend_end_date`) VALUES
(1, '1', NULL, NULL),
(2, '1', NULL, NULL),
(4, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_dependents`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_dependents` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `ed_seqno` decimal(2,0) NOT NULL DEFAULT '0',
  `ed_name` varchar(100) DEFAULT '',
  `ed_relationship_type` enum('child','other') DEFAULT NULL,
  `ed_relationship` varchar(100) DEFAULT '',
  `ed_date_of_birth` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_directdebit`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_directdebit` (
`id` int(11) NOT NULL,
  `salary_id` int(11) NOT NULL,
  `dd_routing_num` int(9) NOT NULL,
  `dd_account` varchar(100) NOT NULL DEFAULT '',
  `dd_amount` decimal(11,2) NOT NULL,
  `dd_account_type` varchar(20) NOT NULL DEFAULT '' COMMENT 'CHECKING, SAVINGS',
  `dd_transaction_type` varchar(20) NOT NULL DEFAULT '' COMMENT 'BLANK, PERC, FLAT, FLATMINUS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_emergency_contacts`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_emergency_contacts` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `eec_seqno` decimal(2,0) NOT NULL DEFAULT '0',
  `eec_name` varchar(100) DEFAULT '',
  `eec_relationship` varchar(100) DEFAULT '',
  `eec_home_no` varchar(100) DEFAULT '',
  `eec_mobile_no` varchar(100) DEFAULT '',
  `eec_office_no` varchar(100) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_history_of_ealier_pos`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_history_of_ealier_pos` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `emp_seqno` decimal(2,0) NOT NULL DEFAULT '0',
  `ehoep_job_title` varchar(100) DEFAULT '',
  `ehoep_years` varchar(100) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_language`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_language` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `lang_id` int(11) NOT NULL,
  `fluency` smallint(6) NOT NULL DEFAULT '0',
  `competency` smallint(6) DEFAULT '0',
  `comments` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_locations`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_locations` (
  `emp_number` int(11) NOT NULL,
  `location_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_member_detail`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_member_detail` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `membship_code` int(6) NOT NULL DEFAULT '0',
  `ememb_subscript_ownership` varchar(20) DEFAULT NULL,
  `ememb_subscript_amount` decimal(15,2) DEFAULT NULL,
  `ememb_subs_currency` varchar(20) DEFAULT NULL,
  `ememb_commence_date` date DEFAULT NULL,
  `ememb_renewal_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_passport`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_passport` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `ep_seqno` decimal(2,0) NOT NULL DEFAULT '0',
  `ep_passport_num` varchar(100) NOT NULL DEFAULT '',
  `ep_passportissueddate` datetime DEFAULT NULL,
  `ep_passportexpiredate` datetime DEFAULT NULL,
  `ep_comments` varchar(255) DEFAULT NULL,
  `ep_passport_type_flg` smallint(6) DEFAULT NULL,
  `ep_i9_status` varchar(100) DEFAULT '',
  `ep_i9_review_date` date DEFAULT NULL,
  `cou_code` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_picture`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_picture` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `epic_picture` mediumblob,
  `epic_filename` varchar(100) DEFAULT NULL,
  `epic_type` varchar(50) DEFAULT NULL,
  `epic_file_size` varchar(20) DEFAULT NULL,
  `epic_file_width` varchar(20) DEFAULT NULL,
  `epic_file_height` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_emp_picture`
--

INSERT INTO `hs_hr_emp_picture` (`emp_number`, `epic_picture`, `epic_filename`, `epic_type`, `epic_file_size`, `epic_file_width`, `epic_file_height`) VALUES
(1, 0xffd8ffe000104a46494600010100000100010000fffe003c43524541544f523a2067642d6a7065672076312e3020287573696e6720494a47204a50454720763632292c207175616c697479203d203130300affdb00430001010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101ffdb00430101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101ffc0001108013301cc03012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00fefc47f9e9d3bf5ff3f4a08c7e59ff0027bf1cfe38a3f2ff0039f4ff00f5f4c52e7dba0e7afd391e9d38fce80020704743d7a641efc71fcb141539c7eb8c0e3dfa76fcfad274ff00eb673d3a7a7f9eb4e078e9d33fdeeff98faf23f0ee0084018ce71f873d3a73faf3f4e334a0004e78c7b03c7033d3d7dbf1eb499e9c0f4cf3e807ff00aff4c752a0f3c0f70393cfa8fe7db8f7c500211f5fa639f6fc0938e3a77e702940fba39cf24f03819eff008f5073e9c7426ec8e47e65b919fc7f3cff00852e73d00ee4fdef5cf381ce3f11f4e8400dbd791d7ae0606319cf071ec381c5340e99238f4c13d79fa8e339e7e98cd3b39078079cf1bbf0ededc723f0a4ce0741db232dee47d3a9ee73d680108e78e40e3231f4e9f5e99ebebd0d21c60633dfd07f2c9fcff0a09e4f18f5ebcf7efc8fa519e7279fad0027f9ff000ffebd1fe7fc2973d3dbebfe3fcb15f3ff00ed0bf1aedbe0df832ff52b55b7baf11cb617b3e9767390ea9e4c2ca970d6fe644d7724976d6f6d6b651c8b2dccb29f2d66317d9e6ba74e756718415e527f24bab6fa24b56fb1339282e696d74925bb6dd924bbb7f2eada5767a07c46f8a7f0ff00e1368327893e2178a34bf0ce94ac2389efa626eaf672caab6da75842b2deea170cce8be55a412b2ee0d26c4cb0fc70f8e3ff0005a0f0cf87ee359d2be087c3fb4f15cba7c7762dfc47e32d59f4fd3ae2e34eb88e1d4235d12c5ad9d45b42c6eb377e23b19841b259eda21340b3fe34fed43fb46f8fbe39f8eef3c41e26bf9e0b8b0fb58822d664d4eca1b7b1b49351d6e0b59937ae9fa6ddc36be648fa7d9ccd14d666df4996ecc68d01f8e27b5bbb93b2078ae6ce68edc5c6949ab5ec30b5fdd0b6b99e4d52d67be9aec4d6d7f73716a219acd9da7793cc6f2e3b3827de6f0b866a33b55a8dabb93946117a36a3156727d139369c5b7caa4923aa8e0311888f3c9ca9c2d750824e4d696729eba6e9f2f259af8a4aecfd1ff15ffc16bbf6dbd72f35097c3371e0df0bd8da4f145731e93f0d23bdb6d21673b10dede7889b5d9a670cae4c51c7148a83ce62b185327303fe0b55fb75e9243cfe30f87faac8b2e67b7bef87fe1c48a040885629a2d321d3ae60de63988325dbdc3f98882084a16af846d74136fa2ba5cdeacfa8344d751ac905f5e5cde4b6172b1949e17d968274b2b4b6982450b0b68e120cb1655a6e43c41e188ad9ade782edd65b684dc43a825edac8a90bcd1cc6e56ca56758960b7f2ae52225a2092cd241729344b22e7fda9838693a346f7b25ece17fb3de37bea95d36aede9dfa7fb0ab492719d56ad76fda55f9eaa765b37676e976f75fb15e01ff0083813e3a69ac1be23fc2bf853e22b1814c97634b97c41e0dd5da3036eeb79df59f14585c069b319dba5452a15f9a0016431fe9a7c0dff82dcfec95f141f4fd3fc7675ef841ab5f98116e3568a5f12f85966988411b6bba4d941a84237956f3eebc3d6f66b04b0cd25da2c985fe3b2ef4892f2d6ef50b116893a4b771dbbc304a34ad49df37171f68b3bb6bf70f33b2496168cede44909303c4ae218bce00f10abc502e9b24b1c125ddd69e618af0b5c59c71c9bece0b69525b416d6c82361f6b9408ff007892fc910895ac6e02aa77a50834d26e15391ad16b6d60d3db54ecefaab3392a6518ca4e2e9d6aaeebe19c3da45bd347cdef77f8671766babb9fe98de0ef1b783fe2168569e27f0278a7c3fe30f0edfae6cf5bf0dead61ace9939daacd1ade584f3c2268c3a8960671342c424b1a30207518ce7d4673f281dff4fc781d32075ff3a9fd9bff006ccfda0bf656f18c5aefc2ef1aea5e1e9cde22eb9e1f9d5af7c3facd8ab46f1daebbe1e907f666aeeb0db4f6d130822bbb6849bad3af2ddc89a3fec4ff0060ff00f82a27c1efdb1adec3c19aab5afc3cf8e4b64d34fe09bdb966d2fc56b691bb5e6a9e04d4ee3636a10ec866ba9743ba2358b0804a636d4edeceeefe3254a2d73d093a91dda6ad38ab5eed2bf3457f32e9ab8c51c7cf5294953c4c15393692a91bba5393764aef5849bd3965a5da8c6729687ea210013d7819e83ea78c7bf5c1c7a63a2e3918c1ea380a0e7e98fa73ce064f4a40739c0ebd47cdef9e83bfe3fe013fecedce7d7dbb631cf4f7efef81b0840c7bfd303bfa753c7bf7e4f6630c83f4cfe54fec7e5edd4678e71dfe9fcf9c9e1b4132574d5aff00d7f5f91957832bec067f9e3b83fe464566d937fa42a0c9248e9d719c9f5e3ae7a71c0ad6bb52508032791c67d40febf5fd6abe9d6852469587cc471c1e3be07e6738ebc9e2bb1492a7ccf6b6da75e9bf9f97eafc495294f1718c53b369c9b5b2f75bf4f99b6146060f3d00ff001f6c73d2bc2fe2bdda5adb4b1b108c6232464ff1601e99c038ee3be7b8af75cf1f74e0139c123f3ffeb93d3b0af14f8dfe1abcd6fc297577a602751d391e78d541cca8012f19ee4151800f7c719af9acfb09571796e221422a75a1075210fe7e54dca0bfbd25f0f7765d4fafca6ad1a18fc3cab3e5a4e6a0e6f687334a3297f753b29793bbd8f8c27d6da59248d9cf048ebee78e4f5e993eb8ce48e799bdba6590303904927079fe78c8cfe1835e75ff00091c82e268a7dd14f1c8c92c6df2b23a901b83d31c8cf4c7b54b26b82418dfd8f39f7ea0fe19e38f41e9f8d519372b493ba6d494934d34ecd35a6a9ab356bdd2f33f51ad0e4578d9a695aceeacd27756e96f979b47d33f0c35ed2d2f6386e9e24959be5de40cfd09e4939279fa8f5afb6f4892de5b68da265652a082b8c1c8181c67383c1ebce335f89daaf8e67d0f50b792298c67cd565c315c1520f0463078ce3d3a6462bf4cfe0278de4f147876ce59642ee6253b89e0f033c93dcf6f7fc2bf5de10cce96270b3c072a855c3a73528e8e716d295ecb78b69a7a26accfc8b8b32cc4d2c5431edba94aa4b95c657f776775faab6e7d30a3271f9d580bc7038ff0fd4f6a863607f1c63f5ff3c71ef5637fca00eb8c7ffab07ae6bea25ccad65d75f2f5fd7b1e2e1e30b5e4d2766fced6fbfb2d3fcef1b7dd3f4aa321fbdf423f4ff1ab721e07e27f2c77fc6a930c839ebcfd73ff00ebad60b56fe5fd7dc71e2e5ab4be7f72bfddf9fa14c92727240ec3939f6e38f4cfb9a911f0d83ffeb079ec3b7b75a8cfdf3c67fce7b7f81f7068180571f4ebdfa13fafe99e8456d6f76fd9dbaf97f5d3e6cf1949a9efa35b75d2d75e77fd3c8bb45203900fa8a5a93d05aa5e8829ac323dc7b649f6a751419ce3d7a7e5b25fd7f4eb1008c1ff003fcaa22bb7a9fa7ff5ff00c9ab25719ebd33d3b7d7dbf0fe40c4c323fcfa1c67bf7ed41cf38d9df4b3ff0022207fce703df3f5e9d7da9ae3839e78fd0ff9e7b71e94e208e0d231ca9e3a0c67f03c7e7923fce3484ba3f97f56fc5bd918548dd7e7f85bbfe5ddefbfc33fb5ef873fb77c05addbf97b8cb6930036e496db919e31d71c75e8319afc61fd9bbc3b2f843c77ac5d30f2d9b51623f872102a83d73e9f88e8062bf7fbe3558477fe1fbcb765c868981eff00c24fbf6f51835f8cf71a52e81e31d4de34d8bf6b6390081dbf0ebe99e7a0eb5f9df1841cb3ccbeba4ef0a2a9c1aebef4df4ed756f5d137bfe8bc23a64b8fc2a6f96a4fda35e6b917e9f7f5d5b3d73f692f8d177a27c38bc860b86591ec2e176ab678316d1df3839e063be2bf989f0dc8de27f8ef2beae864fb4ea2d3ee75dd9324b93927d8f3dc7a7435fd15f8ff00e19eabf117c393e2392581a3208c311865cfd383cf18e72060735f9c7a77ec73ad5bfc51b696c2064bdf33ce4cc64fcbe6824b1c642e0e33d47ae2a78a72dc6c729c2622519cbeb12a50508a7295ea59455acdb726edb6add9765f5bc0f57054f1f89729c633a709ca52768da3057bf4d2f76deda2ebb7ea27ecc3e08b3d3068d7b651a207b78e362a38e003d460641071cf73eb5fab56462b1d1a2791c19114703b7cb9073f97ae71e98af933f67ff833a9f86b44d3ceaf33bcf0c51931aab0552179c127381c673d0f6c6057d33aac32c76d25ba86fbbb40ce464f419ce78cf5c918edebf57c0196e3729cbeb50c661dd18d5a9ede11935cc938453ba57e56f7b26df7b35af85c758dc1e698b84b0f5d54942d0728ad25695fe69276be9e5a1cee81e2a4bff190b495b26168f6e79f9a462a31ef81c1231f9f3f5bcb72b15947129ff96609c7a95ce3fce01e7b57c8fe12f03c90ea736b926ef39ee62651e8a8aa47eb9e07d0e057d2ba55c36a0190e7f7722c4727a14001c1e9d7af6cf7afb7ad4e15b0d87c42568c2539555a2b5dc792fdf4fc55b63f39c4cdd3c4ce94756e31517f25cdf3e6d574d8f36f192395666e0b12473d3afae49ebf98f415f3b6a56ffe9727039e7903d4fae0ff009fc07d47f10adfc9894f4f9d57b81dc738edc75f4fa1af9db50841ba7240278e4e7d4d7d2657514e8424baa565e8d2fc34bf5d3c8f13131b4e49ee9a4faad95f7be9756fd6e7e8a63fcfe7fe1c7f9c2939cfb9f7e83a773ee4fbf3934a0723b8c7a0cf7f5383d0fad340cfe9f53938e2bf2b3f4814919e31f4c6077c67ae4f6f4a01ebdc7d33c7e7e9cfb63ae334a403d339c138c0eddb8c73f81fca900f5ed927a64707ff00add48e72319e800a4823af71ce31f5279c7e5d48f4c1a33c923d32383c018fcb8e0f18f7c1a08e063bfb0e38e3a0ce71c9f7e7de900ea7b01d38ce38f5ce7f219f5e71400a08c127278c1c827a91efdb07b8ce68e0fd4e738ddeff005f7ec7b8e290293c8ec78ce39fd7e9c74feabb79c10dc74f7e7af4e9c8f5c7af4a0040704e0f7c0c0ec73d3be7818e7afe3467918278c6063d3b75cff33c9a003cf4effddea4e071db9fd338a43d8fe5c0c1c13ffd6ebef9ed400139c739ff003faf0073fe14947f9fca8fe94015af6f22b0b49eee620470a162320176242c71293c6f964658e3f577033cd7e0effc1437c53a8789354d3bc3d1ea1abdc5dda5ebde6a496f1476f69a45d5d476b343f6299b7de432dd5bdbd8e9962b35fe927ed56ed26afa6c50d843771fecefc4ed7a4d234db6820fb479b73f6b998dbf9885561b678e157985b5c411e6e678e748e60a665b499a10e61900fc20fda2a5d4359d66ebfb392f3598fcf92f27769af71243a4c7a3ae956d6890d96991cd7ba3c81b5ad3c5c99ace7b69a4b07753a93a597b380a36a352ab8eb3d13eb64f65daed3bf7d37b33083553174e1baa4eed37a39349ebadac9356bea9a763f29359f0b6a9a95f6a7e54105bb8b932cd7819e7b6900b69edf57b5789e5f2a0ba578f4dbf9d6ce6996cae2f6d0ca6cc497683c7fc4de0b5d13538e7d3d60824b8d46d2f2f2d45ea0bab5b4bbb7481a31716f1adc24598a7d36eadfed0b1dcbdbd9f9b72e8a41fd06bcd0e3b926f45db35fdfdd5e4179259324d3476d35a36a2ad73124cee86e65b4fece50f1b94b8682e10bc515c4e3e77f893e02d516ea58feced3194697334984305d4305c4b38b6802db19648a19a58de7212180cc5a49a685bce4af9ecd62e9ca528479525b37a74bdeed37d7aa69bdf4b9f79974213518cdabcb9755a5adad96965b6ab54d68d1f26ea33aa5c986dd2e5ee669e786f3ccf3659adaea7bc33ca6dee249ee5912d6595d22591819044a2e3ce6f2dea79fc1f6f79f68324be6492c06de745796491e369448639a5690962eb9668f6158c3f969b006f335fc51a7cba6ea2c245b38d4cee258e08d8dc6e132ec241c4cf23c6b1c4d36f604a22e32137c90ea91816e1328eabf71b31b06563b99c32fc9955e411b890769255c9f87c4622bb92929357576d3bb76b3bb6f556d35d2cada9f6b83c261ece33845a49249adf45169595ecfcef6bf6491e77ab7832de3f26ded1e2b5b5804ab711c70dbb150c8caa5a593cb6de8cdb372aa9788051e5b3486586d741b4f20d8ce96b77036c68d122081241188cc98570aacc067610634f9a30ab17c95d06b37c1cc8c1c319c9e305800a79c0cb0c165e0b032e0866e1b15c5c3aa79733465d15b697dc3201c1c02d8271d0839ea570411d32a388aef4751de2efd1df5df5b3ba6eeddefd2ed3b118bc1e1a32e654559bb3df4f457b5ade56fcdf25e32f8651c62e753d196286ded943476f1a12d167ed12c5ce1ade348ae266695442a1b70b859637851d39df03df788fc37e2bf0f7893c3f7fa9e89e21f0ddc26a9a0dfe9d7674ebdb5bab385e5d30e9fa95b49088a51731dac91ce3cbc79a5d375c3b08fe8cd1755b79112dae5949922624f6c49c7018062a70011c823e623a67b7d1fc13e1a9af57546b28cb5cb4418260471b8f2e7dc88a4fcad73189b054482e01972031dff599566752972a9f34a49de367aa69c53f54ef74defb7afc267994d1aae5c8a318cd38b8db469abfa2ded65dd357d4feb5ff00e09c9fb6337ed59f08238bc5b3431fc58f02c569a7f8c12345b75d7eca6de9a6f8aed2d97cb00dd089ed35a48218e183558bed31c16967aae9f6abfa207afa8e720023807f9f39e7a1ea7a57f2ebfb0d9d6fe12f8b2d3c7de0513eb375a3dedaa5e786a355f3b52f04eb9a9e97178cf478103c29f688b4f86e35ad37ce6311d66c74d926491a1873fd3fe9f7b67aad8d96a7a7dc4579a7ea3676f7f61796ecb241776777124f6d71048b959229e091248a45386475604822be8f11184953c4d256a5888b928dadc95236f690b74576a51d95a5ca97ba7c260eacd4f1182ad2e6ad839a873b777568c95e8d5beee56bc2a3d5b9c399bbcec5acf1f41c0e71924fe1d33ffebe8da711ee38008e072304fd0feb9f5e0d579678e11991801d79207f9fa0c9ae649b764aecee6d455dbb2124500e5b07924679c7e9fca9d138238e0e7f97bff9fe75c56b9e29b6b34215c16c10307818ce73d3dc7d3f0ae36dfc771acca5e4e0b007e6c0c1c7b8fc48fc7d2baa3425282bbb5edfa6eaebfa57dec71a9c6352524b4bbebadb4bbebb79ec7b78230416e3bf073d875f4e07d738205472c51cf1c91380c922bab2952430618c74fe7d0fe759da5ea11ea3024a8c08650770e983dfae3d3bfafa62b542f272463ae411d38eddbf95734a2e12717a35dbefdceb8c94e375b3dffc8fcc4fda4fe0edc787b589bc51a0404da5d3b4b730c4840cb1259940cfcc339c77e7d38f92935062a416dac320aff1023a8c75041e39e9c77cd7edff008bb41b3f10d9358dcc6b287561860a7039eb9071c71f8d7e6efc70f8147c3a6e35ed163648c3349716e8a4232e4e58003861d0e3af4e7ad7e7dc53c3d283ab9a602926a507531342292f7bed55a6b6777aca3d75b6a7db70f6750a9ecf2ec6d5778c9428566efa7d9a737d39764ddeda2db6f85bc4da76a1afdfd958d8abb4d35d46abb72485661b8f1ce319ce471c0e8715fad1f00f481e13f0de9967723cb996da12fbb8e768c8e79c0ebd3ae7ad7cb9f04fe1b47a9df43acddc02592320c41d32b19278c0239638e5b9fc3a8fb5355d3a6d2b4b792106368e23f773d9780001ebd7d4fd081dfe1fe5789a746ae3abc5c658b7154e32de34ac9eabbcb76ba2d3b9c9c6d98619ba581a4d35874dcdad6f51b4da5d7dd496b6dee7b7a6bd68e36acb19641f74300738f4078fd3f1acabff0017dad902eedf2a93bb91d063f2e39e47e9d7e07ff84f7c47a57881e496e6536fe714f2d890026e2075c0e38e9d873e95e837be3737d6ab20738741b94939c91c8c7b75e7e9819e7f4f5824a5aa4d3d5adbb5fb75b7dcb7b1f9455c4caa26937069e966afbad346d6df7a3eb1b6f1ee9176a0adcc633d8b0047e6c08e7a8ea33cd6fdbeaf6978b98664937719539ea3f2f4c73f9d7e735f6ad70923bdb4ce81b9f91db1bbd460f00f70303afe1dff00c2bf1c5f9d4fec1757323aee5d9bdc9f949c639f4e7079edcf38abfa9435b2e5edadfb68fee77fcf4470d5c4d68c5cbda39db74d2775a6cd5fb5addafb1f70e496cf5e7b7b63d3f019ef467903d0f5e849cf7ebfe7b554b29ccf6d1c99c975049e9dba7d3ad5b5ea3ebfe7ff00d7fcfa5704938370ecf5fc3fe06bf75b5bb83e78c676b5d5d7cec5c4fba3f1fe669d48bd07d3fcff00f5fdea42a31dfb9e87d3bf031cfae3b9f6acdb4ac9f5d8f4a11f7525d12dda5bff00c38ca28a298dabee15132e391d3dbd7e9d87e752d14194a1a3eabb3feb52ab2e7d8ff87af19f6fe8715091d41fa1ab8e31cfbe31f87e9d3dea165cf3dffcff009ed4d369dd1cd2a7a595da7a79a3c3fe2b444e8d7876e7f76d8c7a60fe181f5ec7839afc89f11e9171a8f8b5ad2142d2dcdfee6c7511abe4938cf1fa6723be2bf667e22d89bad22e91464794c3a7fb241e3e9dbd315f13781be159d47c652ea9710ef8c4eca8194fcaaadee31ce39c760464f4af9ccc3012ccb3ccb60e3fbaa57a959f48c1493b3db74b4e8dbe87d9647596072cc5ce56e692e5826ecdb697cf7f2d6d6dcf4ff865f0ce03e138e3b881496b7566dca393b33cf19ce78fad79045e04b6d3be27aea12da44b0a46d026500c9df9e38e9c703f1cd7e88e9ba341a7692b04718454882a8031901792471fa93c723922be7ed7b40fed2f11836f182f1c870ca307393edf9fa7604f35fa0d2c2e1b3597b0aa92a3859c2b536da518ca8d9c5f6b5d2efb2b9e4d0ccab65d3ab562df36229ce9c92bed3b276b6eecd2fd753b4d2a4b2b7b28d228d7200014018fcc76191c900fe355b50b392421bcbc2b90791c618823b0e9fe3c5753e1df0b4d1c61ee412548daa47a700ff91f4ad1d5acc46eb184c60e00f7ce00cfe271df1e80d3c557a1494e9d192938ad649df5d2c976b5fefd2db1c5878d5af5bdb54775297324db5bbbb6ef65e8ff00a5c6e99e75bc739f2cf96b28c10383b54038e3d41c1ce0e3ad775e0c4122dcbf7fb4ca7df1d7d31fc5edec48adbb5d0e14d2d5590729b89c0cee2338f52793efdf8e6a9f8460117dad00ff009789067be7711c8fa63b8ebd39e30fac4259757a514d4e9ba4a4efa3bc9745b59dfaedbabeaf9ebc1ff695396d1a97714fa59c6d7f3dac725f13954596e23959636cf3d32011d4f66f7e9f4af9e2e937ca5b1d40eff5f715f44fc555dba5c8dcff00093fa1e39f6fcc76eff3c87570ac48fba0720f6fa115f45932be5f4a5d9ca2ff00076fbb6b79f5679b8c4beb1523dacf65fadffa773eff00009e3dfd7a9193f4e9d33f877c1f30c727b77e3dbd871403c631f88cf1d7aff5c76fd0cfa719eb827fcfa6719e9c1afcd0fd045e738c9e98393db9ea718e9df9f5f6a4e4739e39039fc78ee39c7a75f7a5c9071f4e0ee38f6e9f971e9cfa80e338c77f53c7e591f5c8edc679a003e6f53ee09cf400f43d7afb83efc128011d0f3f5c7b9cfd075e8323af6a371e3b9eddfd3b75cf1cf6ee3d6824e4f047b64f72339fae7dbfc40146e23ae4e7fbc39e9f81ebebf87aa76ebc302719f43d4f1edf89ee2941c0c8001ff8173cff002eb9e79fc394048c7d39ce7a038c7b7a0f73c9ec00004fa9fa0273cfe0477e47af5a4208ebe9ebdbf97519ebdc53b2793d867939fcb8c60f3c1c03db20714dcf23d063b9fc7f3f6a004a28af927f6a1fdb4fe06fec9fa7d92fc49f1768169e2ad6edd6e3c39e0dbcf1168fa26abaca4b726cade449355b88a3b6b6b9bd57b4b7b8911d66b88e48a25768dc04da8a6e4e314b7726a297ab6d25f7eaf4dcd2952ab5a71a5469cead493b4614e2e727e8a29bb25ab7b249b6d24d993fb4bfc4bb0f00e9da878925ba306a5657961a2684939bb1697372bf669eea40b6b771665b33aa5c46970637952e7cb48edee8c200fc37f1078e25f12eb972ad71711df5f8ba363fda4f71aa29123430c71497f7f6eb7e2dc69716abb2ce59ecad6eacb55964b98e49d6489697ed0ffb7f7c3bf8f3a96a7e0a8a2f16783bc75a5596a7aac3e1bf105b4ad35d4725ec1757ad673e99249a5eaba6dca8b28c5d2dcb3db69a2e04b0c72b4013ce6d7509b588e29adedb17663d28c91184da2be9fa75c28823bd5592063a8c96d696f732cd04f098eef7c96a2e04704edf4346a52786a7ecaa46ac791454a0ee9b5a356f277e68cace33e64f97635c2602b52ab53eb346746afb4bca356369453e5696ab6e57eeb49c649a926d59af53d020d46ef54bdb69ef2e422dd117362d7534f2c77cd6af6b1cf0473091f50b7d45e392edcdff00fc782dc4b6bb84ea88f5fc51e0893546927bab686c25da66b4b89609243048ad2476d771bf90f6caf731154bb895e462ceb3412b3cd1c4f8fe19bb926d40ea124d7d1de5bc11413dbea514c925adb5b4b21b631d9dd5c486d6e51545b4f8b8bb12234d6e934ed6cd25751e20f89763728fa5db247e74716d8d5551d5d91e497cd0c76e638e4c3004aa175dc085de47935e9ceb29aa918479b45757e68a5a6aeca2dbd7dd6ddd2b377d3e9a938d1509424dd926da76b5f975ebccb4b6b6eabb33f36be22f872f6df51d58eab6f0d95e5add3c6604322868eda05f225f319a432c52c2b0889a3965122309061810ff00356a1ad416721b6843997c96133b6597cd61f7010e71b36941bb692776301588fbdfe23496de259ef21b5b292f2fad961b7c43b596e2362b19940511b9686355c481c499277a8e1d7f3f7e20787a4d1755bb5bcbb8b4d55951f6cd70b94528e424b2332f95b02aa191c2ed3c30884aa95f1f8ecb55194a7392e5937afc2debeedd2576d2ea959bbf73e8f09994eaa8c69c6ce295dbd56895d6f64f477bbba5a6cae736dafacce50372497f95d8065385520b37008255971b8edc938c30c392f58dc990cb8c9650b149b83166f986dddc10c792700283827e7225b4d2e0b88e39ad6e959d15f75c4722dcc129561212a2266e32ce590397c13d1c151dc68ba098e7b6964b36b92b2ef7755c6eddbfe674c2866524fde21ca8c93fc2be2c3d8fd614232527f0d96926f46f576eaed7be8d69d0f567ed65877526b96dadf75aefa2bbd37beaad7bd8d3f0e594b7304722977285976eddadb917381d3e52abb81393bb852a5989f7cf06dacd315b68d242caca15f692b11d996121390a597939dab95c60670799d161b712314872c77c712a127ca7c2ba9955be73b4b329e08542bfc1935eff00e05d0a3454bc2e238ee64895640df228d80fce0a30e49f9f7160aa491b40007d36518684b11186ad5a5cc9eae2e2d68db56bdf6f25a2eabe433bad28e164f44d38f2c95d369a4b4b377db55d35d6c7d6bfb3bf8c2e3c15e22d19f7ca9b9e28e68c064565696253233283237964cdb4281828032720d7f49df00b5fb4d7be1ee9d1d94514169a59fb1d8dbc5854b7d359167b381114958e0b4df358dac48004b3b5b7046724ff00305a3db5c69da969d736805dc505c05650afb9b6152c36a9ce586f28abd589e000a95fbc1fb16f8d526b78f4112158750d3d8c51b3ee7379688d750654e4c59b21a833aed5c1d8197e5427ee6b617fe13e7149274546a2575b2b293ffc01b7d136bcd1f91cb11ecb3aa336db8e294e84dbdf9a5cae09aff1a8fdf6d2d67fa10738e483d07504f53f8febfe03cabc73a95cda23b46c428ce719e074ed9c7d7fc2bd54918c91e87393efea3a6071dfdcf35e43e3e21ede6ee406e4f278fd47d3f1ea6bc9c2a4e6eeb657f3dfa76f9e9d0f6f13f0a4b7bb6be56f99e117faecd3b309642771e727ebd33c74241e9f4ea4e1477e8f21424924fcb9cf04938e7d3fa8c75eb9f7b97925524ee0cd8fae4fbf07dfd073cd72ef7cd6f3e324107073dbbf5e003e9dabd554d696bdf4b6dd3b2febeeb9c1cd6d5f4b74bf6f9f447d9bf0ef5c84e9891cf200ebf260b0ce14e071f8e07d31db9ebb5ff1769da3d94972f3296552514b2e59b1c00067af1d3a7031dabe47d135cb98e0125bcacac40dc013f89edf53ebdc0ad3b99ae7572a6ea679029dc14925723a707a9f438e3db9ac258684e6a72f2babe8f6d3d3fccb8d79c62e106975bfaf2ea9dfb6c7bd7843c5c75c9a49661b0bb1db193fc19e33ee7af4e06055bf881a35b6bba15f5a9446692de40a0ae49254e38e9fcfb578e68576da74b1bc27632101941c640e4e7dfb8efd2bd9ed3524d52d40246f2b8209fc08e3a735957a10abcd16bdd9271945e974fc9dfa3f27d7736a15674dc6516f9a12524eeef74d3dd7fc1db63c07e12684da4493595cc3e5b457122a9da00640ff002919f4071cf4ed5ef7ace9915cd93c6ca195a3230464648ee31d08c7b0e7ad3acb42b78e5699235572dbb20004b13c92475c9ebc7b7a56cdda13095ee0639fa76e71ff00ebf5a9c2d18e12952a34dd952d22add3749ad3a596d7e9b264e6159e2aaceb4d7bd3d5f9bd2ff8ebe5dcf857c77e1536f7533c717f1920818230c7b8ea0fd3be7ebc9436ae6db6f2180031ea71c71c8e7b678c64f1dfeaef1ae84b7304926cc70c738ea7079fd32319e7a7039f9c6580dbdebdbb0e0e40c0c0ea71f973ff00d7af6a9d4738a6fe6feedbefd7e7d757e1d7a0ad749dddaf656ed6efbede7a1e7b707c92caf804647273d33fe73ea0fa53fc137a62f1559ec3c3baaf073fc5823f53fe1d697c4f6b25bb9750406f41c0279e9f8fd3824f7ae67c113ffc54f65b8f29703f1048efed9c9efc74e79de2b9addbaf97e9afcfabd8f2aa2f766bc9fe1e47ea5f8749974db62464941ea4fddfe40f4e7a71c5740001d3fcff009cd73de133bb4bb520e7f740fe83ebebf8fe35d41507ebebfe7ad78988d2b54ff17e88df0d4dba1192f477f97f5d7505fba2a4dc718fc3f0e3d31ffeafc49601b463ebcff9f6a5ac1a4ed7e9aa3d08cd2566edd3d57ddf20a29a5b03f4fc7048fe5f5c104e298643ec07bd313a8ba2fbc969090013fa557321e993c0fa7e7d0fa75f5fce33231ff27f2ff38a0ce557a6d75d3aedeaedf2dafb9333faf4f4f7c7f3c7d3dba9a8d5896c63afe27fcfe1517f9352440b48a075ebfcb8fa9cf14f95dafd3fe0a5fd7a18c25cd5611d5b738ab25b6a9ff5f8997af69c6f2c648401991483f42318e9ce464fd4fad73fe17f095be9adbc421589dc4edc724e7d7d4f73cfb735e8ce8ae30dc8f7ff0027dbff00d553c10aa212a00cfb7271ea7b8f4ac63515373a96fde4e2a3cdda3d96cffe18fa7e46e9d3a2afcb1b49ad6cda5d7d3bfe5d295d5ba88182f1852b8f4c8c74e470793e95e5fa569908d727765f9cb96e47246eebf4e3a93cf1f8fae4ca4c4fc76faf20e78fcabcfed2361adc84f6563f91c7a7f93f8d7a5975692a38b4a4d7352bb7d6e9a76bdfcddf5d35dcf2f1b4d2ab434de4935d2d78dff0f9f9f45dc7d9e28a2e00e17e8071d80ff3fa570d7e3cfd4614ebba551c7390a3b7b7f23ef5de4ed883eaa08fcbdf1ebffd6af3a59d66f1108473f678c31ff799b03fc47e3dab8e8de4a4dddbe65aebeaff0023d0b4610ba4928c76f5b25b68db76f5ee76d7b325b69c49206d88b67a638207e9efdbdeb8cf09dcf993de01de532019e0863d73df04febf8d6af8b5e48f469ca6722260704e76ec3d3d33838e98ed5c1f816f8bde3c44f5556e4e7a13f9f6c64e7ea3047af84c2f36558cacb597346fdd7238b6fb2bddf6dacaef6f07155bfe1428abfc1ca96daf35b6e9a5d69dcbdf1463f374b9540c925718e7a9eb5f30890a650e7e524741d8fbd7d71e32b4fb6dab2019f9723233d14907b9f7c63a0e6be4dd4e07b7bd9e2e46d73c60707bf523bff9ce6be9387650960951beb1939f77ba5f775f37af91e666378e21cd68a5a3b376d96ba6cf4f3dfa9fa0f918ced1d79e3d7238e79e9d3d6931ff00ebc01ebef8edc7e34631839fcf1df38f51dbbff8e0c0c0233c7dee9c7d3fa75cfd6bf333f441dc0201030339f53c75f5fcbbf19f468c679c7eb8cf6ea7f4381ebd393033c671d73c1c8c13d31ed8fe74b81cfa74fe1ce4f4e79eddfb60f4e450018191d39273ce0741c0e4f43fe718a5e0311c607a8edd7e99e40c9e7a75e414c038c7009c1c85e38f6c7f9f7ea15e48ef83c607a7d4f3f41c9fcc001c63b739ce00e3a600c1fae3bf1d38a7631d94e01ec3b67df8fa81ec718a680bef9e38257fafd39efed48463b74ea7afb123a7af4ea2800271c6075e7f0f4ce7be79ebec3a537fcff9eb4a46323b7f519fcb3e9e9eb8a4ff003fe340057f05ff00b667837c43fb747fc1677f68387c4d729ff0a9bf675d6b42f07ebb79a95dc16da2693a3f856ca1d32cbc3d6f717922d943aaf8bfc4715f4d696cec4c8e353d45e3686cee40fef32695208a59e43b63863795cfa246a5d8fe0a09aff343fdad3e3678d6ded277f0e5cdd699a9fed31f1e7e34fc72f1aebb68f22cda86b7a97c48f107837c2fa23dc820cb07877c39e1ab192deda4205a6a3aaeb124481ae5ddfc2e2095b02d59cb9a4da8293873b8ab38ca4949c60a3273934aed47955a4d35fa2786f4675339ab2a4a2aaba11c353ab28a9aa12c4d45fbc8c1ca2a552d49c29a724b9a5793e5524ff69af7e0bf86b5ad5b49d760f0b3e8f790ea57da17866c356b5335f5be99a38b5b5cc320546f25edafe5d2e3f2dd924862db23482520fd2faafc20b5d12c6db48bb5b94bc9f49b791f6cc6de6821d867b74863b46122dc5a5c45359ac10c3721608609cc772209a36f02ff008269fc0ed7be1a69b7175e33d66ffc75a8cf32ea37d7fe21d6ae75c6b6d5ac12dedb541a25bce7fe257a46997115a787d60517106a5a8d96ab7b6ce238e303f42be231d3aef529b52b9f2a19f73e9704ed70c8e8cd0caf3c7122bb472ac57171671890c65d7fd211488f7b1f43841555869b9b769b8b8c1a95a138429c6a4949c60a5ccf6495ad14d3d5df7e349528635528f239d2972caa45af7e2e52708a5cf36b916ea527252935a5ac7c5afa0bf87edefa78e6335b94b6b6b582231178cd919424135d050f2ac2c5e208ef30431a4618a42918f97be2378fed7c26d2ea77172d1cb74a542ddab2fee159e55526158b725c3623f302b26d3f32b37965bec9f1edd269916c08b709f68822cb18c1927965f2e52c002accd2cb2b2a920caea438903b30fc94fdabbfb43c5faf2d9697018ed34f21246469162668b0924836aacacccd10638cec2ea44480107d9ce6b470f8693e66a526a317ab6aeb57bab595d7cfbd99e165ca75eac172f3414799a7a6dcb68f56db7adb5d9e9b9eb3a07c6a875cb3927d22d1e2666b88a778adcc892cae59097da047b123cb2858de31e631458c9535f0efc6af065debfac5d6b37f7d3aeeb94b9024b5d8370545910a5bdd2050be5ed8d4c42de1568e18a28f193e6efaff008a3c30f1f86f48f14c1e1e37456da03a95bdddc5a4775330501a043a7ea52c6ee479ad67706483e47f3228d59d78ef8dbe23f187c38d16cedfc451e8baef88efa3b4feccbef0ed8f88a4f0c6ab1dcea368219e7f18c9f142eeff0045375a319e48926d2b50864d6a396c2dd9e18deea1f8d847158d938d2c5fef672b52e69d382a9caaea0a553dd4ed16945c9733b24ef667d055a983c2d353ad836e9d249d6718d594a9ddc539b8d3bca71bb4dc92972ad5db634b4fb0f19786aee2bbd0f5992fa05918cb69742764449a32ad2c3e74ec4cb09f94237970b444aba6793ec9e01f8bfe23b6b9f23c4ba75b984b3c0b2c31159e192494c08c0a80af110c511db7217380f216f317cb6e6d7e27fc2df0df827c43f13fc17ac783f4cf887a38d6fc316bae5c45a85a6afa72b485ce89aeac705dc37ad6f1aea49a2789229afefac2686e6daf560cb3f51e17bbb1f13df95d15e378d44572a564deb173bc00aaace877a991784c638236a8af3b308d5cb2a539d7a54ab4d249cf9573c5c5ae64e5056bddf6d74776accdb2c9d3cda3529e12b56a507cca3495472a73d2c928cdde3a45e9a34ef1693565fa0be1eb7b2d6acecaead9e18a4901335bc80977211246da8a4061fbb9010bd00dc51b059be85f0ac315b69eb612840f189194accd1cc249219198effde9dd1a4a366c565daac99da6365f87fc3d7971a7c76986b8630e276747791037019a46243901b217f8432e0a3f53e8bab7c4dbfd1974f8564f2ee9a079f73aabb0dd94dc039f2df88d3cd1800ab005980057d2c9f3aa7cdcf3828547cd27c915ef2f77477b797bdb6b6d373cdceb24c44697b28ca5282e58fbd2bb8bb5dedd1f55b2b799f62f8766d5ad6eae9a412dc2c72892df72b3864919d51a2cf967ce0a3cc8803b63936ab2676edfd44fd893c677d75f153c23626631b5c6a13c4f1028a2451a3ea105eab46598ee6830ff0026d0429603686afc6efd9fbf6a4f87b71ad2783bc5e9f63ba7758d6fae6190d9dc4573fbb491af0aed85e32c011208dc92a541ca86fdb2fd84be1b4edf1c46b68b1c9a2786b49d73c43a75e5bb24d69769a9585b68764be6e0279dff0013bbc9a30a384b2dfc0622bf46a75a33cbe756329497b29297327af3ab2bdeda6b65ab5a9f8c6634671ce30f4254e3197b6a32a725669aa728cea5adb35caddb46ece47ed34cdb2362074524640c9f73dff0f4c707bf89f8ce7061941f46c0c67d7a77fd7a9fcfd9af08585f1cf1c7e2a4f38fe95e0de37976c52e4f38231d393f863fcfd6bcbc2ed27e6ff0b7f99edd77ef2f25fd7f5fd3f9ea79c3ea0d17ab30c6073c91d38efedc1f5cd67eaba4339f3901c9049e3b7a9e98ff0023a74a2f78a359237f1e691ff8f74c7a7d71d2bd522b31736b1954ce40fc88c9fe479cf4f7e6bbd4ece2efb2d5efdb5bdaf7eeb54733827192b7eb6edabb3d7fcce3b41b79238c823a763c9e0f6fcbafd0019aeb2d5f63ed3f4fe8401edf5c559b7d37c9246dfd3df20f1f42071c7a60d57910c731c0efe9df23039fafbf6aae6e66f5d6f7dba68ffe07e36316aca36db6efa755f2d2eff3d8bb2878dd65424671900f1f8f6e983ebf89af44f0e4d210a549ed91927ebc0c761cfaf1ebcf011b2ba0078c75e338f6383cf41ffeac5771e1d95636032083f9703b7008f7ebfae2b277ebbad3e5f3df7b69d1761a972ebfd6e8f60d3df785f523ae3d78e3fcfd7b1ad0b8b6dea48e09071f8e7bfafd4e7f3ac7d3e500a9539c63bf07f0effa63f10474e8c1e3c639ff00eb103f519fe82b96ab7092696975d92d6dfaa378a8d54d3b7e0f55b7ebf89e6facd90922962719dca48381e98ff3d0e7d4715f30f8ab4816f78ee170c18b7423a1fe64718c75ed5f63ea56eaca4e3ae4e719fafafe7debc23c6ba2f9caf22c6720b723d8fd0718f61f4ed5db42a5d793eeed67d3f3e9b5dee725485f9a0d5d5ae9f5b5d3b5bcba2e9b1f3e6b3a52ea1661d54160bf371d081f89e47519edf8d79069ba6cda6f89ace628421b9453c63196c771efcf27a75e6be89b781a391ede60704e0670338e33f539edff00d6ac3d57418d2e21b8541feb237071e8c0f07af6ee49c0e3d2bd2a5512d1f5dbf4d7efd344f63c1c4536a524b4f4fced77a775d8fb33c0ce5f46b2271fea633efc85ff003fcebb864cf3c8cfe46bcfbe1eb83a3db2e49db1aa8fa600079e9eff00d4e6bd2f03d2bc4c6371c44eda6b7bfaa4ec7a196525530bbeaa4ff1b755e9d3f1452287b1cff9ff003fe34cabaca319e98ff3fe4d5471f31f7e7fcff3fc6b1849b6d3f537ad4943b5bafe7d5dfb7f486e01ff003f87e14dd8be9fce9c083d28ab39a2a2d5ede5bb2331a9f5fd3fc29a62073cf3f4ff00f5ff00239a9a8a01d34ffe0ff5e5aee533191f5f718feb562d633e6024703f11d33cf3dfb7f9cbc807a818f7fe79ed53c002866180003d7d47d7d6949da2fd2df7b1e1e92f6f0d9f2be6d75edf95edd7533eeaf52de50acc012463d73dce38cf5c724ff315b90c8ad0a3023950474e78cf1cf4af02f1f789bfb3b5fd26c924dad752e08ce32148e0e3b1231e839f415ed1a44a66b18189cfc8bfc875a75b0ffecf4eaded7938bf96debd5f4dee7b31aae359c2dbd3bfaddaf27d3eff00cf4a423ca618e818fb631cf527f1edf8d70700235b94f2418c81ff007d727f5fe99aeea4388dcfa29ae3e20a754739e76b739ea33dc7af18fad69836a34f10ace49d29aeeb5e5d74e9ff000e71e32f2a941bdf9d7e8bf1d19b97b37976ace4e02c64f3c0e99fd0f5e723f3af19f0a6a5fda5e28d5d95f77972ac2307a14c1e39c71bbafaf1c57a1f8d3525d3742bc989dbb6ddc8c1031f2fa67af231fcbbd7c75fb3e7c428b5bf1d78b74f67c8b3d45a30c4f059d439009243001802477e3b115d582a12a919b8a6f968d6aa96eed18afc7debfa2dd8f1551468a5b5e74e327e4ddfa79249f6bae87da3ada79f6cf6ed8c346548c76239f63d49cfb5792f87a09b4af108899584720700919e320fb63a75f6e33d2bd7af1d5c938c8db9e31d0818f63f51ce0d737159c73ea314817e64e41c64f2475eff00cfe878aedcbf13ec70d8aa324dd3ad4e4a49b76e64ae9dbc9d96cdea7858a8fb4af09c5fbd0a917177dd7ba9a7d3faf9ae8b518c4b12961c34671c7b7e3f53c1e9ef5f34f89f49c6b170553e56c30e83392dfe1ed5f516a3115b32d8e89d7a74c83df18eff0088c76af11d5d526bd772403b40e832705b939ae9c8abce9b9b4f449c7e778b56b6bbfe23cc29a6e29ad6d193eeef6d34e9f9ede9f4c648e0e40e8727d73ec78ff0fa61067b7f9f6fd3273d706978200cf1dce0f079c0ea73ce4ff9c033c7af4e0e7b703183e9ec3ebd2be2cfb90e49cf3cff00fabb0e474e83da83bbdf8e79ce7f3c7607d852e4678e319c63be73c73d33f4fcba001e339e78ce73cf073ce4f3ff007cf41e9c002738cf3d49e3a71df18c75ff000e2839393d71df071db3dbd3e9ebf5538e8723ea0e0607a67af3effd4ae46481d39cfde18ee78cfd72303eb8a00660fa1f5eff009d29dddf207be7dcfa7f9fa74703dfdc60e0939fccfe59e7f4a43f41df9dbd81ec3a64739cf41df3400d39e739f7ce7a7f874fcc5251fe7ebf5e68a008ae215b8827b772424f0cb0391d764a8d1b63df6b1c7bd7f1bff103f607f137c15f097ed23a3fc61f0159ea3e14b2f1e6b30fc10d7aff004a8753bc23c5bab5ceb1792e812cb0ca9669613adc5fc1711bc6ce9aa2a97b670b2bff00648eca8acec70a8a58923a2a8c927f004f7afc6eff008288f8e7fe1329bc0de09f0d6a37114daaebda668d2b40b1cd6b72fe20be8ac9209e09e1786564088209a111dca3975f34a31030c460638ca355ca0e6e953a8a0afcb1e7ab1e4577a75f795a49a696b6ba7ee6419a57cbf1b4e95292843135684aacd26e708e1a6ea2941a7a27cd2849352528c9e89a4cf08fd813e04de7c23fd9fedae7c4fe2bd63c653df5dea5e22bad7fc49b7fb5560bc086db4a27cc9bcdb6d2ed6c1223733c8d71a85fc9777f73fbc9c01e1ff16bc7893deeb0ed3d9bd9ff006925cc1e4bb1758e5bcb58a08d5c849629e359a369554bc725b4cd1ac0558bcffa1bf1227d37e11f81adbc0f63745e1b089ed19d001bcaf97084f92462c5c62595b770a641919615f991e3ef05dbea96f3916f1457725db4e4acbb618cdaea56fa879854121d592c4ba07081ae26f39d6ddd3627bf96d1a583c353828c62a9d351696b1e6715cdaeadeadbbb6db7abbddb1e3ab56ccb155b112a939fb6aae54e52f75b8294795a564968a31d172a4924acec719e2bbfb5f1168f6b762701e48e1686113328124cc58c6e8ae15cc630149de498498e57428ebf307c41f04adc59dcdddac0c242ae24b950b28919e32a0a92f198d0794ee8a236925cb3f9723bab2fb16a9adda685e569f3dcc4af6f34c20926d8af1c42251e4a8260f2e58de48a425c4e92dbb4615bcb74c61eabe29d0134399a6b94437109f3601b8c8af3ef92100b901001f75d9b732e1ed882a057ca712d48d7738c6568c63abdb56926f75bbb24bbab6d63eab8728bc3aa729479a4dab2bad758b4afaf9eceff0034cfcc4f1bf816db5fb4bfd3b52b086e26dacaf098d18e773b214942a9de81519a439de0931bb0f9dbe39d5bc2bf127c23a911a15edc6a96b0b2aa695e248db50b7f2958a2a5b5dee4b88915008c32c92c8aa31b82ed07f42bc57e21b47d5a56b5f210b46a1903e224f2d554aaa9662711946058fc9b99543ae59b91d67c47e12b0b6496fed45f5d4911291425373e71909208e4cae72036ee70246da719f92caa75e519d38c694e30929c556726a334d7bd171b548bd2eb964af6f247d1e6f86c3aa90abcf569d49c39272a0d45ca0ed78ce3252a738d9d9c671924dfdff19f8fbc57f1e3e33e89a2783fc68358bbd2f46b98a4d2f7f8bf55bb834ab889956296c57c4167a82dbc112008b6f6edf340d242ad1c6e02fd2df017c3367f0fe09ed356d1cdd35d22c8f74be7bfef00dc622250ebe4a2b46d149bb0c449f287319aeafc23667c417c7513e0e96d74b8c09a08a16377a95e442468d2674dd1dbc092344e434934cc4aca123200cfb2585d36a626d3f4ef0fdce8f2461e15b6bd85238a6b43c0b80f1b3472a3eff9a32232170a71820c667994eaca742aaa16868fd9f3d46e4ac9b7394a6db4dab47e257d6d668db26c8beadecf154de217b4bba71aae9d28f249f37b94e30a69467f1732566f66d6fd2699a8f86ae342f105ec968b6d1e9b692cce255588ac691bc8f2c6db5118c2233231209c74284035f9e1affed05e1ad5fc552e9f6bad4b7132dc4b6f6f6a96b24b240a266015144640c85c158ce72b93b9768afd11d33e1d4d7e354d29d9c9d5b48d42d5d6225e2696eece550d1c29295dc1d83288c23b0f90677293f8cde3bfd9be7d1be3b5b68de32bb8fc2cd2c5630e89797f66971e1cf16787e29e50351b0d4639f4ddfadaa5cdd0b9b6d5e5bc65bedf06a2d3c0b1dcdc7d064780c1e330387ad39ba09370ad51479926e5cd1a8efab8d9a8b4ad66bbedf21c478fc760f34c5d08455794d42a5184a6e2a51e551708ad573a945b5a34d3efbfde7e1af0f59f8bdd26867923d5e38a265492de5b5d42d9a7c8b6b8b8d3e68565092805ade79ad59654def6f2600907f54fff00043ab9f88afe01f8e7a4f8fad2e278bc3de25f05d8f87bc433c72f97a84373a4eb13ea5616b34a5bcd4b23169d7328548cafdba13247b5e103f9ebd3fe12689e01f167c30d6bc01e18f1de91f006f7c3da369f6fad78dfc45a378ab5fd27e23785afb49bbf10db6889a65feb1a843e1cf88be0b8751b09b4ebcb987466f13bdaded9d96932595831fedbbf664f863a67c28f82fe09f0e5969f169d7f7da55af893c46880b3c9e21d76d60bdd4239a42aad37f67ab43a45bc8caa7ec5a75aa6c4da40fac85396170b2a719b9d0aea2a9de5cd1bc2709f3c1ff2b50b5b4d64b4d2e7e7b8bab4b1b8ba7395350c4e165394eca378f35395374e766fdef7dbfe6b4756acafedf79cc2df424ff9fad7cddf11aebc94946ee48618e9fe1edeb5f49de90b6f231ecb9cf39e9d7bf7c1ff00eb57c7df13b52124f708adc2ee1d7af51fd3f2f435ae17e193ecedf7a5fe5fd58e6ad6e75f8f7b69ff000773e5fbbd6445e2011b3e3f7a0819e7ef0eb82327a9f6c83c57d61e0d78afec2160437cabdf2738e4fe3f9739c722bf38fe23788ffb175b8a7dfb41700fd73c8e31fd3dbb9afb23e0578a86b1a5dbb6edd9552391d081f8139f6fa1aea7fd7a75eabfe0131d535ddbf4e9d1f6ff002bf43dfee74e0aa5940c8e723f1c9fc78cfd6b89bfb66590b6303271e9edd81fc7b76e2bd7fecfe6440919c818fa0ffebfe3ce315c5eb362a9b8803d7800907fce3f5ef8a717677fbd7e86128a69d96bd3fcbb5add0e32372bdf823f33c738e9ea7fc7a5741a2dd959d5377f17e79c1f6c7e47bd72f70de596078c67b7a1c01cf6cf39e3afd735ec753582f231bc1cb0040c75c8c639e48e73c74f6e43bdfbafd764afded6ddb5df4d8c64b46bafdfb7fc36bf7eacfa6b4a66312b673c01db3cf3ee7db81e87ad7636cc4a8c74c73fd0f6ff003d6b85f0d335c5ac521ced280e00e9fc59faf4f5ff001e8ef3505b08cb331e07200e38c9ee327a63b74ee0579d89c4d285d369b5d53ed6d3cdabbdbf43d0c260eacd46d169bb5977db477b2deebabe9a265fbd55208279e7dba8c8fd7d3ea6b8bd4ec62b98265201ebe9dc7f23d7f03f8f33acf8fd559a38c805490589cf031d393ed9e7031d89a86cbc5705e42cc651bc8c6723048ebc76eb9fd78e4d6385cc70f567ece151392e9757be9af4daf6e9d3e5d988c9b19469fb79d26a0d5b6dafd6eff07e878f789ed974ebcc81b70f8f4fa7bf3cfb77e2b2aea78e7b38db209c8e9d783cfe87f2fc0d5ff887a9c1b8c80ae58e41cf53dfdffa83debcfac753f3acf0581dae319cfb1e3afb7a57d1d0d629df671b75bded7d7bf5efbfcbe3b1f1e59b76d5ad56cee9a4efe5ff0006da1f5cfc359c3e9f18ce70a3009ee323033cfe98e3bf5af601c807d40af05f8557024b255e060e0f3eddbbf7fa7a8eb5eee24509927a0fe5dfdbfcf5af3b1dad54fbc57df65ff0df9f77794cd4615632924949bd7a7c3fd6dd35bf4573c01ebcff009ff3daa848e064e4f3d31d71d07f87f5ee24777949118e7d482471d80fd7ae3f3c5412db322348ec58004e38faf7f5eff960573c124fde766d68bafa7abfd3d4d7113a95b99d183925a396aa2bd3bb5f76af633e4be48982e792703d3ea4f7c73c018e0e6afc32f98bea719c8ee3f97e5c571b2892eaec853b543601e718cf1cf1d3a02339fa60d761676b2c71ae707e5033903d3b71d31e99f6aeaab0a70a709297bd2def6f2dbfaf4f3f2b0b2c4cb112838b9455b48a768aba5b59b7bf77e562c514e78e55070b9fa60ff9f7cf4fd6b85d7fc596fa111f6c531ab305df825724f5c8e39271fe79c23ef5adadff00afd51ea4a152ced09f35af6e577f92efaf9fea768e7a0ff3fe7dff00c79579445031ff00673d7db1fa74f6ec6b94d27c4da7eac88d04eac580c0c8e7b0c83cf357357d463b5b295d9d4058d9b93d38cf1ec476193573a538bb4a2f5b3f27b3fd57e37ea6584ad0f6f6bd9ad2cd6b76d68d3b7f5d8f84ff00683f8830786fc7fe1692e6611dbfdac44ec5b08ad230c6ee9dc773d39e4e6bee0f87de21b3d7340b1b9b6991d648632a548233b47f1038c9e0fb671c57e2b7edc7e217bd8aeaead66d971685a58244382b2464b211839e081c760339ef5f517fc13e3e28ea7e36f867a3ff0069cad25d4310b791998e4bc2db0b724f65c9e0f635e853a71c460aa52da7497b68bb5ef1f76325f8c6fb69d4f571509509d1c4af81b5467d3de6ae9f927df5d55fab4bf5065ff56ffee9ff003ffebe3d6bce22bd0be217849e4a36074e8d93f877fa641e48aee66bb48edcef600e30493db1f5ef8c7e79c015e1e3502fe30628dfbb11904f50727ea07407ebcf06a32cc3b9c7149af7551959bd75ba4ade575d7e5d0e2c7547ed28774d3efdaf7f45a6bd7ef26f8c577703c337e96c49736f22a91ea50f39e33cf5fc33d6bf3b7e0dea29e15f8a90e9524862bed5e39af2601b89648e60646f718900c9c9c7bf35fa27e3d9619745bc594071e4be01e87e5393ce4e300fe032315f8ef6fe2e07f6c7f0c7852d65568d74bb9b8728c0f96af3c68cbc75c95200edd71c023d0e1faf3967d472a78793854c062aa3adcadc63ece9caa545376495e30dbaad16a70e6b38d3c03c47b45775e94145bb36e728462fabd1bb796b73f73ad5c4f6b049de58d793d49c7a9ef82091f4f4c192cad8ade6e238c7e272481ea4f3eff4a852230e9768539d9021cf7fbb91f89f6f7fc6d6997714df3646e518c13c823ea73907a7d38ee2bcc9f328d7953d69f3ca9b5d62b9b4bfaad2ff0089953b2ab4a33b27784b5b24ed66edfe5fa0be2bd4a2d374e919d80c26d1938c9c741cfa63fc8af1457fb50f3b83b8f7fe9c74e7f3cd751f11a69ee4dbdb440f9687cc948271803e5078ee493c8c71d738ae42c268c5b202e0104820804f000eff00d38af7b2bc2fb2c142a2d67564e6fab515651d2fa5f57e57ea4e32aaab889dfe18a515d13492fc7456f99f540038ce7f21fa739fa7b839e3383afcbc673c1e3a7d41c13edfae7a1e87239e3af3e9e9c71ebf87a5073d0b6791df8efcfe1fe7835f087dc8a08cf4073db00fd392c71ee0f4a00e78c700f653ebea48eb81f4f4eb49c821b393cf4233dc1f5e383cf22946467e6f5e73dff227b751f4068001c63804fa6149fc3073ea7241edf520c739031f86792307839ef8233d3b93460f5cff00e3c339c7af23fcfe3498233823a73839ebc13c0e9dcfb63bd0019e074f73853f4fe5df9c9fa51d39201047a0e9d8e01eb91cfa7ad0339eb827be7f1e4fbfa7a8f6a0e79048c03ebc73e83f9e0714009f4fe5dbd7ad1fe7d3a7f5e9ff00d7cd21fd3af6ff003fcbf9579878fbe2258785acd9239564ba7dca70c36c632236248395656607276a8da4975009ad29529d69a8415dbddf44bbb7fd5fa113a91a71e693f45d5bec8c7f88de3cb17f0cfc4fd23c3d793c9e20f07e816771ab7d9e10458aeb6b78b6e1657575771058ddc970ca8c96b1a8691d0e76fe42fc21d1a6f8a9e3bf054bae5bca6e3e1b7c4eb2f116a60b42ebf66f0f787759f1268827455ded6f36ada4a288dd5cce77c7e632b2b1fa834df8aa9f0f7e315bf8b359992f3c2be368bfe11df1aac9b65822d3af67c69faacf1cae90ba69978cff006a4585fcbd36f2f54eff0039dd6bf8d7e1837ece1e3cf14f8bf4057bbf865f1174b6b9d32f72653e17d7edd755957439ee40626caeedb5bbc9f43bb763e6d8dbbe9d31f3ac45d5f7a6a11a0a7876bdd9da7193d79da8c54a2fb3528b696d67e4eef0555cdca6b4aae3cb0ef16db4d2d74fddcdbff1455f5693f947f68af1adc5cea3a8a5b4d0c32c52bb40e43b46d099a2477037ab295261314a149da5a4547f2994fcabe28f150b4d1a69a693cc98c50c31cd23c9e7085edcee90452012970a72e668c3e51f3b5880727e2d78ea3bcd5f5ebc8b50845c34378d6ef24afe544f6920489d3e5955a49269238d9d9b61865dccef1aac6bf1c7897e2bea2749924d46fe1b8d426f3a68a2867796d9899659cdbc19d8b2e55d9e21198e172d32c5b2285635e6c5e23930751c1af69c9269732bd96ef4495d257ebd3476b1f4f82a1fed74a124d535c8a4da764df2d93b6b67a26b4eb74afaf85fc6bf186b716b7686deecdbab6a0e1a279954dd2c93491b1ba922d8c9292cb244acbb8caad13029264f9c789bc6d7eb0470c32c8ef25b96091946335c1c0565790b3895420f2d5994279b2100956c711f17f50bdd72d6e6e2d2454903afeeee84cd1c6b1b451cccd1a4cac85fcb321018333e409a32eecfc768f791c2560925769ac0a2cc924b3b2452ac72b2dcc729f3c790e8b1dcb056d90b3865cb3123f3fc4579e2f0ceab9ddfb5709c62ba3946ee527649372d1bba6f66b4bfddd0a51c2e2fd8a8351f6519c6a4a7f6b974518a4db7685dad24acdbba3ccfe20f88b53f0bdea5cf883528f475d4194c290ffa55c3eff302025e58e289d7cbf9971f2ef4625778cf61e0eb3d1f59b75bd9f5a1348e0a837d2471956dc40d91338421c02c19598104727e755f807e36f8f6dedfe2569daa78eeef556f0f79b6111874d824ba5b79628c208d032a011c96bbb73070249a79c2a344c631fa43f0e2f3f66ed53e1ce8fe24d2af3e226836b3d8dddcdb6b1ad783b50b6d2f5278b508ad754d43fb548b8b510595fdcc7628232d158cd72be72912c3bba715818e1f094552589854ab0539d4c3d195582564f95ca29b6d697bb7bf958f332fcc6a6331f8894feab3a546af2429e2ab7b2a9292b7bd18b6a0aed593b2b6abf9ad89a0fc53f8a3f04fc4574fe137d2b5fd2f5059eda2d3f5c21a1b6f37cd696d25926b3bf0d652ef79e37583cfb696567fde4491b5b7b97c1dfda01bc57e20b987c6de096f08eafa9c32886d6df50b6bfd1ef65f3ae025ff0087752b5786391034727da74ebbb3b1d4210031b4fb3b4570df3feb1e01b1d4ee567f08f8e2d7c4560b20b9862594437c4a3a1846dbf7b795bc98b6405e197cc957cee017d94ed33c0be2eb69a0bfd2ee534b92c6ea34326a486686e19b2f0a5c5bbca4acad2baadbdd5ac90dd330496295262d8f2a785962a8fb0a3cd3a8ee9d474eac1c5bb24e54e4b9137f6a4a0e4f77267d1d3cc9e0f10ebe220e9d15caa34613a1521350b35cb5a0bda3b26d462eaa846fa42d78bfd89fd9e2ff00c3b0fc4bd22efc4b244d65f6ab6fb32dc426585e4574404ba96522256dca8c02a94030482a3d4ff6bffd9dbe1cdd6b13685e25f0f68371e05d7f5ebf8fe1ef892196dee145c5cd959eb915ab46e7ccd36eada1bc16d13c0cf6f7d6d6132ca85e23137e7cfc1ff1278eb45d434d9ee5fc376dacc6d31d2e7f124375269aae8ea93dfae9d69a95a49aa9836f996c92df5ac4ec3e791a1dde6ed7c7af8a7f102e34df0c58f8cb5ab3d4d745d54de5eea1a5585de8f692ea37aa90db5ec1657dabeb52d8c70441ad62417f7408b995d9ca3a245f63c3f471993656a8e3f0d4a9b737384a9ca12954bf2a97b677973426b58f2a8b8f2abb7d7f39e29ab80e21cf96272cc65695385354abc6a29d354db7783a097272ce9cb4a8a72a916a6e2ac9292faa7f660f821143e25b6f861e22bad48f8516e9174fd245e48fa447791b9b8b1d49208e56b4fb54172b04ab78b125c4193e44d0f9642ff64d6d0a4105bdba0016de28e15c2800ac68a800009c00abc0e7031ed5fc897eca5f132deef50f0c5d5cbadc6a5a6de5b2a5cb82f2c826f2c0cb0dbb9f90242c4b6d8f38c48651fd75dbbc8f6f04928293490c4d2a742b2346a5d48c71b1895209ce4706bd8af569d5a18774ea46693aaf4b2e5e654ed16acb5566af6dbaf43e42586a986c7e2d55838b953a1abda728baca73577f697237d7449eaaef07c51a8269fa5dc4ac4025582824727181d73ebd89feb5f10f8bee5ee64b891f3862e4f39e09240e71db9ff1edf4f7c44be96e1859c6c76264b007b8f5c1f5e7afe98af98bc51108e0973d95b927dbd3b0e7f3eb5b508b8c12ef76f6ea935af92fd7c8c6524e6e5d2fa6bada36d56cacddfbbedb33f353f68aba7b49e1911b1fbe038ce7ef0f4e4fe27a7e9f5c7ec8f752dd68d68d23139443927afddee7b738e7f4af8d7f6896371791c4bcfefc018ebf787a1ed81dbae3935f607eca0bf60d12cc3e54f96bc9e31c0fd31c7bf35d492f672d2f2ba5eabafe97d0c93f793ba49faf4b69d7cf4ff267e900956380638f94771e9fd718c74c5711ac5d2c9bb0477e9db07f11ea3a13d47bd175ab868c46ac492a3001e7e98ea3bf38fa5518b4cbbd4d860148f3c9c72727a71d3f0fae2b34adf9f4d2f6d15ba7fc38689f47fe5f9dbf2eb6384d4f7c8488959dcff0a0ce7afd463a7ff5fbd3d0fc35a9de6a114b711347123860bce4e08c67a0039e703f33cd7bee95e0fb640acf1863c1248ce0ff009eb91ffd7e99f4ab4b25120445c01d00eddf3fe7a75159d59da9cf95fbdcaed6d75ff86fc8ba5cbed61ccbdde6575f769f7eeb5b977c3d6df66b38d08008403a74c607e18c638fe79ce578ba711da48a80990ab63eb83efd88f6f7aa7278a2cece6f24ccab8e02f039fe7c1ebd7eb5c9788fc449344fe5b062548001e4fbfa7ebd79ea4d7c4e271718aa94e72fde45bbdda72d6d6d1bbf6f2bbd4fb0c0e0aacf114aaf2fee9f2ca3a34adf969f759f4563cd174b96e659a4bb94aa92c4203dbdfb9cf3c679fd6b9dd5b51b6d0e3709295c9273bb03d081ec490707b67d2b5fedec8d24b79308e31b9846a46e3dfe63dbd303af5e3bfc67fb407c5db6d02dee044044b02b9dfb88e4739cf4edf88f415e6617114b0f38549ca34d393d66dca4d68eed2bf4efe87d4e2a9d4ad4aa528a753ddf862928a76d97777dddedab3d17c57e2a7bb9117cd2ca496043678ee0e78233d39f5aa1a6789eca058a09a64019b2d9618edc13bba71f8fd78afce2b4fda82cf564914dc20780c8818b039232382707b6473c7bf35e41e31fda62eadcb8b5bf659118b4651f1823a0241c9e7af6ea003d2be8313c6d9665d495eaaa92514bdd7f6925ac757f2d1ad1edadbe0a9f036699ae21b8d29538f3376926b46d3f79f4d9eff8ee7f461f0afc67a298fca49618d811c87503903af38ce0f5f5afa0d3c4163328db7719e9c6e5c738cf4e73d3b0e791d2bf929f0c7edc1f103c3175e624d0ddc24f2aaf2094c7d70416604fae319f60315f52f80ffe0a411cd776ebaebddda85642f1b6e2a7071c36f2bc73e9c67af6cb05c7b9163a37ad52a50acb44aa45a8eeacf99e9ddd9a56e977b6198f86dc45826dd0a31ad41fbd3f652e696d1ba708fbd7d7b75ebab3fa5bd3cc7244ae84302339f5cf3db3f8e4f5f6cd63f89b5fb1d1ed1dae2540c4602e4679e4679f607fa57e6f7c2ffdbd3c31e30b64d3b45492f2f442a7cb8cab9c9c0cb61881c9031c73c76af6cb2d47c4df106f20bdbc496ded0caafe57cc3f744e42b03dcf04e324f4ee6bea32b8d2cd26abd0ab1a9864eeeac5a7156b7bb7eb2d5d92eb6e87cb67188ab92e0e586ab41d2c535caa9cd5a6ef6f7b95eaa29abdda5b5d23dfb44bc9752945e05220049033c904e41c77cf38e4fd4935e8d657d0b7cbb80ed83db1dbdbfc7d2b8cd36d534fb08614006d403b0e839f7f6c76354af2edd03bc4db1d464153d71c0ce0fb63bf5c57ad5b0f0c43708ae58c7dd87a2eaf45bbb2befbedd3e4f059857c2548d595a739c94aa45f7d1bb3db47a5b6dfa33d6728e3820f07dfff00ae33fcfd8f1c8f88fc2b61aedbbc57312b0607b723a9e3df3ce38e9c039c0f11d47e37d9f85675b6d77304664da27391195cf52d8c0239c83ce31dabd5bc35f123c3be27b78e6b0bf827575046c914f51fef11919c670307bd7972a1570d55c6328c9c6cdc53bbb69676ff0086dac7dc47131c550a75a746a525515e3512d2ead75cddbe6ed6bf7470127832e3c32de6d9492342bfc07394033dfd003927bd790fc49f88f77a36937d1dcee4d90b84981383804a863c7b0e4fb75c0afae353b9b6b885d49560c38e413d3a8238ebcf5fd473f38fc48f87d178834fbc8c44b2c7346ea401c8c83d41efc13d0f3cf5af5684e55e95aa24a5b2938d96cb54be7adba2f92f26a53a54b130acdb92528b9b5a395a49ddf77ebbf5ee7e14fed0fe37975f835169655642b27033939cf5193c1e0f3e95f577fc12f7c441fc2d35931205b6a1771af1c63ce6e839c67233ff00eb15f357ed39fb3ff8d74b9a63e19d36ef52b6b898abdac48c6489198e4a6461979e87a738ec2bee4fd81be07ebbe07f0ac373a85ac96973787ed1240ea54c6d27cd861d7712493df279cd4529cb052ad1aed7bd42508453bf3b938b8f2e97b2e56ddd6bd55ec7d2578d2c6e1682a1256f6f1a936d72b8282d54b4b26efa5af7df5b1f72fc5df894fe1586d52324b5dbac4a030182c3079e3181fae71599e0bd486a51a6a93300f2aa1c93c80727f2c927afe15cb7c6af85be2bf17dc69a34c6023b594492332923681c0e0827d7a633c678153e89e11f15787b4458a78c3bc4a0646572157192791d7b7a71ed5e8650aa4f0f5fdab853552aa8d04dc799c2d1bbb7337694afbefa5ada37e6675530696169d071752106eb4a3fccdae54fa689dfc9ef746cfc5af12db691e1cd4eeda740b0d94cd82401feadb91c9e7b1e3f0afe727e087c638bc67ff0525d0f41d3a46bc966d22f64bc48cb3c7a7dad9dd91be43ced69e570b8247dd5c8e6bf57bf6b3b9f89f75f0ef5db6f08e87a85fea6d657090470646f91a360aa19880371e07e67a8afca4ff8248fec93f167c2bf1ebc7ff1bbe3568d369de28f106a2f65a459dc1f364b1d223b8778635700aa19198cb2043cb1cb1c8e3f45e19cbe8e0b0998e675d52756184c451a726e3cf1f6b4f91f2a6eeaebb746d68ae7c36715e35a587c2a9b4e55a949415df338ce2dbec945ddbbd9bb256d4feb5ade257d3205233fb88d47ae020fe5d7ad726d6d71657c0c218c529f980e46477fd08207d4e062bad83725a4018101634183d01daa319ff39a6a2a4c49e09ddc608cf1c9193e9dfa7e80d7e474abca8cab69cf09b92945ecdb7a3bf7beb77e9ebef57a4eafb1e5ba9c630b6b6d92d75def6b6cfb6af4382f1118c4799b05e61b578c8381c29f4c819e0f5e3915e417363a82ccfe50611b1de831d01247f4af74f11436d2c4f1b90ac986472780d8e9eb9cf6e73ce7ad7935d6bb05b4a60900dd10d84f1ce09e7a739f5afa9ca6acde1d7b3a7ccef671959ab5d72b8aedd3ba77bbede7e229fbef9a5d7469eba5aeafa2eed5ba58faac1c0e98e473cfe5c7f4fe78a3afe238ebebfae393dff13c500f3eb8c903a81c1edf5c1ebda8dc7bf4f4ec79cfe9edec3a57e787e8a2e4f71eb9ce7049f51ea47f3f4c61beb91cfa9ce79ff39e69fbb1df8fc33f418231c74cf39fd132091d33ebf4e9ceefd720f0280101391900e30bce7ffae33f87d052e4e49c738e7aff0091db3f4e0fa84923e6c707b75246072323f4e7e82919c2f2c4051c924f1d38249382071d48e3f2a004dddfa7a9c9e7ea49ce3f1fe42b9dd67c5ba06856f35c5fea1013187ff47b7717374cca3223486324876e02f99e5ae580665cd7947c4cf8b9e19d06ceeeca292fb5abc84ed934ed015ee2e3cc1288824f2c258c5189480c42c9860a8eb970a7e34f15fc41f13f88adeeece0f06ea1a15bcb27da9659ed2fafae5dd05b8d9ba586da18dc36f3e59b7457da9b496638f470f81e74a559ca29b4f91249b5a6edbba76be9cbd2d75bae1ad8c507cb4d464fbbbb5d364b4ebd65e6d58fa02e7f69bb7d4fc5571e1cb5d32e749b35807d95b5086482faf6eb73c914618b18043791c2628fcbfb92cd1a899d9976f837c64f15ac2b3df0bc3765e27d4228a067677b555f3098d431762c0094021079916cdcac028f00d57c3979a8e946d166b8b3bc2f2429e6feee68fce48c31877864b791ddb706859e4f350bab07002fc83e3ff8b7e2cd016efc31e3a13dd69b144a9a4f892149659756062731dbdfc0a1e48f58574795ee25692c6e011756d22225ce9965ea469d3a517ece1c8d5b4d6eed6d5bd5b76eaf5eeac9b7c2aa4ea4d2ab2e6bd9a77b6ed5d2d9251be96d1f9b3d975ff18278db4fbcd2c0513da895a382778d0344630274546905cbf990289028619d81e55232abf71fecc9e38d3bf68cf821e33fd9d3e21dfac7e28d0743934482f44cd71a85c787a52ebe16f13db0b931c93dfe8b7b6b124c4bbabdde9aa6690477214fe0bf83be29c165e25b5786f277b596468e098ddc4a638de491d61940bdb6693cb224b75388cb471c7219148c27d47a07c56bcf83fe36f0a7c61f085b3c973e1cbe487c456566b34d26b5e0cbd9214d634e2b146e6e668a04b7bcb512dcb81a94113bb6d6b82fcf88a5ede16b7bcb58b6b4be9eb75aad1f76fc8de8d6f63522d4b4d1dd3d9ae5b3bec9ab6fd2df33f33bf6b8b0f1dfc19f887e36f83df1005f68baae89a95ddbcb7b6aae906a364e20b8b2d7b4d778d1eef4fd56ca6b2d56c5c4ef733c6cf6e91c577692b47f9eb65f107c52c8226b9bbd427d2ef59628ef51609751fb6dd1b7490f976f24291daadd878c4db246486358ae44ecab1ff61bff00053efd91347fdba7f665d33e367c1b86db51f8c3e06f0b7fc255e089b4fb7334fe3df0b490a6ad7de0c7781e29a6bdd9e6ea3e16674b978b56f3f4c8e048f5cbb913f880b7f16dde937d1dbea36be6cd68d6119b13f6bb09d2ea0d5e192f2d25b4b85b69ed20548ee229e290ce4e177491498683c1ad49f2f2d44a4ad28352e6bae977aead2774f47f91f6f82cc156e5a91728555c9751e5b732e5e5925d2136aef476d53764afebda8f8ba5d5da28ae1e496e2ee212ac6f1b18d1556399fcd688c57109802aa4bf698544cf322c7e6c6a71ad712db68962daab5cc31adcdb89512ea5290b49334703a2b3894169308e5c18c10924c54466467f9cefb53bed0b54b04b61a698af2ea3bb8a68e7b778879f34d28b9be8266431c3e75dc52082e97c811791124665994d70de2cf1e6a176749315cb6a167693dba08db500de53c370f24093d9cb2c70cd7531cdd471adb48ad1982331405e4497c65927b5e5e59ba787ab2739c2df1bba6b56fdd6d6b7b3d6c95d356f7a5c41ec7daf3538cf134d469c24fec2b45594545b928eb6f793b7c4959dfec0f06fc05f077c561aae9de31d3ed7578b51b49b4f91d7cb335a17b87b8b59ad64548becd2d9cb22cd03195664c24c49593cb5fd4cfd9ffe1f7c2af0afc1cf17fc1af8dba759dbf87adbe1b6afe0cf01f88acb4468eff58b9d6f5137571a5eabaa68979a42a4da888ed96d60d5ac758b4bfb8dc5d6c9a1851bf393f678f133ea914dab08121b569e15b95854c7696d70249a56b788990cb39b54308999c2a23304f31f6bc927e8b4ff0013748ff847a3b064b55ba8a222d9a7914c57aa47efad271b1b7c2f19912360eaf010a00d9e62b7818acc31582c756a152b5454f95534e0edec9c611845a8ab2b3859492beca5a6a9fd5e5781c2e2b2ca7530f4e11a95a70ab36e34e529b8d5f6aa57a909a72537249c949384e74d3f794a3f4fe9ff00f0418f873a678852f3e1c78afc46ba23f866d75593fb0fc4d6973e198f52bbb7f2d6385668218afee6e2549afede582c6384c2591af21492027f3ebe397ecb5fb4bfecabe121e3cb8f09bfc61f87775e21d52cb4f8fc21a75cea1ac5ae95631cb731788ae74ebdb7d36e2c6d45ac4ff6d9b4bbab8b68045f68b99238664d9f627ece9fb557c48f874752d3fe1c78fee7418e72c2e7c25e258e3d6fc373cca5a146b78a6370fa64968af23c5269eb05b5cec0b26c52ccff0078ebff00b65eaff17fc1d07c32d67e005af8a754165368f06b5a76bd6ba57869acae5ec9351581a56b9d5b48fed0d3ed23d3e692d247b8fb15ede18e4b5631c55f4d9772e2674ebfefaa7b58caca3522dbf860a29c76724ecdce9f2c95ad2b6abe5734ab8fc142a616be1f013c3509439b1342953c26269c6ce52a9529d5ab4a15da9f2be58d5928284b9a309bf77f077e047c69f823f1ad6ffc13a8eaba37817e22e9d6f19b75d5af5f47f12693769e62dbd8df693ac4c97734525d7ee9e4d3e69104aeabf303e5c9e03ff0b2f56f897f1235af84baa5bdbeb0a963e2786e2fad0c9bacce8915edc59dce0c45248a036f6f6c9209d5a60eee3cc912655e37f68eff00827878f342f897e23f8e9f103e2235c7c44f1c78ea3d5a6b2f0a6911e83a625f5edfc31c3a4e95682f2fde0d3ecd2382c2d4c97534b72d179f24a85896faa7f656fd9ce4f86735eea9aa5bea1e2af895f116eed748d1346f37fb4b5ad4fed973184b7b5872d7510bdba31dbdd3208b6a91636cadf6860befd49b9b7859daa28a4b95d94a335fdef855b9a0e5abb35b2d53f889fb3a72a98ea13e4e793719c1fbb5217d1b85d5d49424f5b25d252b731fac1ff048ff00d98b5ff1ff008c347f13f89ed9d7c19e04b7b1d5f5779a321753d422b890e93a39dca7cc5bdbbb633ce0841f60b3bd4de656403faa695be476c73838e49fa0e78ea7b75f415f33fec8bf02dbf67ef82be19f076a5f6797c617f10d7fc6f736a910b7ff00848b528e37974ab2318553a5f87ad96df43d38a92b3c564f7ec04d7b396fa6d806e0fd0fbf27af3903fc7d315c50a70a295386b18bdff99e977e8ed65e4975b98d6c454c554957abeece6b48edc91d79636beeaedcbfbcd9e51af698f71e6ca549639278ce33f91ff27dabe57f8908f6314dbbe5003939eddbf21df9e99e9dfed7f11dcc3696923305042138e87a71ce0f3e878f4af84be289d63c4525c5ae970b3062ebbd572307ae4e3a019cf7eded5ead0bce374adadfbf6febcf5d3bf9753dc6e37bb5fe7f97ddd4fceaf89767fdb9adaac41a4f2e652d81907e6ebdf918c7239cf6afb4be00f86efa3d32d8796f0a945080a6d6c600cf38e31f974f515cef843e08dd5f6a6925fa3bb2b867775fbcc5b2d8e3d7247ff5abef7f08f84ac7c3f656d088557646ab9da382001d875272323d7ae393d1754e2d3b3937a24fbdb7ff002f3e9631a7cf2df48ab2f3d2ced7d1db5febad2d3bc2772d246ee5a407696dc338fa7603db2318af4ab5b086cd1170060633e847e7d7f1f7ad2b79208d405db8c77c01c7d7f2c74f7ae7357d54432796adcb1e307391edf4f6faf02b07294ddaed595feff25ebdfcfa2354add5bf5d4ea639e345c2e381c9e3d39ef93fd335cf6af785e29501e769233c638edf955682f310ee66c92324e7aff9fc31f5c1ae4b57d5d10bfcc3e80fb7ff00affce3316b369f4fc7b7e77ea54775eabf33e64f8a1e319fc3934974f232ac6c5b827a824fb73d71c7d7b5715a5fc66b4d5ac04de7aa6d055b7b80db87072339cf1f80e41eb9a1fb44abde695752db02ede5bb6d5f500fa7539e39fc3dbf1eb5ef8a7e23f0cea77d60ed3c300672b9660bc31cf5c638e3f4ea2bf3ae29852c1d6fad294ad3569c62ae93d37b6bff0002fe87ea5c23888e3684b07562b9e9b4e9cdeee2ed7493dda56dddac95d1fa5bf10bf682d0bc390dcdc5dea91469123b153229660371c05c9249f41cf623b57e4afed09fb4bffc2c85bbd2f465fb35a12f1b5c97c4b32e7692b8c95047a93c7619af05f89bf12353f104f329967752cd9259caf3f7b1d89fa1e9935e15a6472ea37ad0bb36dce48e72dcf53c13f975edeff90e6399e618aaee9d29ba586e6d64afcf3d56976bdd5cb74ecafa68fa1faa60327c1d3b549439eb36b954be1879f2addedbfbab4eed9d2e9f1eb91c4ed632314c963fbc249cf6ebd48e73d3d7b81d1e8107f6924b16a3117b91c1dd9249c60904e7af6ebce3bf15e97e10f075b6a5618b593cb9a21fbc4241dc319271dc9ee474fa1e2dc3e1896c3570618f2c841906dc679c1619fc39c7d4d7461700ea72d4aadcdd959ca7ccada68d3fbd74be9b9ef7b2a34524a31bab7324acdded77d377d56d7e9d7c9cf856e6c75032c70b34723651181236e7eee081c7b77c1c918e762fbc0d7f76f612e9d66e6e2ee75891101c6e383b88001c0e49cf61ed5f40dcf86ee2f121b9f282ecdac0aae32463e53818e9dfaf07d057d17f013c1fa76bbe27b6b5be8e399a0daea8c14ec70401c11c67b9e7b9cf7afa6ca729c3e3b1d86c2b497b7ab0a73924be16e2a4d79db55d3e4781c4398d3cb729c7631462e587a152ac52777cc92714df672e54fbf93d4fa7bf610fd9c3fb374e875fd6213fda3732248db94828bc6d553e800ee083db2306bf6d7c3de1fb3d2eca248d141441d1792718ce3df23d4e303e9e29f05fc2f69a36950a4312c604681428c74031e848000e73ea39e4d7d1b0ae63c0fc87b8edfe1f966bfa16384c3e5382a196e063ecb0f420969f14dbe572949e8dca4fde6df5d0fe27cc334c4e7d98e2731c73e7ad56a3767f0412d2308c744a31564b4e8f5ef9f78e1223cf63c74007d78c723d7f0c715c5ddddc618ab1ebc75c1f4c67d476c8f7e0d7657e9989877efd4f4ce7ebd00e3ebd2bcc7538dd646da0fdff00cb9f5e9d7f2fcebd0c1c60e376ef67f9f5f349df4fc753e7317525092ecdbefa756f4decfe7f81c4f8ebc0769e28b29164884a1c120e0120b0ea0f5c8047a0e7f01f375b7853c49f0e6ee4bad2ee2e52d049b8c219c458ec368e14f6271c9fc6bee2d354bc288c03640e0fa75fafd467fa8aadace9da4c96ceb77144d904e1955bdf1c8fa11dbf0c54e216162dcabd38b945371a8aca714edb4ba6aacd3d1d9a3d9caf199ad6f6743073a92a778f35377942edc559c6d6efaab7a9e1fe06f8afaaeb57634cb982512205dcd2e54f5c646e196071c727815f4f580173023b953bc2ee5f73d7af4e3be791ed9af9625834db2d7d6eac5228044ac8587ca0f3b86703047f21c0e95ea3a578b58b24624f4070c7191e99ebd87ae7bf15f0f5f89614ead5a50517184dc23256bcadcbe774d5f57f91face1f85a5528d0af553f6b3a709d4834f9632695d596b6eb1f277daecf40d5be1f689acb6e9ed61762c1b2c884e7d4e474e9c035e83e1bf0c69fa0d94705ac091aa81f71401f88038ee0fad729a36b4b2cd0452be59d8753ce71c7ae3e83b7638af5189c3a2b2904103b9e0f63db9f4a584c54f17ed7112726f9bd9eaefca9a52696aed7d37d74e8678aa1f56e4c3db9636e64ad6bd9f5ef6dff3b6a88dad216e5901e06323af4f6fd7dbaf4acabfb3b5602368d4a9e48206076c9e3fa7e7db7b767af3f5e79fd383e9dba8e45727e20b99215f3233c2e01209e0671dbeb9ff00233ea617da54ad08466d3fb3abb27a25b7a2fb8f0b328d1a78773f671bb92d6caf6bddfe4bbfdf632751f0c68d7b018a7b481908230d1a907231e98e79eb9ce7dab8bd1fc03a268daa0bdb4b4862919cb2844540a33d480a3a8ee7a7d715dde9f75f6e6249cc68147ae5b9c938fe5ce4e718c669b79708b314420b83c007a6383eb8eb93f438e95ecd2c56368fb5c32ad56d3838d48a9b71b492b26df4b3d5bb69a1e04e951a8a155538a51768b695ddacaeb4d36d2caeefe5636752260d2e59a31968e36618cf2403e9cf6c67a9f5af30d0bc651491324ae1678e590904e77649e39381c76e3dfdfbebebe48b4b984cc001031e7b9db81d3a7a63f2e4e6be684b523519de362165959bbe32cc4e78e0139c103af271debbb25c1e1f1143170c4a69aa919539eb6d13e68dedaa69edb5ddf432c7ce70ad87ab495a2a9a8cd5ece3b7bde6effd687a6f887535d42390dac81c303bd01c3a360e3a1cf5c60f239ebdebc32f2def0dc487cc6396279e48e7a13b8723fcfa56a6bb7d75a2badc44c78c6e524ed70464a9f5f6f43ee2b3e1f17693748269824729e1d5b00ee00127a8c839e0f3fd07d460f0f53074a2e84156a32492947592775a3eaf4d9f5d9f979d5654abced39f24d59dafbdedaf45fe57ea8fbb703fc91e83fae7ff00d608a063f3c7b9fcb8f4edd3a77a4ff3fe7b7d69d82475ea7919f5ee7b751debf253f4903b79c6783dc8e7d3ebcfd78a303df071dc1fc0ff004ce3e95cd78b7c5fe1ff000468d71af789b518b4dd3add4e5a46ccd3cbc9482da01fbc9e7918e1235079cb3b2202cbf989f1aff6dcbed465bcd0fc1d33e83a5248f04b75e7c29a95dac6be74bbae165cdb472c60c605b6410cdba490703a68616a57774b9617b3a92f87d17593f25b756ae8c2ad7852d1de537b423bfcfb2ebdec9d933f467c6df157c13e03858eb9ad5aa5ded668f4f82449af6420608f211b31e1863333c484903782c33f0c78eff6ded30ea3258e93a1fda6cadee423c97b7f3436ee91a34d2b3c566006c448446af2dca1908243a83b7f396e7e27a6b5733dd5f5f2dc9988fde4d76cee41799e55dcc70cc05b0ddbb05d9894202a83e5d71adc1732cd7091452e23b993ca54577da4a2038712b967f9e4774281b69e0296cfa9470b468bdb9e4927cf3b6edaf857449ad37ea9bd8f3aae22a545abe58bd5455b4d9abbd5c9f979dd75b7e88dc7ed7569a9ed821f0968ef0ced20648ef8944124af1ee581a3f2833812ca43c2a4e1083bb96e6f51f8a1f0e7c4ed72752f080d1ae2492d633796f7f7ba74ec48819263359cf12346ce61668a113bc9848bc9383247f145bd84535ae951a85926fb2432cb1c925c15431c5f36e48e19a303ccba4270a8d21080a90081a50da16be9c179638ece1263f26ec0846f95c10b6f85611a850a188c8249db9e1bae3349f477b3b349dd36adaabf5d3af7d7738aa464d5d49dd2f2b5f457b59fab3df3548b472b38f0ff0088f57b578e37db05eea5279d15cba9905c46669e74910b6d21dd59a463ba468dc3c75e03f12bc1c7c4ba65f5addcd733416c9e54113bdabdc3bc101413bddf92ad24c92220b68d63b686392330996e63025a96db5bb859c5b3ce2e218d59a25b9c2cd13990c2890b346b13a0117291b0605b7cac41cd685eddddcb00905cef89d7cbfb3dc60b2aaaa95124ead1b7967e610c8e1421c302cc18bf542929c54a11b74b3bdf54acbe7abd52ebd4f3a78974e5c951d9e96e9dacd36fa6dd355d0fcc0d4a3d77c05e2892ceeb74c91912c2e91c836b2491a89a384b348ab70a8e4c6ccf1405a74244904ae3dbf49f895737da62c6f124b1cb6cf6f7026865658c2b858dde4b9b8852231bcd2cadfc0c53011c1da997fb55e8f73a37836ebe22db4173750f83678757d6ad959a5793c2c1523f10cf0b2a4927da740b42de2111968a7b9b7d32ef4bf3e08ef24957c1b4ebd9cd996b594dd5b5cc51cb672d8bc13db5cc13a41324d095bf8659927b49d9a0668d51f70652492d59d5c328b4d2df5d75d1e8fa5b47a3ebb3bea8eaa38a5523cd74d37c8f6bdd72bd7d53badb67bec7ef2ffc12b3f6997bfb3f167ecf1e28bc097fe169e6d7bc142693e697c3da85c31bfd321cb3e5347d4a4f94072b1c37f6b0c6bb2315f919ff0005cbff008271ea1e11f88177fb647c11d02c8fc3df17dddbc7f1abc25a4d99b71e14f1adecd731b7c4386dad6258d342f16493c31ebf2db186eec3c5b226ae45dff6dc82d3cf7e1bfc5af10fc21f8c3e07f899a6c7a9ecd0f50b63ad4211237bbd0af228a0d7adc66fd8cd2c569bef208b040bab5b6720b22eff00ea55bc45e1bf8a1e040755b1d23c53e11f18e82f61aee93a85bdbeaba26b7a36b362629e0bbb3b8496d6fb4dd46cee59648a68de19a09887565241e1ad85e76e492f7ac9a5d1a4927f3564ede89eacf470b8e74651d76b7ce3a5e2ecf752b3f24fb2d7fce16f35cb6b8d352688a2c3613cfa835bcf25bcd7cf6cf72fa7df59dc45e44a67d5522f26f21be8668ad92d2296275943432af256305aeb02cb51b9b4d3627332451c29672c371169fb9e4b9fb1e9f6cd6bbd9e59d43ad946f0a1739b7b48a2dcff00b61ff0563ff826ae91fb28ea0bfb41fc1dd1ef27f807ad6a420f14dac7149abcff000aeef55bbb6fb0c7a8f9fe74da87816eef76d9db5ede3b49a3ddcf656da94d736734b2cff80dafcd77e1bd4205b271782d2490eeb268aff4cb2fb614b44bd4bf49d40747558d24bbc489e6aee921f393cee09519c23c8b4b5dc5a5a72bdacd5ecef6576ba34a29efefd2c652ab6aaf552b465796aa5eeebe7a6ab5bbdcfd39fd9c67d32c7c117fa43cc34d37d3cd77a6c4e9756b39ba91d2199a6171717324973f68f2bcc92195236f3e34ff0058a01eff005f935796d25b359e66552a8934453747e58da1d0c8c8e1d08c15dacec18a2ba8e5ff0037fc11f14e08af6d67bad46e7fb42c6d922b59ac6decad619ce9f6f73169f0ddc411d2e954ce1892f0fd9c4977788b7322c2937dd3e11f884badc96eb34fa7cd2982296e6662c1e588001248da758d2478d4ae55a691fcb914ef562457c866994b9e2255f57cf2739292fb49d34f95b5b72eedabb76493bb3f41c973d54f0b0c35d45422a10946cfdd92935ceafcb74fa2b28a6f5d8ceb1d73e25689aa47159a2eb3682426e65964b982faca03864114a844fc4acca01760ca01cc6576d7eaa7ec7fab78a5e6bbf1378badf54d3ed74812491db5c6a065fed09e30acc912c68d2f9089b5f7c9282d1b956dc7728f98fc157be14bb765bd8ac267b988085a62b6f2caec234648ee186088da442544ac10063bd43e5be98bad7b538ecfc27f0d7e15e8d77e32f889f1275ab1d174bf0e58c325edc5d4370d0413d9476905dd9dca40c9725eea75c470c16f72667824476af7b29c9e4bd955a2fd9c6dcd349ce0a2d59c9ca2a6a9f7bb514dbd5eba9f2bc43c42a34f1142a4a53737ece3a427ce9da3cb194a329abee95e492d5763bbd77c4dff000b97c697b7ca2df5dd3adafad6c743d32ccfdaaef54d52696096dbecd12c45a1f2e5fb3ada800caeb224ea8e278993f753f613fd81ed3e125fdbfc74f8b96505efc58d46ca23e19d067895ed7e1e69f2daf9288b1b6e54d7dad65962f286e1a42dc5d06925d46e247b3ea7f617ff00827cf877f66ed1341f1afc404b1f107c643652ccc96ad14de19f035c6a60bdf5bf87a258634bcd665f3248b50d7e452a1a5b8b4d1a2b6b269ae2ff00f4bb0df4c671938fae3fce3ad7562274d549468bba4da75169cfddf9b6efef6dd15d2527f3743dace941d74e3b4a349fd856568b5b2e556565daf2576d0a4007af71dc71ebc724fd31f9d35ca22962785049395c606493c73f9fff005a9dce7ef76c9e4f4ebe9fa73d7a62b9bf10ea3f64b628a7f792f03903af404f1dbd3f2ae7845ce492f9becbab379cb962ddaef64bbb6711e26375aedc359c0e63b75389197396ff00641e31ee49f5c71d7320f0869f6b090c8a5b18666c1627a92491cf3f8e79e9c574ba71458cc8e727ef127192c7079cf73919f63d7a9ac9d5753f2c950c0039039f43cfaf6c75fa0af439f9528a6a315a25e4bf0fc0e3506dded76deba6bd1ff93b79189fd9f65a7c8ad04681f763e51cf3dbf2e3a73c55cd5f565b6b50aa46e0bd07f780edc8efd07e559e189ccd21e0658679cfa64f4fd7271c570dac5fbcf332039192a0027f018ebf8e3a7618c527514945dd5fa6ab776b3ff81d0a54ddf54ecb7d3af55f2eb7b1df697abbcf10666c1c739278ce73cf5ede9edd4d60eb3a885b942cfc8f7f43db38cff9fad665b5c8b3b4cbb04f979e47a77e7f5f5239c75f22f1478e2da0bb75598164ce4020f393d7071818e7eb91d6b19d7505293b6b74bceff2efd74b2f4368d14dab7cedadb6d5ebf969d6dae9eb17be2a8ed2039902f18393c63bf381dbeb8cf6eb5e27e22f899616f23a3dda1249e378e393efcfd3db26bc17c73f12aedd254b795c6430c296ce79e38eb8c761d871c66be4cd7b58f15eab70ef6e677059b014b1cf7e9ee3be0ff872caad6aa9fb38bb69d3d3e7a75ff35657cb08bd5abe9bbf45f3e9e5e76b1f64f88fc53a6f88ad66856647dc846372907208c7724e727f9d7c15f12fe135aebf7d33a4032ec482aa01c93c60e391cf4f4e2bd03c316be2e2e4cc260a7e6dad924fa8ef81ce0f27a9c715ea36da5de4f2c4d3c47712324a9c64753c8efebc74fa8af2eb6572cc1db130e68a6b7d53db5b5aefcf5db53bf0d9954c0c94f0f3e593576d36b4d2f6b3d3cddfb6a7c5fa77ecad0eb103efb0562410098f2791d7eef3e9f538c73cf8afc42fd94aff00c1ce75bd3ede58d532d246aac50a8193db031d791cf4f6afdccf01787636854340a720039504f3cfa647e78e71dcd59f8a7e00b0bef0e5c86b58d8b4520fb8bd4a11c1c73fafd6b8ab706e02b53b2a718cada3b2feeebfd6cb6eb6f5f0fc679ae16b466ab4da4d369c9bd16b66af6f4fd373f9b9b6f1ada785f544858ac12c47cbb9889da5c0382db781f2e727a039e32057d19a25d7877c456b0ea96f2c493ed0ceaa472a47cde9c119c7a7af71f07fedb1e0bf13f84be2235cf86629844f24c648630c03007208038f5e31cfd4e2b33e0dea9f16e5d122b8b3f0eeafa8c6b1fcf14104ae78c86c2e09e7e8718e3b67e6b15c199850972e0e9cabc2f74a92729c53b3b38c6ef6ebaabab7548fd232ff0011b2bc461d2cc2ac3095d45294eaca30a73d9371936a3bd9d9bbd937e67e8a6bde28f0fe8fa5cec2f2dd648e36c2315dcac01180091df27b700e3bd3ff0064af1fcfe20f8aad0da6e96012ac5e6267cbc8719ce323233838e3d0715f05dd7c3af8eff12b5f586cbc37af6976134ca93a5cdadc27048dc572a0608ebe84f1d6bf6ebf613fd91efbc0905a6adad58c897d27972bbcd1fcfb885249c8c8e7231ef8e30057b1c2fc319950cd70d5b1942ad0a742a7b56ea46516f952692bad2efe76befa1f1bc79c7594d6c8f1982c06269e2ebe2a9ba4bd9ce1251e66aeef1936edbea9dd6eaed1fb2bf0dd255d2ad4c8305a3438db8392a0fe79c8e2bd8a01f2f1d7fc33fe1e8071f971fe1fd2c69d69045b71b502e075180073d78c64fa7b5763011b71d7073cff0087b57ea58c9f3cae9e97b7cadfe6b4ef63f9cb06ad2b4b4d1b775e69fead797c8a3771e438f504f27f103f4f5e07ad707a85ae65391d4f5c8cf39ff0e79e9fa7a15d7f163fbbcf4fc3d31db9fcb915c95e852e738ebc8ed91f875f6c01dfbf3b60e6d69de2d7a6cffaf97a1c798463cd6eff002d747a69e7fd6a882cd1618cbb100046eb8ec07a818e9fa63ae01f0ff883e2d6b40f0dab6640c55b1cf18c73dbbfa9e9f81f7668f7c000eea475ebd40fd064fa1e78c71f34fc46f096ad79772cf6185cb658118dc31d41e9d0f4f51d08af0789a9e3ab6126b06a739395a4a9af7b95b4b47a7e0ff267e97e1c4b29a3525f5ba94a151a4e2eab5cbcda369dd5f4b2b7cb5dcf1f9b5abcfb4f9b3c823576054330270727a0f4cff9eddd695e22b1b429235c7992aa863960067ae719c7d41c1cfbe2b80b9f076a2892cf7b336e4424228e490338cf63c81fa7a5788f8935ebad1e6923cc8815fcbcbb63d71c67d3ebd3a0afc971582ccb2f8fb6c4e1a74e336f96555de4ef6d6caf6be9e7d3b9fb76171397e6737430989a75a50d25ec95e29e9eedefad979ef6ee7defa3f8c5249a0b95941c489801891807923f971db8f7afa5f44f135adddb47fbe5c9507195e840fc7a9f51d066bf26bc33f132dec2d09bb932d1c6482ce003c6738cf1dff3c7bd2c7fb49eada5dc7956d6d753c05cec9232c40c9e0633d0718ee7af239af738673ac3d1aef098de68d3c4b4e3552baa7523a5e5bda325a5ed65d4f0b893876b62687b5c2f2c6ad05b5d2e78e8ecbab7e5bbfbcfd807d62041932271cf61f87a723f99c678ae4f56d66d6e124459118f20a87e79f5e4fae319c93ce79cd7e576abfb58ebb1c042dbdc29087aef07e5e49232327903a7e95e30dfb7149a56ab68baabcd14379a8c1a780f952249640aa57a6412dd803c63ae6bf60cb32fc1626b53543194aa54938ca10528f336ad6f56bcaeefbdf5b7e478ec2e63469549d7a15152a7f1c92728c52b5dbb37a776ef6daf73f713468d2dad19d9c72a64ebd8ae40cfa8f5f5eb5cd3dd817b2cbbc36e720e4e7bf4e783eddebc6fc17f12eefc47e1686f2d22791a68226593f876ba8390c7ae41f4cd3ffb6efe3dcd2c6f9c9248ce7afbfd3af3edefdf1caabd2ad89756d29ca5cb6d1b6925ab5d16dd7a3db53c655a1385351d145377be8dbb77b3bab1eb7aeddfdab4e9618d882c879cf2001d38e71ec3ae3b1ebe167519ac6e9a190eec1c2b700900f7ee7a75a6ea5e3d3650b09b791d3692c49ec3b1efdb18fc8e7ca351f1b8bb9d9d632be848209ef9fa67807073cf233c7a383cbf134e128c68c9c24ef7b35abb6aefa74f3dba1152b534d39ce29dacd36b5f86db6ff77927a1e95aeddaea96e619338da4861d4775e4f500f4fe95e257b657d0dcc91ab6e507e53827839c6739e7f1f7ef535e78fd2d2360e0f4c73dbaf5ce38c01d3af2471d3c9754f88c5ef2431ca02e0700e707273ce0f7ed9af7b0183cd945c68d29385d34a51764ddacd68f5efd3c8f2b198acb23cbedeb538cb4b3725769aebd7eed3d743f6cb8faff002ff1ff003df3c60789bc4da5784745bcd7359b85b7b3b38f7100e659e5c1f2adedd090649e5618551c0019db6a2b30dd2caaa598855505998e005500924938c00324f207ad7e5efed2df19751f11eb53e8ba5dd8b4d034879a3b40762adf5d2e217b994cbf3059d260236daa218f81bdcb57e3b83c33c4d5b3baa70b3a8d7557d229f472b3d7a24deb649fe9189aea842fbce4ed15f9c9f4b2f5d5dba5cf9a3f6a4f8d1e32f1df8990bcf7967a25adc2c7a7e9f1242d6f042b2092540a6402599d630d2cb210d26573848828f88354f0df89bc41a85cdddaac91da19ae9964912283cc8d7302346be7a44db4057ddbc827636fc906be8cd4eef54bfbdb99cdea48f0e9b7370b8314b12cbe407565f2c9944ea768c052a9d0141f7b92bd9dad6e8e9d05fc427022898fc8b261e18e49086dbc091dd98a9322a03b91176943eff2c128c6368c62928a4ac95ada3d17cf57e76d6fe4734fe27ef3937272ba7ab69ddeef7b2eba256bdec79b69bf096eedece39ee2f6f23730dc4d249972523586696426382f2652ca66508563206e284295515ccdff0084b5b8ecaeeeed253222dbbed27ed28cc1c32461bcfb6237c8ce4282500e4a9e403eea04cb6e1e7905d8be923b54dacca5f7967b84dca11d4c7142cbb5628fee818c71525edcc734763a7c13c16d1128f23192e54aa41b9f32b9963c068e265c96232e410542968e44af6be9bb4df7eeff00c85ed5ecd2be8d2696f65bbd1a7babecaf7b1e4514bafe93042cd6f233b398bcb86ee3568e4448b69d915d26ed925bfdd688062187ca198d6ee91e276ba9275be89e391a75b797ed48caa1637258338408c764bbb707f2d90a82199315e916d6e2fa682ca4956e8bbc930f2e785950202e824f322322ac92b81b59d14a4a3b93986eb45b2b5b801f4e1e4c801334659102305dbe6150ecb93220e72802153bb6718548cdb56bb5adb5bed67d7af5f9697d6fac2a52e57cd149b493b68d27657db6ebff00019c4dc9b69a1926b59925125c279655e2936ec10921a40160288c67f9267df93b4f2173aba0ea2ef6c74ad552592585e18d64f2f6c9109a3527e68cab637aa8c22b617617209c0747e1fd36c44e968d346d33cd73180e257124d3489147ff002ca5dcdf6a455511a8f91845f798564e9d2cba77882e41dacb35d595bb5a3a8498821964ca4aa092aa132cea78c1dca4ef1ea60a6a2d4257d6d6be9b72e8b7b5969d3cef7478799d25383a94ecf95dfb35aababdb6eab5efa22e789fc35a76b3a26a7a3eb7691ea1a36b167a8691a9dbcb1a3ef82eed3ec5750c9e5712477b6724a970aaa5db7cb8251762fe3afc2ad5aee3d6be267c0ff11bdbcde38fd9ff00c471782a7324d1da5cebfe06b9b73a9fc33f15c8b1244aafab7861adecee02412c2b0db5acbf697b8bb65afdcf9ed232c62657549230ca36c61b31a4728324518d9319a076323c68adb3312abc71f983f163fe0a0bf0e3c61f027e32f803f6baf84de1ed57c537f71a7dbfc36f8c3f0f344b796fb52f887e029b5186e2c6fb43b2b54696efc53a04d7104ba4a4092cf730436b92d69a7359cbe855a69c54924edf12def4da5ccd75f72d7695db8a7a5da3c9c0e23df74a52b39f2f236ec9d54fdc527a59494a51ec9b8b6d24cbfacd94f04d6b29b299a352b12008a6dd22958924ca4465d999d4ef6595588901625371fdbdff00826ffc569fc5bf0a6ebc07acdc34fab7c38bc8fc3d3890bef9fc3d7a935cf86eeb648aaca904515e68f0055c791a4452eefdf003f26fc3161a47c4ff0007691e28f0b47ad4167a8da24cab79a3dd693acdab6ef3aead355d2b514b7b8d2751b0b90f63a859ea3690cd6f77184949899241f67fec3ba7ea9f0efe3446ba8dfc89a778df4e5f0edde9bb96544bd8d0dfe997f34cdb1e59649e14822648d16dd6fe78b74c8e244e49506e12927756bdb7bdacd3574f74ee9edf23d0fae2528c652b4934927ba7749efebaf66bd0fda0f10781fc3bf103c2be24f879e36d1b4ff12784fc4fa45f68bac68bab5b4779a7eaba46a96d2da5ed8dd5bcc1925867b69648644618c139e4835fc137fc144bfe09f3e27fd813e3b5ff00856d74abef147ecfff00116e750f10fc21f10ddacb2ddd95a2be351f066a1a900f236b7e156bab5b79d6772bac68d369baba4b6d7b7578963fe873a268c26485dc1254028f81c0f4c81d07279cf1cf7af28fdabff63df879fb5dfc15f117c25f1ed8dbe2f91754f09f88cdb2cf7de0cf1a584338d0fc49a79ca4a440f34b67aadac52c4753d0ef752d35a5845df9b1f8d595bdebda49fbaf4d24ada3bef7eddececec7b784c5284a2a49ca9cb49a5fcba2ba77f8a0db6b5d7589fe64373f067c7b0d99f14e8da76a9a9e911ceb2ccf0e9da82416ff006890a04b8b96b34b732ab6cc49e6cea2216e59b12c40dbd23c79abe88d6cba97db212236d3ca5dc4c72900f29229ae262559c29621bcd2889b033aa8523fad9fd857c39e1af865fb65785bf62cf1268d06adf113c2fadeafe22f8ada4b44b77a578774bf0b585d6a3a53ea170d1f917f0f8865b5b2bfd3207565bad1e44be902dadcdb4771fbdbe18f017ecf177a83f8935bf815f07b599a5973abdcdefc2cf05ea1acc32282bfda16d7771a24b7b2bc11b309e112169e06631069562574e8d3c552755d2729c22b48bb49b926acafb5f96caf74def6d2fbcf35a997e2a143da254aac9b8d4937caa3685a4ec9f346eefa35cab569b6d1fe7b1f093c61f13fe27ebfa67813e13783bc5de3df12ea4f1a59687e13d1356d7b5d9d2258aea782cacf46b1bdf96d62fb6cb732c8ab6690e1af2458239123fee27fe0943ff04e1bafd9a3c2f6df1bbe3f68b6b77fb4a78934e9b4db1b1d4174ad526f84de16335ec6fa4e9da9594fa95b1f1478912eae2e3c51ac697a81b7974e974fd16048841a9c97ff00af9e16f0b781fc3f6303f82bc37e17d0b4dbb821b8b73e1ad1b49d26d2e209220619635d32dede278de1706375c8d8df2656babebe98efc28c7d3241ef8cf15e74f17cb46586a30953849fef6539b9549db4717a2518bb7bcb56f66f95b4fd3745d6ad0c4579c6a4a2af4d423682beaa7bbe66becec95dbd5d9a42474e8381ce7818193c1ee79c739eb9a338ce0f18e783ec3239ebee71fd297a67ee139e9c63f3c8ff003f5cd1e9d3df3b7d074c9e7a1ce08ebdab84e910e33f975078ebc1e7b1faf1dce30794f1258b4f12cea32630723dbd39e3faf6aeb01ebf74f3c1207f2c83fa1a8a68d6546460a430231c1c7619c703f0f7aa84b95dfbe8fd19328f32b79a6bd51e413dd8b581577609f7ee3dbb7a633f966b86bdbe37374ab91856c9e7838231f87393f87d4fa4eb9e16be9e726d5808d893df8cfe9f81efdce38a3a6f80196512ddb9739e578da71cf007279cff00faaada9c9d9592eee492b7e2f5df4d2da7ae6e74e9ad6ee5a7ba9372be9a2edbef7d6fe68e36f2f316ec4021157a81c74fa76e3a7b7a0af329b558dae6408858ab7279c71fe1c71e9c8cd7d477de13b57b568910636e080083c0ec7bfb7f915e39a8f830412ce20889662403e84f5eddbf5c638e6b48d372b7ef2e9bb3b2b6ba697bdede7faded11ad16dc791c6daae6d3e7b6fb75bf99e0fe2df15de25bbdbdb64310ca368e738e3a73ed8e47f4f0b9349d56fe579e512b348c4fcc491c9e9d7d4e7f98afae67f87a1a42f729b8e738c67b9e32719f5f6f4f474de11b5b3401215ce0f3b4633dbd39c7bfb0ef5d3f56a4d479b56b6ecb6b377eaadd2ff892eb4b5b7e9b75eb6f9e96d7ab3e4487e1bbdfbefba43c9e873d3d0f04f5cf6cf7c575169f0d34fb6407eca99c6325013d39ea0f3c7af3c6057d0cba443164f96a3072381efd3f5c8cfa8f4aa371027dd0a3b0c633c7f4e3bf03f3ada318455a3a7cba689f5bdfefd177db1726eedfcef7bf46f4b79bf25f7dbcb34cf0059b31db0a83b4e06dc7f9f7e3f3a75ef84adad1b1e5a839c0c0f7ebc8e38e9ce7dc77f69d1ad010c768076fa75ffebe39c938fcc1ae73c4a91c64f206491ec0e7a0f7279e09ee4e40cd34b5db7feb5d35d3c869bf35f876fe9dff00e09a1e08d1d446802a8c807a7a0fc4fa75e39e39c67aff0019e8b149a0ce8c80feedbb73f74fe9edefec6a9f832455862e57a0e411f8e4ff003feb8adff1bdfa47a44c0b0188589e4761e99fcff023b5652e6e656578e9a2f2b69f87a2fbc2524f7b5deff85bd2dff07a5cfc38fda0be0f695e26f1adb4d71691c9b6e1b7ee4072a5c06ddea319278238e7d6bef6fd98ff00666f0769fa359cd1e9968e92468ec3ca42096504f1b4f1ce7d319e3ad78378deea2bcf1730f958090af6cfccc47d4febc81c8e87efcfd992f275d37ec7296db090a9939f948e08ce30003f4c8aecc0d59539e21ab293a774ed74b96c9f7b5efdfa687cc67d4954fab464dba709b6d5dfdb495fb5bcfbfe1ebd6ff023c1568cb341a358c6dc1dcb6f18e47a10a3dfd0f3f80ef74cf0a69fa422a5adbc71aa8e362aae31ec31d8763f4c6715dd0ff543bf0073f5c55439e72318e703803903fce3daa1e3b1151ae79b976eebfe07f5d0e296028528a74d5b9a2a56dd6aa3e5fd26676d09c6318e9f4f6f6ff0f6a7ab6d071d7b75ff003fcbebd29f30c1edcf3dbf2f5fe9fa621a77e65af5dfd7fafc0e16bd9cdb8dde9a5b7e9f86bf9e962b5d4bb11b91920e73dbd3ff00d5ede95c6de5c8de49200ce3391ebdb1cf3fe7b8ae8f5366507d307d4f0318e3f1eddc9e335e737f3e2520e703dcf27ffd47d0d7a984a575cd6bfdde5ff0d7ec8f171d5da9b4fa5f7daeed7d7fcfba48ec2de5cc4bf37d738ff3f91aa77b6a973bb705ef9e0ff9fc381df35069d2ef8533e8074f41e9d873c9ff00f5d6837dd3f43fd7fc7f9d6ae0949df5d76fbb5fcf4d7733a75e715cd4ea4a378d9d9daeb4eab65af4efe4799f88345b4589ff0076300127800743ec781d0e33cfb1afca1fdadbc6b67e0257bd0a00326c2abdd8b10a4ff88ce4e718afd7ed622f3bcc8c701909c8ee30471db3e9e98e3ad7e5dfed93f046ebc6da24d258c324b3432a4cc8abb830570718efc0cf6af1b8932ea3986515e134fda5383ab4b96ca4e5157b736faf6d9ecdea7dbf00710e332ace6935252a35aa2a555d4949a8a925ef59e8da6d6b6ba4ee9b47c27f0dbc7daefc41be8e28fcd5b5925445c6e0bb4903d318c11e87d3a9afd50f86ff00076caef4c826bd8d5d9e25662ca09e403cf1c7af739af8b7f672f83575a09b78eeac5a378e400ee42086cf7cfe40f1f8f22bf5cfc1da60b2d3618ca6d0b128c63d07be7fce79afcd728c05284a4e54f6d1732bcafb2f25d74d3b2f3fd9738cdf11899d955b4774a327677b3e9fe7bf4d8f9fbc43f073c3d14726db589d829caf96bc8efd8febd3eb5f951fb55f85741f0bf8a3c1ecf0a5b409e20b49a450800764943aa9f52580e33d46715fba3e21b7521cf4001273ede9e9f877f4edf809ff000541f11cda5d85a5c69adb6fecafe09add80e04ab202b90bce0739cf63f89fb4c8d4a966d83a94db8c69d684a4a3d60a5172496df0fe1a5efa3f9dc5e265f51c5426f9954a3521ef3d13945c62debb26d37e67ef2fc016d32e7e1c68d32b44a25b2b6720b2f0a625dbd31db00f6e31d2bd335397c3f1210f708091f777af03a1ebc0e7e9c1cd7f36df03bfe0a09ac687e06f0f689a84ef16aaf6b143f65c3b929122a6f5c6702427e4271c6076c0fbdfc21f163c73e3dd3135c8267b6b69622d1ace73b88009663c601cf4ce76fa1e6bf5b786a1524f1f531bc94ab4dca1087c4f58d934f58b56b3bf6d7667e615554a76a31a5771b2727a25f8dbee5b3ed73efdd6ff00e11a60ccf3c4472492411ebd4f1d093d78e32735e5faa6a1e0bb20e5ae2166c1ce5d0e319eb8fd73cf3cfad7cabae6b3e37d5b4db8fb3ea31c3730ab060b265473f794e738239e4e7f1ce7e40f1b5ffc47b549da5f1098b1bfa39e31ff00021c0e39e08fd4fb185c760e9c545e22b4947756bb764b4d95d6fb3efbd99e5d5c156ad26e2a9a6f66f99d9bb6dabd7f15d3567e88ebfe23f06481d4cf0e0824e255cfe59e807f2ce3a578eddf8bfc056f3bc6d3db820e4e65507924766f6fad7e3ef8e3e217c43d345cb0f15b8d85f19627a679fbfd3dbd0e47a57c2de2af8dff001346b57213c613a280005576c70586796cf27d6bd1871665b845ece4b1526acbdd514acadd5bb3ed6df73cdadc2b89c4daa7b6a4aefaa6fb5bbeff0085b4d4feff00be3af8e9bc25e195b1b49962d435a13c41f7ed923b3862692e0c4172e649ced814a8c046979076d7e4af885d1eeaf67dc0c92b471b497093cd9519b8126d2e8e465c2ba98c36e52846393f4d7c7af1fdc6bde3877786ed2c74d8122b3272aa8253e6c4d1aee44dcd0333c9e6a39f9598a93f28f957c442253333b0595cdc4ec4df86cee560a9228311c2200aaa9f2a96e0f1f2fe3784a1f57a11835efcbde9f9c9d9eaedaa8ab25e97f5fd331155d6ab295fdd4dc629eea29a49efd5eb6eefaadb9fb5b3b6b60f74678e3697c985f65b5dc1084b968e49a41b242a5160864503036f98a73c283c74b7b6375acea1703c98628da578cacf7124861da4cb27953855dd1feedb0a18319360042e4bbc48d7b1e94044f089276b8922d979768e91bbc5691c8d3473e07cb13eecc4c06402140916b86b1865f32f5164732909bbcab8470ae596da65579a291df2f183c302b83f285236eb2774ed6fc375cab7f47bfde671d56efb5eebbab6eedf7abefadac8eb35068ffd061f32231dbacaf134bb17cc925036a8f346f2c02caa64cbaa2975041c0ae6e610a4d79348a1e48ad4431c90e0a24acc808db03a126330871b5f2479872e64cd63ea1ab49677abf68b8944714890f97e499177456ed31955da231b31f380640a4b144c32ecda395975667d3a668aeadee1649eeee76c913c0d200f348db1964c2ec59020c60b2aafc84e0d66af75676bb5b7defc9f9ff4c537a6be7dff003b6dbf935b79743a6fdbe6bbb975b99a2982c11a3c9fbd01fefc611e268a485da410c9894b1650993b58a8b1a878a752b4fb64914b33222c76f081348b292f3c65d44772b81292ec163de7cdda18938415c9f86756bb8379963795a679e62eb3c2e8890a482305b111729e4232ee2c557aaab018d492e2d2e8db61d1d6ea69ae65696246554b48ae267f3260b82be6158c93211903e550541d545736ab4d2ddadd75efe4f7f3b9cd29c946fbaebe7aa7adb44bcbb5ef70ff0084e565b8bc5bc8e242d32c313185adae58d8cad129618600e0440379c7772f840aa45b5d4e2ba79a5f3c9914cd3c12dd158a41b90431451bb2fd9d503c8a55da3dc5957e5e99e4afb44b596ca1325b8df3bdb869a194b2132f993cdfbc05645d9c3386dd10182172a318b64d2594f3bc531b888dd858d8a9947d9e18c3e76baa6e460fe592850068c90c4835d11828494adeedec9ab68f4decbf0df6bd8e6949548ca317ef2f8a1bdd595eda77bae97dd23ea0f0cea497d6d67249086923b97b566e17779b6d3c56eead90a436d60ab22bc6c1d082154b0f98bfe0a1bf087c45e37fd96bc59e29f87b25c8f88df06e6b1f8b7e0e5b64962bcbdb8f02cd26a1a9693188f3e71d4f4086fedec610ae8750fb1b6c8d97757b4f80ef913edb6b0c4f14b1d9477584cf97e669a91cf26d0af31499944c8d24523336e28ce8980df55f8763b6d7749bab7b80925b5dd95c5aea019524574bdb7c4cc00042160496127cbf31058852c3d3694e938bbda49a6fb276d579eb75abd5755a1f31393c362635216e6a5523349ab5da92959ab6cf6777aabe87c6ff000c7c41a67c75f843f0cfe346891dafd87c77e07d03c432cc85cc665d42ce29b58b6883cac17ecba88bcb672ed9dd6cb811ac6ac37345d1a6d1bc43a4eb36e2489ec2e2d6e9ef99a640b35a4e27b552cc6395537451b88e3562de53159674001f9eff00611d364f823f12ff00691fd86f5691e38fe1578c1fe2f7c138ccc5cdff00c0df8a978f791697a75bb3084db782bc4f1ea9a0cf242a0a3cd197d80ab1fbf759f0a186f45cc51c51a3ed569257329493cd7648e08b61443c89379124db402c91c6cc6a69cb992e6f38cb74b9a1653dad7bc936afd1ab2b241592a7567c9cdc8dc6a527b374aa72ce9f6bb51945495be24d37b25fac1e04d523d4743d23518f2d0ea5a7da5f439fbc89710a4a15fd19371565ecca411906bd974c28d191c10dcf38e08e7f973c1ffeb7c93fb3b6ab36a7e05b5d3ee0b1b9d12ee5b43962ce6d2e9deead99f2091966b88954f1e5c4ac020611a7d51a69f2012c78e0e0fa8ce38ea49e9db9fa8af9cc7c146535b352692b5eeb46b65a69adbbbd7cbea70357da53a73bb778abb7f73dafb34d7f563e4bf12fc01f80fe14fda4fe20fed0da7e85069dfb43fc4af815a9e8326b08e106b9a4f80adc5835e4713158bfb76df4bd4744d2a4ba27ed12e8b616f12110d94acbe3fa5eb575a3dc2dcb9cc487cc78fe62f7085bcb255700284242c990422e59865543759fb4b5f2ea9fb417c27d334bbc2359d1b419f4cd421827915a1d2fe256a12f872ffcd884be4978f4e825bb06581a68e2fde412c624612f8f683a9ddeb9a4b7f68dbbc3aff87ee9ed3588d1d4894444a198e324198b8691c90184ab291c023d3c0516a846536dfb4a74dd9d93514e518abad795c52b2eeefbb56f1b33aea588708ffcb9a938a95dbe67cb4672eafde4e525a59595b468fba3e06f8e53ed6de12b9b82d65abade6afe1559486fb04f11f3b5bf0dabf2be5c3bff00b634b8892cb6b2ea10a1f22ca254fa8768c7504f6e401d3e9fd79ef8afcadf0aebf77a76afa7cfa7bc71cfa7df5b6a3a6e49db16a1665d92df713831dcc2d3e9f718c892caea64e48563fa83a5ea56fabe9961aa5a64db6a5676d7d06ec86115d429320719e1d438575232ae0a91915e167384546b46b415a15afccbb548daeddbf9d34fce519cba9f53c398e788c34b0d51de787b723ef465b45757ece574ba284a9c56d634703a93e80e0818fc318eb9e87a7346073ce324e0e460f4cf41c7b7a67a7aa64fa71d304b761cf4c0faff002eb464fa631c81cf7fd7a0cf51c0fcfc53e9038c9fd3a633c9e98e46703a63bf19028c0fc7b648231eff00af1f4a3247a81dba81f864f7f7a5dc739e9ec7771df9ff0038e3a74a0036a93d78fd4f2471c0f41da80abf4c718c8f6c11c0e319faf6a4e4e0e338eff367a9c73ec71efefe873e9c6791f3633c7079eb9fd7f0a006ba820a83d475eb83cf7c0ff3915ce5ce9a24776da39249ff000faf4c7a7d735d21ebc7e0067f0ebf87e638eb4c65041f5c1fc7dbdeb484dc5f93febfafc0caa5352d76697dff00d7ccf3abdd3d1412ca3e5031c74c74f5c738f7ae0b5685416c027693ebdb39f6e83d739f5e2bd67545daafff00020303ebfe7f9679c794eae70ec09e493f8f5f603d7b77f702bd1849c95df97e2bfad4e47a6fd3f4384bc1b738f5623a8edffd6ce7deb9e65def823ae08fd3a741f51d38f6ae82f981240f71f9fa75f5fafaf5ac80b961c606739073df8e9d8f4000f7e715643f892bdeda34ecb7f96ba745bf9ea6f69d108eda473c6149cf4c139ff1e38f7f5af06f88be224b1936bca1086c0cb633cf7ce7afae327d4f53ef52cc2db4d91c9032a7273ec7804fe809e2be13f8c7acc73ea51db2b905a523838248273c0edd7a7a115c78ead5f0f86a9570f0f69520af18d9bbfc96bdf6ff0087e9c3c694a718d597245b49b7656db6e97f4f935d7dbfc35e3c48a38b17098da0905873f87a67d7a73d79acff00883f1333a45cc71cca4881f92d9ce14fa30fa7a609c715f271d664b540b15cb2155e79233d38ebf8639e7eb5c078bbc5333e9f744dee4089c31dc47518c9ce41ea3af183d4924d7c84f8af1d87f76b602529a5bda49df4d5dd5edf7e977a1ec2c930d5dc670c5c629bd9bd56d7fc3cf5d6dd8e6ad3c533eafe2b918ca19bed8011927e5f33ae793dff001fc6bf56ff006759e38e0891b87210f7e41c119ebebd7f966bf07fc0be34b56f1e8b2fb4239375f3658724360f7e3b609e4e4fb9afdbcf8257a225b2954fc92c71f73dd473d3d09e38c935f5790e653cc70eb133a4e939c9c1c15fa28beacf92e26c0c309888d184d544a119a6b5d5356fbfb6baadf5d3eff8e40d08fe9d0f72793fe7e9510e73cf6cfd71ebff00d7cf7efcd51d3e7f3ad91813f7467f11fa0e7f3f7abe063a91c8e87fc9fa83c1ec39e9d8e0e351afe57f83dbafc96bafde784ea73538f94547e6ac9efe48a93f55fa1f5fff0057f5a895720f1f43fe7f5fd39153cc32011ffebf4ff3d4f4e69a838c707a81d477c03c6739fcbd7b67752b47ceff00959ebf97a1e7ca17a8f5d2df82d2ff007efdfe48c5d493745f50471ec09eddcf18c75c7a5795ea4bb6638eedc7e19fc7be7d3d315ebf7a9ba37e3a73df9e39e9effaf15e65ab5a9f318e0e3248c03c7d7f33d7db8e95ec60277566edd6cfd12fc9fe1eacf9bcce9b552e95d349f4f2ff002d7f05bb2ce98c7c95e7006475c638f5e3b8ad0697838e07af3e8073dcfe3faf4aa3a7c4eb08c83f97438c7b647e79ee3bd4b2e403ed91d71fe7a7b7f874bb4a6ffe1fa757f99c716e308dd6b6b6bd3b5fcadf96862df4cab329380090bebc7f9fe5d86705cf842c7c416b89604904a3e70cbbb3ed8391e99fc2b335462ae9d33bbb7420e41f4e3a76ff0011eb1e178b759c580305578f73c83eddba573662f930d7db9af1eda36ba6cf4f95ada68cf778693ab98a86ca294ef6d6fa6d6d9bfc753c6e0f85ba6699725e0b58e33b8b7caaa3049ce7a75ffeb76aece1d222b6836a8e14638e3a018fd3a7507838f4f41d55305881db9e3a0e39cf5e9fe715e7f7d7b2a17503201ce3a1e47b019e873f403d6be27d9c536d2d6ede96d5defd95fb7f573f5e5394924dbd125abec95bfcd7dfd4f3ff00145be2d6e0c780de5b63d890467d7f0cff003c57e387ed5ffb3edefc4879e6b989a68a169ae3254900aab05ce738033bba1c91debf66f547f3d1c3e70dd4f7c1fc31dcf4fafd7c6fc5fe1fb3bed36fadbca5324d1bae71ce0ab0201233e87fc73452c4cb0b515482b4b5d53d96cd6be4deb75f2e9bba31ad4f925b592b6eafa3efdd2d7cba9fc956b3a7d9780355bf436f1fdb345d44dbaac80285862c2af500e0ae1b20e39f415f52f863f6ce3a3fc3db8d12c228edef2da22a92acc37125476da3e5ce38046381ce0550fdbdfe07de787e5d63c43a54325bb48cf2cad182ab215dc72e00009e9fae0d7e5a784340f115e44e9772490a3310cce588319e41ec4e476f5c8c0eff00a6e4b47139d61a9d4c3cb969a9a526d68a4947997556b6bafcecd9f238ca54b0951c2bc79e5c8da5777b37a3efa5969d3adee8fd50f047edb2f1e9da9dc6bb7c2268d64525e424bb6303664824e0720f2073c75af93fe2cfedeba4cb35e5bdb5f8690970155c923ef63807031cf518c0ee4f3f2578f2c868fa6de88e4902430cace77900be0f5c632783fd71cd7e646b5adc97be21bf40e488d9f24b9206e638e483c800f5c601ee457d4d7cb2960e10529a9549b49dbabb6ebf5d2de97d7cda1155272e58da31b689dd25a3dfd6de7bf53ed9f88bfb63cd7a6e161b891839619dc7032304e781c8ce3d7af5af91354fda22f2eef669cccff39cf50dea472707a1af21f12f97e5b7209c13fcbd067a9c73dbf107ca641873e993fccff9fd3a62be671987846ab8eebaf7dd3d7a6da77f33d6a718a5aabebbe97d92ede4ff00e1eedffa6b7c51ba1e226bfb981da1bc79da1036b873b904658ac71b61460600508bb892a48c57cbb7fe21beb393c8bb9658bc8c5944d34d244267198da5659221bfcb2238d9b728f3005518073d0f893e2c5ce95e2ad6348bdd324b232df5d3dbcf3c5249208b73245e42e5085799930cf2125e2791339527c63c57e36bc92e6d7edfa55ecd6535ddc4493dadb2cb3c6434f224d2289c8f2c00a15b0a11b19243e17c492f34d69ff92a5d3afcae4455da6acaf6df44f6d3f1d56df23d2b50f105909acb4e975037082da00e04892a09e5927908dc65528c55f76d58d76a0cef20961c56a1730f9735c45a964bba1671b76c32958ddf7edf3558b49708e8c548011b6e3e663e3d3f88ee92dee351d1ee5af2189a6b94b39206b7997cb9658122554799900842226e2c14a00530432d74f1925dda2c6aae6eed418da31790199cc6b003985a3464dc213c9450766d20808c73924d37f95b5db776d16cef7d8d34d9e96b2bab2bf5baf47af6efe5e8975a94b75e5ccb2c3751799732490bc817e6690c49cec565678625c2b22a870dfbc6045606a10493e9c2dae21b76055079be6a5c85370d1ec54313a9460b095cb3ee3181b9705c0f3b9fc79a649322bb3db4a208e38d6768364ccfbe30b9956327f792a95fdfb261430c1240d1d435b99823e54a2c0d33471dc64e2ded5f63a8064881dd760a82f1265797504a9856934e3a3d2fdd6de8969e56ef6be9151b49a95edd2dd9dadd95d68fcb7ec6d69b15c24930b76629044b1a344e8ce2499e0f3098e7e4b24572e9b5142aa7cc8c48761d4d819a3599a731db85d3ae9c43756f7116e7bc3e4c3f3bbb24a50beeca28601b6b2a8018f9b69be2931c73062d1e2448c1f2e091a4784cbbc80acc229310c64b1e06ddd8c10c7d26c355827b484585c3b0ff00418a68c48ae1edd666ba6511b2488d245f633e605453fbc705bd36b3bdad6d57f5d747d7e672df4934eeb4bebd9adf7b5976eb657d4bad3be50aaa4b17957932496b7081da4b6416dc056de50084960d849243b0a6cc93ce5dd9b42b6b7514665b7749d9cc2d1a4aa9e748e59a320210d142ec8f136d5f30803cb2715ef754b131234d246669a38c01e625a5c46d7b72cf3796d0ca5095c6ec332abab00fc015b9a3eab6b24ada6bcb0ddda858ed6586645f35018672d309a3da9b51ae14964cab072c082467d1a30bd3519f5df7f54fd6fd7fc8f27115392a29c1eb17b7471ea9fcadf9ea3fe1ceb691f88611772ac91de14d3a6472d6f3896fdf65cc71b1611308a39424af1aecda8c540f9997eb7f829ae196ee5d3de45328516d3dbb3c877c91196da6501576315789be41e4ed0401226dc57cdfa67862d26d434dd44315866d4e79a58a649250ad15ccf243224c8c65390b1edf358ac788b1bc285347e0efc4c6b6f1e6b3672857b6d37c41acdba4920625da3d52589042228d1a40cd232861bda458d982b101ebae31b45c7e76fb96fd6f6bf748f22bb5566ea24efa376be9aad15f77bf4b7c8e23fe0a171de7ecdff1a7f678fdbaf42b4964d17e1b7894fc30f8d696f6c5e5bff831f12ae60d3b52b9bd8e36f36e078535c9acb58b32ebb2de6b8b89d94a44d1d7ea0cdfd9faee836bac687226b9a66a7616fa9e977b6d2c73da5ee977f02dc5b5ddbdc143e7db4f6722cf6ce9f3c80232964c67cc7f693f879e1ef8d3f04bc67e01f104115c68be34f0b6a9a2dd2bc4930c5dda4a9134b18f330119637b6994a912c71963e62007f3b3fe08e9fb46eb3e29f86de3cfd8efe265f16f8bbfb266bd37836de7d46467bbf11fc2869e4b4f09eb2a9bfed372da2ba36893b2ed48ad068711958492e39e5eec95d3b5576beaad520969d37828d95efee4bbabda5edb0dccb59e12566968dd0ad2bc5a4ffe7d5572bb7a5aa456cae7ee0fecd1aa3e9de2ebfd12eae418f5db39bc9b7255b6df69ee6ed18327c88a61374b1e49966170818ee455afb53c57e2bf0e780fc2fe21f1bf8bb54b5d0fc2be0dd0756f14f89359be7f2ecf4bd0b41b0b8d4f53d42e5f9c4369676b3dc498058ac7b554b1507f3fbc3213c37e26d335580cf3c963a85addc4cabe4c6cb6ccc645548c8326fb76684a0091189109c9e4fe68ff00c1c45fb674de05f80df0eff65cf875ad2dbf887f68a31f8b3c797b6929f36cfe12786efe16b2d2e4f2de39614f1cf8aadd611246db27d2fc27afe99709241a8b29f271742557134924d467a4e4beca8a4ddefa29722f756a9d92bea91ebe55552a1385d73c669c55ed7e76add5e9cc9b9db68bbd9deef8ef817fb615f7ed03f1ebc5bf1ae67b8b38fc47e38b4d5bc3b633c8c5f49f0b69f776fa7f877479423b44ed69a1d959c57862da93ddb5d4c0912935fa39e25d2adfc23e3f8bc511acf159f8ae08ac3590935c2dbf9ebbade3431990c0998e45314b122cb334700b892411c453f90bfd9d7e3f6bbf07b50f86f633f87e3d5f4cf18b4f159ea56baa259470dc5a5e1852dae61b9b5708d76558c0cd72202d91713c1027da60fec62d644f1afc36f0f6a73dbcb6b36ade18d23595b39859cf7163a85c6976d7335bc8f05ddedb3dc5bdcbb2bfd9ae6eadbcd0e61b89c2890fbef93969722e54a9c694a16694528c5c12bf449a6acda56b36ddcf1f174e74eb54949f329ce552326d3e66a6e337a49a4f9959ad1f93bdcce6be4d3afa38edd9e50ee0b3ae095739685a35dab202c70acee00218166ce557f4bbe04eb875af0058abb86974eb9b9b071dc0cadc8192436c0d70f1c79e3cb4000c015f942fabb3471acd8f345b471dc5d81b015b702162aa46ff0031254702254540c5b0db4823ed0fd927c6aedaaeb3e149e579adef6d05f584ee41596e2c702448cf405eda499dc0cffc7b8ce06557c7ce70eeae06a34af2a2d554f4da3653f4b42527dda5d353d1e1dc57b0cce9464ed0c446541db6bc9274d6bbdea46296b7bb3ef42703b01c13803d338237739fd71dc534123073ea074207b72dfcfa719a38f63e98c0078efce7f95264739033f4047e03e9efcd7c49fa506381d0707b8e79f73d3df1f81a53f99c91c81c91d3be7fa1e320f7427d97a7a0e727bf3c71f5fd69c4af3803f4ec73d73dc7a0c9fc3800404f038c0ebd3ae4f2092393f5f5c52727bf1f500f6e7af53dc9ce39a5c8e3819efc281d0fafbe39ff001c1011fdd18c9ee33faf5ebc0c0e839340067924103d300741c71920638eddfa0a69ebf80ec076ebc7ad3b23d074ee073ebc64007f3273c714d24649e83f971cd0072fab9daad9f4cfb74cff0051fe7af8d6bb380ee01e99faf27d89fe5fe23d57c45388a3939c7519efc67f975fa7a57836af7be64cc036793c0c773dc7d3bfb60e6bd4a5f02f97e48f3a6d6baeef4ded6bf5b6cb5fc4c0b994b1620fae0fd39ebc1f41f4cfad548653e60c8efd38e80f5fa77e7a9e9c55d650e09c7b1f7e074e4fe5dfdfbd2f2cac80f41d47ebc76047d40c0cf4ad0c9a6acdadf4b5bb5bf3f2fd5a26f115e0834a71bb6911377ebc7a7f2f7fc33f9dbe3c95effc4b2119611b31ef8ea7fc783fd793f6a78ef5816f6332160a5626fe4471cf71eb5f115ec9f6dd5aee639237b004f43824e73f53cf19e783d68926e3eaff002ff877d3d4c71124dc127f86fb5bcf47d37e965b9e7fac193ce210b00a9b71c852073d4e7919fcf1dabc2fe223dc45a35fba48e9fba738eb9ea0609fff005fa0f4fa56eac448cee7d4f18f5cf53d3807d09ee7bd78a7c56b58e1d06e1703e68e4edc9f94e33eb83d3a7af1d473fb0a3524dce9c2765bca09b76b3e577e9a25ad9eba0957ab4d250a928a6d7c326b7f4f4e9f8d91f97df0dfc4b7b0fc6e8ed26b86d86e010a58f5120c1e7b739e07af43cd7f4cbf04f52dfa2e91286e4dbc273ff005e87f31f857f2b9f6d1a37c71d26e33b04f75b0f6e7783edf862bfa5cfd9f357175e11d2240f922da06ea01e11738e79fd38273e836c242946338c22a318ceed24924ddaed592d55d6b6fcb5c33773954a32937273a36bb7d979fa6d7eaf7ddfea4f85aec4d671f20e514f3feee4019f51ebdfa575d9ce4138e83dfe98cf63f8f63df1e39f0ff005549628e3dd921578ce78edf9f3ffd715ec685719e3d41ef823b7ff5bffd7a576e324edab4aedaea92bae9be87894e37728b76b3d13e89d9f5dba7fc12290315c819e7d4fbfa73fcb3f4a814311d3f21fe1d3f2ec7d6adb119e00c76e3dbfcfd3d339a8f72faf7ff003d3fc9a88caf1d575f3fc3fafc95b1ab0519a6a4ef6d569f2bff005e6ac519558ab71d4607d7f0cfe19e48ae5750b16627e5e3a8cfe6327f31dbf015d84ae1ba01f5ebf97bff009e7b5294a1183d4647be3ffd7f875cd7761ea4e9b5656ff2d37fbadf3f91e4e2e842a68dabaeaf65b7e7fd6e7376d68cb080471ea07603a8ff003d3b718aa33dbb0ddf2f1cf6e3f0c0fae4fa74f4aebe240ca474ebdbfce39c1e467b8acfb9b7d848c707dba73db3ce0e3fa67a57542bbe777ddb4faf75f7dd3b74f33cdab85e4845ad5595f4d34b5aff00d6bd4f2dd56d646923c03cc8bea7bf5cf7c77f6efcd7b2f8621d9690838042af5e99033d33f4c0e9fd3cfb5188096338fe307b6383ff00eb23d467a835e9ba1102de3ebf747e380077edd7031f96388cd26e58686d66dfe0bd6ffd6bd0f6384e0a398d57bbe58a5d7aff005b7a6d613531bb70c6739e2b87bad3fcd663c71f91f7ff003d01cfd7babd2371391d7f11dfd4f3f97702b21a356c9e3ffac7be08f5c671dff3af98b296eacfee7b5ff3bf4d6de47e9c9b5b6df81e697ba4bfcdf26723838fd3f0fcfd05705ab692486050e483c8f4fc3271cf1d7f2af7a9a0539e067d80fc3e9cf6c13f857357da7472924a8ce7078fafa139e833fe7194e8a974ddeebf36bf3b79ea7453aad7f97ddb7974b5fee3f1e7f6e3f0145aa78235366b5572b6f2393e58276846c64e3ffaf8ea38cd7e045e6891e9c6448e255183d170003f8673db3d7db3c57f551fb5d786e09fc01acb79632b61707a7a44c7dff53d4f1ea7f9a3f10e9a9e7ce02f037f6c138603df27af19e381cf38fd83801a865b5a924bddaf1d3fc51576be6bccf92e209b78c849b7ef5257defa357f2b6aff00c8fcfafda0e61a3784f54ba90ec668a523248fbaad8f407b81c9fc0f5fc72d2f566bbbbd46f8b9267ba94a9273f22b1551c67d38e4e09c7435facdfb6c5fff0067f81ef638ced2d04800048e0673ebc6076f51c8afc65f0e5e62c9497e5b731ff8131c9c7bfbe6bd2e20aee38bc352bd92a72a8b5fb4da8abf65baf9bb7533cbe1cd4aa4adaf3463ddb5bbfbdfddbadb5e935dbc2e8416cf6f7e718cf5c7b73c7515c4b4873904f3c9f97bfbe01fff00562b6f52b98e40391907ae463ebce7d07ae735cf16407efe3d718ebf91cff8f1dabe5abd4729735ef76badfa2fcfafddd8f412b5946fa5ba7f5bbebdfee3fd207c61a5c1ac3dc4dabd9582a3ac6a92dc0532166fb22799148ca652f186643e5b440b292a33f7bc0fc6be08b936491787b5536ea1e67305cdb4f245262492329f6b3b2e2dd1d932eec9706524a70b8c7c61a6ff00c144352d5efee0597c32d735fb3922f39b57b6bed3e481499638ca06b8754794b188456c91b39523ca76fe2f4ad1ff006b6d03c5e860f11681e28f01cd1493477316ada63cab1bf99f7a3bbb17bc8123575955499621b5941d85982fcf43154a6fdc93715f1370972eb6ead2b6dbdd3f914f075e3f1456b7d14e2dbb5b68deed276be96f99caeb3a4eaba2c939bbd3b50f0fdcac334c9796d0fdb3489ee1ae04b183796aecbcedc2a4b0c321f3f940eab8e6a7f10c6d36db95f2b522ccb1dcf98156e3ca330c93223fcac9296390bb576f4c647d3565e28f09f89e269748d7db520ed6418da5c47b4b24fb9964936796599558b23b2b2e4315cb0032756f865e0dd7c98f53d2e158d637696f2caea5b1bc790db2959166d36e0199c3333969a1746603747d777445c5d9a69dedb6bd9eb6d6d677b74baee633a6d692524f4b3b34d59c764fd7fc925a9f23f896fb4fbc9e3b59ece48a616514d2df5b3c7214ddb16300e049b84c6595599d1770f94e30a30ed7c7faaf876ee1b4bdbd1a8699245e5c73ce9b648c11a7348649080ae5630a640ae1782a14127181e38d27c35a6f8d27d1f49d7b5ed2a4086dc4faec5697566ea9b8c710bdb28ecef60520a846923bb20213b4062160bfd2eee3f245ec77173a7a173f6eb597ed7a7a47712db4dbfed0a5bc94115b387132c6c06edaa1460ed2c3a5cbd1b4a5aadef669a6fbdfabd6fdce486213bc5b528a6e2f5bea9a4d6b6d7fe1eed247d21a0f8834cf10431dcda5c2a2e19e048b6dc43e6492411e0ab1738c4f293167680cd87c0217b0b8d46e6d6c6448d20db6e97f319e3006d02d63d985656549375d6d7076c67eee43022bf3da1b8d5bc23a95a6afe1bbc778e68a737365135c4b188d24b7776db013147246dc31d814e1589c85afa83c1bf14f46f165a8b7be1226a0a12d65b79638673e64a426e8c22a9de042b9f2e1258c80b740188a4da8cb4b3d25b2b5d76f55adbf1d54d58b8de749b9c5ef1b6ab6d3cfb7c92d1e87b0dde9f61e285d2ed92e67b1d5ad82b125956698c767146abf248eac2276747daecdbe41fbb2c30be77ff097eb1e05d4ff00b2bc431969ae2432db5d10aa92b48565811863cb7917f708368fb4b2c22472a33bbd31ca5b6a9a65f5b885becd2dc484ab147dab219a17fdfa798199884650a792bc8039ddf895e07d3be2978253c49a7c6cbace83622eded032adc09ec11d83c1705b748af1e06c9b29867223dc140f5a9c5ca16fb7057525a732b2b7cdebafe87815671a7513d5d1a8fde8bd5c64da56df45d96dd2d7dbd53e1bebf15d699a61266bbf2e28772440bdc46b33330690334ecc8d12c8d236e91540cb488ccd9f8d7e03f8ee593c4badddb5dcf2477fe26d56e16017712850ba9dc5db8d92a6cd8ecf2fde5e636da36b6d6ad0f84de3f974bd275f9ae1cc6fa3e93ab5cdedb4a5619211676334aacd0b1caac30ac9287f30aa3972f105528bf327ecb5e267bb6d2bce9274f32f618de4501da77f345ddc89595278dfcb2b2a9cbab15e492c49677d61fdedfa3d1256eba372d34b7a6e651a3ef57eaad0e5edaf3376f54aeedbef7d4fe90fc31aa2eade14b6678c48218a559500289b99ca08e58c48f0344fb1943fcc1df80d0b643ff2e3fb6578bb55ff00827a7fc1467e17fed81e13b7997c15e2dbe5d0be2b68f6a5c5b6ade14d41a0d3fc4767800319a0b60bac69cf22c8f6fa869f6d2a29daa5ff00a45f825e20b6d4b4110397567647f913ca706737170f14abbf616097087ee444900138fbdf91ff00f05a6f82d0f8efe02eabaa456514ba868133ea104980d36d432872180f31cb452991e362c0901948d849cabd394a9d58c6ea6ad529bb3bf3c1f325b3e978bd9d9b5b333cbea4696329c2a2bd2aadd0aab752855b45f75a36a4b4d1addd8fe85fc33acf873c65e13f0ef8cbc31a8dbeade1df13693a56bfa06a36126fb3bed2b56b45d42ceee1951da4945d5a4d1bb348d18dcdb5406240fe3abfe0ac1f0d75af097ed6df1024d42ef54d474ad525d3bc4de143a9df5e5f4763a1f89639356b9d374b5bc9241a669563e27baf1125be9d6de5da5a9595a38155d845fa03ff0006e97eda13fc4df821e2bfd953c71aabde78dff67e916fbc1897526fbad5be186b77c44109f35f7dc0f0a6b72cd60eb17efbec1a9e9b1a0091127d43fe0b61f0946b10fc2cf89f6b68bcdb6b3e00d4e558f091c9117f12f87212c1465a5493c4ce81b2e4ac6aaa18bac98e1e6ab463257b548ffe4f169dacbf95a71d7f0d8eb50782c6ba337a466e09dadcd09db927dbde566f7b36d2f2fc34f8456f65e2ed3b41f0bebb1bdb8d0af1eff4dbf802b4b12cd2c13bc45646f2d93cc8fcf85818cc53191b7aef9644fecd3f671d6d7c49f04fe1f5cc92092fb4dd1e0d2e59cffcb76b18842d2c8ac77664865182edb540de41cee1fc83fecf5e1f6d5355b6800db3d8cb1485802088ccaa1d801b7f7676991029c24b18036a989a0feab7f643bf36de0cbaf0ab1045ae91a4ead6f1b630a6e44b67222962a3734f64e7e408cb22852496561d925fb88c9eea5be89b49595f449d95b5ede68c31d38fb6514f45ccd2de2a52e57269ff0079a4fcddf43b0f1be94fa2f881fcb86de38aec196042c5207990f98d6c41da183bbdcb380492c801f97695f50f82be244f0e78d3c3ba834862b682fe0740aa16478272d1de43e596cac72412ca19dc82ae8c88304038fe3db61ab6923518e3f327d36e05e9391e798e394adc62376f2e5708920dbb13e49c3aef2016e37c39298afe0960292333c7287f30ec12120a9dd97fddbae0a9c950cae8a88e1aa2515569ca1257528ca125dd356dfcd3b37e6f63cf84e54aac2a41da54e71a907fde8b528be9b348fdc550c4641e08041c9c1c8ce41e87ebd3df1460919cff003e3f4c0c673f4e4715cb7827546d6bc1fe1ad51ce64bcd1b4f926e491f685b78e3b800939204e920e791f524d75278f5f4ce08c8c0e3afe9e95f9b4e0e139c1ef094a0fd62da7f8a3f61a55155a54eac7e1a94e1517a4e2a4bf061838ef8cfd739fa707d33f414bb48c8c8eb82338ce7d8e3ff00aff85271c103ebd7f1e73e9c9f418f7a0f27a9393cf1f9753cfb64e6a4d0002324741904e78ffebfff00aa976b7ae075e4f4c7f2ebfce98d22a025c8038196380493819e7af3c75e831eb4e078e338cf38ce31efce39ff00f51a3fafebef42babdaeaeb56afaa4f676039f5c8c9efc6719effe4f6eb4c638049ec3bf4a713c9c646707bfebfcc75aad7527970b1f623ffd5fe7fc438abb4b7bb4824ed16fb23cbbc697be5c6f820641ce3fcfaf07f0af9fae6e8bcec739f9b3d79fcbfc0fe35ea9e3cbccef5ddd73d0fa75181ebcfe59192315e177170564ce48f9b8e9ebe80f1dff00fd55eb42368c52edf37b76f5fc0f2a4fdeef6b7fc37de75304808eb9ff003d7e9cfa9e8296e5d6389e46206d539e40fa1e7d0d73b6d7a3b90002319c7ebdfa63d318e07618fe24f10476b6922ac8030524f201e9fe38ebcd32eeb95c9eb6dfd6cbf5b6da1e41f12f5a0166883e09cf391f41f5f61ed9ed5f3fdb42364b2e3aee273ea4e73f5efd49e9ef9ddf176b526a5a83229dcbb8eec13d01f5ed8edd3afbd6646bfb8c60e4febd338c6738e9ff00ebc04ef7d2d6b7f5d7afcacfee3cf9ca339b7b24d5bf0eba79ddab25d3a99af10da4f73f4fc31dbb75233df9af9ffe31ee3a64f18e823618e9d883f9e79fcbdabe93f20b01803907e809ea78fcb938e38af19f897a135f594e3693947fc383cf1fa7e27a669d38f3293bdb4bb6fa6d6bea9befd3b9129a52a7db756ff2d74bdb4d1f99f847f14e77d23c7da5ea5183ba0d4a26c8c8c0328cfe9d39f619afe85bf64af158d4fc15a29de3e6b2808193900c4a71cf4c6781cf5f4afc2bfda1bc20d65a87dab691e5dd2bf4e47ef012738e3f4ed8ef5fab7fb116b5e6785b4684be76db42a467fba00e00f5201e9f8f53586124e15abc1bbf35a5a5ecaff3ecbf0b6c75e6915530d87aabecbe5d37d52dfbf5f91fb3df0e75d31ddac2cff759530589ee48383ebf4ea307a57d5b677625811fef7ca3f971cf4fcebf3f7c2da84b6faca247b984810f1c818e73c76fd7a0c57d97e1ed45ded22f314e4a29c1ea4e3d3af07a7d4fbd7a72a5ed175e8ef6dd5927a3eba6ddd7ddf2b88a8a83e6e6b37af77a77ebdfaf63ba79cb64120671d339f6e7fcff002a8cb0c06278e3af6f6fc3d2b1cdd82d8c1c75ebd3f4ec3ffae7ad3cdc0c70bc8cf7f5f5f6f61efeb9a6a8dad64d5fcb7ff2d3bf9f99e44b16a4db726fe76edfd5efdd7527b8ba099c67e9d3f3fe7cf3f4eb5926fb32603753d39c74e9d7393fe3cfad6bebb08ac704f7c01eb9e3ff00addfbe7a572ff6d91e5c2ab75f7c71db8e0fe3f4c7271dd470e9c6ed5b4dfe695edb6bdf5ebe47955b17272d1e89f4f9744feee8cf4ab4937a9faf3d47231cfd08c7f2f4cc9728193e991efcff009ff39aa1a46f68159876ea7ae71d3ffac49feb5aae32ac3d47d3f5ff003f43d2b86a251aad277b3fbba77e8bbfccf4e8a954a11e657bc7fe1b6f3dbcac715a84796538fe253d3a9cf1f5ea7bf51d85773a21c5bc63d1476f5e3a678ff3c76ae4b528dd48c0380d9efcf39c7e471cfe1c574da2b110a6eed8febd7b7e3fcf3558ebcf0b0ee9bb24fbfaf6d7af747770dfeef329c5a6ae92d7d6ff00d79f41fa8928ec41e87dc71df8f7e38e077ac7f3fa83c76cf7e3afa76e78cf5eb5b77ea5dcfae474e39fc7e9cfbf4ac392020647d33ffd7e9cf4c74fa57cd3bdeeb7b2bdba6da7df656f95b43f4a5d9f5f9f6e8b5fc766edaeec91b7676f519ffebf1c9f4c7e19ace972a73d41ff00eb7b63db9f51d79aba2320f19e9fe1e87dfd78e339aab3ab018c67838c773fa7f9cf0475b8ceed27f7f7fc342947aad75ed6baf9e8fa3e9d75bdadf247ed58f18f87fad1207fc83ee3839c63ca6ce7fc0e3a76ce2bf988f15b245f6d9ba00d2b29ed8dcc464fb823824f19cfa57f4d1fb5846f2780b595527e6d3ee063bf309f4e9d0e79e0671cd7f315f129fec9a75e0ce0812838ebc12304838edfcf919afd678134c1e25decdd587cac9eb6f47aff00c31f279f3ff6ba2acff86bd756affe577e7a1f931fb65dc36b1e1fbc853255565427d0e1bd3d7823be3be0e6bf1b3459644f36d48c34323a1ec7e46618edd78fccf7afd8cf8eaeba958ea96cc0393e615c9046790719079e791ebf5cd7e486a1a63697e22bb565d91cf23ba023187c9c83e99183c639cfbd74f11c64f1342b2bdace9b5e574d77bbd2cdfcf7b1ae5d24a9ce3b3e6ba6ff00c2ac9dbb79795b57ac3730c847248f5fc39e47e3d3f0ef59c6193b81cf3d6ba2bb53b064751fa0073900ff00f5c9f7ac57660d8c9e001cf5fd79e460d7ce5456b3ebebdd5d1d8e52d12f4ea9dba2d3f0fd4fee63f63efd96b4bd33e14e8de30f14d87da5a7b74bbd174e95411e52db932dfc9e62210d3dd21107ce4bdac6b2aedf3ca8daf18fc1d8a3bd6ba1a3dac0d3ceb7313333bbac31c8aaacc3ecd24617cc791b633363084007711fa07af2a691613f853c3f68b6fa3e8e90e91a62a22a0874fd2f40d2eced52055dc84797165cc60fef1a460a4162de5fe20d2acee23b77b926eee1ac6356085d8427ca93cd5326e5890b3a85fbb9518dc58824f934a30a54a14eda457bcf4d656d65db577defa59791152acaa54954bb6dbd16ba46eac976d95f6bbdb73f3475cf0758db6a9765235b16642a6e34e1776f751bb4bb996496d65b51fbb521df28e581e718c9d0d323f1b69905949a66a63c4da7aacae6df54548ef0ac4d243b5251712ac836602ab5bac8aec37b7391ec9e39f8696bab43ac4ba6b4b677525d794db589dcac23528ee822c64dcb217464664c9c6156bc757c2de23f08de449716378f676c88cb2c0259810b279db0aa393ba48ed96495b636f668d54aa120a9518caf28ab3be8968d6db256db45ab69b5ae85aaf24b964eeacbdd92bae8faecd7dfead9e5df102cbc3ff106268756865d0b582ca915da5be6e20bac4a03becfddcb6cbbf8749936aa7c9246490df266a3a9fc52f819ab25f6b305deb5e079e5732ebba4492dfe9b0d9f9a233fdaab12c92d84d208d22ff8985b347992448e665dc5ff004564f0c68be2ab2ba4689e1bbfecf13b1df3078e6324eca0a19c7953107710886324b17dc01c71177e01f1169b611cf0c777a8b1122180c6f3acf14bbb2af03c4f1bc4c63dcc9b7390720a122ae156a534a9d4bce9ab24dabca1b2dfaae8d3dbd0e5a9429546ea524a9cdeb3836fd9cb6bb4fecbbf5b6fb27a1f0cf897f6aff8490ea5613eb3e14d52c12ee27f33c4da0c70456f0c7391b4bd9a486d6ea3f357cfb890c30ca8a91956dcd83d76976fa478c21b4f1a7c29f15e97e2582da58e67b9d2e741acdb9171ba35d67425759d40054b6c59724ab305242b7a6fc40fd967c27f1034e074df0ec5e13d4a4b479c599b47b7d12e2e92745915ad9959b4d6998c615ad631019092f061cbafe78ea1fb35fc45f85be266d7fe1e6b5aaf823c73a5df5c2a2dbc4e96d7ab1c6ccb14d1ab98350b19d5963528b2c722a931b21c81dca10a9153a49d48b4b9a29a538bd13d24da7d5a4ed7b3b347046aca94dd3ad2f6352f68f3373a338bb24ddb54acecda6f96f7e5699faaff000cfe2c2f886eadfc33e294b38b5488ac492b24296f78dbd444d1c804324326f251a19f74cb2ef5284a647dc7e138a1d0754360f235bdaddc11c334376ad24263b9b7bbdcbe76362c72b92aa4928cfb1635662847e1cf803f68df0eeadaed8e89f1cf4693e177c4db696d20fed7b6b768fc2de2c6b66711de4170d11fb26af2caa81e168819a38f01a460266fd78f05fc41d37c51e1c835ab0d422bc9ed2c059c84ab012c91c71a3346a0b8c32b92d2c52938dfc12e02f6e11b6a506f9942cd49a6a4af6f76717692716b46f74d6af77e46674f584d4791cefcf14f9a9b92b35529c92b4a12574adef26acd2d51f03fed2fe2c83e1359fc68b6864459b52d2eeb44d2216923506e7c48174e5313920a1b78af27953ca8d9c241cc5246c644f39fd95984569a4cf736923cb2c2b1c332440a4b35c12250d3dbba8121837046244aa003b42826be7aff008280f8c6eb5afda1e2f02da029a4dae99e1ef105e5bc93e16e7549ecdedacc0f2ce7cab1413b2ee693cc6ba019774286bddbf67c11e98967896e6d5e0b78da19a30242b3b84f2c019476766d8c0065721b0ec50d65397357718e91a6f96f7b7bcda6df9d9593ddbd6dbdce8a34dc706a6fe2ab14dfa24a2ba6ef56f5d6f6f27fbaff0004f5b888b410492424ccf127993bbfeee2dd6c0627459a25b88a045624cc818e046a58eeabfb68f87adfc5df063c63a75cdb8b9173a5dfc0013bdca347232488e36ef58d5b7e19d1c05655e4f96be77f07afdfed3a6dc4334779fba48824dba2b8da66ce7cc7759127df68ca10c8ee1658db2c8014fa07e304106bbf0eb5db1918ce12d2f246866dbe7b6e88b089b2a5e558d8604b19f3422049014552dd692728eed3495eebadbabd7d3baebb33c2a9ee54bf5528bd375aafcb7fc363f862fd967f687d6ff60ffdbabc0ff176cdae0787f49f15ff0060f8ef4eb69046babf80b5dbc4b0f11da3aee10bcb15948d7b6a5c622bdb68665d9222b0feea7f6ecf0f691f1d7f634f1378a7c233c3aee94ba3f857e2bf84752b55674bbd2d0c374f7968aa3764f84f55d567ced660582c926f3cff00049fb6bf8266d13e27ebf2985a3126a574640c30431998904e32d81c094aa16e92a2c9bd47f4e9ff00040afdb2ad3f689fd9efc5dfb0dfc51be4bbf16fc34f0dea90f83fedd3a35c7893e116bf19d36ef4e885c3f997177e12bebf920f22369a5934bd46dbcb36f058b91e1616afd5f17570f2dbdab9c3b5ee9ca36d7e24bd2e9d95debf5399d0588c261f1f495ea5384555495ef06d59e9bf24dbbf949b76513e43fd9d6cbecfe26b9468904b9994328cac8e1e39a246e01d8c51d99805da4b0da629485fdfcfd993c4dfd9de2af03db3480c3e20f0deb7a3ba2b41b99b4d9ed2f2df058b4a1c25f4cd12c65949cb329dbc7e2af81fc197de0bf8ade27f0aea71b43a8f87f5bd5f47d4e368ca31bbd2b50b8b3b9232a1848f736eeec5712318269901779fccfd23f02f8a4785f58f84da95c32c71b78ebfb2b8504a1d4b48bc32fcf824a16b60791c045cafca40fa2a91fdd28f46eeaeae9dd6fdb5dfe56f4f9ac4c9cab269e9ca9e8b77ca9a5df7d3e47eae6a9e6413ea496ce25613190c6ebe5ee824464650416c37951ac658ac8031055416de7c9b43bbb7b4d5244f362cc17c6da50637db02b14b98add1434a37a5bc91b290ea37abec0a2558c7a96accb2ccb7aac1edef6c6391648c6e954c4ace03aa16491bc8b77cc89b6572d8741824f8da5cdb5af8cc219f7c7a9c6c36bacb198ee2de5fb4f9e1245250cf134a5f12480fd99360425cb7347e1ef7f3b76d1f977f2e8f639ffafebfaea7ec8fc11be379f0e3451bb22d9efad873bbe517724ea33d301661b4e17e5db80062bd672739efebf4af9e7f674d5ad27f095c68eb205b8b2bc7ba4888da5ada786de20f1a1c1d8258496feef9d1ee259cd7d0b5f9fe63074f1b898b56bd594d79a9be74f65bf374d0fd5b27a8aae59829269db0f083b3bd9d35c8d3ecd72dac3589038fff0057e955de49074381dba0cfd6ad557917b0f623fcfb735cd06b6b2bee9ff5fa1d951495ed27aeaade5adbf23135413cd6cfb5db7211205048c85e71db9c7b7e84e7474dbd171688e5b90bb5803dc0e840efc11dfa1e691d3767a73c1f718c7bfe959164c6c6fdeddbfd54e77a670064f6f41f977ebd056cd29c396dac758dba6893b69d7b79fcd7139ba756152f64ff00773beaacdab37d347d6c685d6b30db6edd818cf5e3a1c7ebf4fe7c70bac78c6158dc2b8e013d7db1ea3e80e7d73e95d8eb7650cd116280e41e78079ea077edf87a715e27ae6841cb988b2820f00e476e7e98e9cfa1ce4d55350b29463ad95fbf4eefe7a7e6753bc9b8ca6df64b45d15ffe1aff008e9e73e2bf142dccae090c3278e7823f4ff1e7a8ebe6579ae5b460ef3ed9e3fa907a761f8706baed77c3f3aef7524f5c75e7ff00afd33cd784789ac7518cc81011d7919c641e38ce39ff0038efbc6a3764d595ed6fbb6b37ff00076e8633a4926ffaf5d1f9fe7d5337eefc69696c1c2b80475e46001efeb81ce7df3efe41e29f1b35eef8629092d9190d9ea08cf5f5fd49fa5735a9da6a6cedbd980c9e727a1fd78fc38f7ac2fecf20969396cf24f3f87b743c81dab750e75f159ef6eb75ad93d7af5b6ff8f99571152126adeeed7b6f6b7aebe692d98b6c0c8e647196639248cf5f73d31ce7bf27b56bc8c155476c67031fe27f9e39ee793420db11da707e9fe4f4e9c0cf5ef8ab72b295c83d47a7a60f23dbbfff00aab194650d1bd3d7d3cff27df6d498ce1515d2ea9fe4b5efd34f959ec88a650c7774193f8f6ebdfdc7f4ae7fc42905c5a4c1b692c8401819239edf97b7b54b7371b031c91d7a1f73f5c75cf7edd6b93d47512e19724e38c678fa0c9e871f8e7d7a7461e09abf376d3c9dbf056b6cff00439eb4ddd7baf46adfe5b77df4fbb43e00f8f9f0f935b5baf2a1ded9247049ceec9c11d0fd064e7a1af75fd90f4ebbd1e2b0d376b2b01147d08e870719fe9f43eb5e8daa6876ba9963240ae4e73bb04f3d719ce460f73eddabbcf84de1e8344f11584b14689109d37a85e000c09fc3391d3f1e335d10c2d25514d3d64d2765d1d96f7f3ed7e9d34cab632a4b0ce9c96915cd1eb6696dff0007e5d4fd45f87de0a8e7b7b6bb78f32ec462fb727a03d7009ea40f4e8303afd036d60b6512a9001c631e807ae73d38ea31deb8ff008757f6cda65b88f6e7ca8f918cfddc707bf23a73e95e8374e1892be991db1eb8ff002338aeb9ca5192a695a2ad67bb7deff7feba687c8d794aa2954936e4deddb6d96d74bcad77d0a8a39cfa7b638f6fa9fe473d715276fcf3fe7ffad4c404fe278fafae7f13f99a95c118cf42381dba007f33ed8fd6a64f58abf557fc2de7fa7e9c297bb27fd747dd797ddd7631afc060dc75c8f623f0ebcfa64fe95574eb00f203b4649ce7078cfb0ce3ebe99c55bbc1c7a751cf1fe239f6f61d7a5dd1b6efc119e7dfbf7e7bf6e303bf615d52938516d6fa7e292febf131a308d5ad0524926d5fa2e97fc76ff8275d61681225014600eff5e49f53e9c92055f6b7f6047b0f7cf4effa0e9ce6a6b4036e0608c0fcb6f1faf704d5b6518240c11e95f3752b49d47abbb7e4edad97e5f23ef70d82a7f5785a2be15b25a6cf4d2fa2d37f91cd5cd9ab8e99f5e3041e48f7ff001c55ab4b711c6063031eff00d7f3f6e3ad5bb8c2f3c0ca9cf1fe7fce7d4d450bf07247f93c74fc7a7a76ab9559ca972b7749ebf2b5bf4eff0081382a14e8e3d5be27a68bbb5bbb2d356edfa942f4e1c93efc7d7fce7e959c5f3c123f4c73fe3e9e8327d6ae6a279383c9e958a198673ebc673d3a8fc6bcb7a49f93bfebfd7e7d4fb24aebe6afa776babf9edafaadae6d53ce3f227fc6ab4b0e7e841f6e7ffaff00500f5ebd1c927cd8c707f5faff003fd2ac165c75fcbafd31ff00eaf7f6b5696bb3bfe56dbf0dfbb295d79a4bd6fb6dbdad7b7676db46cf9afe3f7861b5af086a71042dbed265c633c796c0e3ae473dc7a11eff00ca17ed096326817fafe913655acef2ea1e411f287628719fee953f8f6c135fd8ef8e2d63bcd02fe3600e21930300f5423d3807ae3f5afe48bf6fcd34687f113c4a8a9b12e36dc0c02b92c1958e38e9b3ff00af902bf47e01c438d4c5616fa4a11a91f5834bf2933e773ea49bc356696ee127af6525fa75d363f117e29b402798c8c4ab33865193953c1cf5f5ea7a76afceaf895a5c297cf35bae36c85d1f191ce48e3db38e3181db3cd7dd5f152e659a59c072bcb75e3d47b7b679efc72723e36f1621b98e58ce249133b480391d48e79eb9e4f38afa7cdd7b4a728b4935ef27bea9dfb75d6efa9cd814e3cbbdb4efd6d676d345d7fccf0b9eee378860f230093d882015fe7df8e707bd64b6f909704e0f4ebdbe991fe7f1362e6d5a1bb732120139dbc800fa9cf7e3d727d697cc8978c31c7a0c8fd303dfbf5eb5f1f2bbd1b4b5efe96befbf7b2f95d1e9b8a7671ebd5bd3a2b7f5e7e47fa5cebfa84625bd9a358459c7ad5f5ada1c16792d6decf4fb58ddb792373c9134923a972c64755508335e61e27d56dd34fbe96465b78228255cb48117cdfb348163cab099494727e488c78e4fdd159fe28d78ead0437f33de22cb75aa5cc3a7da40f0200facdd450191dc9740b6f145e63676850372f384e66fadf54d6ecaeecac6c930f2889194994826d4c464959992dc387cab91b957e5233ceef3a5abb2efd92ded6b5b7f23952d537d1f92dadddeff007796d63cb5f54b8be1a9ae9e26cb5e81fbb42e70be5e1b73ba2119b6f2b2ca031d98dd82a7ab1e19d565824bfbd9ade1b77b7e4ceabbd808a00db708550a8666c10c9b41e846dad8d5aebc17f08fc3b77a8f89b51b55bc91a4b8110912de42b6d998a95122150c4b282480eb83821c3d7e547ed09fb7bdeea92dd683e14bab8b74526d6236ca54131a648dd15c48c49690b124820055e405aa852e696d793b689dacb4bf35fb765afe2c552b4631bb692dfa36f6d9755f7fc99f6778b359f05786cdedcb6a7124b239b6325b4c90bb2ac22050b2175180f24afb86c521189e8587290fc6df8776100497c57a7fda18bbec9a640e040810967de837492348188720fa92ad8fc78974bf8d1f15a65179e25d56ded648de644cb90d197daac5de7dc0979194e41236afca08e3acb5fd9335bbb38bef166b0e1f30c0f6cd23308d022bb3068d067cc941c03bce5810739adbd8f2e92926f7b3e5f2d3ed3dbd3a1cded5cb58a718f4f75bbedb2d17a5be7648fd54f107c6ef04dce93696d613e93a95d8c4093db5c42e544f7312c524a892c892130dbf9bbfcc2d9dc36f5cf3d15c683e314b7b3d62da29f0d24b697b6abe55ee9ef221f26687cb20b4042c6d2c2c024f1f98c4072acbf99307ecddace937d05be9de2cd405cc2048f1df335b8906c9423c32c4e03247120073ddd892bb893edbe04d4be20f826fdad35957bcb3c98099bf7ef10223dadbcb29c42b22005e6c105948201dbd187e5a1bc1ae669b69ab35a5baefbe96f9bd0f37174e789b5aaa94a2b48db95af86fdfb5bb6be87d01e3ff00d983c13f116cef344f1668f69736d7d6321b5be82df1e782b2bacd11dab71697703cb1964123c91bfcc8ac0a337cb1f0b751f1afec9bf12f48f80ff103527d6fc0be39b7bbd43e16f8aaeddde69d34e9e38b51f0d6a32c814aea7a47daed9897005d5a5dc375feb12e367ea87c34d6b48f1ef86eeb4e9dfcbbb8accdcdb9572244960288cd079b895645557ca29705148df8665afcfdff00828bf83eff004cf81327c42922926d57e0cf8abc3de3bf0dea314804f241797a7c2fa8e9de726d686dee9b5f827ba70c0349a646445e62903d19b8ca9fd62092a949395d69cd08eb38bb6eb9799aeb19a8b4ada3f2694a7ed7ea756fc9397228cbecd595a34ea46f7e59295b99ad251ba6b456fc8af1bf8fa4f8a7fb45f8ff00c5975e6ac03c477ba46910c989c0d3bc3f2ff6469fb88de14dc456a6f254003234ec15f01987e8ff00c1668e3b5d3218de36512c53be64500ec5548d59d7e543e63a801f019914283270bf911f0564b8babf3a8de08ee6e6e6ea5bcb99256f2d9ae2e67334d26e7daaccc4898a80e5848c0839da3f593e185c2a5a4135c47244f0052824495031051088fc86689f64b2178d5a3f31b66001806bcba32f68e537f6e729bd15fde775e9e5af96da1ee6221eca9c6115a461182b5becc54746f5d77bbd7aeaf6fd62f84378a67b696230b24283630d837460c68b27eef72304679d941cc817731dbf313f566b722eabe139231beddbcb925380b203235a4b942c46e4579242cadb8b7dc007555fcfaf851a9f93005ccb1cb22ad94592a2307e4876c47cc60d832b4ca5a38b1231c0c6e2bf6ae9d7f712e8efbd92e20792e94c6aff0022111be125c8213ca8e5662e8f1e4ac4aab90a4fa715a45ab5934bcdedfd7de7cd575ef5eda5f4f9dadf75bd7c8fe4cbfe0a47e047b5f1ef88ae3ece6232dc49304084a87e431490601554311e81543315021c349f9dff00b2e7c7df187ec9dfb45fc2ef8ebe0db8961d4fc0fe2282e2f6d51d923d63419dd22d6f44ba08e9e65aea7a69b981a3f300dcc8772310c3f75bfe0a3fe101a95c4fa99079f3112548d76ac4c7cb8e038e11ba10adb5808ff76cacfe51fe75bc5ba4b69f792174f2fcab9491940ca80aebe60e338e1989f9718db9c918af9dcde0e9625558e8db524eced75669aebbedf91f6792548d6c12a552d24a2e9ca2fac5fbb67ea9efdafb6e7f677f14354f08f8e3e34e9bf1a7e1f5c457be08f8e5e16f0cfc55d02f10ab171e2bd3adcea914ea8c445796be28b1d76cef6d77b3477715c092432872db9f176f62f0ff00c38f08f889e58ade1d0fe2c7842ea69a453308a0bab2d7f4eddddd77cb7d12ee381940a739da7f263fe0981f17bfe134f86975f0735bbe69b5ef84ba84be20f06473c923cd77e05f12dcc275bd221f355891a16baebaad9db42ca8969ab6b372cb1884ca7f523f697dd17ecbbe3abe85f371a5ea7f0cf5581a44dd868fe25f84f4eba7f9c3e0ad8ea13001770556de416202fd1616bac460a9d656e6508c6714f69c5a8c979746ae95d3eeecfe4f1f8596131ef0ceea2aa47924d7c74a524e12d56beebb3fef26b7d0fda3f0ef88a3d6bc19e1cd5ade417105c69b6370af0b31420c109de54b17f29adc5c7255fef163b37a29e3f5fdb2dcdaea7651452dd69d741cbc664479446e159088d885f30131e648c6f0eebf362466f30fd963c429e2bf811e01be244a1740b08ee58388cee8716b7314cdb4a906395032298ce402dcab1af59bb86284a093849cf9ae922ae4b424c2e109320042a2c99760379dcc18e54e7657766babb2b5dadf5bdff35fe5c0f476692b36beed3b2eb6d3f1d8fb1be14f8de5f09ea3e13d527948d3eeee3ec9a94cce0422c6ed228e4c1e77a43b84f1648cb451b120808bfa46086190c083ca9e4e41c60823230474e4f4e3d6bf20bc0973a6eb5e175d2256804f0bcd1db488891c717d9c5a98a290c6eeac6382f6d4965c2315919113ee8fd4ff0000dfcda9782fc3377724b5d368d6515cb1ce5ae6da25b5b87c9393ba685dc75ce73df35f2b9fd08a746ba5695e54a77ddab73c1efd3df5e7b2d11f6fc298993face164ef151857a7adf95e94ea2f2bfeeddbd5f53ada6b0c8fa7f9c7e3ea734fe31eff00e7dff98a4af9b3ec5a4d59955d739383903a7f9faff2c7be36a50131acf18fde40430c0c1c03caf1cf3cf6eb5d0b2823dfd7fc7d6aaca8082081823047b11df3c7a76e3bf4ae884f54d6ead75f9fc8e4ad4eea49f5567e6ba3f97e8645cce26b05941cee5dadea1875c9ebf9f5e4f15c2dd8572c081ea0f5ebc63f13d7b576f143b5ae2c5b84914b4478ea47cbc74ebf43d4e6b8aba4649248d861958a918f427a0ebcfbfd4fbee92574add1efd1d9fe1b5bc8ce849b5cb2d5c172befa6df27f33cf75bb542af850060e7d4fd38ebdcf6e99f5af09f1469f1fcf951c92791c1ebfe7fad7d0dac4785638edd71f5f7fc41edebdebc53c4c876bf1d8f6e9ebf4e3a73f88a6b4fd3fe0dcda7f0bf91f376b56aaacdf28ea47b7a639ec71dcf248e0f4ae16eedf21b000c13f87bf39c0c8f7f7c57a7eb9190cc49eac7b7d4ff5eb839fe7c05dff0010e3049c71ec071c76e463a75ae9a6df32b2bded7be9f8277d6cdeeff23c5c5249b4ddacec96dabebbf5bfea8f38d46e96d198b370b9ea7d3ebefdb91e83919a116b714abf24a1864f7e7d3af23a038ff1c570ff0014b599b45b5b8720a8dac55b3c743ee01e9fe7393f20e8ff001a257d61f4c131244d8eb9c0dc415e338fa8ff00f5462a5ecd45c94acedd1bd5ff00c0feae2c152755c945a5a697d34bafd74d9eaf7d525f79de5c2188b1750186739e477ec7afe27db8ebc3dedda17c06c9cf5ebf9f3fe78cfbf31a36b971abdb4643331600f427a8c9c601f5f6e9c7a57516ba35c4ec19a2919723ef2e00faf4ebcfd339ef5546a42c9a6dbb5fc9a56efe7d3434ab87929352692d2d6f97e295efadadf2b2db1690aec181d7a1e40ede871923bf18c77aedf42335bcf1c8870559481c8039cfaf1cfae3f1ce0269fa0ec0a5a3c1fa0c918fc867f1efc63a74d0e9822c6d5c10074ede9dbd3fc90315dd4aa4a564a3a5ef7b5adb6bd57cd6d76ec7056a3049ddf4bbbb5aad3a5fd7aa3ee5f83be239a7b1b78a771b82a8e0e7a018c7d3b9fd6bea14996589181c9da3be4f7ea73d7b735f9eff000a757b9b5bb16c724064da73c71edd3eb8ff001afb9745b969ad226623900fa81d3f9f5ea4638e715dee3cd18cbaabefe8afb5f5db63e471b1f67292e5d1f67e96efd2dbed6f23a78c9e9f8fe3c54acc48e7b64fb9a644bbb18efd71f51c0383d7e8339f5c548eb8fa1071dbf0cfbf627fa573cadcd1febd3f1fe99c769283ddf6d3a5edd7fa57b7631eecf5c7a723b743f8f61c7ebe86972f97380790491ce78e7e9f8647e2292eb3923dba0e9c83f8e71f99fc2aa5b929286f461c91d3b63b0fa9c671c7a575a49d2717d52b2fbbadb7effd5f9a3270ad192be8e2fd76f45d3b6e7aad8ca0aafd33faf4ebe87d0739e95a6e783ef5ce6972ee8d3918f6eb8ce3f5e3ebc66b7b248033c751fe7f1af99ad0e5aad6dcb2ed6bad1ff5e77d4fd032faded30b1ef6b7a68be7b7e3f3285d0386c63a01eff87e950db838e84719e9f5f63edd33ed57275c8cfe7fcbfa8fcaab492476d03cd2b048947ccc41200390000396766215154333310aa199802f997272e9bdfe5bfdde5f32f0f4a5f5e84dbbd96d6f35af7e9f35ae9b99ba89e49ed91f9563029823f3ebcff338faff008d2ea3a9cf6935a9d5345d474ed36f1a643abdddce8d1d9d97956b2dd79da820d51eeede2f2619187fa392aca12710724735e13f13e8be34d034ff0012787754d2b59d1f5387cfb2d5344d56c35bd22fa12c425c69dabe977173a7ea1693281243736b3c913ab004870cabc3ecaa387b5b2e4e651724d6eef676becda776b44f476babfd22af4fda2a17b5469cd45a716d4796fab56ba538b4afb36d269371e9847dc738ebd0fe9c1fe7fa546eccbeb8c76f4cfe7fa9e879c54c859463fcf5fa9fc7db03b5063de01f7eff00a9391f9633d7daa127a5aeaf6dbd6cfb75d7e674475f3b592b6bdba76df5ddabed6b1cdeb40cd61751601dd13003d7e5271cf63d0d7f2ebff054af0c3e9be238f56542ab7315cc0ec0100b210e99e47246ec0fc06735fd505ddb031382072a7b7639fafe591f4afc02ff0082b1f846397c1b75aaa440bda5c46eac00c8577daf8f4e339233c77afabe10c53a19c518b7a55e6a4fb7bcb955edfdeb3ec79d9bd35530729257f6728cd7a5d5ed7b2d8fe433e24b7997332bff00798118f73f974c77eff87cd7ab5bdac424772376d3d482c41cf279c803fcf15f497c4d465bab90bc30693af5c64f5e98e47e47a9e2be60d56ddc99649db8e481d863d7b0fa0ebc72315fa5636f776577cbb37a74dffad4f0f0da25676d34fc34d9eeb7e96ec7cfbe2421efdfca1850cd83d077e475f7f40073e99e5c86c9c0279e78cf3d3fbc3d05747e259905ebec19019bb75c71d7bf4c75fa0ae58cc5bbf2060fddea3eb83dff00c9af89af6e77ad9b776b5b6fb597cbf2bf6f595fdddb67d7a5d59fe1ff0006e7fa38e95e18d4bc677d0dc04920b74b7b6675759d12ceda743aa5dbcd3cacb1a85f39229b7300318de325abcabe32fed09e0bf841a6cba5e897516a5aca19626bd8f625ac322332b79206e3210e4a24ac1b254908bb72dec9fb667c69d33e1278321f08f83888b52d712eee2f678628fed77f086689646c36628646797cb85004450a003b4357e06eb1e18f8a5f19fc410c76f6dac4e64b86cc56ad2bbc90c9e74cae238e36601594ee63f26d2bb89524d7169b6db5dbdfc925dde96b376e8ae713bb565776b5979dd7ebe6afe57387f8dbf1cbc65f13b58b9b28750bbb83713bc4234b9e374d2b45210acb866914965ce4104fca0939d6f83dfb3f8bf9e1d5f5e8a59a5981f215e58943b493c44aba0425182e4162412a4f43815f60fc2cfd8d2ef4592df54f1a6a363a22c2f6cf751dd25bdfea5b01461b6053e4c0f26d705e6995d37a7eecfcca7dcbc4be38f84ff0574ff33464b196f6dc82356bf96deea7f955832a45288ed2dc2bbf9a8905ba94e097c81572ace3150a5eedfabf89ecf45bfcfadd98c68ae6f6955a96d68dbdd8ad377d35ba5a3d3e478b268363a0882d6d74e9e02f64b1e5a2688e27bef310c6f24311263da0f9801caaef01b20b58d3ff00b45a79bc886f6158de56563f3c7bc989b219da3563b612550127f881c938fcfafda37f6e0f857ab6b176754696fb539008daeb44d5eeadb536080c72233d96c8bcb291a7c8e8e01caaf1c8f28f87ff00b4be91e219ac6cbc29e0cf8c5aa48ce8b1ecf13dcac4d2161e5bb34d63244a854282657214637127a5429d369373e59b49c94ed749eff69e9d5376badec6552559b76a778b968e12bab2b6eda4d357e89f6bd8fd8fd23c2f0ead3a4be215b531a5b431ab5c44fe7c6c4ac4040e81992490193282e14ee466c11b80c1bef018b59751b7b431ea363be578d9e347b984c9b06c123300ebe5902376c610fcceaca01f1cd134bf8abaae83a56a369ac5e7862fe753710685e2dbeb6d47cf910b0895f50d2e0826b722309b0359dd22b17dee37b35636b5f1b7c63f0a2ea2b7f88de1cd46c1ae1c470f8860613e85a8ee011441a8c3bed7ed7b0665898c72a264c910cae3bb0f86e64e6ab29425eea4d7bb74e29bf556b76b5adddf958ac538cd4152e5a91b5df326da6a2d2764ba77bdb5db53e8ef005ac7e16f1169f87b8b482ee631dc24c8e0ac11fcac2450b1b08f041575dd1b15ca924807c2ff00e0a91e39b4f067ecade26f0ccab05ccff13757d13c29a333b2b340ff00da76bafea93427258c50dae8974429054492266352c0d773e1dfda5bc0de2d6b13696915cbbb470c4b11248b879cbcd7033b5d46e0b14215a32e72df3019afc7bff82907c6ebcf891e35f03fc358ee8cba6781a0d435dbfb50dbd62d6fc49233d8db91c6f92c7445debe5ed1bb563180ff0029adaadf0f85ac9b4d4fdda76d57beb91a4d6d68b724fb45abd91cf492c663b0cf91a94129d5e6d34a4d493d2fbdd45bd7e2fbbe6df8396a9662cc18583b95954e0f97b62fdcc61cc4cb200c2468a4dca14aaa9258f03f4e7e1d5f450585aaadc886793cb11c4c6008c214724c88e609cc92bef5cc93b2b02a5941041fcdcf86ba65c5a4114bf6870ecf05ac3190e032940b95258ae27324d316c9209c823923ee2f09dedd8305b498fb20484191618a50c88549dd246aab0bb1c05063f9fcc25be620af0d05656d3b7abd1bfc16fa7a77f5714db77d1e97b7c92d13d9abad6ef77aa3f4bbe1cea7017d16dbcd8644dcb713a08638a700670ce671146c5a59a751b1ae1f8468c3e067ee5f0dca96fa498c8052486775528439692290cae4b3953b59957241de235da36b44abf9a1f0b353b2b9b90d2248be64d0d8c5e4ca2681a38fc98ee0b40ce8e24da81e308087f3bddc1fbdbc3b70a21912d66df2986285537b24cd900ac8d6cc55b680aaeeac9300aca09cb466bd4a76696fd1abed6d3f1daefb3d3baf9cc42d5beb77f3d977b69757d37bf56edf9c9fb79f830dc68da85c5bc246c5b98e48dc9711b32413a318c92ec846e757119923dccc595546cfe617e2468fe5de6a09824049831f946dcc69b40546c60630369381d3a1c7f611fb5668afacf847579a68d16685cac5731aee4dc96cdbb6fcaa51f6a3009b9b70f9d19980cff2a1f1a3434b3d735c87ca0822bbb8185042863c0d995ce641cfc8a14f0411b813e66754d4a9c26b576b3eaeeacfa6b7b79e9dac7bbc3f5ad3953bf58bb5ff009ac9dfe7eba593b19bfb2e7c60bef819f16be1ff00c46b677fb1586ad6da6788ad037cba9786f5355b4d6ace5556cb473d8bcd0ba83e6ed76f2d9242187f545fb47f937dfb24fed0579a75cc7a958da7c24d7bc61a35d44c8c977a7f87ad2dfc71a3dec2012a637b7d2ed6e9190e02ed202b0da3f8f8d361536b7f11c8369379cbf22b9db104946d660189c6de06ece40e3935fd54fc0ed566f895ff0004ff00beb19cbdd5c6b3fb3afc49f02dccd24c659ae268fc1dafe936cc439eaba7dde9b0c6a30a6348f003139e4c8eac92c4e19fc32a4e715da70f8ad7d7de8eeffbb7675f12508f2e0f19a7353af1a5377d5c1be78b6afb45c5a5fe27ea7d65ff0004d6f8ab2eb7f05e3d1e5ba593fb366bb8a045893e4b696282ea12ae0bb22f9793b9361cbaf25495afd203abcd76248d371bb8dc3147650eaf3c51bc810c824e4939d9b91812cbfc436ff3b5ff00048af893717ba5c5a04f725e3bdf0ce8379e5fd91989b87d25f4f943b79f2a7cd756e43bfd9f2fb95b030457ef2e8dad428972cf736f15c4eb0930209a2dc3cfba89a4538b6666501421790ae230a1480437b516a518ca2aea4935a6d76baf92b3f5b23e4f13074ebd487693fd3a3d97969e577a92f8dbc7fa87813c3b6bae4497b6f1a6b3040d1ac6a6326782eda4cb20322cae914518566292050ca199096f65f845fb6af8b3c37a68b99a1be1a3d9981174abe47ba4bd3300ec21b6199a190ae559adda29770dee48dc0fcbff0016f595bbf03de6d92c984775a53a4cd28668d0ea16cb2bb3b2ec9225491c6e7901846e2a01183e03e03f1a6a3e39f12e8fe1ff0004dbdc5d786f4bb9136b1e26995a1d2c3c722aad8c374543dcc8ec47993da09137bf966452735a4b0f46bd1946b5284e1cdef29abdd5a2d5935bf67ba5aab5ae674eb56a15154a15674aa2d54e13716946dbd9eab45a5acfae87f531f07fe29e85f18fc0da6f8d3438a7b58ee9e5b4bed3ee94a5ce9da8da951736d2a3ec6230c92c4ec8a5e192362aadb947a71fd327f4fc4ff33f5afce9fd907c5f6be03b5f1ee95f10fc5be11d2adafafb45d4fc296df697d2eeeeed459dcc3ab3cb677ac1a6bb82f0430c86c9a6f353ca95d559b03ed3f0efc5bf86be2dbe6d2fc3fe35d0b53d46304b58c57ab1dcbe0f26286e3ca79b03af921cae0960064d7e7d8fc0ce862b110a346aca85392719a84e508c6518cece7cb6f7799c6eddeeb5d6e7e9d956674b1582c24f1188a11c4d58b8ca0ea5384e7384e54eea9b92779d94ac95bdef755ac7a1535941f63ebfe35f397c47fdadff0067cf85779269de2df88ba4c3a942de5cfa7e96b71acdcc120628d1dc1d3a29e0b7915810f14d347229041418adaf845fb4b7c11f8e96b7373f0cbc7da47881ecae1ed6f2c8b4b61a8dbcf190191ecafa3b799c02461e112c67380f9c81caf0b8a8c155787ad186ea6e9cd2b77bb5b3befb3ee76fd73053a9ec562683a89db9555837cdfcba3b737f77e2f23d7af61c15993878cf381d5475e83f1e7d2b9bd72d4334778830b32e24c6789077f4c3739ee702bb8740ea47a8fc0e7d7d78e9cfe958b25b89a19ecdc7382f1138eb9ca9fcf008cf201ec6aa9cf9a3aef1d2fe4ecbd347aede5b6d9ce1eceaa974968eddd59afc34f2d3a1e43abc5989b8c9e7e87afe78fd71ed8af10f1342712751f7b3d0fae7a74fe4315efdac42511d48c3296041ed8cf5fd4ff215e23e258c624ebce475ee01c71df823a71d318ad3995d25e9e8ee95be5afaf466f6bc6fd3a7e0bbad355b6fb3d0f9cb5c872d2000f538cf1d0feb9ebd3dba62bcf6ee2da5cf42327009eb8e3dbb8f4c73d3bfabeba8033f1dcf3c739fea09ea39e7dabce2fa3197e3ae7af1dbafe03ebf5cd755097be979f6f4fcfcbb3dee78b8f8fbb7db67f8f7db57fa3ebafc85f1fa3ff892dc7073e5499c71d8e38e31d33d0e3d7bd7e63f81f4796e7c713c8e58a098f1db1e6743ce073edd3ae7bfe9efc7f609a45c0383889863af41edc1f4c9e87f2afcf9f86f187f164ec46144c79f5f98f1cf418c8fc3de96615230a6a2ed77a7ded3b6eb7b6ff91595536e539745bf4edb77b5fb5afe9afe8cfc3ad1e24b1b702319d89ced1e80f1c727af6248ebdebdded3485dab84c74040c7a7f4fa73823dcf99fc3d8d0da5b819e1573efd3a8f5ce00c7bf502be89d32cb7a2e178c0c71db1c03c63af1cfa73e95c5869f9e96bf776d3a5fcff00047a95a9abeab5dafae8ba5fd3d37b9ca7f66ec2005e3030793d064e7e981f539ebcd21b6008e38fa7a671f8fe2302bbf974ec29f93191efd7a8c9fc4e3dfa0ac39ed429231c63ebcfa7d71dbaff003af670f3fbad6fbbb7e0fe478f8ca378bb5b4f2e8d2ebff0f6d6da1ade06956d754407e5de4638c7439c13c63a600f6e9d31f727856e565b58d41cfca3073ec08e475e3f3039afce4d7fc63e1ef867a6c7e27f186a51687a52c98826b8591a6bd91413e5585a44925d5ebf001fb34522a64191e353babb0f03fedf1fb37c76f18bdf176a96454608baf0d6b6a7e5ee3cab498608e14939e70429181acf32c061e7ec7118ec2d0a8d5d42ae229d3924edada7356bf4d9bd5f63c5af9166d8da2abe132cc762a9ddc554c3e12b568b6ada2708493f3decdd9bd6c7e98db0ce077e0fe793dbfafe5d8da922f971dbf5e9d327e839f6e95f0aa7fc1453f652b66dafe3fbc76519654f0bf884e3d7e66d3d501edf7bd8f5e214ff008293fecbd73693cf65aef89af6589cc51d947e18bb86e6ea4f9485805ccb0a10dbb877745e1b9fbbbb8e799e5aaf27986079747758bc3becb44aa36fe48c68f0bf10d5e5a4b22cd9d493b24f2fc52dde9aba6a2979b692ee7da97519c9edd7b63d093db38e38f41ef54110e471f975e3a63d7bfb633ea2be1df127ede1e0fb8d22d6f7c25a0ea06eae2626e13c46a962b696a09dafb209a5f367940c84f31228941dd23b3003ee9f8577fa5fc45f01784fc68b2985bc4da3c1aa25b44cb8092b3a8922dd97682554f3627390d13a30661c9787cfb2eaf3a94a8d675a5461194e74e12953b4b64a6a3caecb56d5d79dee974661e1ef13e5787c1e3330c047070c74e70c3d3c46228d3acdd349c9ce97373453e6f76f7934be1dafd5e90dfbb033d88ebf4c01fe73f9d75318660a002491d00c9ff229b6fa15adb6363cbb41e8594938ff0080e00e9d8e40ec726af5d5ce9fa4595cdf5f5d5a69da7d8dbcd777b7d7b7115ada5a5adbc6659ee6eaeae1d2282de0895a49669a458e28d4b3b2aa9238b1189854a8e54d4a4e4d5b4b6e9597577bf44bcae7a59564f8ac3d251c4b8534b7b494db565b6892dad76fafc2c6ad9ef03cd381fdd5ebf8b7403d40ce7d462b3f5482de2361782da5ba36372e6dad228e5903de4c8f6f14d215dd1c4620ce05ddd298ad95da4f3220496fcd8f8b7ff0550f845a56bd73f0e3f661f0778c3f6c5f8b8ad25bc3a1fc1eb69ae7e1fe9b74a4a17f107c4f5b3bfd15aca23979e7f09d9f8b16df63adf3586d6913e67d73f665ff00828cfedba1dbf6a9f8dba3fecd7f0835224cff0001fe089967d4350d3a4ce74ff16eb56ba934dad7daedcc4b749acf8a355d321bb89a787c2362c7c91eb61f87f193847139a57a192e0a51e78d4c73946bd5859dde1b2fa719e33117bab4d528d3d74aab75d93c6e02854787c245e2f16dc538516a52524e2e3edab36a9518df78c9a4ff0096fa1d4ffc1403f6e3fd9e5bc07e3ffd9e7c2ff142e7c73f1c7c7ba26a9e0d82f3c01ad59ddfc33f843aaeaf6f2e990ebbf10756be1a9f826f34bf0f4d24926b9e18fecaf1b7892fc4571a76b5a0697637a2e6d353fe096bf08bc49fb39fc1abff00865aceaa9e3e6d7bc597de37baf16781bc05a77c38f827e167bad0b46d207873e1d697159783f48d5b48d41f4683548efbe1b781dbc2f71a95d6b3ac6b1acdc6b7aadc4f77f44fc01ff827a7ecaffb36416337817e1bd86b1e26b3113af8dbc6e20f14f8996e62002de58bdddb47a4e85740f59fc3da4e9334809134929c9afaea688464b631f30c7af5cfaff4ef9e7bc63b1592e1f0d2c16574317896eeea63f1ee145ca6f92ef0f85a1cce9c138de0e788736acaad376719552c2e32b62a18bc556a745c6ca187c3c54ee95d5ab57a91bcaf194a328c2165af24ef66a6f33e607239e0fd07619f5f5cfd3af179240a99e3a0e4e01e7d3a7ebe9c8e6b08cea180e7afd3dbf0c7e47ebd2c8972a31f8724f6c9c0eff99fc6be793fbdbdfaf45a767d6f749f5be87b096977d6de7a5d5afaf968926ff21d7b38f2c8071c7ae3afd467fcf4ef5f8f7ff0533d0c6abf0a3c4ae13732d9cd2a9eb831a97183ec79031cf5fa7ebadd9fdd924f6cff003c9ff3d6bf37bf6ecd39352f861e208b6ef2fa75cae319073138e9d723b77e7a8af5326ace8e6584a9b355a96bb7da8f4f4bebb59f4ba33c5479f0d5a2b5e6a6edf75d6f7d6ffd753f842f8b45adb51bc057a3bf18ce3e6238ff00f57233ce738f913c4ba9b849572a8391c9f504739c127afa1c64e31d7ed5f8eb6ff65d5b510ea46d9254208c61919d4af3eea474fd6bf3afc677f2cb732450ee3cb020120679ee073e87071c1ed5fac66b59d28bb3d65a249ebb46f6dfa5fa6bf33e6f071528aba4acb7bfa5aeafd7a7569791c46a8d6acd233329258f704f3c939192319efd38c1ef5ca3c51b3122400678ff0038fc39f4a9ef2dee572ccd8c927af5f5e3fc7f23c1188c6e01c052703dbff66c9ff3eb9af8da951a69b825adb55b7cfadadd3cb447a6a3e7b77bdba6892db6db6ef7d0ff00440f8f1a678375cf1636b1e30d1ee358921b3fb2dbb9b992da344b63bb6831920a285dcbc0dc143163babe54d3fe3ec169e25b3f04fc36d174ad2e1b99e4b1bc86c6dc3ba29912312dddeed79a76467d83cc998304ca2857576ed3f6ecf8951f866fae74cd1ae2259c4a59ee63662af15c94702d581c12af20323a8605083ca46a57e10d2bc5da17c10f861aefc78f173c7692d8adc36916d74424bae6a8aad358f95cb6e481c8b999a2f3321228d8b6faf2aad59caafb38cb957da925b2b5dead69656b6ab5d3763a5460a92ab38f336ad18bf8652764b4db5ebbed76ac99dd7ed91fb5a787be015adddbebdad8bcd592cd92e60b69bca792f6dca130908cc599d9bee856009fbcd804fe7b689f0bfe3a7ed1be0187e39fc4e5d57c2df0e7c472cb2780be1ddaf88ad3c2fe20f15690d958fc4baeeb1aa2b3e8de1eba61e4d9476b00d42f616fb624b6f6bf677b8f09fd9abe1deabfb6ff00c7cf15fed0ff001d8dd5d7c0ff0086fae3ebb7fa55f33ad878ab5c12ade69be185df88e7b458847a86bc8ac5c69ab15b6d1f6f0f1fdd3f17fe2df883e33789a3f07f822474d06d425858b5843b2c2c34d84a416f6a12122086de1802c76f6ea150a229c1392b54aab6f9d2f72ed453bde7d39a4d3ba8ada297c4d3be9a3caa508db92f7a9249c9eea17b3b24efef34b776b2db569af24f87bf0a7e11f83353b18b51f0b7c3c7d7bed0b6d1785bc2363378c75e92699808a7d63c71e2b8ef9ae061f0eba469fb704b25d0dc187eaa5dc1f0d7e12f80bc2d772f86342b4d7b57b66ba96c9c24ed6366ea4db49fbcdc59e465608228d51b6a95511f35e07f053e05f853e14e8ba9fc40f1747f6bff00847e24beb9bdb925eea6bd0c860d1ece5917f7cd74019ae76605bdba3721f66de1ededfc71fb45f8c351d46d267b7d120bd114d71e5e459d9292b0456d100564f2220a44318544887cc4b138eec3a8d4bd5ab2f67421a3b69cf276b28d96baf44b67b5accf371ae549468d18a9e22a6a9bb38d28ab2bc9ddaddab37777b5bb3efb51fdab645be36ba6e8c6e1548448ececa086df6ba465230e63775058b8c02b95217a2a8ac75fda7ecf5ed365d0fc65e13b4d4b40b9b87b6d474cd5f4f8eeeca5b4493cb0b3a4b0dcab491231647c0915c060c8e14afd11e0dfd9ebc1ba2c089340fa94e9e5896fa461b5d87065c953b7c973921373c400254801877b2fc0bf06df5b4b143a6c48aca257909324524ae3cc2e649232a92e38df18542324a03915df0c561934a3194127a3695ba6b64dbbf7ff0023c8a985c4c93e792a8ddd349b57dbba56ebdba753e1d93f678f0a45707e28fecfda935d595bd9ddeab37c35b8d462ff0090ac76f24b0c1a26a17ae440f25df9312586b0d88d9dbc8bc64d895fcfceafa8f887c4bf107c41aff891586bba9ebd7d7baa5a4ca50d95d7db0c234f459187971d82c305a469bbf73140a8301093fd73f83fe035bf84fc469abf872e9208e54b882f6cde29a4b1996588b98e4661e4380c011b03ed214fca3047e5d7ed67ff0004d4f18d978a75df8aff00052c46bda36b93de6bde24f09c73eed574ed5ee6792e75097418e4046a3617324b2dc0b267175672168624b88826271938e2214d5295f95b94a09daf74ad2574b6b3492e927a77300a585ad57dbc5a538c542a4dabc6cdde12926f47a3e677bb8d9b5a23f3bbc013321b1c892308aa63dac423493b88a22cb23289232b827e495946576e0963f5af82356c5e31f322dd138650c580616ea9e40c2b326ef37ece920587e51bdb3f771f3368fa55e68331b3d46caeb49bb81cacd6b7f6f3d8ddc6f0abc728c4bb110c71ac8ca1938642bf3316cfd07e0dfecf02d52e37406e195a55505e29114bb100a80ecaecf2162dfbbcc718524e09c68b7a2bebd9efb2e9e5aebb27d3b75d749deda2f9796ba6efadbaebb1f79fc280fbed51adde5fb3ec95e704f9c66b8250e1e3114e3c98d5b21a0923062cf3b481f7678175086e459c2acc1ccf330b6bb452c8ca446aa8c42dc22a47016021915f24131b26777c13f0d498ad5a717d186be96348c444db799117db958a52327ca476dde4c982e49c80c4fd8de0dbabd47f3aea48a7811218fcbba5d8372aa872adb51959f329678a38caa960ce98f307ab4fe15e8b5fbafdff004bf9f4f9ec425ccd5f5d3e5b74d1fa2eed2b589fe2cc4bac689af4532922549634462662cd12a84995ca23cdb91b055d1650147961e30e1ff970fda53466b1f1c788606509b6624285e0c7bb68910654618346ebb738ca82d9619feaafc48d06a5a05c48d32432c8b7664b7b828e11e4b88088527dd2388997214b09232ccc0cb18c03fcd87eda9a68d27e23ea51885e36bbb38dc97c0386b897728230b84caaef51861b09519def8664b9b0b7fe4925ea9ab76b7afcbb9d99449c716a36d5c2da37676945ad36e8f5b6efa6a7e71d9c4a9a8dcdb129b668d8aeed8c844822524ee2405f9402a30d803002e73fd2e7fc132ef64f117eca71e97744907c63e3bf0a312dbd4c49e1df0b40aaa9b015dc2f1fe566dbc8623e6676fe6b65842ebb6db645236aa9c8cb6502e490a87a3105b70000fe13835fd4cffc138bc267c33fb197c3dd726856397c4df15fc7daeb32a9cb5a5eb59e8b0336f456cc83c2ab22856c34254b2ef5f9bc0c9399e3d2d6decea5ed7574e292d754fde6b4eaf6b1f43c44d2cb1b76bcab5149f5beb2d377f0c5eba6da7447e6bffc1297c5771e1ff8d5e10f0c4d3496cb789aa681300ca0892cb539a680323c649da8ec0856040e06159cd7f47be36d5354d22ed0f11b5b492c933fd98cac6d537c823cbbe065b0ac4471b9006d1b540afe5aff00677d465f861fb5d8863b882cd742f8c5ad69c44d22c6ab04baf4968400e8e15711b648c8f5078cff00535f1407dbac63d5985b3badac322bc65e38254784dc20f3219602b95058af94aa507cd85715f4387568a84bec7b9ebcbca974f2f35d77b9f2b99a4b1309ff00cfd8735ecb57269dfe69aeb6d93dae737e2b6d3fc4fe13d40ea7736cd6ba6cba44cb03a4b189e69afa3820865472f17d9a448259dd0b22cb2c089862462f699e23b2f0678752f4a43a735b244c91058951d36930dabac491efb70acb239640c70237daf861c1eb5af0f0ff00c15f12ea92798cebe20f0da3c37db1f6d947a9dbd93246aea374524d3dd0463f2c8b26e1b9381e1bf18f5c371e0637ba5ea0d1e9779651cd1179007b259e122746766d851d90340fd423088806204f5c1c795b9ed1bb76eca317f24eceef4b775b3e0e4949f2afe68c52b75f774b75577d3ba57e82fc50ff008280eb9a9dfbf857c3b3c22f2c9d34f9f5a86357bc584f2f15bc9d44818b2ac8065f009cbe18edf87be2dea5f0cfc1e9e29bfd46f6db5dd756416b15c5e5d9d42e7ed3999912590af9618737372922ba2b796aca5c86fceaf86de1ed2b48d5b52f88fe24b758bc27e1c4b8d426bfbdd90c570e837c6e1ee2688c8b2b2811ac41c333e132c40af33f15fc7af187c73f13dcdee9f6434ff0ad831d37468269e78da4b789d81923d91cf1ed988de70431638002a00786355b73ad34a961dbfdd504aceab8dace6f76b5bdb4deda3691e9ac1c62e34e9479eba51756b369c6927cb68c15d5a5d6f66f5be9d3eb2f167c6ad6fc5da95db3de3cf2b79b1811ccccb14b2e556321c867da324b107d5c963c7dcdfb136b137c3cd46c22b3bc921d475697ed7a848652a24374eb26644071b00243170428604943c8fcbdd0e6b7b4b4d36cce890abd9ca1ae59751854dccac85c875b878277dcd8462b1b2f213926bea8f82ff10b43f0ff008d2df51d721f125859ceeb01d522d36eae74fd3b6929ba7f2126c43bc1de46084da1720e0f2ceb4aa558ce6edada30d1a8df44da5a68bd2e76bc3c21879420b5717cd2d2eda5a2b5ef64ececb4fc12fea8bc23f1efc47e109edadb509db5ad1a58a1716178ef2dcdb46c012d6377f3ce21da41892633478055550e2be99d37e38f803537b3136a12e917b72a185b5fdbcaaa40e2402e61492df009eb23c6c460ec1ce3f0d345f8d72789b49b08bc33ad697ad6a7a7a22d96a7a4dfc1248d6653882eece661382b83f2bc5ba3c053b88006ac5f1b3c46b3f95e257b2ba92d3cb74b5578ac6f257dc36a5c4d88e4f2f2bcc71c0c0a862d2aee3b7831982c24e6a5c8a1564be2a6d479d3b7c5149c1f4d5a527df42b2dc7e634a2e94aa3a9462d2f675d394a9af76dc93e6535d946ed25b2d0fdc7d72ff47d481bad2b51b2bf8664de4da5c45361fbee08c4ae463e52010410475af10f11c6312718fbdc0fc7d3d3ff00d7e83f2eeeff006c7d73c313243653e89712e7325a4826781136c8f18492d648ca9548f6abac84b3637f98339c7b8fdb53e2aeb9121dbe1bb1899325ec74ff0039f63720bcd773dcb79b1ae09fdc2eeddb87a0f39e5352a35ecaa46da5fda5d59596a9a4eeedbd96f6d7b7d0c33ca74e2e388a33bd938ca938b72d9d9c6725c9be9ef3ba5e87dc1af44ed2b8556624e00552c482793c6e39c9c8f73c74ae12e74ad46404c76374d9ce08824e4fe29cf4c7e1cf1cd7c37ac7c78f1f6b71b8baf11eab0028b95b19e4b5123b132b0f26d561550012a03445b09c30cf3cdf85fe22ebf6f7df6cd4bc49abdd6f2cfb269eea60c8ce2258dfce246704b3301800b038ade396ba3cae7595ef6b4617d15b5bb7b36f54d2becfcb86ae65f5a52f678771493779c95dbe8b9631b5fcafdb4d6c7b6fc60f007897c4967750e9da3dfde482391bcab6b6966942a86dcc638c1600007391fd2be0bf0b78175ef0ff0089ae63bcd2b50b7905c10d1cb693a3ae0e30c8d182b83d88e339ebcd7e82f87fe28783fc3f773eb37fafa4334a80b882394cb924794988d49de4004608504b6482bc61f88bf6b9b3b477feccb8ba656731ac978caccb1a3a8dfb4c8ccbe60c30e73ce01522b9b17967d69ae5ad3872caff000269ecf55ccaddf7eb6f33a301994f0d0d70f19396eb9dc5c76b6ad34ecbaf4bf5e9d07c3eb4be486d43d95ca9c26736f28249e31ca0f6c71c9e98e95f53e81692b4280c4e0ed19055bb1e7a8e3bf3cf19f615f06ea1fb62f8a16069a0d56ee38f67eedad237f33cbdc0c603c9315072c80b22b3fcc84602b13f56fec51f11fc4df1dfe22ea91788af750bff000bf873491ac5fc37262fb2dcdd5f4c2d349b0b81139678db17978d1b84593ec51a32b207079a396470f0729577250e8a0b5d568af3ddd9d96de677c7359d694631c325272b24ea68b9adab6a0ec975d1b4aeeced63dcadb41bfd5ee61d3f4db49af2f6e584704102177727a923a22a8cb3bb9548d14b3155048f31f8d1e3ef03fc0ab2934c964b4f1a7c519e3cae876d22cfa27868b8f95f5268c91a85fa9208b738b74209749400e7a3fdb07f6e0f06fc157d4fe16fc36b7d3ac7e213cada6ea7a85b59c16c746b7961864658a58955cdd4d1cd1ba33f0aae0a82ca09fcbbd035bb3d4ae6f75cd72e46a5e21d4a592f65bcb99bed0e1e7690ae525661966206d39601b792170b5f2d9c67ef0aa584c1ce2b156e5a951da4b0eb4bc5a4ddeb6deeed0d1cb5ba5f7fc3fc31f5e74b1b8fa327846d4a8d0b59e23e169b725197b1d6ea495e7dadabf2ef8b1e24f14fc43d4e6d63c677f2999dbcab16964dd6d6ccf931da4299582db8c05446f9829ce597657cf7ab69b69a18b5b9d57527759a74489770b719218aa0da2426438207cc0138d809cad7d91a969d657fe7c976914b1dc06430ce43abc849dc121fde0c85030e5474f9b0839f8ffe2d7c17f1b7c52bc8be197c23bbf166b3e2fd6e3123786bc3569a86b50e8da74d3148cfd96c246ba8a69a457681dee6cf4bd2e1824bbbd9d94c087e0a3879e2aaf34e557115eb4efcd272a95272925776777b76d55958fd6e0a965f41732a386c2d185da6a30a7051b356936a30b34b7b2bbb49f7ced4358f86161686e35cf128d3d63677297176b1829f36789111cb3051b41dc17393bb000f37f85bfb417823c67f126f3e10fc25f0878c7e317c446be32e8da07c39f0f6a1e26d464b29441fe95a8dd58c2961a569f6d2cb247757faa5cd8e9b68b1efbabb8555c8fb87e027fc1bbdabf8f359d2bc75fb687c63f17f873c1b6da9c325b7c2bd1fc65fda7e27d7a044f22d6c7c61e2d95ce83a1b5edd1925bad33c3f6fadcf2a1b7b58356b295252dfd17fc06fd99bf651fd92bc1d67e12f81de05f86ff087c3d22f997535849a6c3aa789ae202221aa78875dd4667d4fc4d7c8772adeeab7f7ef0873142638f11d7d665dc2949ae7c654acafff002ee8252a97576a2e725351492d54632927a4a27c5667e21470f5a30caf0f87ad569c97efb12dba09251f7a3087b29544e4dd9b9c21249b8ca47e71fc15ff00826c78dbe21e9163ab7ed19aedcf81741b858ae26f871e0cd6619bc49716ef92da7f897c716867d37495701a0bdb1f093eab76109fb3f88ad25afd66f1078fbe09fecd7e01d324f18f8bfc27f0cfc15a1d859e89a34de26d6e2d3e39e1d32d62b6b2d23464d4269357f106a31db4714169a6e9b05f6a17588e3b5b691dd54f65ade8f178bf46bdb1d03c79aa6813cc1ad5fc47e139bc357fab5810a19e3b27d7748f11e8f6b3ec901cc9a54d2c6ae8f179520592be7df02fec41fb3f783fc5f77f11b5bd1b5af8b9f11ae9d597c79f1b35dbaf89be24b08919de3b5d2ee3c42935ae9b6904acf259c169671269f910e9ff0064b68e2813ebf2ec165184a752151d7c3c2f0bd1c350f6988c44a364955af5a708412bb6a6d54706df261e57b3fcff003ccff39cfb114ebe32ac317569f3468caa54a74b0b858cf979fd850a31934e5cb152b454ea72c7da55d14978d5cfed9bf1a7e36f99a77ec5ff00b3a7883c5ba5cdba387e3afc77b7d4fe177c218518909a9685e1cbb169e3ff001cc119da2e34e31f832f50379b13ceab86e51ffe09e7e37f8f3776daf7edd1fb41f8c7e38449730df45f063c1934bf0ebe03e993c2eb2456f278674616775e294b6704d96adaeaaf88edd58a4babdc8dccff00a98811515530a8aa1515542aaa81855550000aa0700000018e838cb8b5ab19f54bed16198b6a3a6c56571771346c8121d404e6d9d1d976481c5b4c0ec2763261b9201f4e19bcb0cdac9f05472e705778a7fed798a85d2e678cad051c3b5271f7f0387c1caee2aef56fc09e1a32b3c7e2a55d4da8468c5ac3e1dcda4d455284e552ab6a326e35aad58b49b514923cb7c29f0abe1d7c18f0c59f85be197827c33e07d02131a2e9de1bd26d34c8a668e32a27bd9608d6e350bb2a3e7bbbe9ae6ee439324cecd93d3dacace324633cfa63d3f4ff00f59aec6f6de1bb9520940384320dc3a609071cf1d0f3c640e7d2a38ec2da11c203db381edcfbfa8f41d3db92a62e55a9b75e752b622ac9ce756a4a539c9cacb9a53937293bab5e4defbe871d2cba54b31a95e928430d1518429c6d1e59452e64a31492b37b2d2de5b73326718009e09e0679ed5897505d4b909131f4c8c03d7a93f874fcfbd7a134708e88bd7be3f901f4cfb7af1509dabcec41f400fe5919cf6f4c772783c6e29ff5e9b79e8b6f33d872516b67e5bf55a35e6ae7998d2ee95c1914af03ae738033e9f4f5fcaaf25b1000f4ff003efc7047a9faf5ea6f1b2cc368c6c27a01d474cf18e98c9f4eddf084ea012703030075f6fa0fc78e687492517afbcbaf5db67a74e96efa853af2a8ea46caf07157efb3bfdef5d74ba7a183ab01142fc63e5ff3fe4753f5af843f6a9b35d4fc0bac44406cd9dca907dd188edee7938e6bed9f115e8589c6477e3d78f5c7b74ee41c1c57c61f1c9fedde1cd5adf01b36f28ebea841c67b03f974e28a53f6552134ef69c1df5d2d66bfabe9a1d36e68b4fac6cefe6acf4f9ec7f0b3fb56e8c74ef15f892d426d36daadeae14018492469531d8615ba7b1e335f985e29816da499b682496c9ef9f5c8e7f0e9900d7ecdfedcba03699f117c4a8c9b56ed5ae6318e0bc6ec1b03d71b7f022bf207c596a9334ad8e431c8c1182491ea7d33d7d78eb5faf62e6b1382c3d74aee7469c97af2c799ad7bf9bdfccf9aa3174aa54a5fcb392d754d37a76d2ceda5afe4cf06d465662c0e40c9fa9c63f4ea73ea33eb587907b74c8e54b1ebeb9e3af4ed5d75fdbc31f98642323db9e31c7ae4fe5f9571f24a8ae5410003d0839e79fcabe5eb45deee5bbd2c97f5d174f4676eeb66bf0fc9fe07f605fb41b5ef8f3e3641e0d69cdddd5cea3a443a6365a4b436d791dac931318dccb6d67148d33dc48ccbf668e5666dd17c9f98bfb656b5e20fda37e36f873f671f8511dd4be0cf07bdb683032bb35bdc4f148905eea1745711c525ddc8772bb9b25d630e0015faa5aef86f55d47e26f8afc7b6914933f877e08bdeda4e1f12dbdfea5a7697e169ee428f94c88351bb64d9e66247251b2a48f9dfe17780749fd9fbc19e27f8a9aec76f278e3c437177369935fa28ba696e4491bdcc4afcecb6b790c911185334cc40dc849f1634f99d593bae7a8d4b76d538b4ecbb5dbb5bd34be86ceaa4a8c52e6e5a69a4b6739595ddb5d17cecdec709f14ffb0be05fc2df067eccff000d0c2eba5c28fe20d46cc153ae78b2f806d57519e48b0c585c48f0c1b98afd9922833b63507e84fd993e165bf85f4a8ace6b2373afeb2b0cf78f2c6c313dc3a08c93bbe41f783c65b11931bae0b7cbf25fc27d0755f881e3dbbf1debf6ef7f630dec97315ac88cd148f1c8cdbd038c2ed5c3221e06d03e78d55cfec1f8134ab3f0eda69fa8c9cdf6a2d08d2d495f3dad9f6b7da66de321826608d8a8c8123e4485eb48ae6928a5cb1564edb249a51b75bbe8aff8dccf6bcda5293f24aeda4eefa2deff003d12b1e0bfb58cf7b61a47863e1668b23b5b43235d6a65004fb6dede32c53dc160433048c0822572c628d176839dc7dbbe08782f4ff08f84348b7b48611e55945ba6c049aee7748ccdf6b3f36e6e4aeee4cafc70c083e27f16aceeb5ff008bba61457bb025b6961b7776428e8c15a697e42de5b72662464800aab36e35f4a5d6b367e1eb082d61950ead2a47f6ab101044226da37c519e08c2f05082c4163f30c574ca49c214f68439a4d2db99b56bfcb45d773cf54af56ad477729da37b6b656d356ddaeef6d9efa3b25e9b6f08bf71345b4cb09066b38bf76815feece48014321e0a9652f920ee38ae82dad74b93c88352bb4b2b2624186172ad7a1b3f2b80dba3898ee476665670b95005794dbf8e745d0fc2ba86bb7ce4b440c10c4d3146d42f9f718ed379e59630a249e620e631e51055c11f33e9be3fd7759f145adec9aa48ac2e964bab62445884b975032df66b88d10e015dbf3155393469cae5d12bafebd2fe7f2b84972c941e8dbdedf0dd277f5d7b75bbecbf403c4fe22b6d18451470c4b6f1c71adb36df29628c2ed558c852543a290851b249dd2900935cde91e235d56e5218da489bcc2ab6e76891b7609953630594312c300a7cbf7c63ae678c7538bc49e0c37a60fb3dee8c9079d710f00c170a19649a0c1df8236078998c480e0f383e7df096e06afe248c5c826de060b1b424b6f42d9f9597801c64775fbd90a462b6859d172bda49a5f7a56b5dedd36e9b9c73a6e35fd9d95a51e6bdb7b25a35e6bfa67bcfc40f829f0c7c5fa4acfe39f05f87b5e82645569eff004bb78f536326ec345a942915d42ff30c324aac06412c0f3f03f8abf633f82973a85ec9e16d6fc43e1e96476074b9a3b5d56d2dd8edda90c937d9ae584581b15e791e205b7164626bf4cbe215f09b42b782c64de442b19895d99a3083e6015b0587ca32e8c413bb20af03c1fc31a35aeb9e2ad3ec7523f6479a5574084b4ad8057e6ddf7e36186c1c90060839c0d28cd2a729d4936e3aa57bbe974af76eeec8e5c45292ab18538e92f89aba4b6bb6aea2baeebcafb9f2f683fb28f88f4c4ff8a53c490f8816dadf6c7697b15cd999a4c062a257f36d95f6abafcce1097cabd2eaf6be26f02345a578a74eb8b1273b7cd13fd927728f1b0494ac90b077c6c949847525958c75fa4fe27b497c302df4fd06345548fcc90183679840c81114c025d87cf9cb0077631c1e72fb4fb7f1969d358f8a2cf49b8b196389a4b272b2dec728cb79d0b210d048acaac92c7287180a415254f6d1c5a506ea38a8ded15bcada2bbb69eb7fc2d73cac4e16afb6e4a74e7349252935cb14f47a3764d59eb77e47c2e9a9bdd58da40ccbb64b6469a391c02cfb4dd7ee25909df1796236508b3421554094abd7e0c7fc1426ce2b1f88966ea848bbd3e573990060d1dced650e872c0e76f0782a5879a186efeac2d7c33f0d3c336f25a69de15d22ff0067cb72da809ae66442810ee6b8778d59828059514b37cc79393e75e2cf843fb3b7c5bba4b7f157c19f01788aeb788526d574689ae95246cba437b0b02bbc825550c79ea581e2a3138b856c3ce946125cce3efbe5b2b38b5a2eebb5fbf91d183c2d4c36229d66e2f974704df359a8a566d5b7b6ed7e67f0a770257d6ec8db8ccd35c47042851a42d24d28444050ee3b88545011589d8133c06fed27e16f85acbe1dfecc5f0b7e1f05b77bdf06787fc251dcc91245e6aea12da6dd5e269762ca246d4354bb69c6f712ca85df242b574777ff0004a8fd8aaf355d1bc52bf0963f0fea1a3ea7a76b760de1ef106ad671a6a1a7dec5a85b192d1af27b59e159e10258248bcb922250865e2bebed6be13f87359f0f6a7a2e8b7d2d95d5d0b5782ef51b70618e6b6be82e30ed6e048c1a385a2c88f081971f2ee56e2ca610c356955ab38eae9a8b8deca17bcdcaf15bda3a6ba267567d5eae2a9e1e952a3522a129549a93834e568a872b8c9dec9cd3bdb7eda9fc5bfc4dbb6f0d7ed67f14e446f29acfe28ebf7d061c22864f16ea52ee07a0da8032b02001d011f29feadb4cd4078d3e10f823c471a43710eade19d2e6b916f70ae6492dad45bc9189911c2e5307f7d2290ec010bb867f1e3f690ff824c7ed51aafc69f18fc53f015b783bc69a0788f53d6f5886d74cf124367acc4f7da8b5f4313d96a90daa3ceaaee098a7386c63ef135fa81f053c2df11bc11fb37e85e15f88de05d734ad7f42175a5de699a945348e215590c13dbc969be37842bc6237b7936318823158c64fa94271e7a8aeacea49a7a7c2e4ed6d57495fbaed63c9cc529d3c2ca3aca10a719c75e68b70845dd5aeace2fcaeed7d51e2ff00b40ea979a67ece9e3fb28e7997c9ff00845d21c4bbee957fe128d26346338640243b1c30d9892574da814803e46f00fc65bd97c1e7c2be2dd362d474db152e2fdee120bb3628ad2bdbdfc4fb619ede091ddc4bbe30ab81960b9afa7ff68a99ee7e0e78ced6756b74966d0516054daea89ad69f73b5831566c4d862a4b3a1da8a177103f20bc53a3fc53f89ee7e137c1db0baba1a82a278d7c5122cb6ba4e836128dafa7bdfcf1aedbb9e33bae20b432ba40563c032960f1588a38584ebd7a91852846f2e669295ed68f4bb6fb6b6f4b0f2ec1d7c6d48e1e852955ab2a8aca107271b25ef69b25dde8deed6871df1d3e3c6a9f1cb591f0ebc0333697f0e342bf56d427b268e25d72e6d99e3dea636f9ec2ddcb8846ff2a675f3c8c08f1edff0bfc3f6f069fa75959db5d4b2d9c51c6b1c00ce273b70af2aa19f2c483216254827920703eb2fd9b3f60bf851f0d74e37fe3307c63e2816de75ccf79737363a1c2f1c7c59d9585bba178c1c8334eceef92480318f71f186ade11f0ce8afa5786341d3741b98d9f3fd936914328032a3fd240f3db000ddba4390060f181f0998718e130d7a8a954af2926a9c53e4a564d24a32e5938c568fe0bb776d755fa9657e1f63f14e9d1a95e860a92b4a739fef2ade51bc9ca09c539b7fde718ec9f7f8775c7be82f61b75b7bc845bb089525b7942f9982cf2aa379b1860371dc157040c83f293760d5f504b882e74ebc9ad25b7743ba299edc9f9332799e4c90a6c0a8fc950770c13922bdb34bf1d7896291f4dbf9ac755d32ecb4b1c3a9c56b3dca1036bec9e78de5e07fcb32e53fd90793bdfd95e0dd511df53f0dd96e981171fd9f24904fb4e0921a22f6e0908320c0015e0800f3c9438e32da8e1ede9d4a3cca2fdd6aaa8b7cbcc9af724d2ee93bf65b9d18af0eb38a5cff0057a9431318c9f2e93a7cd15b72f373462e5d549a5afc4717a3fc6bf1959ac8b3dcc37b88bf76f716b1ddb236e558c473c91a4c854a0dae1df24280e4e01e9e0f8a3ad6b92473dfc48ed338967985b4066f946154c8d045310158c6abe7361ce49cb739ba97c3cf06cf133683e2ebad1eed47991e99e22840b6272abb06a9079a888a0929e7c280300c41edccde691e22f0daa7da2de0bcb6cef5d534e9a2d42c5e1cbdc4922ddc2ce1232a912049522525be520ad7d360f31c0e3e2a783c452ad1b26d297bc9bd6f283b4a3f35f89f198fcab1f974f971b83af869f372a94e0d425b5dc669ba72d92d24fd4f65b2f16c32332092fade5263b7636b7b77692869772bc9e53cd7560fb06f61881fe68487c0186f60f0f78c174f6458bc4e6e623857b4d66da127619141617766d667f78e0264da332a6d72c572b5f18596b4f25d5bc92dbdac6027dabe479212e7cd654859249023b904bf9a8e132e46dc7cb5a2de2bff0048457964b46479888ae6d5d564318f2e2559606395cb798a71206ee31c9f4149adba2dfe5f7bd7aa6b7ea96be6ba37566b4df46dfcb7dfd2dd353f4624f18d8394995a0673bcbc369a889626936aec963499048ca17706cc6703700dc9073aff00c7823896486ceecd92c5f2bc77f6877448edbbf7693c0e58b0071e58dfb837cc7247c43a76bba918a5f2a4926458951143ab61a46c33089769882b31601a266014e3e62c0e8def8cb5011ac21cb408f05b39b77696388a62490bc32b6c1c960c020c154f940518869c9f349ddab6f7d2de5d35f26db4d6ba1508b8fbab44bd1eba6fdf47dd7e163e9dbaf1859dd5bc42e6d359b4c4901df25a5ccaafb6539733450cb1ac4c0a2062c0b16c360024e7ddeafa4cf6af8bc8d197779a2559036c925c032092d97207553bf70e1978af9ee3f14417da818e2bb30cb1955755792df95064398c0311271b48c1190a002066ba45f12ce90801da589c3c8eacbe710550ac327ee82b9265940c18e41bd771dc481512d36776d5f4d3b696d6fb3576bcbaebaa8fc29dd2d3a7a3dbaede7bbd4f695bab2113491c704826316d91deeb631df17ca564b70aa4840157e62e54004aed07f75bfe098de178b44f835ae78d66b78a1bef19f89ee238e58d1937e95e1d8c69d6ca0b0562ab7cdaa30e00cbf04e335fcd4cfe2ab21332bc7657096aa91ca658552569176ac8c64b6f2e48dc00e048f1124215006017fea2bf615bdb49ff0065bf83b69a0c66696f3c3b25f2c28dbe4325dea57d3dc4b29672c479b236e95890704e739038f1116e36d9371befa697bb77b25a6ba79f43bb0b17ed55a2dbe593b2577a72ae8af74a4dbd3adbccfc58ff0082945b45e1dfdb37c6ed7f711a5a788747f09f8a2cd5dcfcf0cba4c3633ec0c546c7bbd22f1199092ad1f5048ae6fe0a43e28f8ab78da07c26f09f883c75ac16b649e2f0fd8dc5f5969acce44675ad69d63d2744b7da8409b56beb48d8e42b333107f593f6d0f82ffb01d8fc4dd07f682fdb6be28e976b7fe0ff0005c5e1cb3f8550789dad2df5ab68f54b9d66cb50f10691e1f78bc597d1c32decb6f6ced7fa4787962b8917559e78251e5fe46fed33ff0007207c06f827e14b8f859fb0afc11d134dd134c492db4cd4bfb2348d13468f6a184cd67a1e911ae8b6b72d833a6a52cde3586f4ca8f7d6293aca8df173e07af8dcc3119856af4f0596e26afb58e271138e16352f18fb6f633ae93c44a15253f73074b1955a5f01fa960f8f2380cb30780c3e0a78bcc70d46346695ea52a7cadaa6e70a2ddaf49435af570d04ddb99d8fd6cf871ff04f69748d37fe13cfda77e25e87e09f0ed9431df6a5a068dabdad943636ceeb2343aef8d3567b5d3f4d8db724538b285363ff00a9d4d8856addf1cffc144ff628fd917c3da968df01bc13178db55fb15edd4937846c1f45d175b974648d2e6e355f887acd9cfa9f8a5e28e5b6115fe8d65e2bb591a78a05be843653f8d3d73fe0a29fb4ff00ed25aad9fc5ef8e1f14e7d5bc11a4df1b9ff00844cdf343a75ba08fccb85d26d9a45b4b0b93ba485b4ed0b4fd134ebb91955ac233e5ede1ff682fdb32ffe258f0d5d78664b4d3344f0d7871f47f0f69faa798976c97a96c97d71a98b69a48d2e2e16ce011db457463b5897cbdd24b23cc7e830b97f0de4b0b52c356cd2bca9a7464a4f0383c44f9d4651954949e6388a364dcb9bfb35a92e5841deebc7ccb1dc419ef254cc330a785a2a57785a7055a587a764d49421cb84a55b68c5cbeb8dc5de5515acff00a8afd8a7fe0aa1f193f6edfda27e26f86fc51a469de06f85df0c7c21178ae0f0d595a58dbac9aa2f8b344b3b6bbbd677d5f5892e23b0bb9e7779bc4b716533cab2db68da6b8641f50f897e295ff8f3c61a15e5ecf25af996e6dacf4a5123436164d790416d6f1acb2fcb28b2314974dc349233bb1f9f6d7e23ff00c1b89f0f6f3e29fc4bfda9a5bad6ad203a87c36f0b5b5e5e4768d712dbcbab78a6ee779021bc7f3491a68896332c6aa0467790bc7f513a07ec29e1dd2f5eb5d69be216b3a8c76865492d5f40b35592574588959df5195a20a514aac70e06d01988af56866585ab4954af1c3e0e6a728aa186c3b8d38528da318a74e32bca525aceb549d6a8e2a552727abf83c760b150c6d7a7878d4af4f928a556ad68393aae9539b6d559c6d15cefdda7054e3ccd4124f4faafe10e896da17871d6df55b5d5e2d52faeb52fb55986fb3c527eeb4f96d55a454767827d3e7476648c99038d80835eaaf2479c6e1919e370cf208c1f5ed5e7de13f0eda78534b8741b09f50bf8ad24be3e6cf15aa3a3deea379a94a08478d42c735e4a918553840aa771e69be34f1bf85be1d68175e29f1b6b107877c3f68d1adc6ad7e2436d0b4ec23844a6dfcc705e4608b84fbcd83d463c3ab4fdad79b5294e539be4f76d292bfbaf92d7bb5f6523d4a788951a308384528c54a6f9af18b4a3ccb9968d29def2bb5d36d0f44e0a75e9dfdfae39fcbf1ae7adf4eb48b5dd43554422eafa1b2b3b86e0978ecde6fb3804f23cafb44c48c90771380739f9fdbf6c4fd9cd3c3b7fe246f89da54ba269b74965737d6ba5ebd7691dd39fddc623b7d2e6964df905591190290db80e6b5fc09fb46fc22f8a4d0ddf803c4d37886d5af6c6d5eee2d2754d3ada29efbed5f665946af65612b87169704b42922a155dec0b0ceb0c063a10a951e17131a725c92a8e8d48c3de942518ca6e0a3794946cafba5cbe7956c7e0ea3a3155a839d3709f27b583a9cd1bc5be4526d3516eedad62ddecb43df26571a8452657cbf20a63a92dbd88c76c0ea793d46063352487afb0fd7fc9a59407bcb65dc3212462bd4900ae1b033c6580fae08c9ab2d0a36772fd793cf38fd0f1fa570392fdddff00912dbb49dafd5dadd6fd1a3d4a716e55edaa559b5aff00353a6f47f3ff002b98cf21ce09fe64e3f2e3f2e08350f980f5cfb7735aed670e09e78ce3bf5ffebe3a63f2a81ad6200e7fc3fcff004cf61cd691945f7ecfa76fd2f75af4339c5c5fbcad7d774fd7fa7f7bdce6eece588ea48fcb83c7d0ff004ae61d582b027a039e7ae393ff00d7aedae6da024b7248523f1f41d3279e7d7f022b95b98946f55ce0e72738e39e47b71c1e4f3f96dc8e6925baf27adec95bcfcacbd7b614eac6954aae4f4972dad7e89767e5bdfcf46791f89ee826e50c4f272091db3d7be7a7f5e322be4ef88d70b756f7d6e4eef32395464e739181ebfaf6c639c67ecdd6ed34045926d4ef6d2dd402ced35cc4800c1c93b9c1e3a9ff001c5782f88b54f82d1330bcf10d9dccac71e559c735ebe492083e4232e7231d73d7a8eba432eaf5edc89e96d7966d6b6ea93f5d56bf7953cd70d4d5e6da6bbb8a6fbe8daff2e87f213ff051ff00034b6dafff006d450b0026b882560bcec989209ff8105e49f6ce719fe7e3c5b1cd6ba9dddb491b8059d802ac06371c60907dce071c641cf23fd143e277847f65af103337883c09378a15c9677bdb3d3eced46083b9a4d56485700f24e0f73935f36dff00857fe09eda0da4b75a97c0ff008311dc42589baf1578b3c236ccce09c06851ae0003b8db9f5e95f7983ad2865b430b5949d4a29c79d3a4938bb5972cea465a75d364b4eabc0ad98c7eb352b53a69c276767392d55af26e14ea2f9abdcff3dad6ec6e1999921948393c46e739ee303a741c7e1d6b8d1a2eab2e5e2d3750752480d1d95cbafaf0cb191d08e07418aff422d2bc77fb0a58de092c7e17fecaab0ab958e1b6bfd2f58b8621b8529a7e892b3303f7b05867a1ee7ea3f0ff00c6efd95edf4c862d33e157c075b55276883c25a8ac45f6aee287fe11b2197a00c3820715cf5f0f4db4f9aacb55f0538c97926dceddf5575df421e713d2d42925e75a6ff0543af4fb8fcd4f87b2595e5bf8d9e036e9771780ef74a905d88d95e24d734ed56c30170aca8f24b192430296843f2b26efcb7fda4b5df1178dfc7363e1059dde3b79618a4b68888ed9a281822a228c089b0149da812462c199591b3f7f7837c576fe0fd45ae75c85d746d41b51d22f4b4a854e932c10d8c9719633e3ec970eaee12446f222930a647e7c275bf04e91a47c47bab8d5de3d463b596478afd2356cda4ee67b496e1c9769a396dde164bb8c6d7003331d9bd7c2ac95a0aee29b7cdabd5b71e5beb6d6cf7beb7dcf7e924dcb4bb514d37d96ea377a5b4f2d6dbad3acf81fe02b5d1ac74e12a43069f6b0c73ea04a91bd15423860c37048cfdf5c0014b29e320fafcdafcbe20f1de9b6fa7b7970e9cd1d9da824e05bdbb0f357cbf954c6c8368651f2cb2ef048e471b73e26b4d334b167a4324519491229b9db2c41333bccd9277c8c0897972d082429c14ac0f86539b9f11a6a1146a252cead1190ed500ef967b39c9fb830d23c0db420dd8fbae179db51e4827772945cadadddd596fb277696bb6c99b46376ea3da316a2acbdd565be96bf7d3b799dff00c51d5ed7c39e2fb8f10da461b5092dc37924abc6c81708d1970a55f6805d412b22aee0cae1587907853c5175e33f14c772f2c8619ae48491662196e9db649030e810b7df420310accac92024c9f1eb539dfc5b0e8b6c1e47d49602d724143610174558dd8e4c4e49521803b9b21b6e375656a7ace89f053c2175e23d42246d62f6d5ec34a8248b0f2bcec2393509e2dd1959d033c76f70a792eec376735ace5672e69592777d92ba5f7bd92eed2ec610a77e5b24dcb48a7bb7b6b6d92eafa7ccebfe3378a62824d3b45d36545d3b4989bcf807314b7b3aa09ee86ce1fe507631c3630327927c874ef114ceb18d316479645688b1d924328460e5b6672249252b1650ab9098ce702b9af116a2fe2cf09ffc24f0968f7471c5308a42cf12c8a5d09e0480124839dea31cb1eb5c87c28f105bdbeba96975289ed44b14319427ed09b240f232853fbc323e58aa9ca8fae2b5854854a319d369ad76bdd592d2dbe9d16b7f34ce59d270aee353e2e97d534ecd357dd5aeff00a497ea87c39bebdd4fc1f7fe19bf97ed12ddf86c4d1c8eecb3a5c5abc9232c6482e3607f2c2b120edc29c822b67e04698fa3c9abea17e440b6b34cdb6e943c604472ae4611a1794120ecda0e4373c5701e11f11e9d06b5a5cd6970ad07952e9d2311e598d248565759d42f2a58852e10056cb13825ab6b57f161b2ba9f478e2684dfc8162b98c30470e7831cb1b02a98ca1dac57383b0f458a357dd945f496c9f64ada7cf77d9155e8ae6a7512b5e0d7cddafb6d64b457b5cfad2df504f115b5c5c5b2a0627646d6ec2e2de35453e66c0e7cd89172491c0eff00374ae27c3f0697a0ead73e27f10dd874d35cb585bb314b89d8658480a15f957b2e33b460e066a0f00d8cda3e953ccd7c5556db7b303cb6ef9e462fc8276920964461c0dc46457c85f17fc7fa96a9aa4da3d8dc865795d1591cc32a46bc48bbd7f74f988ff18ce4e3208cd6d195db57494acf6d969bdfb5ff00cedb2e69c2317cd7d5257f9fead777a1f4078ebf6808b51d425745204ce61b641212553380c3aec2f9191f3e49c0183cf376de37f105e206d344aea46d811cbb09259072166e3011b8dc08e170c0d7837c33f879ad7883551a85f4b24b68a5120b4bb511cde4c4a32e19c1475623b1208f98005715f75697a0786fc2ba6a5cead1c5663c9385592dde48e35196603cc2accfd41da31df39c072696dadad76f6f37adb4df5efe8cca30e65792d1b492eafd576ff87beaafe56d378a354862d3e3f3cdf332cd35b4dbf6971b5f9b94003229395048cf00ab0c8af74f873e15b7f0f08f50f114f6eb7b32799140d2ab88ddf206c6c123258e482a501c000015e2fab7c75f0c5b5fb68be14d2a637000437cf099e466604ee76c32a0000c05e30474c574de0fbed6bc4772f7faacf840ad1c651df6e3631caa11853838c6d1820e476a5ed535c97bd9ecb5d74dfbd95fd35ed62fd834b9b95474d2ff002d52d577ddbe8bba3e91bff1344a92470c6248c0c6fb791665183dc0f97a13862579c0da78aa11f8a0dbac7e5399c93c79bf2be405c2b28c87653c12370c00719ce3c5ad6e66d3efe58ed267633caaaca46e5c74762369fe1cf20649c64f1c77974609a2b75915a27c010c90a850ac3192ca0e5598e7393cf524914eed3b5debdb4dbbeba9cd5209ead2767a69bed7dd27db4ff3d3d362f12965cc822cae1b601b195baee0e986047f1eec019f406a6bdf1bdf477ba7f863c3d6c75bf15788149b1d2de6ce9fa65a823ed1ab6af3f3e4e9f6e9b9dcb8dd26ddaa09e6bc7359f11597866cbcfbb26f27f2c2dac519ff00499ae641b2181500064f3242a30a5b804907193f28ff00c14b3e3aea3fb057ec417be32fb79b4fda67f6a79a5f04f80d1180bff08f872eedcc9acead6a83125b8d234890ac33200bfdab7766ad9121009d58d0a73af564d420a52493b39492bb4b6f9db76e31de5759d2c34b135a9e1e8c53a952518ddeaa116d2e67be9677d1376bcacd26617c5dfdac3e0ef8ff00c6fe3af817e09d3749f89175e1d77d03c7df112f9208fc35ff00091c0fb753d13c21a5592247731e8f72a6de6d56e679592f1248e332491492279c7873c29a4681670e9da158db58dbf2f1dbd9c51c51966f9de494a80d24ae4b33c921676ea58e735f91bfb0ee8977a4e95a34a44925f6a5fe997d3ceecf34d2dc1334d75732b92c5a57667964762cce4b3124e6bf5b2c75ab643f668a679e7602226004aa9390cab29f94b63392091915f9a62f37ad9ad7a92af55fb1a55670a1454ad08c572ecbed49a5694da6fcd2765fb9e4fc3d81c8f0746385c341626bd184f1388b3752a4da4db949ddc62aef96117182d74dd97135098457d2cb22408b2bc50333651963382e46718621b19246001915f287c43f15da1f129814a496e801778908898f4203024646074279c8cf273ea1f19fc46ba0e87147631cd14927cac73cb67ef12472188efc0fd40f8fe1d6e1baba06f209250c0b1e031c93b8719e0fd7248c9ec2bc2c4e26a53946318b6e0d36dda49dada28bd396eda774af65b6ebe928e128ce0e55ee94d35049b5caaeaf2e65aa7dad7b7e2bbcd7fc25a478c6dad7ec5aaea3a46a913acd697ba73812c52718496071e5cd1b0003a12b91ce78e1b61278e3c34df65964b2d78c242894c725a5c3a01b433a36e8771030db5f03a8cf6b9e09f8b1f0d60d4ffb1f5f65d1afe220426f55ad639d4630f1cb2808e0e3901bd7df3f46c767e13d6a1177a5df58cd1ca55b75bcf1b901f1c70c49ebc927ea7a1af0ebe26339dabd070ecd2953693b3bc6cf6ea95a49696ba3d9c1e1aad285f0788734edcd19f2d68c9ab2b49495e2f4e8d48f97fc45e35bafb2bc7ae7866fec9e40563bb117996eca73b71730ef41ce0e1c8e7038278a3f0dbc65ade917d2de69f307b37cc13585c9175637b6ccc730ded9386825423760b26f424b4650926bde3c63e1a9ed2dbccb4950c6437eee40a51d41048da370208c6323046703935f387c41f05f8b97c3dff09b7c28b146f1569059b55f0c5ba62d7c496498de6da203cb8f504033179617cde50e581dff004391d1c64e8e2aa65b5af2a74e339d39cb967c8a51d29cd59397359ad13d5d9dec8f99e23af83557094b3bc24634eb5474e3569c1ca973b8b7fbda524daa7249a724debab56dbd9fc41a3f80bc5ec2f3c39736fe07f11dd6d336877b213e1bbd9d0138d3ae5f32e9a1cb337933bf92b9004ae783e5faff0087fc61e0d9ad22f1069b7765657655ad2f6275934ebb8d149436970ad2daca1f97c24c1d9472a48e7cbfc29e35d6359d674eb2f1c786b5df00eb6548934cf12e917da5f992385cb5acd7904704e848cabc32ba9cf5cf07f5cbf620f87da4f8daf7c6ba6fc40b08fc63e0882ce15b3d0efcbdc5a437574ede75f5aaefcc32a200a2484824f73dbec72be23c5bad1c1e2e9734e14dca739b54ea2718ddc6516ad2768b69ab5f7bbd51f0599f04612b5078ec9f131f673a8a30a29aab46d295aeaa7373d3577a465cc95b9525d3f3123f10cd0dcc1fbd8995241772c6249609a20233808fca96604b1da8839cbe4e4d6e41e2f694a7dab68451249ba7884a5492c5a35b886332b63cc60abb815daa71818af64fdbcbe0f7847e017c69b9f0b782c49ff08eeb1a1689e21b4b49dda7934a8f55f3ccfa792c44ade53424c61dbcd01d42e0118f94b54834df0959c1abfc54f1c784fe11f872f2d6deee2bbf156a6e35bbeb29e18ee237d1bc19a4c5a978b6fee6f2293ccb23fd8f63a74ccc9e7eb16b196957ef3051ad8e842786a156af3d3551c630739420ed794f954a308c6eb9a726a31b6ad2b9f99e21470b5674ab4a3170a8e9efaca4ba413b3936b5492bbbdd2b58ee25f130944cf1bcd1912310d1cfe68e5d704248a1f6b6ddc4960e3924e4e2bdfbe15fc03f8f9f18ed5f5df0c784af348f025aba8d57e27f8cefedfc07f0d74a8a362d733df78c7c51269ba2dcc76c58bc96ba75ddfdfb08c88ece690053f01ddffc1443e057c1f564fd9e3e0737c65f1b5b6efb2fc5efda4acade4f0d69d74198c17fe12f829a6cf75a22c90b12d19f1eeade3856da935bc3632fcb1fc43f1fbf6cff00dacbf6a0d41af3e35fc6ef17f886ca3021b1f0d585fcda3784f48b37c8161a5683a635bd8d869f1a6d45b08a216b1a13b635cb13deb03469abe2b174e2ff00e7ce0d471951f54dd68d4860e3092d1ce189af3a6edcd41ea8855e726b928b4b4f7ab374d74bfb893ab74fecb841496d347f44fa87c63ff82697ecad7b69a47c4df8abaa7eda5f1be4b986cd7e1e7c1fb5974ef87969ad4ae02e9773e27d46d5eeb558cddb794da99d2e4d2aea195c45a7c33c6243f706abfb497c64f177852cfc3de15d474dfd9efe1e0b4b75b0f86df05edcf86ef2dad5615586d35cf1e1927f166a7756f1168a63a55fe8562d23b94b4da2265fe273e0f5f691e06f889e17f145fda25f5be83a83eb2f010ab24f26996b3ddc38695021613c41d43b02ec8146322bf72be15fedfbf0d7c5a91594baf416974f853697ec2d6ea0e15482936c120c83b8c7b8e0641e95efe50b2a8272fabe1a15949428cb195238bc4b49464e5fbca70c2a946525ece787c1d2ad049dea3ddf879d623355154b0d56bc70ee3cd5feaa9d1836e494537072acd2516daa95a516dbb25b127fc1443c1110f851a378b02cd752e8de3d806ad753cb25cdcdd45af59ea16ed3dedddcbcd73792bde2db1696ea69242d29f9c96e7f0a7c55a04172b2bd9c7c95dd90aa31bbb2af6eaa37719c77e4d7efbfed19e2fd1fe2f7c06f899e12d2750b2d4b589f40ff00848fc3f0c130926b9d47c33716fafc76b6ca9bccb71736fa75c5ac2884bcb2dc2c43e6719fc0cd27c496174b18ba9625ca81b5ced2a79dfb973918390148ce47238af8ee368b59ad1c54dcaa46be169a9546dcdb9d19ca0d7336f48d374f4be9e87d8f035755328a985a8f96a61b1551a8bd1f2568d3a89b5bde535515f76d35ab5639bf87b7de1df0eea7ab47e326d466d38c082d34d8ae2e92dee66790fda18244e914771b5200b2100b2875cf0419acadac3c41aabdbd96ab79a46997d7d3c9696d3cb15cc7621a7ff45859ee226691a285955d836ec86209aeab54d33c29ae20f31e348d81f9d4a2042dc01c9c0e7804e18f3ea0579eeade16baf0eeb5a50d36e45c69f716b737a84932ed6b56841f981dc855242cc3fbbdb8af909ce338460b91284b99548a5ed24da4ad297c5cab64be157d55eccfaebb8269ad357addefcaddb557f853b3dbcba7f677ff0006be782355f0978f3f6abb8bdd62c356b2bef01fc2f16525b5bb41710ba6bbe2b96413e2596093cc578d8345b0ee0e181f96bfb08d39582dd2124a99d8af3d03b0048e872bce3dc57f171ff06ac78f46abf13bf6abf0e5f3bc1259fc39f879a8299583c32c36fe21d7a06961931868996e932cc385c03b7041fed3a7921b6b692688ab6cb71704af2aea87cc2c08e1b700d923af18a54d354e105069ce5a7da5ad56fe2db64b6d55eccf16bb5f5cc555738b8d3506f5b49ffb2d25f05f9b4b5aced76bdd32b4f9156f6f22dc19965933ce48c1e73e99ebce073d49e47cbdfb75e8d71aff00ecbbf156c2cece7bcbcb7f0d4faa5adbc09e6cf2c9a54b1df621442cd23ec81ce00c900e39af6ad2b5b6b8f16dedbc512c704925cab36e25ddd4921867a0239c0c81bbb76e4be3b69f79aef869f46b7bd96ca3bc8a7b7b87b7660ed6ee02488083804a3104b21ec39e457a90c34a18ca0a5250baa751ca5af2b515f159f75aa4defae87892c646583abece2e7ab8a4b6945ca325ac95f48db5d2dd2fb1fcccf8d75a83c3df09b4af07dbbdcadfdfff00c22bacdf116f244ab713f85f4cb9303cadfb99a477b9f39923c98b76f604e557f417f64079bc1be07b479609eda1bf7b0bb0d05bb8964b8b0b3d49edd821c91f6837ff0026d254e14a9e84fc9ffb44f8962f0dfc60baf85d61e12d2ef57c3d71a45b1bebcb4d42eae6eedef741d22e607531ddac0ec8ceb0b816d8f30050b8e07ec57eceff0006fc43a1f81bc157de2c304716bfa688e3d096cd226d3e09ed96589d9e4df3432b43bff711b011f5209c81f758ba91c365b4bdaca3c95fd8ca2a4d45cdd2873a708a72be915369c52d3596899f2183a752be2eb72c1f345cdcdc573282a9349f33b452579f2a69bd6d65dbef8b68035d58dec51b6d9f4f71248ccd95de6de48d42b1e0b1dd9c631b486c6456d1c8ea0f5f7e7af5e7d79fc4906a91529e400d858d028014631b40c720f65c67b76c7694bb9fe223f01fd41afcc27cd3e56da768d93f7af6bc9abdefd1a5d15ac8fd3684a9d2f6915192e69aa8d7b964e54e9c5d926b7716f5bbbb7adb452390063d7a7f8fe1546791546dea4f6e38e3bf7eff00ca96691b1c4a73dc61381edf27ebfae71593392d9dd71267bf2838effc0467e9c63a75ad29d3b59beb6dafbf4e9b7a1cf88c4293692d9689f2aedd54bbfe96dce33e226bde20f0f78475dd5bc27e1f4f15788ac6ca6b8d274097505d2e2d4eed06e8ed5efde1b85b7121e04a617c7195c1afc56f1d7ed61fb7c437f7e96dfb0b78eb58ccd2053a6fc43f0cc5a715dc76885def2de468b69003342acc06e6519c57ede5eac6cac1a699867fbf85cf5ec00e78fc7bd723771423764f383f79cfb0c8e7d38fae6bddcbf114b0ca4a783a388949a6a755d64e3b7babd9d582b5f57a5df7b687818c8d5ab3528d795256b38250926ef7e6b38defaf7b69a2eaff009f5d57f6a0ff00828c4a6436dff04e0d76e0b6e21af7e23f86999bd37112b9e49c91938f5c5798ea7fb447fc1502eb7adb7fc135b4f01b38fb5fc44d1c80bdb21265c9c139c60673d715fd155ecc232ccb8c0e7876ebedcfb1c71dbea2b9b9f59913385191ce49627031cf2718ebd7af15edd3cc53578e5f848f9debcad6b2de55657f9fdd6679d2a5cb652af26efd611df4b37aafcedb5fb3fe71efbe2dff00c14fb505c5cffc1313c05799049fed2f1ae857498eb8293cb28272307e5c9cf4c567dbfc4bff0082a1598dd61ff04bbf82f65275461aef84805ee376d45627b91b874e99afe8be5f13489d4a8c63a9278f519cf5f4e6b2e7f143affcb48d41e39231fae700ff009e4d6cb1d5dab2c2e192eab96567b6e9cb46dbf34ff38e4a4adefbdb7e56afe7f11fcf3dc7c6dff82cac607f607ec0bf07346d9ca635fd053691800830df201eb803927d8e718fc78ff82ef124a7ec8ff09615c9db18f11e94020ec303553fcebfa1d9fc581724cb19e98c127bf38c211f8f6e7d6b265f1702e70dff008ee7b93dc29fc314a58ac4377fab6112bad1d0a52fe55f6a94b5dfafe7a8dd356fde4fa6aa535b34ac92abd7a75fc6dfca01f12c97d6845c4cf2ac7e6204bb2659de3d41ee64f2ddcc91cd1306b68f11b4926085f9033835767d5bfb4acacc48218af742b686cd1d73fe95a2a11fd9dfc30c825b58258ed1dd63055046b3b3189b3e36756b39a26694c53792b676e932ab99475666192b3218c3cbb88173825b614c926aea9a85fdbcb6f756e12d99a7483cdcb4fbad3c989a72ad07efc4597789e4bbdccd16f528b9c57875209c249b7aad2fab5ac7f26ba77b1f6b49d9c64924d3f93d9efa6eafdecdb7a687a0daeaaaf2cb15b4ad6b6f28786ee0b801a1911a426474e891cac4bab4b118c9121c44edf30f77f85434f58249ada471be6755866cee8db6959248e6f9bcc89800aac84eedad1b29232bf125a7881a19eeaeb5088bda16672209d64091afef9da541b71b4152cea119bcc40a7ef01ea3e0cf8dde1bd203595bdc4125adbcb262391c472c7b154b490331578e4661bfcb708642a728d92f5c1092a352f55d92b72b6b496c95b44ae936dbd1efa2d8eb9c5558354ecdbd1aed7b369a7d6da25b3bee7d69e2bff842b49d6751f1878cb52b248adacd2f3ecb249146f7f1a46cc9140263b56e618822b29387c9c8319cafe33fc6ef8cfacfc66f88b750d9eac60f0f41790d96936c1ed63860b4b79c2c68c70c095443907ae40ddc1ae8bf6aff008fb6facea73695a45cdedc5b8b74424878a08c3a888b918c3154c8280b85c0dadb7207c4fa25d4f14f6d2431cb31bc9fcc593cc58961024de159494da7248c9ce72727bd7958ecc6539ba349a54d4bde9277f68d59d9cbcba2bfadd9d385c1c609549f339b56d7685d2ba4bbfaeb64ba1fac1f0a6f638b491e1ed42e85c595f59bdb12fe5ec8ee20899e225e2c280ec396603200041ddc79e68bb3c3be3f963b95ff004386791d1e262db4c8f946c04c708a381f28ddd08e95fe12f8811acb4d33c325bcd04aa250ed1cbb9b6940c87e70ea50118048e48c03c9d6f887a71b5bc5d52cb7c124f323c8c712432228496470470a08c22aaf24b0031915d997e217272cacf99a76bbb2daf6d5bbdaca5d6ff872e3a8733528a5cd4f46ddb55a5badfb5b7fd4fb63c13aa6cfecbbfb3d484d29b87761707cc29ba31e7a90782b99153660040a7bd7b6e953d86a1ac4978d712dace8559d257ff4363bb7191518040995055d55980e80649af8c3e1beab2c5a3e9d3c8caef0a1b9948c0612489ba4c26f3bb682a02ee3950483c73dfde7c475b2d3f648d192cc023a6e7662c5011202cae80ab10323b74ae953509c9eb6725beeed6b2fc2d75dbd5bc6549d484637d52dede9d174d17f4cfb2b57f1d4f6565776c619444c9b0ddd8ca2e10e461da48836e184fbbd39c0da00af02d334c7d5f5f79f7c3770cb3a79665deb2187766525582c88e4f05816423031f29c798e85e3dfb45ac93d9dfec0b74405331907c9b15a3f225c3e08ddb8eedb92702baa93c752ad9c11fd911cadbb07bad35d619e3672242eca486620b119da4f039da6b658a5bb575b7bba595d2fbdfaae8d69b7354c1b6a2935a34da92b745d9f4f2b5f5bf43ed9b2f1a7873c11a0c76e0225d84da2389e39dd79c05689f6950cc09f95707039c0c1f91be28fc63d53c4faade69f65751a46123b58d76cb000923b019cb6ccec241da37003279e2bc3fc55e30b99de0961bad4e485b6891ee0ab9730c5cbc71e434bb1d9919a35201ce30471e43a2eb577ac5c6a57fa65fdbddc305d2432cad3c51c4ae8bbb64925c4a10ccca4218957cc00fdd1d6b45514b5528c6365ccb995d6ba73797ddaedaea61ec9c34e593927bf2f6b376d1bba6b74eff7b47e84fc26b3b4d3648e7b90df68bc0d2cd726437281780bb1580741d0ee019475cf183f646877d656d6045bcb04c4c47718c468c923f3bd483f3360f3943c839cf4afcbbf087c4b92e05c69b676cf26a1a75b225cc36ce82480fcad1a91ba3c89d0e622432c8bf7770f987afdbfc5ed960979a69bad42e6d67586fed21564d42ce5418960b880fcdbe33f380b92c00c02a45631a9ecdb4f95ae6bdd35b6967bea9a5b9b4e8aa8a2efd2ed59ebeaf656bdad75aee7dcba15b4935cdc5c79cbfb82d2cab70440ce3f8631bb28f93920a302467a6401d28d6e1b58ee2f35189a358637d81d58a10a5b0415e546d1c10c4704f6af987c35f19935dd32c16dcc3716f7f262e19b72cb1988ec78df038906dc152a08604100026a4f88be2e7b0f0ec42df509208757b95b4581e425042c7e731ee271f29c36dc72ddb935d94a71ace292bb72515a59eebaf4b5eedad123cec453742329cdae58c5becdd979d9b727a6ab7eba59fd17f007498be2dfc58d2351d52379745b1d6224d3ece5c3c2c639016b9c7f1e4a9f28b2fca08e8726bf9b5ff83823f684bcf8f9ff00052c4f847a65db4be0dfd9eb40f0efc3ed1ec6290bdac7aeea715b6bbe25bd5857f7626916e74cb290e0b62c02920715fd2efec8fade9da46b7a24d14b1822e220305786652147d093d8f527a138afe2ff00f69e8753f15ffc149bf695d4f5249a5bff00f85cde2bb9b96983ee5822ba11d9e59fe6005a25bf978e0280178c0af238af11f55c0ce49da10518277d2dece551f95e538c75ec9aee7b3c1185fae6674e2d5e739c64d6b7b39d38ab5b5b46329f65addf53f4bbe096976be1ed134bb5b22449359dbacb70001b898d3091ae4158c0cee1dcfb633f5a787f53b6d321f2efaea08d55832b4d34711249ce46e6cfae4038eb8ef5f15fc3c4bd86cad1ddc8f2a18f0aac5800806d6001dc0f232a48e7a6057a4ea9756cd641e59cca583160599a459413f2900eedbdc640e7a83935f90e0f16e9a94b96fcd26ef27a36df5b69f7d969d8fe80c461a326a1cd6b256e5b5f9572bb2d6eedb2f9eb7dfa0f8cbe38d32f254b28eeade781082c5644931ee1813db39273f53806bc534cbfd08cc1629e379197a1914b6e27a8563cfa1c0ce7bfa79678bd277bd696d8c9b0965c48e7675382b9527af63f87b791dff85ae2f2f23bb4f136aba35e5b379f14fa6cea55587ce3cd865468e58f23e746c6464061d6b49f257729ceb38aa8b549392bae54972c7549e967befb99bbd1508c694aa72b8ade317caed777969769edd5e8df53eaff001378723d4b4c64b9d1edf5281c3955960498ae41e577a9287078da411d88ede5f0781750d3ac4de784bc51adf842f548c5b43793cda79753908f6574f2204ddc7eee48f007000a7784fe26f8e3c3d6cb6de205b5f17e91128c6a1611adb6a8918c0df2d939314e401c88240cd8e1390074da8fc4bf04f882d37e8ba824974093776241b5bdb590e372cd0498910a9ce77019ec7b9e5a34e53a8a8ca0e5173494946528eb6de32566b5d9c57cce8abc90a4b154312a9ce31bca9f3aa734d24f64f5d55b46f75a9ea5f0f6cbe3ddff0083b5ad7750bad2bc6ba5f86809757d32c23bc8f5e8b4a208fed4b488a3dbdd44aa1b7a5bcc64428ca5062bd43c0be29b096ce3bdd326cd8dc2ee78a47fdec2e7ef86279520e55d5b9521b38e6be86ff8272dc6b5aa6bde3dbbb4d32df5cd274cf0be9bf6b86eed9a5b12cf777120b19648b84377024b1390ea4060c78cd7cadfb79dde8ff0c7e2b43e3af86de0fbff0002f847c6774f0f89bc1cc2fa4d2f48d78825f50d36e2e6d2d2082d755c9916dadd0c28eace8cecccedfa451e16c46132e967794387b2c153a4f33c3bad4e139c27285355e9d1954f6928a9cd466a942505672b42d697e51538d29e659cd6e1acf1caa4b1339bca314a8ce4e15234bda3c3d7a91a6a9a7c9172a53a928d477e46ea5d35f7b7eca77bae7c48d73c63e0ff0015e95a07c4ff0087fa32e9b79ff08cf89fcabdd66c05fb4e1db40d46fece622ca2109cd9c9a84096eec896f184231fa77e0bfd9cfe09e89343a9fc20d6f59f863e20bb8337da25e79d1e98971b895823b1d4965b29046e72af6b2794dd572a4d7e2e7fc1377e276b3e1cb8f1cf8ab4ad3af3c52574d81b50b1d3b4ebad66ea3b7694240896b651cd70f22b92551632546e2460647ea9a7ed49e37bbc88fe0d78da68ce3e43f0dbc5320e49c7ca74a75c8cf518fcc035f7b94707ae25cae9e60aa60a94db9d0e7c562634ead3e49a9ae484e3cd18f2b51528c9271e65a6b7fc878938a717c29c438cc1d058c74e70a55397094aab84e32846325395392a73939c64dc6a464d593f4f9e3f6b1ff8269fc5ff008929adf8abc13f186faf7c7bad3b4926bbe26b3d26d5a38b61586d34fd4f4db648e0b481484823b99dd563da91a458047e1ff8a7fe0851fb5cde6a13eafadea93ebd7b2c8de66ab25d681ad4f72ccccf9173a878bdae7cbe5b69d891a7cabb54955afe9534ff00da7fe2ae9d7226d3fe13fc5448188dd611fc38f194b66ebc642c0da3caaa79c61481e801af7ff027ed1971e299e2b4d6fe0d7c65f0cea333006fed7e18f8e2decddc90374a7fb09ec9b9e71360119ed5ef62b0d9ee5385861f17432ecd72fa36708e0b17ece3494525cdf54c3d7a10752cdf34fd8c5b7772a8db6dfcf61738c3e65567570d89c760b15564f9963f0529f3b959f2ac44a94a5cb7b351751e9649248fe3a751ff008226fed4f628d8d1bc4f7810838b0b7f0402d80c3e5f37c5ec71f3608e0807214e083c65e7fc1217f6a1b3244bf0dfe2a5d8230ed64fe01855b6861918d4af4638000c30c9390bd6bfbbbd525ba9e24649e6d3ee278f74316b1a44ba06a6411c66db52b68629dbb103c927d39af9c7e2378cbe2c7826de7d42c3497f11dac4aee61d36c11f55d832408acd6390ddb607ddb52f9380a0e701e5f5f29c7b8d293c0602b4b6a78dab8da0dbf7748d5a939d096fa5aaebdb7b678ecc339c0a7374eb6329477a985a387aa96d794a31ab4ea2f9c16d77e7fc63587fc12dbe3ee93aad95cea7f06be3c490db5dc524d2dbea5e0721630ea64c41ff0008f6a092aecde8f1b164650ca03ab73f71699ff048ff000f6bfa769dab5f7c3bf8c11dc18a39992397c3ba45fab921ca5d7d83c1aafe6236530b9e10fccc0827f7087ed67f196619b5f815f1eaea22ccab35afc14f8812dbb952c3f77711f860c322e790cb232b03c31cf3149fb53fed04cc3ecdfb3ffed044f6ff008b45e36b7c71f7733e8908e3a609007b015f431c930d04d3c66496934da9e354937eed9a4e6b5fbf456f5f127c4998d57192c0e6c9a4d274f053868ed6d555968eddedfafe2b78dff604d43c05e1b8753f0b7c31f8d779ace9779636fa55b5a7892db4e91c99559ee2eaf346f05dadf4ab1431c8c1cdd9944aa81b03afc57e28fd81b5bd6350bad6f51fd9dfe293ea177737d753a43aedc598334851d9a4361e11b596759880630d3dc387691d8c61a495bfa7bff868cfda6b501b61fd9d3e3ccc4e4812f80b54b6e471d6ee3831f8e0fd3bd597e307ed7777936bfb30fc69909e866d1f4db33d38cfdb357808000e73b71e82b3c464581af655734c9234f47ece388a138b6b4bda72936ecdabf45a5b735c2f12e6986bfb1cab32f68ddbda4a9d5a73b3e57ca9c6a256baeaaedddb6d6dfcb8691fb02f886fee1eda3fd9afc6d0245890b6abe23f19e0b8640a9bada2b58f71cb9dc76a809bb0410a7d1745fd86357b3b88e0b8fd99b58b84b33381e6dffc50d458090279a13ec9e24b757591563cb464617712abb4ad7f49317c41fdb6ee48307ecbff0015541fba66d43c1b6c39f513f8ba260727272a7ae307815d0d9eb1fb73de0de7f67af1ad981863f6cf14f81220b9380085f1849c9efc67f3ae57c3792a4af9a64b1b3d1a586959e89ebcdaebb59dd5b6b9d6b8b38824b5cbb34936ff00e7f5649ad2cb96eecbbbbbf2f3f0eff821bfc1797e09fc74f1e59699f02b53f86b67e29f86b716775ac5de95e2cb5b1be96c75ed2e7b7b59ee7c4fabdf6f117daa7ba8e1b710ca4a12a5a357c7f53b2eb015dac3ec2d3e34c58a668ae2d22839464da864b9dc77e4ec037305c927915f913fb295c7ed2d6ff10f50b8f8b3f0f356f09f863fe114d40c37d71e27d06f99f568f54d126b6b55b7d2758bf9c35c5aa5f0129458d021deeb91bbd9bc57f0f3f6a2d5b59d5efbc37e218e7d13fb6ef2e2ded2cf521a7dccb62f317b35688d9db2bbb4050ee1397ddbc32f00d7cf63b23c14b1ce8d3cd72d54214612856949c294aa49fbd183a52945ca16d53945b7257d2e7ab83e21c7c30aebd5cb33075ead76aa52828d5aaa108c23173f6a94b924aeaeb992b5923ed7fb769d657cd7c34c36773e7611ee7528a3572c8a188548e639c7040c9ef8c1a8bc45e30f0dced6ebab5df87ec5225c47f6fd623442cc410ede7476619720641906ec6030e6bf36bc79e12fda4b4fbad34bea5ae58cdab4b359451c3ad5d5cc7692da62469eede00e9682512feed90319832672ca0d79ceabe18fda762d466b4d45752f117d8228636b8b9b2b4d4911dca3450c336b364933320937b49013144bb8c922e3234870ee1eb385579e65aa493b73d6a89dbdd8e936ea45abb695dbf2dd99cf89abd153a4b25cc1c5c95d428d3d5ad568b926b4e5ba4eebaec7e8c8d27e06c3abcbe28d5aff00e0e49abdcdca4f2ebbaa5c7872e35232468ab1b3cb25eac91c712c291409193e5c5122e33927d9f42f893e0bf145dc16ba1f8c340d765d3e1774834096de68d54058599806b978cc6a76c6a92c395620ab0c11f911e17fd9d3f683f882bab6af27c3af0ddcb3ea2224bbf133f866d6f2e447689fbc6b5bbb669dadd0e6d95955a224a88d8c62464fa87f65ef847f157e1ef8a350d4be20783adfc31a65ae8fa8da69b696fa968cf6f71aabddc01ae45a69b752abc2b6715c18ee250ac7cc45854ee24639965596d2c3ca4f3bc3e331542318c70f4eb509494a692b462d39a8dafcd25caadd13b23a7059be67394153c8f1583c3e21a73af569575071835aca717185e3277517cd7969cccfd2c70c5148e090adcfb8efefc90463afd335033ba83c8e9c75fe44e3eb9f5ebe8d375198636df9dc88738f5518f41f876e3e95424bc8f925f23f2cf3d0e7d33d3a7f4f868424f78dfcacefadadfd75d74d4fb6af5a1cda4ecdc637f7bb25e6f55f7f9b49dd6591f1d49e3faf4c0f7c13587772b8046481f5f63c676f6fff005127a4b73a942b901c7439e703a761d79f6fafa573d75a84249fde0ffbe87ae3a60e71d71db9e9dbaa9c249fc3dad7575dfa74d8e0a9579be19377b75bfcfaa7f7bfd0a77b3b0dc793918001c0ee0f39c9c81d3df00f35c75f5c1c9daa72339cb9c027aff1ff00f5f9f526ba0babb81b3fbe1f98c01fd38ee4f4ce7a572f7925bfcc7cd392723047b9e0f1dba1232338f6aefa7a5f4eddff00e1bb6dafe060efebeace5752b90aa772e78e858f503a01bbf1e4fd0e38af3cd4b52d85b6a1c1c80770f4e0ed393c8ce4f5e9d715e817f25b60e243dfebf5e477fcb3d73922b80d54d9e4962c464f66c678e4f7ea78fcf8c9af4283578a69b5a3d1ff00877dbeff00c11c559b4bceef7ef75ebaf5fc4e1aef57756270a0e095e18b1f6fbbc9ec719cf200ce0d605ceb9280703d3fbe377518cf964107be3b8edd2b5f517b2f986f618071d78ebce491d474ed8fc0572b3c9a70382ec48cf040f9b9e00e460fe59c126bd356d3b696b69fd6879f294aed737a697f97c979e96209f5f74e76eeee00ddc75f5418e33db39e71d2b29fc453863885c83cf4cf3dfb0fd47e3535cde58e085439cf3c292460e01e7e9c803a738ac296f23de76ee03fdd07f5ee3d3db8ed55a7677df7e9a797e8b7ea4394975d6ebbdb75a37aefe9f2dcfe49f55256d4cea592537c559d0942c1e18ddb70520312cccd9209049208adc12cb2d9e9de64b2b7996f3f983cc701f734c1b700c01c803208a28af11feabf347e831f817a7ff207177aed3c112cb890196de33b957ee5cccab3ae7190251c360f60460818f01f88f1a1f1368b06d1e5cffdb524d8e1e474b0bcf2cbca31291198e368d77ed8d915902b0068a2b8eba4e324d26b961a3575ab573a20da74da6d3d754ecfec75f9b3c37c6369020d462085d21822f2fce9249d977e777cf33c8e73fed31c76c5647c3cfdf69970b296711dd3aa06663b4165c81ce71c71e9db1934515f27884962a69249733d12497d8e8b43dea1ad28b7ab70776f7da3d4fb33c1aef069f68626287cd879ce4f2221ceece782460d7bc19e5bed27cabc6fb4c6d1da65655571967803104ae5491c6e520fa1a28aedc0fc51f93f9de3afe2fef392beb177d7dd5bfac0f5278934ed2aca3b25fb3a08170a849ff58c379cb1639381ce720640c0241f3ebf9a4bb64fb437987ed13f38553c49c64a05271818c9e3b51457a15f4946da7fc3c0c687c2be5f9c4a368cc2d2401880b77228c120ed21c9048c16c9eec49f7aebf4dbebb68b734ee5913cb53f2e4205501781cf0072724e3934515841bb6fb3d3f02ac9ee8f1bf154f3c9e2cb677b8b82d047651c58b8994227996df285570a4727a83d4fa9cf176c8b178a3c7f6916e8ad9bc5b15c9861778944f71a7d834d2af96ca51e462598a15e492304d1456b14b9e868bde4efa6feeadfbfcce19ef2ff00b77ff4a81edf683ecda18bc819e2bbb8b7b08e7b9491d67992111794b24a1b7b88f7305dcc480c4038245740f2ca9616f7c92cab7775158dc5c5c2cb22c934f1c2b0a4ae430dd208911776327682d96e68a29d48c799e8be27d17645d36f5d5e964b5e9dbf05f71ebff06353bf3e2ef1869ad752b58a5e585cc76cc43451cf7ba75acd7524618128d34aef23ed206f62c0026ac7ed35af6b1a66a1f0eec2c750b8b7b491ef247815832338daa189915db214607cd8fc68a2bd4cb778f939a5e4acb45d8f2b34fe13f38d3bf9fc3bf7ddfde7d25fb3478935c5b8d25d752b80cb7106d236646240063e4f4e2bf0c7f6b1d3ecacbfe0a55fb4235adbc709bcd6748bdb92a0fef6eaeb48b4371390c480f29552db42ae4640049c94578bc7497f61567657f6b475ff00c18bf26d7cd9ef7873a711616da5e8d5bfdd41fe7afaea7d13a348f6d636e60631168d0314c0c8dca3f913cf5e73d6af5ffcb18c13f3484312c49394c9c92493cd1457e1f49be45abfbdf647f43544bdac9d95edbd95fe0ee78cf89e79a34ba5472a002400075e79e41e6bc1bc4779751d9486399d0c81439520120f38c8190324f008e38e9c51457b78449d4a29abae65a3ff00b70f2b12daa72b36bddfcdebf78783f54d40a006ee560b30401886017038c10463f0fe55e2ff001b350bcd33e34f83e4d3a76b26d47428bedc6d82442ef6dc10a675550b21038048ce38ce28a2bec72e843d9e3bdc8e90a56f7569efaf23e1b3a9497d41a949378969b4da6d72c746d3d5796c7f5b5ff048db48f42f80171aae8f2dee9ba878a75991b5fb9b4d42fe16d48d939b7b5f3516e7cb8c430e515604881058b066624fd5bfb7df807c29e3dfd953e2cc5e30d366f1047a368171ae6969a86abac3ad8eada7a34d677d004d4102cd048032e728dd1d597228a2bf6bcba953ff0057e92f670b4f2bb4d7246d252a4f99495bde52bbbdef7bbbee7e138da953fd62a93e79f347364e32e697345c6bc395c5dee9c6cb96cf4b2b1f207fc10cf40d26c7c1be22d56ced3ec9a949770249796f3dcc33c893206916478e652e18aaf0f903002e0715fd45698ccb67060e4f92096701e427d5a470cec7d4b3127bd1457ce452fecdcb95959c75d37d227b59e6b9de3dbd5f353577bdbd9c1dbd2fa98b7fad6a704b1ac374514c854811407e5c1e3262247d4107deaee9faa6a12ee2f773310fc7cc1475c7450074f6a28aa74e9fb34f9217efcb1bfd9eb63c0552a734573ceddb9a56de3e658d52c2c7c4166f63adda5b6a96b2a32bc3790c7329183f74b2ee461d9919581e41cd7c01f1c34db3f05eb3059786a3934db3b8936cb686e2e6f6d8a927e5483519aee2897da25403b0a28a5463193a94e518b87237c8d270bdb7e56ad7f3b5ccb1ba4294d693e74b9d7c56e65a736f6f2b9d4fc07f1f78c61f110d013c417eda3b5a898584cd1dc4113e40ff0047f3e391ed9304e23b778a319e12bedb4d575171f35dcc720f4217a1ff00640a28acf0b4e9ba1770837ed24aee316ecb657b5ecbb6c67ed6aa9b8aab512518b4b9e564ed1d52bd8ea2c269258b3239724024b609ce077eb5b2228d946e456fa8cf7f7fa5145797884a3526a2925e5a7f2763dfc3fbd4a0e5abb2d5eaf65d5929b3b53d6da13f58d0ff003068fb1598ce2da019eb8893fc28a2b9b9a5fccfef676f243f923ff80aff00222fb2db0031044320e708a3b9f6f6156610154ed0a3e63d00ec78edef4514db6d2bb6f57f9214528f2d925eef4497f2f638ef155ddc5b244f0baa3199d77797131c61781bd1b1f862bc2fe206b3aaa5ca451dfdcc51b5bc2ccb0c8610c5932c498b61393db38a28af5b06972537657b4f5b2beeba9e363a5257b49abb57d5f64731e13d6f58b59835bea97f11624315bb9f9073c105c8c7b62bd8b4150f279ef979a5ded24aeccceece77396662492cc4b1279c9268a2bb71118aa69a8a4dd936924da56b26f7673516da826db4a52693d55ef1d6cfa9e9a64716d1618fdc1ffa0d63cb34ad1e4b927206781c648ec3d28a2bc7a29765acddfef3b6aef1ff00b87ffa4a39bbc964c31dc78fa7b8f4ae6aee47ebb8ff00fa8647e47a514574a4aef45a3d34f24065c9239539627ffae79ac5ba66cb7cc7a9ee7d475f5a28ad23f12feba09feabf16ae71da948e0901db186efee3fc4d79f6ab34a376246eddfebfe03f5f53928af4282575a2fea47156f87faee8f3dd465909625c939ebf813fcc0ff24d723348e18f3d9cf41d73f4a28af49257dbed3ffd2a279d2ddfabfccc7bb96446628c54e474c0efd3a74f6ac979a52c73237e7ed4515765d97dc89693dd27ea91ffd9, 'employee.jpg', 'image/jpeg', '86096', '200', '133');

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_reportto`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_reportto` (
  `erep_sup_emp_number` int(7) NOT NULL DEFAULT '0',
  `erep_sub_emp_number` int(7) NOT NULL DEFAULT '0',
  `erep_reporting_mode` int(7) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_skill`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_skill` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `skill_id` int(11) NOT NULL,
  `years_of_exp` decimal(2,0) DEFAULT NULL,
  `comments` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_us_tax`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_us_tax` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `tax_federal_status` varchar(13) DEFAULT NULL,
  `tax_federal_exceptions` int(2) DEFAULT '0',
  `tax_state` varchar(13) DEFAULT NULL,
  `tax_state_status` varchar(13) DEFAULT NULL,
  `tax_state_exceptions` int(2) DEFAULT '0',
  `tax_unemp_state` varchar(13) DEFAULT NULL,
  `tax_work_state` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_emp_work_experience`
--

CREATE TABLE IF NOT EXISTS `hs_hr_emp_work_experience` (
  `emp_number` int(7) NOT NULL DEFAULT '0',
  `eexp_seqno` decimal(10,0) NOT NULL DEFAULT '0',
  `eexp_employer` varchar(100) DEFAULT NULL,
  `eexp_jobtit` varchar(120) DEFAULT NULL,
  `eexp_from_date` datetime DEFAULT NULL,
  `eexp_to_date` datetime DEFAULT NULL,
  `eexp_comments` varchar(200) DEFAULT NULL,
  `eexp_internal` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_jobtit_empstat`
--

CREATE TABLE IF NOT EXISTS `hs_hr_jobtit_empstat` (
  `jobtit_code` int(7) NOT NULL,
  `estat_code` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_mailnotifications`
--

CREATE TABLE IF NOT EXISTS `hs_hr_mailnotifications` (
  `user_id` int(20) NOT NULL,
  `notification_type_id` int(11) NOT NULL,
  `status` int(2) NOT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_module`
--

CREATE TABLE IF NOT EXISTS `hs_hr_module` (
  `mod_id` varchar(36) NOT NULL DEFAULT '',
  `name` varchar(45) DEFAULT NULL,
  `owner` varchar(45) DEFAULT NULL,
  `owner_email` varchar(100) DEFAULT NULL,
  `version` varchar(36) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_module`
--

INSERT INTO `hs_hr_module` (`mod_id`, `name`, `owner`, `owner_email`, `version`, `description`) VALUES
('MOD001', 'Admin', 'OrangeHRM', 'info@orangehrm.com', 'VER001', 'HR Admin'),
('MOD002', 'PIM', 'OrangeHRM', 'info@orangehrm.com', 'VER001', 'HR Functions'),
('MOD004', 'Report', 'OrangeHRM', 'info@orangehrm.com', 'VER001', 'Reporting'),
('MOD005', 'Leave', 'OrangeHRM', 'info@orangehrm.com', 'VER001', 'Leave Tracking'),
('MOD006', 'Time', 'OrangeHRM', 'info@orangehrm.com', 'VER001', 'Time Tracking'),
('MOD007', 'Benefits', 'OrangeHRM', 'info@orangehrm.com', 'VER001', 'Benefits Tracking'),
('MOD008', 'Recruitment', 'OrangeHRM', 'info@orangehrm.com', 'VER001', 'Recruitment'),
('MOD009', 'Performance', 'OrangeHRM', 'info@orangehrm.com', 'VER001', 'Performance'),
('MOD010', 'Payroll', 'OrangeHRM', 'info@orangehrm.com 	', 'VER001', 'Payroll Module');

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_payperiod`
--

CREATE TABLE IF NOT EXISTS `hs_hr_payperiod` (
  `payperiod_code` varchar(13) NOT NULL DEFAULT '',
  `payperiod_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_payperiod`
--

INSERT INTO `hs_hr_payperiod` (`payperiod_code`, `payperiod_name`) VALUES
('1', 'Weekly'),
('2', 'Bi Weekly'),
('3', 'Semi Monthly'),
('4', 'Monthly'),
('5', 'Monthly on first pay of month.'),
('6', 'Hourly');

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_pay_period`
--

CREATE TABLE IF NOT EXISTS `hs_hr_pay_period` (
  `id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `close_date` date NOT NULL,
  `check_date` date NOT NULL,
  `timesheet_aproval_due_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_province`
--

CREATE TABLE IF NOT EXISTS `hs_hr_province` (
`id` int(11) NOT NULL,
  `province_name` varchar(40) NOT NULL DEFAULT '',
  `province_code` char(2) NOT NULL DEFAULT '',
  `cou_code` char(2) NOT NULL DEFAULT 'us'
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_province`
--

INSERT INTO `hs_hr_province` (`id`, `province_name`, `province_code`, `cou_code`) VALUES
(1, 'Alaska', 'AK', 'US'),
(2, 'Alabama', 'AL', 'US'),
(3, 'American Samoa', 'AS', 'US'),
(4, 'Arizona', 'AZ', 'US'),
(5, 'Arkansas', 'AR', 'US'),
(6, 'California', 'CA', 'US'),
(7, 'Colorado', 'CO', 'US'),
(8, 'Connecticut', 'CT', 'US'),
(9, 'Delaware', 'DE', 'US'),
(10, 'District of Columbia', 'DC', 'US'),
(11, 'Federated States of Micronesia', 'FM', 'US'),
(12, 'Florida', 'FL', 'US'),
(13, 'Georgia', 'GA', 'US'),
(14, 'Guam', 'GU', 'US'),
(15, 'Hawaii', 'HI', 'US'),
(16, 'Idaho', 'ID', 'US'),
(17, 'Illinois', 'IL', 'US'),
(18, 'Indiana', 'IN', 'US'),
(19, 'Iowa', 'IA', 'US'),
(20, 'Kansas', 'KS', 'US'),
(21, 'Kentucky', 'KY', 'US'),
(22, 'Louisiana', 'LA', 'US'),
(23, 'Maine', 'ME', 'US'),
(24, 'Marshall Islands', 'MH', 'US'),
(25, 'Maryland', 'MD', 'US'),
(26, 'Massachusetts', 'MA', 'US'),
(27, 'Michigan', 'MI', 'US'),
(28, 'Minnesota', 'MN', 'US'),
(29, 'Mississippi', 'MS', 'US'),
(30, 'Missouri', 'MO', 'US'),
(31, 'Montana', 'MT', 'US'),
(32, 'Nebraska', 'NE', 'US'),
(33, 'Nevada', 'NV', 'US'),
(34, 'New Hampshire', 'NH', 'US'),
(35, 'New Jersey', 'NJ', 'US'),
(36, 'New Mexico', 'NM', 'US'),
(37, 'New York', 'NY', 'US'),
(38, 'North Carolina', 'NC', 'US'),
(39, 'North Dakota', 'ND', 'US'),
(40, 'Northern Mariana Islands', 'MP', 'US'),
(41, 'Ohio', 'OH', 'US'),
(42, 'Oklahoma', 'OK', 'US'),
(43, 'Oregon', 'OR', 'US'),
(44, 'Palau', 'PW', 'US'),
(45, 'Pennsylvania', 'PA', 'US'),
(46, 'Puerto Rico', 'PR', 'US'),
(47, 'Rhode Island', 'RI', 'US'),
(48, 'South Carolina', 'SC', 'US'),
(49, 'South Dakota', 'SD', 'US'),
(50, 'Tennessee', 'TN', 'US'),
(51, 'Texas', 'TX', 'US'),
(52, 'Utah', 'UT', 'US'),
(53, 'Vermont', 'VT', 'US'),
(54, 'Virgin Islands', 'VI', 'US'),
(55, 'Virginia', 'VA', 'US'),
(56, 'Washington', 'WA', 'US'),
(57, 'West Virginia', 'WV', 'US'),
(58, 'Wisconsin', 'WI', 'US'),
(59, 'Wyoming', 'WY', 'US'),
(60, 'Armed Forces Africa', 'AE', 'US'),
(61, 'Armed Forces Americas (except Canada)', 'AA', 'US'),
(62, 'Armed Forces Canada', 'AE', 'US'),
(63, 'Armed Forces Europe', 'AE', 'US'),
(64, 'Armed Forces Middle East', 'AE', 'US'),
(65, 'Armed Forces Pacific', 'AP', 'US');

-- --------------------------------------------------------

--
-- Table structure for table `hs_hr_unique_id`
--

CREATE TABLE IF NOT EXISTS `hs_hr_unique_id` (
`id` int(11) NOT NULL,
  `last_id` int(10) unsigned NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `field_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hs_hr_unique_id`
--

INSERT INTO `hs_hr_unique_id` (`id`, `last_id`, `table_name`, `field_name`) VALUES
(1, 4, 'hs_hr_employee', 'emp_number'),
(2, 9, 'hs_hr_module', 'mod_id'),
(3, 0, 'hs_hr_leave', 'leave_id'),
(4, 0, 'hs_hr_leavetype', 'leave_type_id'),
(5, 0, 'hs_hr_leave_requests', 'leave_request_id'),
(6, 0, 'hs_hr_custom_export', 'export_id'),
(7, 0, 'hs_hr_custom_import', 'import_id'),
(8, 0, 'hs_hr_pay_period', 'id'),
(9, 0, 'hs_hr_kpi', 'id'),
(10, 0, 'hs_hr_performance_review', 'id'),
(11, 2, 'ohrm_emp_reporting_method', 'reporting_method_id'),
(12, 22, 'ohrm_timesheet', 'timesheet_id'),
(13, 3, 'ohrm_timesheet_action_log', 'timesheet_action_log_id'),
(14, 7, 'ohrm_timesheet_item', 'timesheet_item_id'),
(15, 1, 'ohrm_attendance_record', 'id'),
(16, 3, 'ohrm_job_vacancy', 'id'),
(17, 4, 'ohrm_job_candidate', 'id'),
(18, 106, 'ohrm_workflow_state_machine', 'id'),
(19, 0, 'ohrm_job_candidate_attachment', 'id'),
(20, 0, 'ohrm_job_vacancy_attachment', 'id'),
(21, 1, 'ohrm_job_candidate_vacancy', 'id'),
(22, 5, 'ohrm_job_candidate_history', 'id'),
(23, 0, 'ohrm_job_interview', 'id'),
(24, 31, 'loan', 'id'),
(25, 27, 'employee_loan', 'employee_loan_id'),
(26, 32, 'loan_deduction', 'loan_deduction_id'),
(27, 11, 'deduction', 'deduction_id'),
(28, 20, 'deduction_line', 'line_id');

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE IF NOT EXISTS `loan` (
`id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `principal` float DEFAULT NULL,
  `term` varchar(6) DEFAULT NULL,
  `interest` float DEFAULT NULL,
  `description` text,
  `organization_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loan`
--

INSERT INTO `loan` (`id`, `title`, `principal`, `term`, `interest`, `description`, `organization_id`) VALUES
(25, 'Maendeleo', 10000000, '3', 19, '', 3),
(30, 'Mkombozi', 10000000, '3', 15, 'Mkombozi loan from NBC', 1),
(31, 'Loan', 5000000, '2', 0, '', 6);

-- --------------------------------------------------------

--
-- Table structure for table `loan_deduction`
--

CREATE TABLE IF NOT EXISTS `loan_deduction` (
`loan_deduction_id` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `emp_number` int(7) NOT NULL,
  `amount` float DEFAULT NULL,
  `loan_id` int(11) NOT NULL,
  `balance` float DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loan_deduction`
--

INSERT INTO `loan_deduction` (`loan_deduction_id`, `payroll_id`, `emp_number`, `amount`, `loan_id`, `balance`) VALUES
(29, 59, 1, 208333, 31, 2916700),
(30, 59, 2, 208333, 31, 3125030),
(31, 60, 1, 208333, 31, 2708370),
(32, 60, 2, 208333, 31, 2916700);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_advanced_report`
--

CREATE TABLE IF NOT EXISTS `ohrm_advanced_report` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `definition` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_advanced_report`
--

INSERT INTO `ohrm_advanced_report` (`id`, `name`, `definition`) VALUES
(1, 'Leave Entitlements and Usage Report', '\n<report>\n    <settings>\n        <csv>\n            <include_group_header>1</include_group_header>\n            <include_header>1</include_header>\n        </csv>\n    </settings>\n<filter_fields>\n	<input_field type="text" name="empNumber" label="Employee Number"></input_field>\n	<input_field type="text" name="fromDate" label="From"></input_field>\n        <input_field type="text" name="toDate" label="To"></input_field>\n        <input_field type="text" name="asOfDate" label="AsOf"></input_field>\n</filter_fields> \n\n<sub_report type="sql" name="mainTable">       \n    <query>FROM ohrm_leave_type WHERE (deleted = 0) OR (SELECT count(l.id) FROM ohrm_leave l WHERE l.status = 3 AND l.leave_type_id = ohrm_leave_type.id) > 0 ORDER BY ohrm_leave_type.id</query>\n    <id_field>leaveTypeId</id_field>\n    <display_groups>\n        <display_group name="leavetype" type="one" display="true">\n            <group_header></group_header>\n            <fields>\n                <field display="false">\n                    <field_name>ohrm_leave_type.id</field_name>\n                    <field_alias>leaveTypeId</field_alias>\n                    <display_name>Leave Type ID</display_name>\n                    <width>1</width>	\n                </field>   \n                <field display="false">\n                    <field_name>ohrm_leave_type.exclude_in_reports_if_no_entitlement</field_name>\n                    <field_alias>exclude_if_no_entitlement</field_alias>\n                    <display_name>Exclude</display_name>\n                    <width>1</width>	\n                </field>  \n                <field display="false">\n                    <field_name>ohrm_leave_type.deleted</field_name>\n                    <field_alias>leave_type_deleted</field_alias>\n                    <display_name>Leave Type Deleted</display_name>\n                    <width>1</width>	\n                </field>  \n                <field display="true">\n                    <field_name>ohrm_leave_type.name</field_name>\n                    <field_alias>leaveType</field_alias>\n                    <display_name>Leave Type</display_name>\n                    <width>160</width>	\n                </field>s                                                                                                     \n            </fields>\n        </display_group>\n    </display_groups> \n</sub_report>\n\n<sub_report type="sql" name="entitlementsTotal">\n                    <query>\n\nFROM (\nSELECT ohrm_leave_entitlement.id as id, \n       ohrm_leave_entitlement.leave_type_id as leave_type_id,\n       ohrm_leave_entitlement.no_of_days as no_of_days,\n       sum(IF(ohrm_leave.status = 2, ohrm_leave_leave_entitlement.length_days, 0)) AS scheduled,\n       sum(IF(ohrm_leave.status = 3, ohrm_leave_leave_entitlement.length_days, 0)) AS taken\n       \nFROM ohrm_leave_entitlement LEFT JOIN ohrm_leave_leave_entitlement ON\n    ohrm_leave_entitlement.id = ohrm_leave_leave_entitlement.entitlement_id\n    LEFT JOIN ohrm_leave ON ohrm_leave.id = ohrm_leave_leave_entitlement.leave_id AND \n    ( $X{&gt;,ohrm_leave.date,toDate} OR $X{&lt;,ohrm_leave.date,fromDate} )\n\nWHERE ohrm_leave_entitlement.deleted=0 AND $X{=,ohrm_leave_entitlement.emp_number,empNumber} AND \n    $X{IN,ohrm_leave_entitlement.leave_type_id,leaveTypeId} AND\n    (\n      ( $X{&lt;=,ohrm_leave_entitlement.from_date,fromDate} AND $X{&gt;=,ohrm_leave_entitlement.to_date,fromDate} ) OR\n      ( $X{&lt;=,ohrm_leave_entitlement.from_date,toDate} AND $X{&gt;=,ohrm_leave_entitlement.to_date,toDate} ) OR \n      ( $X{&gt;=,ohrm_leave_entitlement.from_date,fromDate} AND $X{&lt;=,ohrm_leave_entitlement.to_date,toDate} ) \n    )\n    \nGROUP BY ohrm_leave_entitlement.id\n) AS A\n\nGROUP BY A.leave_type_id\nORDER BY A.leave_type_id\n\n</query>\n    <id_field>leaveTypeId</id_field>\n    <display_groups>\n            <display_group name="g2" type="one" display="true">\n                <group_header></group_header>\n                <fields>\n                    <field display="false">\n                        <field_name>A.leave_type_id</field_name>\n                        <field_alias>leaveTypeId</field_alias>\n                        <display_name>Leave Type ID</display_name>\n                        <width>1</width>\n                    </field>                                \n                    <field display="true">\n                        <field_name>sum(A.no_of_days) - sum(A.scheduled) - sum(A.taken)</field_name>\n                        <field_alias>entitlement_total</field_alias>\n                        <display_name>Leave Entitlements (Days)</display_name>\n                        <width>120</width>\n                        <align>right</align>\n                        <link>leave/viewLeaveEntitlements?empNumber=$P{empNumber}&amp;fromDate=$P{fromDate}&amp;toDate=$P{toDate}&amp;leaveTypeId=$P{leaveTypeId}&amp;stddate=1</link>\n                    </field>                                \n                </fields>\n            </display_group>\n    </display_groups>\n</sub_report>\n\n<sub_report type="sql" name="pendingQuery">\n<query>\nFROM ohrm_leave_type LEFT JOIN \nohrm_leave ON ohrm_leave_type.id = ohrm_leave.leave_type_id AND\n$X{=,ohrm_leave.emp_number,empNumber} AND\nohrm_leave.status = 1 AND\n$X{&gt;=,ohrm_leave.date,fromDate} AND $X{&lt;=,ohrm_leave.date,toDate}\nWHERE\nohrm_leave_type.deleted = 0 AND\n$X{IN,ohrm_leave_type.id,leaveTypeId}\n\nGROUP BY ohrm_leave_type.id\nORDER BY ohrm_leave_type.id\n</query>\n    <id_field>leaveTypeId</id_field>\n    <display_groups>\n            <display_group name="g6" type="one" display="true">\n                <group_header></group_header>\n                <fields>\n                    <field display="false">\n                        <field_name>ohrm_leave_type.id</field_name>\n                        <field_alias>leaveTypeId</field_alias>\n                        <display_name>Leave Type ID</display_name>\n                        <width>1</width>\n                    </field>                                \n                    <field display="true">\n                        <field_name>sum(length_days)</field_name>\n                        <field_alias>pending</field_alias>\n                        <display_name>Leave Pending Approval (Days)</display_name>\n                        <width>120</width>\n                        <align>right</align>\n                        <link>leave/viewLeaveList?empNumber=$P{empNumber}&amp;fromDate=$P{fromDate}&amp;toDate=$P{toDate}&amp;leaveTypeId=$P{leaveTypeId}&amp;status=1&amp;stddate=1</link>\n                    </field>                                \n                </fields>\n            </display_group>\n    </display_groups>\n    </sub_report>\n\n<sub_report type="sql" name="scheduledQuery">\n<query>\nFROM ohrm_leave_type LEFT JOIN \nohrm_leave ON ohrm_leave_type.id = ohrm_leave.leave_type_id AND\n$X{=,ohrm_leave.emp_number,empNumber} AND\nohrm_leave.status = 2 AND\n$X{&gt;=,ohrm_leave.date,fromDate} AND $X{&lt;=,ohrm_leave.date,toDate}\nWHERE\nohrm_leave_type.deleted = 0 AND\n$X{IN,ohrm_leave_type.id,leaveTypeId}\n\nGROUP BY ohrm_leave_type.id\nORDER BY ohrm_leave_type.id\n</query>\n    <id_field>leaveTypeId</id_field>\n    <display_groups>\n            <display_group name="g5" type="one" display="true">\n                <group_header></group_header>\n                <fields>\n                    <field display="false">\n                        <field_name>ohrm_leave_type.id</field_name>\n                        <field_alias>leaveTypeId</field_alias>\n                        <display_name>Leave Type ID</display_name>\n                        <width>1</width>\n                    </field>                                \n                    <field display="true">\n                        <field_name>sum(length_days)</field_name>\n                        <field_alias>scheduled</field_alias>\n                        <display_name>Leave Scheduled (Days)</display_name>\n                        <width>120</width>\n                        <align>right</align>\n                        <link>leave/viewLeaveList?empNumber=$P{empNumber}&amp;fromDate=$P{fromDate}&amp;toDate=$P{toDate}&amp;leaveTypeId=$P{leaveTypeId}&amp;status=2&amp;stddate=1</link>\n                    </field>                                \n                </fields>\n            </display_group>\n    </display_groups>\n    </sub_report>\n\n<sub_report type="sql" name="takenQuery">\n<query>\nFROM ohrm_leave WHERE $X{=,emp_number,empNumber} AND\nstatus = 3 AND\n$X{IN,ohrm_leave.leave_type_id,leaveTypeId} AND\n$X{&gt;=,ohrm_leave.date,fromDate} AND $X{&lt;=,ohrm_leave.date,toDate}\nGROUP BY leave_type_id\nORDER BY ohrm_leave.leave_type_id\n</query>\n    <id_field>leaveTypeId</id_field>\n    <display_groups>\n            <display_group name="g4" type="one" display="true">\n                <group_header></group_header>\n                <fields>\n                    <field display="false">\n                        <field_name>ohrm_leave.leave_type_id</field_name>\n                        <field_alias>leaveTypeId</field_alias>\n                        <display_name>Leave Type ID</display_name>\n                        <width>1</width>\n                    </field>                                \n                    <field display="true">\n                        <field_name>sum(length_days)</field_name>\n                        <field_alias>taken</field_alias>\n                        <display_name>Leave Taken (Days)</display_name>\n                        <width>120</width>\n                        <align>right</align>\n                        <link>leave/viewLeaveList?empNumber=$P{empNumber}&amp;fromDate=$P{fromDate}&amp;toDate=$P{toDate}&amp;leaveTypeId=$P{leaveTypeId}&amp;status=3&amp;stddate=1</link>\n                    </field>                                \n                </fields>\n            </display_group>\n    </display_groups>\n    </sub_report>\n\n<sub_report type="sql" name="unused">       \n    <query>FROM ohrm_leave_type WHERE deleted = 0 AND $X{IN,ohrm_leave_type.id,leaveTypeId} ORDER BY ohrm_leave_type.id</query>\n    <id_field>leaveTypeId</id_field>\n    <display_groups>\n        <display_group name="unused" type="one" display="true">\n            <group_header></group_header>\n            <fields>\n                <field display="false">\n                    <field_name>ohrm_leave_type.id</field_name>\n                    <field_alias>leaveTypeId</field_alias>\n                    <display_name>Leave Type ID</display_name>\n                    <width>1</width>	\n                </field>   \n                <field display="true">\n                    <field_name>ohrm_leave_type.name</field_name>\n                    <field_alias>unused</field_alias>\n                    <display_name>Leave Balance (Days)</display_name>\n                    <width>160</width>	\n                    <align>right</align>\n                </field>                                                                                                     \n            </fields>\n        </display_group>\n    </display_groups> \n</sub_report>\n\n\n    <join>             \n        <join_by sub_report="mainTable" id="leaveTypeId"></join_by>              \n        <join_by sub_report="entitlementsTotal" id="leaveTypeId"></join_by> \n        <join_by sub_report="pendingQuery" id="leaveTypeId"></join_by>  \n        <join_by sub_report="scheduledQuery" id="leaveTypeId"></join_by>  \n        <join_by sub_report="takenQuery" id="leaveTypeId"></join_by>  \n        <join_by sub_report="unused" id="leaveTypeId"></join_by>  \n\n    </join>\n    <page_limit>100</page_limit>        \n</report>'),
(2, 'Leave Entitlements and Usage Report', '\n<report>\n    <settings>\n        <csv>\n            <include_group_header>1</include_group_header>\n            <include_header>1</include_header>\n        </csv>\n    </settings>\n<filter_fields>\n	<input_field type="text" name="leaveType" label="Leave Type"></input_field>\n	<input_field type="text" name="fromDate" label="From"></input_field>\n        <input_field type="text" name="toDate" label="To"></input_field>\n        <input_field type="text" name="asOfDate" label="AsOf"></input_field>\n        <input_field type="text" name="emp_numbers" label="employees"></input_field>\n        <input_field type="text" name="job_title" label="Job Title"></input_field>\n        <input_field type="text" name="location" label="Location"></input_field>\n        <input_field type="text" name="sub_unit" label="Sub Unit"></input_field>\n        <input_field type="text" name="terminated" label="Terminated"></input_field>\n</filter_fields> \n\n<sub_report type="sql" name="mainTable">       \n    <query>FROM hs_hr_employee \n    LEFT JOIN hs_hr_emp_locations ON hs_hr_employee.emp_number = hs_hr_emp_locations.emp_number\n    WHERE $X{IN,hs_hr_employee.emp_number,emp_numbers} \n    AND $X{=,hs_hr_employee.job_title_code,job_title}\n    AND $X{IN,hs_hr_employee.work_station,sub_unit}\n    AND $X{IN,hs_hr_emp_locations.location_id,location}\n    AND $X{IS NULL,hs_hr_employee.termination_id,terminated}\n    ORDER BY hs_hr_employee.emp_lastname</query>\n    <id_field>empNumber</id_field>\n    <display_groups>\n        <display_group name="personalDetails" type="one" display="true">\n            <group_header></group_header>\n            <fields>\n                <field display="false">\n                    <field_name>hs_hr_employee.emp_number</field_name>\n                    <field_alias>empNumber</field_alias>\n                    <display_name>Employee Number</display_name>\n                    <width>1</width>	\n                </field>                \n                <field display="false">\n                    <field_name>hs_hr_employee.termination_id</field_name>\n                    <field_alias>termination_id</field_alias>\n                    <display_name>Termination ID</display_name>\n                    <width>1</width>	\n                </field>   \n                <field display="true">\n                    <field_name>CONCAT(hs_hr_employee.emp_firstname, '' '', hs_hr_employee.emp_lastname)</field_name>\n                    <field_alias>employeeName</field_alias>\n                    <display_name>Employee</display_name>\n                    <width>150</width>\n                </field>                                                                                               \n            </fields>\n        </display_group>\n    </display_groups> \n</sub_report>\n\n<sub_report type="sql" name="entitlementsTotal">\n                    <query>\n\nFROM (\nSELECT ohrm_leave_entitlement.id as id, \n       ohrm_leave_entitlement.emp_number as emp_number,\n       ohrm_leave_entitlement.no_of_days as no_of_days,\n       sum(IF(ohrm_leave.status = 2, ohrm_leave_leave_entitlement.length_days, 0)) AS scheduled,\n       sum(IF(ohrm_leave.status = 3, ohrm_leave_leave_entitlement.length_days, 0)) AS taken\n       \nFROM ohrm_leave_entitlement LEFT JOIN ohrm_leave_leave_entitlement ON\n    ohrm_leave_entitlement.id = ohrm_leave_leave_entitlement.entitlement_id\n    LEFT JOIN ohrm_leave ON ohrm_leave.id = ohrm_leave_leave_entitlement.leave_id AND \n    ( $X{&gt;,ohrm_leave.date,toDate} OR $X{&lt;,ohrm_leave.date,fromDate} )\n\nWHERE ohrm_leave_entitlement.deleted=0 AND $X{=,ohrm_leave_entitlement.leave_type_id,leaveType}\n    AND $X{IN,ohrm_leave_entitlement.emp_number,empNumber} AND\n    (\n      ( $X{&lt;=,ohrm_leave_entitlement.from_date,fromDate} AND $X{&gt;=,ohrm_leave_entitlement.to_date,fromDate} ) OR\n      ( $X{&lt;=,ohrm_leave_entitlement.from_date,toDate} AND $X{&gt;=,ohrm_leave_entitlement.to_date,toDate} ) OR \n      ( $X{&gt;=,ohrm_leave_entitlement.from_date,fromDate} AND $X{&lt;=,ohrm_leave_entitlement.to_date,toDate} ) \n    )\n    \nGROUP BY ohrm_leave_entitlement.id\n) AS A\n\nGROUP BY A.emp_number\nORDER BY A.emp_number\n\n</query>\n    <id_field>empNumber</id_field>\n    <display_groups>\n            <display_group name="g2" type="one" display="true">\n                <group_header></group_header>\n                <fields>\n                    <field display="false">\n                        <field_name>A.emp_number</field_name>\n                        <field_alias>empNumber</field_alias>\n                        <display_name>Emp Number</display_name>\n                        <width>1</width>\n                    </field>                                \n                    <field display="true">\n                        <field_name>sum(A.no_of_days) - sum(A.scheduled) - sum(A.taken)</field_name>\n                        <field_alias>entitlement_total</field_alias>\n                        <display_name>Leave Entitlements (Days)</display_name>\n                        <width>120</width>\n                        <align>right</align>\n                        <link>leave/viewLeaveEntitlements?empNumber=$P{empNumber}&amp;fromDate=$P{fromDate}&amp;toDate=$P{toDate}&amp;leaveTypeId=$P{leaveTypeId}&amp;stddate=1</link>\n                    </field>                                \n                </fields>\n            </display_group>\n    </display_groups>\n</sub_report>\n\n<sub_report type="sql" name="pendingQuery">\n<query>\nFROM ohrm_leave WHERE $X{=,ohrm_leave.leave_type_id,leaveType} AND\nstatus = 1 AND\n$X{IN,ohrm_leave.emp_number,empNumber} AND\n$X{&gt;=,ohrm_leave.date,fromDate} AND $X{&lt;=,ohrm_leave.date,toDate}\nGROUP BY emp_number\nORDER BY ohrm_leave.emp_number\n</query>\n    <id_field>empNumber</id_field>\n    <display_groups>\n            <display_group name="g6" type="one" display="true">\n                <group_header></group_header>\n                <fields>\n                    <field display="false">\n                        <field_name>ohrm_leave.emp_number</field_name>\n                        <field_alias>empNumber</field_alias>\n                        <display_name>Emp Number</display_name>\n                        <width>1</width>\n                    </field>                                \n                    <field display="true">\n                        <field_name>sum(length_days)</field_name>\n                        <field_alias>pending</field_alias>\n                        <display_name>Leave Pending Approval (Days)</display_name>\n                        <width>121</width>\n                        <align>right</align>\n                        <link>leave/viewLeaveList?empNumber=$P{empNumber}&amp;fromDate=$P{fromDate}&amp;toDate=$P{toDate}&amp;leaveTypeId=$P{leaveTypeId}&amp;status=1&amp;stddate=1</link>\n                    </field>                                \n                </fields>\n            </display_group>\n    </display_groups>\n</sub_report>\n\n\n<sub_report type="sql" name="scheduledQuery">\n<query>\nFROM ohrm_leave WHERE $X{=,ohrm_leave.leave_type_id,leaveType} AND\nstatus = 2 AND\n$X{IN,ohrm_leave.emp_number,empNumber} AND\n$X{&gt;=,ohrm_leave.date,fromDate} AND $X{&lt;=,ohrm_leave.date,toDate}\nGROUP BY emp_number\nORDER BY ohrm_leave.emp_number\n</query>\n    <id_field>empNumber</id_field>\n    <display_groups>\n            <display_group name="g5" type="one" display="true">\n                <group_header></group_header>\n                <fields>\n                    <field display="false">\n                        <field_name>ohrm_leave.emp_number</field_name>\n                        <field_alias>empNumber</field_alias>\n                        <display_name>Emp Number</display_name>\n                        <width>1</width>\n                    </field>                                \n                    <field display="true">\n                        <field_name>sum(length_days)</field_name>\n                        <field_alias>scheduled</field_alias>\n                        <display_name>Leave Scheduled (Days)</display_name>\n                        <width>121</width>\n                        <align>right</align>\n                        <link>leave/viewLeaveList?empNumber=$P{empNumber}&amp;fromDate=$P{fromDate}&amp;toDate=$P{toDate}&amp;leaveTypeId=$P{leaveTypeId}&amp;status=2&amp;stddate=1</link>\n                    </field>                                \n                </fields>\n            </display_group>\n    </display_groups>\n</sub_report>\n\n<sub_report type="sql" name="takenQuery">\n<query>\nFROM ohrm_leave WHERE $X{=,ohrm_leave.leave_type_id,leaveType} AND\nstatus = 3 AND\n$X{IN,ohrm_leave.emp_number,empNumber} AND\n$X{&gt;=,ohrm_leave.date,fromDate} AND $X{&lt;=,ohrm_leave.date,toDate}\nGROUP BY emp_number\nORDER BY ohrm_leave.emp_number\n</query>\n    <id_field>empNumber</id_field>\n    <display_groups>\n            <display_group name="g4" type="one" display="true">\n                <group_header></group_header>\n                <fields>\n                    <field display="false">\n                        <field_name>ohrm_leave.emp_number</field_name>\n                        <field_alias>empNumber</field_alias>\n                        <display_name>Emp Number</display_name>\n                        <width>1</width>\n                    </field>                                \n                    <field display="true">\n                        <field_name>sum(length_days)</field_name>\n                        <field_alias>taken</field_alias>\n                        <display_name>Leave Taken (Days)</display_name>\n                        <width>120</width>\n                        <align>right</align>\n                        <link>leave/viewLeaveList?empNumber=$P{empNumber}&amp;fromDate=$P{fromDate}&amp;toDate=$P{toDate}&amp;leaveTypeId=$P{leaveTypeId}&amp;status=3&amp;stddate=1</link>\n                    </field>                                \n                </fields>\n            </display_group>\n    </display_groups>\n</sub_report>\n<sub_report type="sql" name="unused">       \n    <query>FROM hs_hr_employee WHERE $X{IN,hs_hr_employee.emp_number,empNumber} ORDER BY hs_hr_employee.emp_number</query>\n    <id_field>empNumber</id_field>\n    <display_groups>\n        <display_group name="unused" type="one" display="true">\n            <group_header></group_header>\n            <fields>    \n                <field display="false">\n                    <field_name>hs_hr_employee.emp_number</field_name>\n                    <field_alias>empNumber</field_alias>\n                    <display_name>Employee Number</display_name>\n                    <width>1</width>	\n                </field>                \n                <field display="true">\n                    <field_name>hs_hr_employee.emp_firstname</field_name>\n                    <field_alias>unused</field_alias>\n                    <display_name>Leave Balance (Days)</display_name>\n                    <width>150</width>\n                    <align>right</align>\n                </field> \n                                                                                               \n            </fields>\n        </display_group>\n    </display_groups> \n</sub_report>\n    <join>             \n        <join_by sub_report="mainTable" id="empNumber"></join_by>            \n        <join_by sub_report="entitlementsTotal" id="empNumber"></join_by> \n        <join_by sub_report="pendingQuery" id="empNumber"></join_by>\n        <join_by sub_report="scheduledQuery" id="empNumber"></join_by>\n        <join_by sub_report="takenQuery" id="empNumber"></join_by> \n        <join_by sub_report="unused" id="empNumber"></join_by>  \n    </join>\n    <page_limit>20</page_limit>       \n</report>');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_attendance_record`
--

CREATE TABLE IF NOT EXISTS `ohrm_attendance_record` (
  `id` bigint(20) NOT NULL,
  `employee_id` bigint(20) NOT NULL,
  `punch_in_utc_time` datetime DEFAULT NULL,
  `punch_in_note` varchar(255) DEFAULT NULL,
  `punch_in_time_offset` varchar(255) DEFAULT NULL,
  `punch_in_user_time` datetime DEFAULT NULL,
  `punch_out_utc_time` datetime DEFAULT NULL,
  `punch_out_note` varchar(255) DEFAULT NULL,
  `punch_out_time_offset` varchar(255) DEFAULT NULL,
  `punch_out_user_time` datetime DEFAULT NULL,
  `state` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_attendance_record`
--

INSERT INTO `ohrm_attendance_record` (`id`, `employee_id`, `punch_in_utc_time`, `punch_in_note`, `punch_in_time_offset`, `punch_in_user_time`, `punch_out_utc_time`, `punch_out_note`, `punch_out_time_offset`, `punch_out_user_time`, `state`) VALUES
(1, 1, '2015-03-09 07:31:00', '', '3', '2015-03-09 10:31:00', '2015-03-09 07:31:00', '', '3', '2015-03-09 10:31:00', 'PUNCHED OUT');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_available_group_field`
--

CREATE TABLE IF NOT EXISTS `ohrm_available_group_field` (
  `report_group_id` bigint(20) NOT NULL,
  `group_field_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_beacon_notification`
--

CREATE TABLE IF NOT EXISTS `ohrm_beacon_notification` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `expiry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `definition` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_composite_display_field`
--

CREATE TABLE IF NOT EXISTS `ohrm_composite_display_field` (
`composite_display_field_id` bigint(20) NOT NULL,
  `report_group_id` bigint(20) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `label` varchar(255) NOT NULL,
  `field_alias` varchar(255) DEFAULT NULL,
  `is_sortable` varchar(10) NOT NULL,
  `sort_order` varchar(255) DEFAULT NULL,
  `sort_field` varchar(255) DEFAULT NULL,
  `element_type` varchar(255) NOT NULL,
  `element_property` varchar(1000) NOT NULL,
  `width` varchar(255) NOT NULL,
  `is_exportable` varchar(10) DEFAULT NULL,
  `text_alignment_style` varchar(20) DEFAULT NULL,
  `is_value_list` tinyint(1) NOT NULL DEFAULT '0',
  `display_field_group_id` int(10) unsigned DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `is_encrypted` tinyint(1) NOT NULL DEFAULT '0',
  `is_meta` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_composite_display_field`
--

INSERT INTO `ohrm_composite_display_field` (`composite_display_field_id`, `report_group_id`, `name`, `label`, `field_alias`, `is_sortable`, `sort_order`, `sort_field`, `element_type`, `element_property`, `width`, `is_exportable`, `text_alignment_style`, `is_value_list`, `display_field_group_id`, `default_value`, `is_encrypted`, `is_meta`) VALUES
(1, 1, 'IF(hs_hr_employee.termination_id IS NULL, CONCAT(hs_hr_employee.emp_firstname, " " ,hs_hr_employee.emp_lastname), CONCAT(hs_hr_employee.emp_firstname, " " ,hs_hr_employee.emp_lastname, " (Past Employee)"))', 'Employee Name', 'employeeName', 'false', NULL, NULL, 'label', '<xml><getter>employeeName</getter></xml>', '300', '0', NULL, 0, NULL, 'Deleted Employee', 0, 0),
(2, 1, 'CONCAT(ohrm_customer.name, " - " ,ohrm_project.name)', 'Project Name', 'projectname', 'false', NULL, NULL, 'label', '<xml><getter>projectname</getter></xml>', '300', '0', NULL, 0, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_customer`
--

CREATE TABLE IF NOT EXISTS `ohrm_customer` (
`customer_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_customer`
--

INSERT INTO `ohrm_customer` (`customer_id`, `name`, `description`, `is_deleted`) VALUES
(1, 'Asyx GCL', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_datapoint`
--

CREATE TABLE IF NOT EXISTS `ohrm_datapoint` (
`id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `datapoint_type_id` int(11) NOT NULL,
  `definition` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_datapoint_type`
--

CREATE TABLE IF NOT EXISTS `ohrm_datapoint_type` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `action_class` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_datapoint_type`
--

INSERT INTO `ohrm_datapoint_type` (`id`, `name`, `action_class`) VALUES
(1, 'config', 'configDatapointProcessor'),
(2, 'count', 'countDatapointProcessor'),
(3, 'session', 'sessionDatapointProcessor'),
(4, 'organization', 'OrganizationDataProcessor');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_data_group`
--

CREATE TABLE IF NOT EXISTS `ohrm_data_group` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `can_read` tinyint(4) DEFAULT NULL,
  `can_create` tinyint(4) DEFAULT NULL,
  `can_update` tinyint(4) DEFAULT NULL,
  `can_delete` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_data_group`
--

INSERT INTO `ohrm_data_group` (`id`, `name`, `description`, `can_read`, `can_create`, `can_update`, `can_delete`) VALUES
(1, 'personal_information', 'PIM - Personal Details', 1, NULL, 1, NULL),
(2, 'personal_attachment', 'PIM - Personal Details - Attachments', 1, 1, 1, 1),
(3, 'personal_custom_fields', 'PIM - Personal Details - Custom Fields', 1, NULL, 1, NULL),
(4, 'contact_details', 'PIM - Contact Details', 1, NULL, 1, NULL),
(5, 'contact_attachment', 'PIM - Contact Details - Attachments', 1, 1, 1, 1),
(6, 'contact_custom_fields', 'PIM - Contact Details - Custom Fields', 1, NULL, 1, NULL),
(7, 'emergency_contacts', 'PIM - Emergency Contacts', 1, 1, 1, 1),
(8, 'emergency_attachment', 'PIM - Emergency Contacts - Attachments', 1, 1, 1, 1),
(9, 'emergency_custom_fields', 'PIM - Emergency Contacts - Custom Fields', 1, NULL, 1, NULL),
(10, 'dependents', 'PIM - Dependents', 1, 1, 1, 1),
(11, 'dependents_attachment', 'PIM - Dependents - Attachments', 1, 1, 1, 1),
(12, 'dependents_custom_fields', 'PIM - Dependents - Custom Fields', 1, NULL, 1, NULL),
(13, 'immigration', 'PIM - Immigration', 1, 1, 1, 1),
(14, 'immigration_attachment', 'PIM - Immigration - Attachments', 1, 1, 1, 1),
(15, 'immigration_custom_fields', 'PIM - Immigration - Custom Fields', 1, NULL, 1, NULL),
(16, 'job_details', 'PIM - Job', 1, NULL, 1, NULL),
(17, 'job_attachment', 'PIM - Job - Attachments', 1, 1, 1, 1),
(18, 'job_custom_fields', 'PIM - Job - Custom Fields', 1, NULL, 1, NULL),
(19, 'salary_details', 'PIM - Salary', 1, 1, 1, 1),
(20, 'salary_attachment', 'PIM - Salary - Attachments', 1, 1, 1, 1),
(21, 'salary_custom_fields', 'PIM - Salary - Custom Fields', 1, NULL, 1, NULL),
(22, 'tax_exemptions', 'PIM - Tax Exemptions', 1, NULL, 1, NULL),
(23, 'tax_attachment', 'PIM - Tax Exemptions - Attachments', 1, 1, 1, 1),
(24, 'tax_custom_fields', 'PIM - Tax Exemptions - Custom Fields', 1, NULL, 1, NULL),
(25, 'supervisor', 'PIM - Employee Supervisors', 1, 1, 1, 1),
(26, 'subordinates', 'PIM - Employee Subordinates', 1, 1, 1, 1),
(27, 'report-to_attachment', 'PIM - Employee Supervisors/Subordinates - Attachment', 1, 1, 1, 1),
(28, 'report-to_custom_fields', 'PIM - Employee Supervisors/Subordinates - Custom Fields', 1, NULL, 1, NULL),
(29, 'qualification_work', 'PIM - Qualifications - Work Experience', 1, 1, 1, 1),
(30, 'qualification_education', 'PIM - Qualifications - Education', 1, 1, 1, 1),
(31, 'qualification_skills', 'PIM - Qualifications - Skills', 1, 1, 1, 1),
(32, 'qualification_languages', 'PIM - Qualifications - Languages', 1, 1, 1, 1),
(33, 'qualification_license', 'PIM - Qualifications - License', 1, 1, 1, 1),
(34, 'qualifications_attachment', 'PIM - Qualifications - Attachments', 1, 1, 1, 1),
(35, 'qualifications_custom_fields', 'PIM - Qualifications - Custom Fields', 1, NULL, 1, NULL),
(36, 'membership', 'PIM - Membership', 1, 1, 1, 1),
(37, 'membership_attachment', 'PIM - Membership - Attachments', 1, 1, 1, 1),
(38, 'membership_custom_fields', 'PIM - Membership - Custom Fields', 1, NULL, 1, NULL),
(39, 'photograph', 'PIM - Employee Photograph', 1, NULL, 1, 1),
(40, 'leave_entitlements', 'Leave - Leave Entitlements', 1, 1, 1, 1),
(41, 'leave_entitlements_usage_report', 'Leave - Leave Entitlements and Usage Report', 1, NULL, NULL, NULL),
(42, 'job_titles', 'Admin - Job Titles', 1, 1, 1, 1),
(43, 'pay_grades', 'Admin - Pay Grades', 1, 1, 1, 1),
(44, 'time_customers', 'Time - Project Info - Customers', 1, 1, 1, 1),
(45, 'time_projects', 'Time - Project Info - Projects', 1, 1, 1, 1),
(46, 'pim_reports', 'PIM - Reports', 1, 1, 1, 1),
(47, 'attendance_configuration', 'Time - Attendance Configuration', 1, 0, 1, 0),
(48, 'attendance_records', 'Time - Attendance Records', 1, 0, 0, 0),
(49, 'time_project_reports', 'Time - Project Reports', 1, 0, 0, 0),
(50, 'time_employee_reports', 'Time - Employee Reports', 1, 0, 0, 0),
(51, 'attendance_summary', 'Time - Attendance Summary', 1, 0, 0, 0),
(52, 'leave_period', 'Leave - Leave Period', 1, 0, 1, 0),
(53, 'leave_types', 'Leave - Leave Types', 1, 1, 1, 1),
(54, 'work_week', 'Leave - Work Week', 1, 0, 1, 0),
(55, 'holidays', 'Leave - Holidays', 1, 1, 1, 1),
(56, 'recruitment_vacancies', 'Recruitment - Vacancies', 1, 1, 1, 1),
(57, 'recruitment_candidates', 'Recruitment - Candidates', 1, 1, 1, 1),
(58, 'time_employee_timesheets', 'Time - Employee Timesheets', 1, 0, 0, 0),
(59, 'leave_list', 'Leave - Leave List', 1, 0, 0, 0),
(60, 'leave_list_comments', 'Leave - Leave List - Comments', 0, 1, 0, 0),
(61, 'allowance_list', 'Allowance List Permissions', 1, 1, 1, 1),
(62, 'allowance', 'Allowance Permission', 1, 1, 1, 1),
(63, 'tax_rate', 'Tanzania Tax Rates', 1, 1, 1, 1),
(64, 'pension', 'Pensions Permisions', 1, 0, 0, 0),
(65, 'loans', 'Loans Permissions', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_data_group_screen`
--

CREATE TABLE IF NOT EXISTS `ohrm_data_group_screen` (
`id` int(11) NOT NULL,
  `data_group_id` int(11) DEFAULT NULL,
  `screen_id` int(11) DEFAULT NULL,
  `permission` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_data_group_screen`
--

INSERT INTO `ohrm_data_group_screen` (`id`, `data_group_id`, `screen_id`, `permission`) VALUES
(1, 40, 69, 1),
(2, 40, 72, 2),
(3, 40, 73, 3),
(4, 40, 71, 4),
(5, 41, 78, 1),
(6, 42, 23, 1),
(7, 42, 80, 1),
(8, 42, 80, 2),
(9, 42, 80, 3),
(10, 42, 81, 4),
(11, 43, 24, 1),
(12, 43, 82, 1),
(13, 43, 82, 2),
(14, 43, 82, 3),
(15, 43, 83, 4),
(16, 43, 84, 3),
(17, 43, 85, 3),
(18, 42, 74, 1),
(19, 43, 74, 1),
(20, 44, 36, 1),
(21, 44, 86, 2),
(22, 44, 86, 3),
(23, 44, 87, 4),
(24, 45, 37, 1),
(25, 45, 88, 1),
(26, 45, 88, 2),
(27, 45, 88, 3),
(28, 45, 89, 4),
(29, 45, 90, 2),
(30, 45, 90, 3),
(31, 45, 91, 2),
(32, 45, 91, 3),
(33, 46, 45, 1),
(34, 46, 45, 4),
(35, 46, 92, 2),
(36, 46, 92, 3),
(37, 46, 93, 1),
(38, 47, 56, 1),
(39, 47, 56, 3),
(40, 48, 55, 1),
(41, 49, 57, 1),
(42, 49, 102, 1),
(43, 50, 58, 1),
(44, 51, 59, 1),
(45, 51, 101, 1),
(46, 52, 47, 1),
(47, 52, 47, 3),
(48, 53, 7, 1),
(49, 53, 8, 1),
(50, 53, 8, 2),
(51, 53, 8, 3),
(52, 53, 9, 2),
(53, 53, 10, 4),
(54, 54, 14, 1),
(55, 54, 14, 3),
(56, 55, 11, 1),
(57, 55, 12, 2),
(58, 55, 12, 3),
(59, 55, 13, 4),
(60, 56, 61, 1),
(61, 56, 94, 1),
(62, 56, 94, 2),
(63, 56, 94, 3),
(64, 56, 95, 4),
(65, 57, 60, 1),
(66, 57, 96, 1),
(67, 57, 96, 2),
(68, 57, 96, 3),
(69, 57, 97, 4),
(70, 56, 76, 1),
(71, 57, 76, 1),
(72, 58, 52, 1),
(73, 59, 16, 1),
(74, 59, 98, 1),
(75, 59, 99, 1),
(76, 61, 119, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_display_field`
--

CREATE TABLE IF NOT EXISTS `ohrm_display_field` (
`display_field_id` bigint(20) NOT NULL,
  `report_group_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `field_alias` varchar(255) DEFAULT NULL,
  `is_sortable` varchar(10) NOT NULL,
  `sort_order` varchar(255) DEFAULT NULL,
  `sort_field` varchar(255) DEFAULT NULL,
  `element_type` varchar(255) NOT NULL,
  `element_property` varchar(1000) NOT NULL,
  `width` varchar(255) NOT NULL,
  `is_exportable` varchar(10) DEFAULT NULL,
  `text_alignment_style` varchar(20) DEFAULT NULL,
  `is_value_list` tinyint(1) NOT NULL DEFAULT '0',
  `display_field_group_id` int(10) unsigned DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `is_encrypted` tinyint(1) NOT NULL DEFAULT '0',
  `is_meta` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_display_field`
--

INSERT INTO `ohrm_display_field` (`display_field_id`, `report_group_id`, `name`, `label`, `field_alias`, `is_sortable`, `sort_order`, `sort_field`, `element_type`, `element_property`, `width`, `is_exportable`, `text_alignment_style`, `is_value_list`, `display_field_group_id`, `default_value`, `is_encrypted`, `is_meta`) VALUES
(1, 1, 'ohrm_project.name', 'Project Name', 'projectname', 'false', NULL, NULL, 'label', '<xml><getter>projectname</getter></xml>', '200', '0', NULL, 0, NULL, NULL, 0, 0),
(2, 1, 'ohrm_project_activity.name', 'Activity Name', 'activityname', 'false', NULL, NULL, 'link', '<xml><labelGetter>activityname</labelGetter><placeholderGetters><id>activity_id</id><total>totalduration</total><projectId>projectId</projectId><from>fromDate</from><to>toDate</to><approved>onlyIncludeApprovedTimesheets</approved></placeholderGetters><urlPattern>../../displayProjectActivityDetailsReport?reportId=3#activityId={id}#total={total}#from={from}#to={to}#projectId={projectId}#onlyIncludeApprovedTimesheets={approved}</urlPattern></xml>', '200', '0', NULL, 0, NULL, NULL, 0, 0),
(3, 1, 'ohrm_project_activity.project_id', 'Project Id', NULL, 'false', NULL, NULL, 'label', '<xml><getter>project_id</getter></xml>', '75', '0', 'right', 0, NULL, NULL, 0, 1),
(4, 1, 'ohrm_project_activity.activity_id', 'Activity Id', NULL, 'false', NULL, NULL, 'label', '<xml><getter>activity_id</getter></xml>', '75', '0', 'right', 0, NULL, NULL, 0, 1),
(5, 1, 'ohrm_timesheet_item.duration', 'Time (hours)', NULL, 'false', NULL, NULL, 'label', '<xml><getter>duration</getter></xml>', '75', '0', 'right', 0, NULL, NULL, 0, 0),
(6, 1, 'hs_hr_employee.emp_firstname', 'Employee First Name', NULL, 'false', NULL, NULL, 'label', '<xml><getter>emp_firstname</getter></xml>', '200', '0', NULL, 0, NULL, NULL, 0, 0),
(7, 1, 'hs_hr_employee.emp_lastname', 'Employee Last Name', NULL, 'false', NULL, NULL, 'label', '<xml><getter>emp_lastname</getter></xml>', '200', '0', NULL, 0, NULL, NULL, 0, 0),
(8, 1, 'ohrm_project_activity.name', 'Activity Name', 'activityname', 'false', NULL, NULL, 'label', '<xml><getter>activityname</getter></xml>', '200', '0', NULL, 0, NULL, NULL, 0, 0),
(9, 3, 'hs_hr_employee.employee_id', 'Employee Id', 'employeeId', 'false', NULL, NULL, 'label', '<xml><getter>employeeId</getter></xml>', '100', '0', NULL, 0, 1, '---', 0, 0),
(10, 3, 'hs_hr_employee.emp_lastname', 'Employee Last Name', 'employeeLastname', 'false', NULL, NULL, 'label', '<xml><getter>employeeLastname</getter></xml>', '200', '0', NULL, 0, 1, '---', 0, 0),
(11, 3, 'hs_hr_employee.emp_firstname', 'Employee First Name', 'employeeFirstname', 'false', NULL, NULL, 'label', '<xml><getter>employeeFirstname</getter></xml>', '200', '0', NULL, 0, 1, '---', 0, 0),
(12, 3, 'hs_hr_employee.emp_middle_name', 'Employee Middle Name', 'employeeMiddlename', 'false', NULL, NULL, 'label', '<xml><getter>employeeMiddlename</getter></xml>', '200', '0', NULL, 0, 1, '---', 0, 0),
(13, 3, 'hs_hr_employee.emp_birthday', 'Date of Birth', 'empBirthday', 'false', NULL, NULL, 'labelDate', '<xml><getter>empBirthday</getter></xml>', '100', '0', NULL, 0, 1, '---', 0, 0),
(14, 3, 'ohrm_nationality.name', 'Nationality', 'nationality', 'false', NULL, NULL, 'label', '<xml><getter>nationality</getter></xml>', '200', '0', NULL, 0, 1, '---', 0, 0),
(15, 3, 'CASE hs_hr_employee.emp_gender WHEN 1 THEN "Male" WHEN 2 THEN "Female" WHEN 3 THEN "Other" END', 'Gender', 'empGender', 'false', NULL, NULL, 'label', '<xml><getter>empGender</getter></xml>', '80', '0', NULL, 0, 1, '---', 0, 0),
(17, 3, 'hs_hr_employee.emp_marital_status', 'Marital Status', 'maritalStatus', 'false', NULL, NULL, 'label', '<xml><getter>maritalStatus</getter></xml>', '100', '0', NULL, 0, 1, '---', 0, 0),
(18, 3, 'hs_hr_employee.emp_dri_lice_num', 'Driver License Number', 'driversLicenseNumber', 'false', NULL, NULL, 'label', '<xml><getter>driversLicenseNumber</getter></xml>', '240', '0', NULL, 0, 1, '---', 0, 0),
(19, 3, 'hs_hr_employee.emp_dri_lice_exp_date', 'License Expiry Date', 'licenseExpiryDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>licenseExpiryDate</getter></xml>', '135', '0', NULL, 0, 1, '---', 0, 0),
(20, 3, 'CONCAT_WS(", ", NULLIF(hs_hr_employee.emp_street1, ""), NULLIF(hs_hr_employee.emp_street2, ""), NULLIF(hs_hr_employee.city_code, ""), NULLIF(hs_hr_employee.provin_code,""), NULLIF(hs_hr_employee.emp_zipcode,""), NULLIF(hs_hr_country.cou_name,""))', 'Address', 'address', 'false', NULL, NULL, 'label', '<xml><getter>address</getter></xml>', '200', '0', NULL, 0, 2, '---', 0, 0),
(21, 3, 'hs_hr_employee.emp_hm_telephone', 'Home Telephone', 'homeTelephone', 'false', NULL, NULL, 'label', '<xml><getter>homeTelephone</getter></xml>', '130', '0', NULL, 0, 2, '---', 0, 0),
(22, 3, 'hs_hr_employee.emp_mobile', 'Mobile', 'mobile', 'false', NULL, NULL, 'label', '<xml><getter>mobile</getter></xml>', '100', '0', NULL, 0, 2, '---', 0, 0),
(23, 3, 'hs_hr_employee.emp_work_telephone', 'Work Telephone', 'workTelephone', 'false', NULL, NULL, 'label', '<xml><getter>workTelephone</getter></xml>', '100', '0', NULL, 0, 2, '---', 0, 0),
(24, 3, 'hs_hr_employee.emp_work_email', 'Work Email', 'workEmail', 'false', NULL, NULL, 'label', '<xml><getter>workEmail</getter></xml>', '200', '0', NULL, 0, 2, '---', 0, 0),
(25, 3, 'hs_hr_employee.emp_oth_email', 'Other Email', 'otherEmail', 'false', NULL, NULL, 'label', '<xml><getter>otherEmail</getter></xml>', '200', '0', NULL, 0, 2, '---', 0, 0),
(26, 3, 'hs_hr_emp_emergency_contacts.eec_name', 'Name', 'ecname', 'false', NULL, NULL, 'label', '<xml><getter>ecname</getter></xml>', '200', '0', NULL, 1, 3, '---', 0, 0),
(27, 3, 'hs_hr_emp_emergency_contacts.eec_home_no', 'Home Telephone', 'ecHomeTelephone', 'false', NULL, NULL, 'label', '<xml><getter>ecHomeTelephone</getter></xml>', '130', '0', NULL, 1, 3, '---', 0, 0),
(28, 3, 'hs_hr_emp_emergency_contacts.eec_office_no', 'Work Telephone', 'ecWorkTelephone', 'false', NULL, NULL, 'label', '<xml><getter>ecWorkTelephone</getter></xml>', '100', '0', NULL, 1, 3, '---', 0, 0),
(29, 3, 'hs_hr_emp_emergency_contacts.eec_relationship', 'Relationship', 'ecRelationship', 'false', NULL, NULL, 'label', '<xml><getter>ecRelationship</getter></xml>', '200', '0', NULL, 1, 3, '---', 0, 0),
(30, 3, 'hs_hr_emp_emergency_contacts.eec_mobile_no', 'Mobile', 'ecMobile', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 3, '---', 0, 0),
(31, 3, 'hs_hr_emp_dependents.ed_name', 'Name', 'dependentName', 'false', NULL, NULL, 'label', '<xml><getter>dependentName</getter></xml>', '200', '0', NULL, 1, 4, '---', 0, 0),
(32, 3, 'IF (hs_hr_emp_dependents.ed_relationship_type = ''other'', hs_hr_emp_dependents.ed_relationship, hs_hr_emp_dependents.ed_relationship_type)', 'Relationship', 'dependentRelationship', 'false', NULL, NULL, 'label', '<xml><getter>dependentRelationship</getter></xml>', '200', '0', NULL, 1, 4, '---', 0, 0),
(33, 3, 'hs_hr_emp_dependents.ed_date_of_birth', 'Date of Birth', 'dependentDateofBirth', 'false', NULL, NULL, 'labelDate', '<xml><getter>dependentDateofBirth</getter></xml>', '100', '0', NULL, 1, 4, '---', 0, 0),
(35, 3, 'ohrm_membership.name', 'Membership', 'name', 'false', NULL, NULL, 'label', '<xml><getter>name</getter></xml>', '200', '0', NULL, 1, 15, '---', 0, 0),
(36, 3, 'hs_hr_emp_member_detail.ememb_subscript_ownership', 'Subscription Paid By', 'subscriptionPaidBy', 'false', NULL, NULL, 'label', '<xml><getter>subscriptionPaidBy</getter></xml>', '200', '0', NULL, 1, 15, '---', 0, 0),
(37, 3, 'hs_hr_emp_member_detail.ememb_subscript_amount', 'Subscription Amount', 'subscriptionAmount', 'false', NULL, NULL, 'label', '<xml><getter>subscriptionAmount</getter></xml>', '200', '0', NULL, 1, 15, '---', 0, 0),
(38, 3, 'hs_hr_emp_member_detail.ememb_subs_currency', 'Currency', 'membershipCurrency', 'false', NULL, NULL, 'label', '<xml><getter>membershipCurrency</getter></xml>', '200', '0', NULL, 1, 15, '---', 0, 0),
(39, 3, 'hs_hr_emp_member_detail.ememb_commence_date', 'Subscription Commence Date', 'subscriptionCommenceDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>subscriptionCommenceDate</getter></xml>', '200', '0', NULL, 1, 15, '---', 0, 0),
(40, 3, 'hs_hr_emp_member_detail.ememb_renewal_date', 'Subscription Renewal Date', 'subscriptionRenewalDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>subscriptionRenewalDate</getter></xml>', '200', '0', NULL, 1, 15, '---', 0, 0),
(41, 3, 'hs_hr_emp_work_experience.eexp_employer', 'Company', 'expCompany', 'false', NULL, NULL, 'label', '<xml><getter>expCompany</getter></xml>', '200', '0', NULL, 1, 10, '---', 0, 0),
(42, 3, 'hs_hr_emp_work_experience.eexp_jobtit', 'Job Title', 'expJobTitle', 'false', NULL, NULL, 'label', '<xml><getter>expJobTitle</getter></xml>', '200', '0', NULL, 1, 10, '---', 0, 0),
(43, 3, 'hs_hr_emp_work_experience.eexp_from_date', 'From', 'expFrom', 'false', NULL, NULL, 'labelDate', '<xml><getter>expFrom</getter></xml>', '100', '0', NULL, 1, 10, '---', 0, 0),
(44, 3, 'hs_hr_emp_work_experience.eexp_to_date', 'To', 'expTo', 'false', NULL, NULL, 'labelDate', '<xml><getter>expTo</getter></xml>', '100', '0', NULL, 1, 10, '---', 0, 0),
(45, 3, 'hs_hr_emp_work_experience.eexp_comments', 'Comment', 'expComment', 'false', NULL, NULL, 'label', '<xml><getter>expComment</getter></xml>', '200', '0', NULL, 1, 10, '---', 0, 0),
(47, 3, 'ohrm_education.name', 'Level', 'eduProgram', 'false', NULL, NULL, 'label', '<xml><getter>eduProgram</getter></xml>', '200', '0', NULL, 1, 11, '---', 0, 0),
(48, 3, 'ohrm_emp_education.year', 'Year', 'eduYear', 'false', NULL, NULL, 'label', '<xml><getter>eduYear</getter></xml>', '100', '0', NULL, 1, 11, '---', 0, 0),
(49, 3, 'ohrm_emp_education.score', 'Score', 'eduGPAOrScore', 'false', NULL, NULL, 'label', '<xml><getter>eduGPAOrScore</getter></xml>', '80', '0', NULL, 1, 11, '---', 0, 0),
(52, 3, 'ohrm_skill.name', 'Skill', 'skill', 'false', NULL, NULL, 'label', '<xml><getter>skill</getter></xml>', '200', '0', NULL, 1, 12, '---', 0, 0),
(53, 3, 'hs_hr_emp_skill.years_of_exp', 'Years of Experience', 'skillYearsOfExperience', 'false', NULL, NULL, 'label', '<xml><getter>skillYearsOfExperience</getter></xml>', '135', '0', NULL, 1, 12, '---', 0, 0),
(54, 3, 'hs_hr_emp_skill.comments', 'Comments', 'skillComments', 'false', NULL, NULL, 'label', '<xml><getter>skillComments</getter></xml>', '200', '0', NULL, 1, 12, '---', 0, 0),
(55, 3, 'ohrm_language.name', 'Language', 'langName', 'false', NULL, NULL, 'label', '<xml><getter>langName</getter></xml>', '200', '0', NULL, 1, 13, '---', 0, 0),
(57, 3, 'CASE hs_hr_emp_language.competency WHEN 1 THEN "Poor" WHEN 2 THEN "Basic" WHEN 3 THEN "Good" WHEN 4 THEN "Mother Tongue" END', 'Competency', 'langCompetency', 'false', NULL, NULL, 'label', '<xml><getter>langCompetency</getter></xml>', '130', '0', NULL, 1, 13, '---', 0, 0),
(58, 3, 'hs_hr_emp_language.comments', 'Comments', 'langComments', 'false', NULL, NULL, 'label', '<xml><getter>langComments</getter></xml>', '200', '0', NULL, 1, 13, '---', 0, 0),
(59, 3, 'ohrm_license.name', 'License Type', 'empLicenseType', 'false', NULL, NULL, 'label', '<xml><getter>empLicenseType</getter></xml>', '200', '0', NULL, 1, 14, '---', 0, 0),
(60, 3, 'ohrm_emp_license.license_issued_date', 'Issued Date', 'empLicenseIssuedDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>empLicenseIssuedDate</getter></xml>', '100', '0', NULL, 1, 14, '---', 0, 0),
(61, 3, 'ohrm_emp_license.license_expiry_date', 'Expiry Date', 'empLicenseExpiryDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>empLicenseExpiryDate</getter></xml>', '100', '0', NULL, 1, 14, '---', 0, 0),
(62, 3, 'supervisor.emp_firstname', 'First Name', 'supervisorFirstName', 'false', NULL, NULL, 'label', '<xml><getter>supervisorFirstName</getter></xml>', '200', '0', NULL, 1, 9, '---', 0, 0),
(63, 3, 'subordinate.emp_firstname', 'First Name', 'subordinateFirstName', 'false', NULL, NULL, 'label', '<xml><getter>subordinateFirstName</getter></xml>', '200', '0', NULL, 1, 8, '---', 0, 0),
(64, 3, 'supervisor.emp_lastname', 'Last Name', 'supervisorLastName', 'false', NULL, NULL, 'label', '<xml><getter>supervisorLastName</getter></xml>', '200', '0', NULL, 1, 9, '---', 0, 0),
(65, 3, 'ohrm_pay_grade.name', 'Pay Grade', 'salPayGrade', 'false', NULL, NULL, 'label', '<xml><getter>salPayGrade</getter></xml>', '200', '0', NULL, 1, 7, '---', 0, 0),
(66, 3, 'hs_hr_emp_basicsalary.salary_component', 'Salary Component', 'salSalaryComponent', 'false', NULL, NULL, 'label', '<xml><getter>salSalaryComponent</getter></xml>', '200', '0', NULL, 1, 7, '---', 0, 0),
(67, 3, 'hs_hr_emp_basicsalary.ebsal_basic_salary', 'Amount', 'salAmount', 'false', NULL, NULL, 'label', '<xml><getter>salAmount</getter></xml>', '200', '0', NULL, 1, 7, '---', 1, 0),
(68, 3, 'hs_hr_emp_basicsalary.comments', 'Comments', 'salComments', 'false', NULL, NULL, 'label', '<xml><getter>salComments</getter></xml>', '200', '0', NULL, 1, 7, '---', 0, 0),
(69, 3, 'hs_hr_payperiod.payperiod_name', 'Pay Frequency', 'salPayFrequency', 'false', NULL, NULL, 'label', '<xml><getter>salPayFrequency</getter></xml>', '200', '0', NULL, 1, 7, '---', 0, 0),
(70, 3, 'hs_hr_currency_type.currency_name', 'Currency', 'salCurrency', 'false', NULL, NULL, 'label', '<xml><getter>salCurrency</getter></xml>', '200', '0', NULL, 1, 7, '---', 0, 0),
(71, 3, 'hs_hr_emp_directdebit.dd_account', 'Direct Deposit Account Number', 'ddAccountNumber', 'false', NULL, NULL, 'label', '<xml><getter>ddAccountNumber</getter></xml>', '200', '0', NULL, 1, 7, '---', 0, 0),
(72, 3, 'hs_hr_emp_directdebit.dd_account_type', 'Direct Deposit Account Type', 'ddAccountType', 'false', NULL, NULL, 'label', '<xml><getter>ddAccountType</getter></xml>', '200', '0', NULL, 1, 7, '---', 0, 0),
(73, 3, 'hs_hr_emp_directdebit.dd_routing_num', 'Direct Deposit Routing Number', 'ddRoutingNumber', 'false', NULL, NULL, 'label', '<xml><getter>ddRoutingNumber</getter></xml>', '200', '0', NULL, 1, 7, '---', 0, 0),
(74, 3, 'hs_hr_emp_directdebit.dd_amount', 'Direct Deposit Amount', 'ddAmount', 'false', NULL, NULL, 'label', '<xml><getter>ddAmount</getter></xml>', '200', '0', NULL, 1, 7, '---', 0, 0),
(75, 3, 'hs_hr_emp_contract_extend.econ_extend_start_date', 'Contract Start Date', 'empContStartDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>empContStartDate</getter></xml>', '200', '0', NULL, 1, 6, '---', 0, 0),
(76, 3, 'hs_hr_emp_contract_extend.econ_extend_end_date', 'Contract End Date', 'empContEndDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>empContEndDate</getter></xml>', '200', '0', NULL, 1, 6, '---', 0, 0),
(77, 3, 'ohrm_job_title.job_title', 'Job Title', 'empJobTitle', 'false', NULL, NULL, 'label', '<xml><getter>empJobTitle</getter></xml>', '200', '0', NULL, 1, 6, '---', 0, 0),
(78, 3, 'ohrm_employment_status.name', 'Employment Status', 'empEmploymentStatus', 'false', NULL, NULL, 'label', '<xml><getter>empEmploymentStatus</getter></xml>', '200', '0', NULL, 1, 6, '---', 0, 0),
(80, 3, 'ohrm_job_category.name', 'Job Category', 'empJobCategory', 'false', NULL, NULL, 'label', '<xml><getter>empJobCategory</getter></xml>', '200', '0', NULL, 1, 6, '---', 0, 0),
(81, 3, 'hs_hr_employee.joined_date', 'Joined Date', 'empJoinedDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>empJoinedDate</getter></xml>', '100', '0', NULL, 1, 6, '---', 0, 0),
(82, 3, 'ohrm_subunit.name', 'Sub Unit', 'empSubUnit', 'false', NULL, NULL, 'label', '<xml><getter>empSubUnit</getter></xml>', '200', '0', NULL, 1, 6, '---', 0, 0),
(83, 3, 'ohrm_location.name', 'Location', 'empLocation', 'false', NULL, NULL, 'label', '<xml><getter>empLocation</getter></xml>', '200', '0', NULL, 1, 6, '---', 0, 0),
(84, 3, 'hs_hr_emp_passport.ep_passport_num', 'Number', 'empPassportNo', 'false', NULL, NULL, 'label', '<xml><getter>empPassportNo</getter></xml>', '200', '0', NULL, 1, 5, '---', 0, 0),
(85, 3, 'hs_hr_emp_passport.ep_passportissueddate', 'Issued Date', 'empPassportIssuedDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>empPassportIssuedDate</getter></xml>', '100', '0', NULL, 1, 5, '---', 0, 0),
(86, 3, 'hs_hr_emp_passport.ep_passportexpiredate', 'Expiry Date', 'empPassportExpiryDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>empPassportExpiryDate</getter></xml>', '100', '0', NULL, 1, 5, '---', 0, 0),
(87, 3, 'hs_hr_emp_passport.ep_i9_status', 'Eligibility Status', 'empPassportEligibleStatus', 'false', NULL, NULL, 'label', '<xml><getter>empPassportEligibleStatus</getter></xml>', '200', '0', NULL, 1, 5, '---', 0, 0),
(88, 3, 'hs_hr_emp_passport.cou_code', 'Issued By', 'empPassportIssuedBy', 'false', NULL, NULL, 'label', '<xml><getter>empPassportIssuedBy</getter></xml>', '200', '0', NULL, 1, 5, '---', 0, 0),
(89, 3, 'hs_hr_emp_passport.ep_i9_review_date', 'Eligibility Review Date', 'empPassportEligibleReviewDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>empPassportEligibleReviewDate</getter></xml>', '200', '0', NULL, 1, 5, '---', 0, 0),
(90, 3, 'hs_hr_emp_passport.ep_comments', 'Comments', 'empPassportComments', 'false', NULL, NULL, 'label', '<xml><getter>empPassportComments</getter></xml>', '200', '0', NULL, 1, 5, '---', 0, 0),
(91, 3, 'subordinate.emp_lastname', 'Last Name', 'subordinateLastName', 'false', NULL, NULL, 'label', '<xml><getter>subordinateLastName</getter></xml>', '200', '0', NULL, 1, 8, '---', 0, 0),
(92, 3, 'CASE hs_hr_emp_language.fluency WHEN 1 THEN "Writing" WHEN 2 THEN "Speaking" WHEN 3 THEN "Reading" END', 'Fluency', 'langFluency', 'false', NULL, NULL, 'label', '<xml><getter>langFluency</getter></xml>', '200', '0', NULL, 1, 13, '---', 0, 0),
(93, 3, 'supervisor_reporting_method.reporting_method_name', 'Reporting Method', 'supReportingMethod', 'false', NULL, NULL, 'label', '<xml><getter>supReportingMethod</getter></xml>', '200', '0', NULL, 1, 9, '---', 0, 0),
(94, 3, 'subordinate_reporting_method.reporting_method_name', 'Reporting Method', 'subReportingMethod', 'false', NULL, NULL, 'label', '<xml><getter>subReportingMethod</getter></xml>', '200', '0', NULL, 1, 8, '---', 0, 0),
(95, 3, 'CASE hs_hr_emp_passport.ep_passport_type_flg WHEN 1 THEN "Passport" WHEN 2 THEN "Visa" END', 'Document Type', 'documentType', 'false', NULL, NULL, 'label', '<xml><getter>documentType</getter></xml>', '200', '0', NULL, 1, 5, '---', 0, 0),
(97, 3, 'hs_hr_employee.emp_other_id', 'Other Id', 'otherId', 'false', NULL, NULL, 'label', '<xml><getter>otherId</getter></xml>', '100', '0', NULL, 0, 1, '---', 0, 0),
(98, 3, 'hs_hr_emp_emergency_contacts.eec_seqno', 'ecSeqNo', 'ecSeqNo', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 3, '---', 0, 1),
(99, 3, 'hs_hr_emp_dependents.ed_seqno', 'SeqNo', 'edSeqNo', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 4, '---', 0, 1),
(100, 3, 'hs_hr_emp_passport.ep_seqno', 'SeqNo', 'epSeqNo', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 5, '---', 0, 1),
(101, 3, 'hs_hr_emp_basicsalary.id', 'salaryId', 'salaryId', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 7, '---', 0, 1),
(102, 3, 'subordinate.emp_number', 'subordinateId', 'subordinateId', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 8, '---', 0, 1),
(103, 3, 'supervisor.emp_number', 'supervisorId', 'supervisorId', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 9, '---', 0, 1),
(104, 3, 'hs_hr_emp_work_experience.eexp_seqno', 'workExpSeqNo', 'workExpSeqNo', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 10, '---', 0, 1),
(105, 3, 'ohrm_emp_education.education_id', 'empEduCode', 'empEduCode', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 11, '---', 0, 1),
(106, 3, 'hs_hr_emp_skill.skill_id', 'empSkillCode', 'empSkillCode', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 12, '---', 0, 1),
(107, 3, 'hs_hr_emp_language.lang_id', 'empLangCode', 'empLangCode', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 13, '---', 0, 1),
(108, 3, 'hs_hr_emp_language.fluency', 'empLangType', 'empLangType', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 13, '---', 0, 1),
(109, 3, 'ohrm_emp_license.license_id', 'empLicenseCode', 'empLicenseCode', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 14, '---', 0, 1),
(110, 3, 'hs_hr_emp_member_detail.membship_code', 'membershipCode', 'membershipCode', 'false', NULL, NULL, 'label', '<xml><getter>ecMobile</getter></xml>', '100', '0', NULL, 1, 15, '---', 0, 1),
(112, 3, 'ROUND(DATEDIFF(hs_hr_emp_work_experience.eexp_to_date, hs_hr_emp_work_experience.eexp_from_date)/365,1)', 'Duration', 'expDuration', 'false', NULL, NULL, 'label', '<xml><getter>expDuration</getter></xml>', '100', '0', NULL, 1, 10, '---', 0, 0),
(113, 3, 'ohrm_emp_termination.termination_date', 'Termination Date', 'terminationDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>terminationDate</getter></xml>', '100', '0', NULL, 1, 6, '---', 0, 0),
(114, 3, 'ohrm_emp_termination_reason.name', 'Termination Reason', 'terminationReason', 'false', NULL, NULL, 'label', '<xml><getter>terminationReason</getter></xml>', '100', '0', NULL, 1, 6, '---', 0, 0),
(115, 3, 'ohrm_emp_education.institute', 'Institute', 'getInstitute', 'false', NULL, NULL, 'label', '<xml><getter>getInstitute</getter></xml>', '80', '0', NULL, 1, 11, '---', 0, 0),
(116, 3, 'ohrm_emp_education.major', 'Major/Specialization', 'getMajor', 'false', NULL, NULL, 'label', '<xml><getter>getMajor</getter></xml>', '80', '0', NULL, 1, 11, '---', 0, 0),
(117, 3, 'ohrm_emp_education.start_date', 'Start Date', 'getStartDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>getStartDate</getter></xml>', '80', '0', NULL, 1, 11, '---', 0, 0),
(118, 3, 'ohrm_emp_education.end_date', 'End Date', 'getEndDate', 'false', NULL, NULL, 'labelDate', '<xml><getter>getEndDate</getter></xml>', '80', '0', NULL, 1, 11, '---', 0, 0),
(119, 3, 'ohrm_emp_license.license_no', 'License Number', 'getLicenseNo', 'false', NULL, NULL, 'label', '<xml><getter>getLicenseNo</getter></xml>', '200', '0', NULL, 1, 14, '---', 0, 0),
(120, 3, 'ohrm_emp_termination.note', 'Termination Note', 'getNote', 'false', NULL, NULL, 'label', '<xml><getter>getNote</getter></xml>', '100', '0', NULL, 1, 6, '---', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_display_field_group`
--

CREATE TABLE IF NOT EXISTS `ohrm_display_field_group` (
`id` int(10) unsigned NOT NULL,
  `report_group_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_list` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_display_field_group`
--

INSERT INTO `ohrm_display_field_group` (`id`, `report_group_id`, `name`, `is_list`) VALUES
(1, 3, 'Personal', 0),
(2, 3, 'Contact Details', 0),
(3, 3, 'Emergency Contacts', 1),
(4, 3, 'Dependents', 1),
(5, 3, 'Immigration', 1),
(6, 3, 'Job', 0),
(7, 3, 'Salary', 1),
(8, 3, 'Subordinates', 1),
(9, 3, 'Supervisors', 1),
(10, 3, 'Work Experience', 1),
(11, 3, 'Education', 1),
(12, 3, 'Skills', 1),
(13, 3, 'Languages', 1),
(14, 3, 'License', 1),
(15, 3, 'Memberships', 1),
(16, 3, 'Custom Fields', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_education`
--

CREATE TABLE IF NOT EXISTS `ohrm_education` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_email`
--

CREATE TABLE IF NOT EXISTS `ohrm_email` (
`id` int(6) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_email`
--

INSERT INTO `ohrm_email` (`id`, `name`) VALUES
(1, 'leave.apply'),
(3, 'leave.approve'),
(2, 'leave.assign'),
(4, 'leave.cancel'),
(6, 'leave.change'),
(5, 'leave.reject');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_email_configuration`
--

CREATE TABLE IF NOT EXISTS `ohrm_email_configuration` (
`id` int(10) NOT NULL,
  `mail_type` varchar(50) DEFAULT NULL,
  `sent_as` varchar(250) NOT NULL,
  `sendmail_path` varchar(250) DEFAULT NULL,
  `smtp_host` varchar(250) DEFAULT NULL,
  `smtp_port` int(10) DEFAULT NULL,
  `smtp_username` varchar(250) DEFAULT NULL,
  `smtp_password` varchar(250) DEFAULT NULL,
  `smtp_auth_type` varchar(50) DEFAULT NULL,
  `smtp_security_type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_email_notification`
--

CREATE TABLE IF NOT EXISTS `ohrm_email_notification` (
`id` int(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `is_enable` int(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_email_notification`
--

INSERT INTO `ohrm_email_notification` (`id`, `name`, `is_enable`) VALUES
(1, 'Leave Applications', 0),
(2, 'Leave Assignments', 0),
(3, 'Leave Approvals', 0),
(4, 'Leave Cancellations', 0),
(5, 'Leave Rejections', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_email_processor`
--

CREATE TABLE IF NOT EXISTS `ohrm_email_processor` (
`id` int(6) NOT NULL,
  `email_id` int(6) NOT NULL,
  `class_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_email_processor`
--

INSERT INTO `ohrm_email_processor` (`id`, `email_id`, `class_name`) VALUES
(1, 1, 'LeaveEmailProcessor'),
(2, 2, 'LeaveEmailProcessor'),
(3, 3, 'LeaveEmailProcessor'),
(4, 4, 'LeaveEmailProcessor'),
(5, 5, 'LeaveEmailProcessor'),
(6, 6, 'LeaveChangeMailProcessor');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_email_subscriber`
--

CREATE TABLE IF NOT EXISTS `ohrm_email_subscriber` (
`id` int(6) NOT NULL,
  `notification_id` int(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_email_template`
--

CREATE TABLE IF NOT EXISTS `ohrm_email_template` (
`id` int(6) NOT NULL,
  `email_id` int(6) NOT NULL,
  `locale` varchar(20) DEFAULT NULL,
  `performer_role` varchar(50) DEFAULT NULL,
  `recipient_role` varchar(50) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` text
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_email_template`
--

INSERT INTO `ohrm_email_template` (`id`, `email_id`, `locale`, `performer_role`, `recipient_role`, `subject`, `body`) VALUES
(1, 1, 'en_US', NULL, 'supervisor', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/apply/leaveApplicationSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/apply/leaveApplicationBody.txt'),
(2, 1, 'en_US', NULL, 'subscriber', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/apply/leaveApplicationSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/apply/leaveApplicationSubscriberBody.txt'),
(3, 3, 'en_US', NULL, 'ess', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/approve/leaveApprovalSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/approve/leaveApprovalBody.txt'),
(4, 3, 'en_US', NULL, 'subscriber', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/approve/leaveApprovalSubscriberSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/approve/leaveApprovalSubscriberBody.txt'),
(5, 2, 'en_US', NULL, 'ess', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/assign/leaveAssignmentSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/assign/leaveAssignmentBody.txt'),
(6, 2, 'en_US', NULL, 'supervisor', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/assign/leaveAssignmentSubjectForSupervisors.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/assign/leaveAssignmentBodyForSupervisors.txt'),
(7, 2, 'en_US', NULL, 'subscriber', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/assign/leaveAssignmentSubscriberSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/assign/leaveAssignmentSubscriberBody.txt'),
(8, 4, 'en_US', 'ess', 'supervisor', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/cancel/leaveEmployeeCancellationSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/cancel/leaveEmployeeCancellationBody.txt'),
(9, 4, 'en_US', 'ess', 'subscriber', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/cancel/leaveEmployeeCancellationSubscriberSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/cancel/leaveEmployeeCancellationSubscriberBody.txt'),
(10, 4, 'en_US', NULL, 'ess', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/cancel/leaveCancellationSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/cancel/leaveCancellationBody.txt'),
(11, 4, 'en_US', NULL, 'subscriber', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/cancel/leaveCancellationSubscriberSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/cancel/leaveCancellationSubscriberBody.txt'),
(12, 5, 'en_US', NULL, 'ess', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/reject/leaveRejectionSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/reject/leaveRejectionBody.txt'),
(13, 5, 'en_US', NULL, 'subscriber', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/reject/leaveRejectionSubscriberSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/reject/leaveRejectionSubscriberBody.txt'),
(14, 6, 'en_US', NULL, 'ess', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/change/leaveChangeSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/change/leaveChangeBody.txt'),
(15, 6, 'en_US', NULL, 'subscriber', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/change/leaveChangeSubscriberSubject.txt', 'orangehrmLeavePlugin/modules/leave/templates/mail/en_US/change/leaveChangeSubscriberBody.txt');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_employee_work_shift`
--

CREATE TABLE IF NOT EXISTS `ohrm_employee_work_shift` (
`work_shift_id` int(11) NOT NULL,
  `emp_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_employment_status`
--

CREATE TABLE IF NOT EXISTS `ohrm_employment_status` (
`id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_emp_education`
--

CREATE TABLE IF NOT EXISTS `ohrm_emp_education` (
`id` int(11) NOT NULL,
  `emp_number` int(11) NOT NULL,
  `education_id` int(11) NOT NULL,
  `institute` varchar(100) DEFAULT NULL,
  `major` varchar(100) DEFAULT NULL,
  `year` decimal(4,0) DEFAULT NULL,
  `score` varchar(25) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_emp_license`
--

CREATE TABLE IF NOT EXISTS `ohrm_emp_license` (
  `emp_number` int(11) NOT NULL,
  `license_id` int(11) NOT NULL,
  `license_no` varchar(50) DEFAULT NULL,
  `license_issued_date` date DEFAULT NULL,
  `license_expiry_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_emp_reporting_method`
--

CREATE TABLE IF NOT EXISTS `ohrm_emp_reporting_method` (
`reporting_method_id` int(7) NOT NULL,
  `reporting_method_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_emp_reporting_method`
--

INSERT INTO `ohrm_emp_reporting_method` (`reporting_method_id`, `reporting_method_name`) VALUES
(1, 'Direct'),
(2, 'Indirect');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_emp_termination`
--

CREATE TABLE IF NOT EXISTS `ohrm_emp_termination` (
`id` int(4) NOT NULL,
  `emp_number` int(4) DEFAULT NULL,
  `reason_id` int(4) DEFAULT NULL,
  `termination_date` date NOT NULL,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_emp_termination_reason`
--

CREATE TABLE IF NOT EXISTS `ohrm_emp_termination_reason` (
`id` int(4) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_emp_termination_reason`
--

INSERT INTO `ohrm_emp_termination_reason` (`id`, `name`) VALUES
(1, 'Other'),
(2, 'Retired'),
(3, 'Contract Not Renewed'),
(4, 'Resigned - Company Requested'),
(5, 'Resigned - Self Proposed'),
(6, 'Resigned'),
(7, 'Deceased'),
(8, 'Physically Disabled/Compensated'),
(9, 'Laid-off'),
(10, 'Dismissed');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_filter_field`
--

CREATE TABLE IF NOT EXISTS `ohrm_filter_field` (
  `filter_field_id` bigint(20) NOT NULL,
  `report_group_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `where_clause_part` mediumtext NOT NULL,
  `filter_field_widget` varchar(255) DEFAULT NULL,
  `condition_no` int(20) NOT NULL,
  `required` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_filter_field`
--

INSERT INTO `ohrm_filter_field` (`filter_field_id`, `report_group_id`, `name`, `where_clause_part`, `filter_field_widget`, `condition_no`, `required`) VALUES
(1, 1, 'project_name', 'ohrm_project.project_id', 'ohrmWidgetProjectList', 2, 'true'),
(2, 1, 'activity_show_deleted', 'ohrm_project_activity.is_deleted', 'ohrmWidgetInputCheckbox', 2, 'false'),
(3, 1, 'project_date_range', 'date', 'ohrmWidgetDateRange', 1, 'false'),
(4, 1, 'employee', 'hs_hr_employee.emp_number', 'ohrmReportWidgetEmployeeListAutoFill', 2, 'true'),
(5, 1, 'activity_name', 'ohrm_project_activity.activity_id', 'ohrmWidgetProjectActivityList', 2, 'true'),
(6, 1, 'project_name', 'ohrm_project.project_id', 'ohrmWidgetProjectListWithAllOption', 2, 'true'),
(7, 1, 'only_include_approved_timesheets', 'ohrm_timesheet.state', 'ohrmWidgetApprovedTimesheetInputCheckBox', 2, NULL),
(8, 3, 'employee_name', 'hs_hr_employee.emp_number', 'ohrmReportWidgetEmployeeListAutoFill', 1, NULL),
(9, 3, 'pay_grade', 'hs_hr_emp_basicsalary.sal_grd_code', 'ohrmReportWidgetPayGradeDropDown', 1, NULL),
(10, 3, 'education', 'ohrm_emp_education.education_id', 'ohrmReportWidgetEducationtypeDropDown', 1, NULL),
(11, 3, 'employment_status', 'hs_hr_employee.emp_status', 'ohrmWidgetEmploymentStatusList', 1, NULL),
(12, 3, 'service_period', 'datediff(current_date(), hs_hr_employee.joined_date)/365', 'ohrmReportWidgetServicePeriod', 1, NULL),
(13, 3, 'joined_date', 'hs_hr_employee.joined_date', 'ohrmReportWidgetJoinedDate', 1, NULL),
(14, 3, 'job_title', 'hs_hr_employee.job_title_code', 'ohrmWidgetJobTitleList', 1, NULL),
(15, 3, 'language', 'hs_hr_emp_language.lang_id', 'ohrmReportWidgetLanguageDropDown', 1, NULL),
(16, 3, 'skill', 'hs_hr_emp_skill.skill_id', 'ohrmReportWidgetSkillDropDown', 1, NULL),
(17, 3, 'age_group', 'datediff(current_date(), hs_hr_employee.emp_birthday)/365', 'ohrmReportWidgetAgeGroup', 1, NULL),
(18, 3, 'sub_unit', 'hs_hr_employee.work_station', 'ohrmWidgetSubDivisionList', 1, NULL),
(19, 3, 'gender', 'hs_hr_employee.emp_gender', 'ohrmReportWidgetGenderDropDown', 1, NULL),
(20, 3, 'location', 'ohrm_location.id', 'ohrmReportWidgetOperationalCountryLocationDropDown', 1, NULL),
(21, 1, 'is_deleted', 'ohrm_project_activity.is_deleted', '', 2, NULL),
(22, 3, 'include', 'hs_hr_employee.termination_id', 'ohrmReportWidgetIncludedEmployeesDropDown', 1, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_group_field`
--

CREATE TABLE IF NOT EXISTS `ohrm_group_field` (
  `group_field_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `group_by_clause` mediumtext NOT NULL,
  `group_field_widget` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_group_field`
--

INSERT INTO `ohrm_group_field` (`group_field_id`, `name`, `group_by_clause`, `group_field_widget`) VALUES
(1, 'activity id', 'GROUP BY ohrm_project_activity.activity_id', NULL),
(2, 'employee number', 'GROUP BY hs_hr_employee.emp_number', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_holiday`
--

CREATE TABLE IF NOT EXISTS `ohrm_holiday` (
`id` int(10) unsigned NOT NULL,
  `description` text,
  `date` date DEFAULT NULL,
  `recurring` tinyint(3) unsigned DEFAULT '0',
  `length` int(10) unsigned DEFAULT NULL,
  `operational_country_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_home_page`
--

CREATE TABLE IF NOT EXISTS `ohrm_home_page` (
`id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `enable_class` varchar(100) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0' COMMENT 'lowest priority 0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_home_page`
--

INSERT INTO `ohrm_home_page` (`id`, `user_role_id`, `action`, `enable_class`, `priority`) VALUES
(1, 1, 'dashboard/index', NULL, 15),
(2, 2, 'dashboard/index', NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_candidate`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_candidate` (
  `id` int(13) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact_number` varchar(30) DEFAULT NULL,
  `status` int(4) NOT NULL,
  `comment` text,
  `mode_of_application` int(4) NOT NULL,
  `date_of_application` date NOT NULL,
  `cv_file_id` int(13) DEFAULT NULL,
  `cv_text_version` text,
  `keywords` varchar(255) DEFAULT NULL,
  `added_person` int(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_job_candidate`
--

INSERT INTO `ohrm_job_candidate` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `contact_number`, `status`, `comment`, `mode_of_application`, `date_of_application`, `cv_file_id`, `cv_text_version`, `keywords`, `added_person`) VALUES
(3, 'sas', '', 'asasas', 'asas2SDS@SADAS.FSDF', '', 1, '', 1, '2014-10-21', NULL, NULL, '', NULL),
(4, 'Mac', '', 'Mac', 'mac@mac.mac', '', 1, '', 1, '2014-10-31', NULL, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_candidate_attachment`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_candidate_attachment` (
`id` int(13) NOT NULL,
  `candidate_id` int(13) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `file_type` varchar(200) DEFAULT NULL,
  `file_size` int(11) NOT NULL,
  `file_content` mediumblob,
  `attachment_type` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_candidate_history`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_candidate_history` (
`id` int(13) NOT NULL,
  `candidate_id` int(13) NOT NULL,
  `vacancy_id` int(13) DEFAULT NULL,
  `candidate_vacancy_name` varchar(255) DEFAULT NULL,
  `interview_id` int(13) DEFAULT NULL,
  `action` int(4) NOT NULL,
  `performed_by` int(13) DEFAULT NULL,
  `performed_date` datetime NOT NULL,
  `note` text,
  `interviewers` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_job_candidate_history`
--

INSERT INTO `ohrm_job_candidate_history` (`id`, `candidate_id`, `vacancy_id`, `candidate_vacancy_name`, `interview_id`, `action`, `performed_by`, `performed_date`, `note`, `interviewers`) VALUES
(3, 3, NULL, NULL, NULL, 16, NULL, '2014-10-21 01:12:07', NULL, NULL),
(4, 4, 3, 'Data Analyst', NULL, 1, NULL, '2014-10-31 15:46:11', NULL, NULL),
(5, 4, NULL, NULL, NULL, 16, NULL, '2014-10-31 15:46:11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_candidate_vacancy`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_candidate_vacancy` (
  `id` int(13) DEFAULT NULL,
  `candidate_id` int(13) NOT NULL,
  `vacancy_id` int(13) NOT NULL,
  `status` varchar(100) NOT NULL,
  `applied_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_job_candidate_vacancy`
--

INSERT INTO `ohrm_job_candidate_vacancy` (`id`, `candidate_id`, `vacancy_id`, `status`, `applied_date`) VALUES
(1, 4, 3, 'APPLICATION INITIATED', '2014-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_category`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_category` (
`id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_job_category`
--

INSERT INTO `ohrm_job_category` (`id`, `name`) VALUES
(1, 'Officials and Managers'),
(2, 'Professionals'),
(3, 'Technicians'),
(4, 'Sales Workers'),
(5, 'Operatives'),
(6, 'Office and Clerical Workers'),
(7, 'Craft Workers'),
(8, 'Service Workers'),
(9, 'Laborers and Helpers');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_interview`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_interview` (
`id` int(13) NOT NULL,
  `candidate_vacancy_id` int(13) DEFAULT NULL,
  `candidate_id` int(13) DEFAULT NULL,
  `interview_name` varchar(100) NOT NULL,
  `interview_date` date DEFAULT NULL,
  `interview_time` time DEFAULT NULL,
  `note` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_interview_attachment`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_interview_attachment` (
`id` int(13) NOT NULL,
  `interview_id` int(13) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `file_type` varchar(200) DEFAULT NULL,
  `file_size` int(11) NOT NULL,
  `file_content` mediumblob,
  `attachment_type` int(4) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_interview_interviewer`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_interview_interviewer` (
  `interview_id` int(13) NOT NULL,
  `interviewer_id` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_specification_attachment`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_specification_attachment` (
`id` int(13) NOT NULL,
  `job_title_id` int(13) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `file_type` varchar(200) DEFAULT NULL,
  `file_size` int(11) NOT NULL,
  `file_content` mediumblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_title`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_title` (
`id` int(13) NOT NULL,
  `job_title` varchar(100) NOT NULL,
  `job_description` varchar(400) DEFAULT NULL,
  `note` varchar(400) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_job_title`
--

INSERT INTO `ohrm_job_title` (`id`, `job_title`, `job_description`, `note`, `is_deleted`) VALUES
(1, 'Accountant', '', '', 0),
(2, 'Receptionist', '', '', 0),
(3, 'Data Analyst', '', '', 0),
(4, 'Electrical Technician', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_vacancy`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_vacancy` (
  `id` int(13) NOT NULL,
  `job_title_code` int(4) NOT NULL,
  `hiring_manager_id` int(13) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `no_of_positions` int(13) DEFAULT NULL,
  `status` int(4) NOT NULL,
  `published_in_feed` tinyint(1) NOT NULL DEFAULT '0',
  `defined_time` datetime NOT NULL,
  `updated_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_job_vacancy`
--

INSERT INTO `ohrm_job_vacancy` (`id`, `job_title_code`, `hiring_manager_id`, `name`, `description`, `no_of_positions`, `status`, `published_in_feed`, `defined_time`, `updated_time`) VALUES
(1, 1, 1, 'Web Developer', '', NULL, 1, 0, '2014-10-20 11:29:34', '2014-10-20 15:12:54'),
(3, 1, 2, 'Data Analyst', '', 1, 1, 1, '2014-10-31 15:45:50', '2014-10-31 15:45:50');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_job_vacancy_attachment`
--

CREATE TABLE IF NOT EXISTS `ohrm_job_vacancy_attachment` (
`id` int(13) NOT NULL,
  `vacancy_id` int(13) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `file_type` varchar(200) DEFAULT NULL,
  `file_size` int(11) NOT NULL,
  `file_content` mediumblob,
  `attachment_type` int(4) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_kpi`
--

CREATE TABLE IF NOT EXISTS `ohrm_kpi` (
`id` bigint(20) NOT NULL,
  `job_title_code` varchar(10) DEFAULT NULL,
  `kpi_indicators` varchar(255) DEFAULT NULL,
  `min_rating` int(7) DEFAULT '0',
  `max_rating` int(7) DEFAULT '0',
  `default_kpi` smallint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_kpi`
--

INSERT INTO `ohrm_kpi` (`id`, `job_title_code`, `kpi_indicators`, `min_rating`, `max_rating`, `default_kpi`, `deleted_at`) VALUES
(1, '2', 'Returning Clients', 1, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_language`
--

CREATE TABLE IF NOT EXISTS `ohrm_language` (
`id` int(11) NOT NULL,
  `name` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave` (
`id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `length_hours` decimal(6,2) unsigned DEFAULT NULL,
  `length_days` decimal(6,4) unsigned DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `comments` varchar(256) DEFAULT NULL,
  `leave_request_id` int(10) unsigned NOT NULL,
  `leave_type_id` int(10) unsigned NOT NULL,
  `emp_number` int(7) NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `duration_type` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_leave`
--

INSERT INTO `ohrm_leave` (`id`, `date`, `length_hours`, `length_days`, `status`, `comments`, `leave_request_id`, `leave_type_id`, `emp_number`, `start_time`, `end_time`, `duration_type`) VALUES
(1, '2014-11-21', '4.00', '0.5000', 3, '', 1, 2, 1, '09:00:00', '13:00:00', 1),
(2, '2014-11-22', '0.00', '0.0000', 4, NULL, 1, 2, 1, '09:00:00', '13:00:00', 1),
(3, '2014-11-23', '0.00', '0.0000', 4, NULL, 1, 2, 1, '09:00:00', '13:00:00', 1),
(4, '2014-11-24', '4.00', '0.5000', 1, NULL, 1, 2, 1, '09:00:00', '13:00:00', 1),
(5, '2014-11-25', '4.00', '0.5000', 1, NULL, 1, 2, 1, '09:00:00', '13:00:00', 1),
(6, '2014-11-26', '4.00', '0.5000', 1, NULL, 1, 2, 1, '09:00:00', '13:00:00', 1),
(7, '2014-11-27', '4.00', '0.5000', 1, NULL, 1, 2, 1, '09:00:00', '13:00:00', 1),
(8, '2014-11-28', '4.00', '0.5000', 1, NULL, 1, 2, 1, '09:00:00', '13:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_adjustment`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_adjustment` (
`id` int(10) unsigned NOT NULL,
  `emp_number` int(7) NOT NULL,
  `no_of_days` decimal(19,15) NOT NULL,
  `leave_type_id` int(10) unsigned NOT NULL,
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `credited_date` datetime DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `adjustment_type` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_by_id` int(10) DEFAULT NULL,
  `created_by_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_comment`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_comment` (
`id` int(11) NOT NULL,
  `leave_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by_name` varchar(255) NOT NULL,
  `created_by_id` int(10) DEFAULT NULL,
  `created_by_emp_number` int(7) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_leave_comment`
--

INSERT INTO `ohrm_leave_comment` (`id`, `leave_id`, `created`, `created_by_name`, `created_by_id`, `created_by_emp_number`, `comments`) VALUES
(1, 1, '2014-11-20 17:30:41', 'Admin', 1, NULL, 'Y do u want to leave');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_entitlement`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_entitlement` (
`id` int(10) unsigned NOT NULL,
  `emp_number` int(7) NOT NULL,
  `no_of_days` decimal(19,15) NOT NULL,
  `days_used` decimal(8,4) NOT NULL DEFAULT '0.0000',
  `leave_type_id` int(10) unsigned NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime DEFAULT NULL,
  `credited_date` datetime DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `entitlement_type` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_by_id` int(10) DEFAULT NULL,
  `created_by_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_leave_entitlement`
--

INSERT INTO `ohrm_leave_entitlement` (`id`, `emp_number`, `no_of_days`, `days_used`, `leave_type_id`, `from_date`, `to_date`, `credited_date`, `note`, `entitlement_type`, `deleted`, `created_by_id`, `created_by_name`) VALUES
(1, 1, '60.000000000000000', '3.0000', 2, '2014-01-01 00:00:00', '2014-12-31 00:00:00', '2014-11-20 00:00:00', NULL, 1, 0, 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_entitlement_adjustment`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_entitlement_adjustment` (
`id` int(11) NOT NULL,
  `adjustment_id` int(10) unsigned NOT NULL,
  `entitlement_id` int(10) unsigned NOT NULL,
  `length_days` decimal(4,2) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_entitlement_type`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_entitlement_type` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_editable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_leave_entitlement_type`
--

INSERT INTO `ohrm_leave_entitlement_type` (`id`, `name`, `is_editable`) VALUES
(1, 'Added', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_leave_entitlement`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_leave_entitlement` (
`id` int(11) NOT NULL,
  `leave_id` int(11) NOT NULL,
  `entitlement_id` int(10) unsigned NOT NULL,
  `length_days` decimal(6,4) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_leave_leave_entitlement`
--

INSERT INTO `ohrm_leave_leave_entitlement` (`id`, `leave_id`, `entitlement_id`, `length_days`) VALUES
(1, 1, 1, '0.5000'),
(2, 4, 1, '0.5000'),
(3, 5, 1, '0.5000'),
(4, 6, 1, '0.5000'),
(5, 7, 1, '0.5000'),
(6, 8, 1, '0.5000');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_period_history`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_period_history` (
`id` int(11) NOT NULL,
  `leave_period_start_month` int(11) NOT NULL,
  `leave_period_start_day` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_leave_period_history`
--

INSERT INTO `ohrm_leave_period_history` (`id`, `leave_period_start_month`, `leave_period_start_day`, `created_at`) VALUES
(1, 1, 1, '2014-10-21'),
(2, 1, 1, '2014-11-20'),
(3, 1, 1, '2014-11-20');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_request`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_request` (
`id` int(10) unsigned NOT NULL,
  `leave_type_id` int(10) unsigned NOT NULL,
  `date_applied` date NOT NULL,
  `emp_number` int(7) NOT NULL,
  `comments` varchar(256) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_leave_request`
--

INSERT INTO `ohrm_leave_request` (`id`, `leave_type_id`, `date_applied`, `emp_number`, `comments`) VALUES
(1, 2, '2014-11-21', 1, 'I am sick');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_request_comment`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_request_comment` (
`id` int(11) NOT NULL,
  `leave_request_id` int(10) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by_name` varchar(255) NOT NULL,
  `created_by_id` int(10) DEFAULT NULL,
  `created_by_emp_number` int(7) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_leave_request_comment`
--

INSERT INTO `ohrm_leave_request_comment` (`id`, `leave_request_id`, `created`, `created_by_name`, `created_by_id`, `created_by_emp_number`, `comments`) VALUES
(1, 1, '2014-11-20 13:34:19', 'John Doe', 2, 1, 'I am sick');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_status`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_status` (
`id` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_leave_status`
--

INSERT INTO `ohrm_leave_status` (`id`, `status`, `name`) VALUES
(1, -1, 'REJECTED'),
(2, 0, 'CANCELLED'),
(3, 1, 'PENDING APPROVAL'),
(4, 2, 'SCHEDULED'),
(5, 3, 'TAKEN'),
(6, 4, 'WEEKEND'),
(7, 5, 'HOLIDAY');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_leave_type`
--

CREATE TABLE IF NOT EXISTS `ohrm_leave_type` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `exclude_in_reports_if_no_entitlement` tinyint(1) NOT NULL DEFAULT '0',
  `operational_country_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_leave_type`
--

INSERT INTO `ohrm_leave_type` (`id`, `name`, `deleted`, `exclude_in_reports_if_no_entitlement`, `operational_country_id`) VALUES
(1, 'Meternity', 0, 1, NULL),
(2, 'Sick Leave', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_license`
--

CREATE TABLE IF NOT EXISTS `ohrm_license` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_location`
--

CREATE TABLE IF NOT EXISTS `ohrm_location` (
`id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  `country_code` varchar(3) NOT NULL,
  `province` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zip_code` varchar(35) DEFAULT NULL,
  `phone` varchar(35) DEFAULT NULL,
  `fax` varchar(35) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_login`
--

CREATE TABLE IF NOT EXISTS `ohrm_login` (
`id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_role_name` text NOT NULL,
  `user_role_predefined` tinyint(1) NOT NULL,
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=349 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_login`
--

INSERT INTO `ohrm_login` (`id`, `user_id`, `user_name`, `user_role_name`, `user_role_predefined`, `login_time`) VALUES
(1, 1, 'Admin', 'Admin', 1, '2014-10-20 06:11:13'),
(2, 1, 'Admin', 'Admin', 1, '2014-10-20 06:27:31'),
(3, 1, 'Admin', 'Admin', 1, '2014-10-20 06:34:17'),
(4, 1, 'Admin', 'Admin', 1, '2014-10-20 06:36:23'),
(5, 1, 'Admin', 'Admin', 1, '2014-10-20 06:37:08'),
(6, 1, 'Admin', 'Admin', 1, '2014-10-20 06:41:11'),
(7, 1, 'Admin', 'Admin', 1, '2014-10-20 06:42:34'),
(8, 1, 'Admin', 'Admin', 1, '2014-10-20 06:45:13'),
(9, 1, 'Admin', 'Admin', 1, '2014-10-20 06:50:15'),
(10, 1, 'Admin', 'Admin', 1, '2014-10-20 06:54:49'),
(11, 1, 'Admin', 'Admin', 1, '2014-10-20 07:09:22'),
(12, 1, 'Admin', 'Admin', 1, '2014-10-20 07:11:13'),
(13, 1, 'Admin', 'Admin', 1, '2014-10-20 07:20:23'),
(14, 1, 'Admin', 'Admin', 1, '2014-10-20 07:40:17'),
(15, 1, 'Admin', 'Admin', 1, '2014-10-20 07:41:58'),
(16, 1, 'Admin', 'Admin', 1, '2014-10-20 07:56:21'),
(17, 1, 'Admin', 'Admin', 1, '2014-10-20 09:31:37'),
(18, 1, 'Admin', 'Admin', 1, '2014-10-20 11:18:27'),
(19, 1, 'Admin', 'Admin', 1, '2014-10-20 16:11:10'),
(20, 1, 'Admin', 'Admin', 1, '2014-10-20 21:07:50'),
(21, 1, 'Admin', 'Admin', 1, '2014-10-20 21:59:11'),
(22, 1, 'Admin', 'Admin', 1, '2014-10-21 05:26:47'),
(23, 1, 'Admin', 'Admin', 1, '2014-10-21 06:46:31'),
(24, 1, 'Admin', 'Admin', 1, '2014-10-21 08:23:48'),
(25, 1, 'Admin', 'Admin', 1, '2014-10-21 10:45:31'),
(26, 1, 'Admin', 'Admin', 1, '2014-10-21 11:18:42'),
(27, 1, 'Admin', 'Admin', 1, '2014-10-21 12:54:43'),
(28, 1, 'Admin', 'Admin', 1, '2014-10-24 07:36:08'),
(29, 1, 'Admin', 'Admin', 1, '2014-10-24 09:14:44'),
(30, 1, 'Admin', 'Admin', 1, '2014-10-24 11:32:08'),
(31, 1, 'Admin', 'Admin', 1, '2014-10-24 12:21:09'),
(32, 1, 'Admin', 'Admin', 1, '2014-10-24 12:26:19'),
(33, 1, 'Admin', 'Admin', 1, '2014-10-24 13:04:16'),
(34, 1, 'Admin', 'Admin', 1, '2014-10-27 06:03:09'),
(35, 1, 'Admin', 'Admin', 1, '2014-10-27 07:07:25'),
(36, 1, 'Admin', 'Admin', 1, '2014-10-27 07:35:55'),
(37, 1, 'Admin', 'Admin', 1, '2014-10-27 07:57:15'),
(38, 1, 'Admin', 'Admin', 1, '2014-10-27 08:15:53'),
(39, 1, 'Admin', 'Admin', 1, '2014-10-27 08:57:54'),
(40, 1, 'Admin', 'Admin', 1, '2014-10-27 09:39:11'),
(41, 1, 'Admin', 'Admin', 1, '2014-10-27 11:23:08'),
(42, 1, 'Admin', 'Admin', 1, '2014-10-27 12:32:53'),
(43, 1, 'Admin', 'Admin', 1, '2014-10-27 12:36:25'),
(44, 1, 'Admin', 'Admin', 1, '2014-10-27 12:40:51'),
(45, 1, 'Admin', 'Admin', 1, '2014-10-27 12:45:29'),
(46, 1, 'Admin', 'Admin', 1, '2014-10-27 12:48:34'),
(47, 1, 'Admin', 'Admin', 1, '2014-10-27 12:50:33'),
(48, 1, 'Admin', 'Admin', 1, '2014-10-27 12:51:10'),
(49, 1, 'Admin', 'Admin', 1, '2014-10-27 12:51:59'),
(50, 1, 'Admin', 'Admin', 1, '2014-10-28 05:38:18'),
(51, 1, 'Admin', 'Admin', 1, '2014-10-28 08:41:17'),
(52, 1, 'Admin', 'Admin', 1, '2014-10-28 09:41:27'),
(53, 1, 'Admin', 'Admin', 1, '2014-10-28 11:10:44'),
(54, 1, 'Admin', 'Admin', 1, '2014-10-28 12:05:50'),
(55, 1, 'Admin', 'Admin', 1, '2014-10-28 12:06:48'),
(56, 1, 'Admin', 'Admin', 1, '2014-10-28 12:31:35'),
(57, 1, 'Admin', 'Admin', 1, '2014-10-29 06:34:14'),
(58, 1, 'Admin', 'Admin', 1, '2014-10-29 07:26:57'),
(59, 1, 'Admin', 'Admin', 1, '2014-10-30 05:47:34'),
(60, 1, 'Admin', 'Admin', 1, '2014-10-30 08:33:26'),
(61, 1, 'Admin', 'Admin', 1, '2014-10-30 08:37:38'),
(62, 1, 'Admin', 'Admin', 1, '2014-10-30 11:45:16'),
(63, 1, 'Admin', 'Admin', 1, '2014-10-30 12:27:43'),
(64, 1, 'Admin', 'Admin', 1, '2014-10-31 09:15:41'),
(65, 1, 'Admin', 'Admin', 1, '2014-10-31 11:18:07'),
(66, 1, 'Admin', 'Admin', 1, '2014-10-31 12:48:35'),
(67, 1, 'Admin', 'Admin', 1, '2014-10-31 13:48:42'),
(68, 1, 'Admin', 'Admin', 1, '2014-11-03 06:06:15'),
(69, 1, 'Admin', 'Admin', 1, '2014-11-03 10:48:48'),
(70, 1, 'Admin', 'Admin', 1, '2014-11-03 11:24:41'),
(71, 1, 'Admin', 'Admin', 1, '2014-11-03 12:33:10'),
(72, 1, 'Admin', 'Admin', 1, '2014-11-03 13:08:51'),
(73, 1, 'Admin', 'Admin', 1, '2014-11-03 15:03:31'),
(74, 1, 'Admin', 'Admin', 1, '2014-11-06 10:46:09'),
(75, 1, 'Admin', 'Admin', 1, '2014-11-06 11:51:19'),
(76, 1, 'Admin', 'Admin', 1, '2014-11-11 05:44:42'),
(77, 1, 'Admin', 'Admin', 1, '2014-11-11 08:33:33'),
(78, 1, 'Admin', 'Admin', 1, '2014-11-11 11:42:22'),
(79, 1, 'Admin', 'Admin', 1, '2014-11-11 12:54:53'),
(80, 1, 'Admin', 'Admin', 1, '2014-11-11 12:57:38'),
(81, 1, 'Admin', 'Admin', 1, '2014-11-11 13:16:20'),
(82, 1, 'Admin', 'Admin', 1, '2014-11-11 13:17:29'),
(83, 1, 'Admin', 'Admin', 1, '2014-11-11 13:20:44'),
(84, 1, 'Admin', 'Admin', 1, '2014-11-12 06:58:08'),
(85, 1, 'Admin', 'Admin', 1, '2014-11-12 12:56:47'),
(86, 1, 'Admin', 'Admin', 1, '2014-11-13 08:37:01'),
(87, 1, 'Admin', 'Admin', 1, '2014-11-13 09:21:30'),
(88, 1, 'Admin', 'Admin', 1, '2014-11-13 11:30:52'),
(89, 1, 'Admin', 'Admin', 1, '2014-11-14 11:39:16'),
(90, 1, 'Admin', 'Admin', 1, '2014-11-14 12:31:15'),
(91, 1, 'Admin', 'Admin', 1, '2014-11-17 06:17:23'),
(92, 1, 'Admin', 'Admin', 1, '2014-11-17 08:35:07'),
(93, 1, 'Admin', 'Admin', 1, '2014-11-17 08:35:19'),
(94, 1, 'Admin', 'Admin', 1, '2014-11-17 08:35:34'),
(95, 1, 'Admin', 'Admin', 1, '2014-11-17 09:23:29'),
(96, 1, 'Admin', 'Admin', 1, '2014-11-17 11:55:09'),
(97, 1, 'Admin', 'Admin', 1, '2014-11-17 13:45:14'),
(98, 1, 'Admin', 'Admin', 1, '2014-11-18 06:02:43'),
(99, 1, 'Admin', 'Admin', 1, '2014-11-18 09:26:34'),
(100, 1, 'Admin', 'Admin', 1, '2014-11-18 12:04:54'),
(101, 1, 'Admin', 'Admin', 1, '2014-11-18 12:17:21'),
(102, 1, 'Admin', 'Admin', 1, '2014-11-18 12:17:56'),
(103, 1, 'Admin', 'Admin', 1, '2014-11-18 13:08:20'),
(104, 1, 'Admin', 'Admin', 1, '2014-11-18 13:18:57'),
(105, 1, 'Admin', 'Admin', 1, '2014-11-18 13:21:52'),
(106, 1, 'Admin', 'Admin', 1, '2014-11-18 13:24:56'),
(107, 1, 'Admin', 'Admin', 1, '2014-11-19 05:46:20'),
(108, 1, 'Admin', 'Admin', 1, '2014-11-19 07:57:58'),
(109, 1, 'Admin', 'Admin', 1, '2014-11-19 10:36:21'),
(110, 1, 'Admin', 'Admin', 1, '2014-11-19 10:38:48'),
(111, 1, 'Admin', 'Admin', 1, '2014-11-19 12:11:09'),
(112, 1, 'Admin', 'Admin', 1, '2014-11-20 09:54:03'),
(113, 1, 'Admin', 'Admin', 1, '2014-11-20 10:10:01'),
(114, 1, 'Admin', 'Admin', 1, '2014-11-20 10:13:04'),
(115, 2, 'John', 'ESS', 1, '2014-11-20 10:20:49'),
(116, 1, 'Admin', 'Admin', 1, '2014-11-20 10:21:19'),
(117, 2, 'John', 'ESS', 1, '2014-11-20 10:22:55'),
(118, 1, 'Admin', 'Admin', 1, '2014-11-20 10:23:09'),
(119, 1, 'Admin', 'Admin', 1, '2014-11-20 10:27:54'),
(120, 2, 'John', 'ESS', 1, '2014-11-20 10:29:04'),
(121, 1, 'Admin', 'Admin', 1, '2014-11-20 10:29:24'),
(122, 2, 'John', 'ESS', 1, '2014-11-20 10:31:00'),
(123, 1, 'Admin', 'Admin', 1, '2014-11-20 10:42:34'),
(124, 1, 'Admin', 'Admin', 1, '2014-11-20 12:09:04'),
(125, 1, 'Admin', 'Admin', 1, '2014-11-20 14:28:35'),
(126, 1, 'Admin', 'Admin', 1, '2014-11-20 15:17:08'),
(127, 1, 'Admin', 'Admin', 1, '2014-11-21 13:17:39'),
(128, 1, 'Admin', 'Admin', 1, '2014-11-22 07:41:25'),
(129, 1, 'Admin', 'Admin', 1, '2014-11-24 12:35:38'),
(130, 1, 'Admin', 'Admin', 1, '2014-11-26 06:56:29'),
(131, 1, 'Admin', 'Admin', 1, '2014-11-26 11:32:32'),
(132, 1, 'Admin', 'Admin', 1, '2014-11-27 09:55:03'),
(133, 1, 'Admin', 'Admin', 1, '2014-11-28 07:27:47'),
(134, 1, 'Admin', 'Admin', 1, '2014-11-28 07:39:42'),
(135, 1, 'Admin', 'Admin', 1, '2014-12-02 08:07:56'),
(136, 1, 'Admin', 'Admin', 1, '2014-12-03 06:14:07'),
(137, 1, 'Admin', 'Admin', 1, '2014-12-03 07:31:44'),
(138, 1, 'Admin', 'Admin', 1, '2014-12-03 08:31:13'),
(139, 1, 'Admin', 'Admin', 1, '2014-12-03 08:44:42'),
(140, 1, 'Admin', 'Admin', 1, '2014-12-03 11:08:26'),
(141, 1, 'Admin', 'Admin', 1, '2014-12-03 13:37:06'),
(142, 1, 'Admin', 'Admin', 1, '2014-12-04 06:03:42'),
(143, 1, 'Admin', 'Admin', 1, '2014-12-04 07:27:59'),
(144, 1, 'Admin', 'Admin', 1, '2014-12-04 08:45:30'),
(145, 1, 'Admin', 'Admin', 1, '2014-12-04 11:03:40'),
(146, 1, 'Admin', 'Admin', 1, '2014-12-04 12:40:40'),
(147, 1, 'Admin', 'Admin', 1, '2014-12-04 13:07:19'),
(148, 1, 'Admin', 'Admin', 1, '2014-12-05 06:39:52'),
(149, 1, 'Admin', 'Admin', 1, '2014-12-05 06:40:07'),
(150, 1, 'Admin', 'Admin', 1, '2014-12-05 08:24:51'),
(151, 1, 'Admin', 'Admin', 1, '2014-12-05 11:48:46'),
(152, 1, 'Admin', 'Admin', 1, '2014-12-05 13:07:31'),
(153, 1, 'Admin', 'Admin', 1, '2014-12-08 06:19:48'),
(154, 2, 'John', 'ESS', 1, '2014-12-08 06:21:26'),
(155, 1, 'Admin', 'Admin', 1, '2014-12-08 07:08:56'),
(156, 1, 'Admin', 'Admin', 1, '2014-12-08 07:09:54'),
(157, 1, 'Admin', 'Admin', 1, '2014-12-08 07:13:00'),
(158, 1, 'Admin', 'Admin', 1, '2014-12-08 07:19:32'),
(159, 1, 'Admin', 'Admin', 1, '2014-12-08 07:20:39'),
(160, 1, 'Admin', 'Admin', 1, '2014-12-08 07:22:56'),
(161, 1, 'Admin', 'Admin', 1, '2014-12-08 07:23:31'),
(162, 1, 'Admin', 'Admin', 1, '2014-12-08 07:31:51'),
(163, 1, 'Admin', 'Admin', 1, '2014-12-08 07:32:43'),
(164, 1, 'Admin', 'Admin', 1, '2014-12-08 07:34:13'),
(165, 1, 'Admin', 'Admin', 1, '2014-12-08 07:34:53'),
(166, 1, 'Admin', 'Admin', 1, '2014-12-08 07:37:51'),
(167, 1, 'Admin', 'Admin', 1, '2014-12-08 07:39:25'),
(168, 1, 'Admin', 'Admin', 1, '2014-12-08 09:13:42'),
(169, 1, 'Admin', 'Admin', 1, '2014-12-08 09:44:10'),
(170, 1, 'Admin', 'Admin', 1, '2014-12-08 09:51:02'),
(171, 1, 'Admin', 'Admin', 1, '2014-12-08 10:30:36'),
(172, 1, 'Admin', 'Admin', 1, '2014-12-09 05:23:05'),
(173, 1, 'Admin', 'Admin', 1, '2014-12-10 05:59:14'),
(174, 1, 'Admin', 'Admin', 1, '2014-12-10 10:31:18'),
(175, 1, 'Admin', 'Admin', 1, '2014-12-11 06:19:07'),
(176, 1, 'Admin', 'Admin', 1, '2014-12-11 07:56:40'),
(177, 1, 'Admin', 'Admin', 1, '2014-12-11 08:36:04'),
(178, 1, 'Admin', 'Admin', 1, '2014-12-11 11:17:02'),
(179, 1, 'Admin', 'Admin', 1, '2014-12-11 12:13:51'),
(180, 1, 'Admin', 'Admin', 1, '2014-12-12 06:07:49'),
(181, 1, 'Admin', 'Admin', 1, '2014-12-12 08:51:08'),
(182, 1, 'Admin', 'Admin', 1, '2014-12-12 11:14:09'),
(183, 1, 'Admin', 'Admin', 1, '2014-12-13 06:40:22'),
(184, 1, 'Admin', 'Admin', 1, '2014-12-15 09:45:15'),
(185, 1, 'Admin', 'Admin', 1, '2014-12-16 07:24:07'),
(186, 1, 'Admin', 'Admin', 1, '2014-12-16 08:57:06'),
(187, 1, 'Admin', 'Admin', 1, '2014-12-16 10:46:19'),
(188, 1, 'Admin', 'Admin', 1, '2014-12-16 11:53:55'),
(189, 1, 'Admin', 'Admin', 1, '2014-12-17 06:22:27'),
(190, 1, 'Admin', 'Admin', 1, '2014-12-17 11:06:41'),
(191, 1, 'Admin', 'Admin', 1, '2014-12-18 10:53:23'),
(192, 1, 'Admin', 'Admin', 1, '2014-12-19 06:29:27'),
(193, 1, 'Admin', 'Admin', 1, '2014-12-19 15:42:51'),
(194, 1, 'Admin', 'Admin', 1, '2014-12-24 07:07:10'),
(195, 1, 'Admin', 'Admin', 1, '2014-12-24 07:20:23'),
(196, 1, 'Admin', 'Admin', 1, '2014-12-24 07:21:55'),
(197, 1, 'Admin', 'Admin', 1, '2014-12-24 07:53:34'),
(198, 1, 'Admin', 'Admin', 1, '2014-12-24 09:25:27'),
(199, 1, 'Admin', 'Admin', 1, '2014-12-24 13:07:21'),
(200, 1, 'Admin', 'Admin', 1, '2014-12-25 00:20:56'),
(201, 1, 'Admin', 'Admin', 1, '2015-01-01 07:23:24'),
(202, 1, 'Admin', 'Admin', 1, '2015-01-01 08:08:47'),
(203, 1, 'Admin', 'Admin', 1, '2015-01-05 08:51:03'),
(204, 1, 'Admin', 'Admin', 1, '2015-01-05 22:57:27'),
(205, 1, 'Admin', 'Admin', 1, '2015-01-06 00:02:02'),
(206, 1, 'Admin', 'Admin', 1, '2015-01-06 07:28:26'),
(207, 1, 'Admin', 'Admin', 1, '2015-01-06 08:10:40'),
(208, 1, 'Admin', 'Admin', 1, '2015-01-06 09:30:04'),
(209, 1, 'Admin', 'Admin', 1, '2015-01-06 10:38:12'),
(210, 1, 'Admin', 'Admin', 1, '2015-01-06 11:56:08'),
(211, 1, 'Admin', 'Admin', 1, '2015-01-07 10:40:50'),
(212, 1, 'Admin', 'Admin', 1, '2015-01-07 13:11:12'),
(213, 1, 'Admin', 'Admin', 1, '2015-01-08 06:05:05'),
(214, 1, 'Admin', 'Admin', 1, '2015-01-08 12:09:28'),
(215, 1, 'Admin', 'Admin', 1, '2015-01-09 10:56:20'),
(216, 1, 'Admin', 'Admin', 1, '2015-01-09 10:59:38'),
(217, 1, 'Admin', 'Admin', 1, '2015-01-09 12:28:42'),
(218, 1, 'Admin', 'Admin', 1, '2015-01-13 06:12:49'),
(219, 1, 'Admin', 'Admin', 1, '2015-01-13 07:55:12'),
(220, 1, 'Admin', 'Admin', 1, '2015-01-13 11:01:44'),
(221, 1, 'Admin', 'Admin', 1, '2015-01-13 12:11:19'),
(222, 1, 'Admin', 'Admin', 1, '2015-01-14 12:41:41'),
(223, 1, 'Admin', 'Admin', 1, '2015-01-14 13:39:24'),
(224, 1, 'Admin', 'Admin', 1, '2015-01-15 11:23:02'),
(225, 1, 'Admin', 'Admin', 1, '2015-01-17 07:31:50'),
(226, 1, 'Admin', 'Admin', 1, '2015-01-17 08:00:55'),
(227, 1, 'Admin', 'Admin', 1, '2015-01-19 06:03:09'),
(228, 1, 'Admin', 'Admin', 1, '2015-01-19 07:57:41'),
(229, 1, 'Admin', 'Admin', 1, '2015-01-19 10:34:48'),
(230, 1, 'Admin', 'Admin', 1, '2015-01-19 11:07:20'),
(231, 1, 'Admin', 'Admin', 1, '2015-01-21 04:28:49'),
(232, 2, 'John', 'ESS', 1, '2015-01-21 04:30:18'),
(233, 1, 'Admin', 'Admin', 1, '2015-01-21 05:32:45'),
(234, 1, 'Admin', 'Admin', 1, '2015-01-21 06:48:04'),
(235, 1, 'Admin', 'Admin', 1, '2015-01-21 07:12:02'),
(236, 1, 'Admin', 'Admin', 1, '2015-01-21 07:29:58'),
(237, 1, 'Admin', 'Admin', 1, '2015-01-21 07:30:34'),
(238, 1, 'Admin', 'Admin', 1, '2015-01-21 10:46:44'),
(239, 1, 'Admin', 'Admin', 1, '2015-01-21 11:48:07'),
(240, 1, 'Admin', 'Admin', 1, '2015-01-21 12:58:10'),
(241, 1, 'Admin', 'Admin', 1, '2015-01-21 13:01:52'),
(242, 1, 'Admin', 'Admin', 1, '2015-01-21 13:10:46'),
(243, 1, 'Admin', 'Admin', 1, '2015-01-21 13:12:37'),
(244, 1, 'Admin', 'Admin', 1, '2015-01-22 05:55:54'),
(245, 1, 'Admin', 'Admin', 1, '2015-01-22 07:08:15'),
(246, 1, 'Admin', 'Admin', 1, '2015-01-22 10:25:49'),
(247, 1, 'Admin', 'Admin', 1, '2015-01-22 12:43:55'),
(248, 1, 'Admin', 'Admin', 1, '2015-01-23 06:02:23'),
(249, 1, 'Admin', 'Admin', 1, '2015-01-23 07:25:46'),
(250, 1, 'Admin', 'Admin', 1, '2015-01-23 08:18:21'),
(251, 1, 'Admin', 'Admin', 1, '2015-01-23 10:30:41'),
(252, 1, 'Admin', 'Admin', 1, '2015-01-23 23:05:35'),
(253, 1, 'Admin', 'Admin', 1, '2015-02-24 00:53:59'),
(254, 1, 'Admin', 'Admin', 1, '2015-01-24 04:14:47'),
(255, 1, 'Admin', 'Admin', 1, '2015-03-24 04:18:34'),
(256, 1, 'Admin', 'Admin', 1, '2015-02-24 05:06:05'),
(257, 1, 'Admin', 'Admin', 1, '2015-02-24 05:56:57'),
(258, 1, 'Admin', 'Admin', 1, '2015-02-24 06:05:25'),
(259, 1, 'Admin', 'Admin', 1, '2015-01-24 06:46:16'),
(260, 1, 'Admin', 'Admin', 1, '2015-02-24 06:59:26'),
(261, 1, 'Admin', 'Admin', 1, '2015-01-26 05:48:58'),
(262, 1, 'Admin', 'Admin', 1, '2015-01-26 07:51:01'),
(263, 1, 'Admin', 'Admin', 1, '2015-01-26 10:42:46'),
(264, 1, 'Admin', 'Admin', 1, '2015-01-26 11:34:17'),
(265, 1, 'Admin', 'Admin', 1, '2015-01-26 12:43:08'),
(266, 1, 'Admin', 'Admin', 1, '2015-03-26 12:57:32'),
(267, 1, 'Admin', 'Admin', 1, '2015-01-26 13:12:07'),
(268, 1, 'Admin', 'Admin', 1, '2015-01-26 13:15:45'),
(269, 1, 'Admin', 'Admin', 1, '2015-01-26 13:28:32'),
(270, 1, 'Admin', 'Admin', 1, '2015-01-26 13:29:02'),
(271, 1, 'Admin', 'Admin', 1, '2015-02-02 06:21:08'),
(272, 1, 'Admin', 'Admin', 1, '2015-02-02 08:00:32'),
(273, 1, 'Admin', 'Admin', 1, '2015-02-02 11:29:19'),
(274, 1, 'Admin', 'Admin', 1, '2015-02-03 06:15:23'),
(275, 1, 'Admin', 'Admin', 1, '2015-02-04 06:29:56'),
(276, 1, 'Admin', 'Admin', 1, '2015-02-05 07:44:16'),
(277, 1, 'Admin', 'Admin', 1, '2015-02-05 08:35:16'),
(278, 1, 'Admin', 'Admin', 1, '2015-02-05 08:57:31'),
(279, 1, 'Admin', 'Admin', 1, '2015-01-28 09:02:24'),
(280, 1, 'Admin', 'Admin', 1, '2015-02-06 08:54:40'),
(281, 1, 'Admin', 'Admin', 1, '2015-02-06 08:57:42'),
(282, 1, 'Admin', 'Admin', 1, '2015-03-06 09:02:48'),
(283, 1, 'Admin', 'Admin', 1, '2015-02-06 10:24:39'),
(284, 1, 'Admin', 'Admin', 1, '2015-02-06 13:21:42'),
(285, 2, 'John', 'ESS', 1, '2015-02-06 13:33:04'),
(286, 1, 'Admin', 'Admin', 1, '2015-02-06 13:35:08'),
(287, 2, 'John', 'ESS', 1, '2015-02-06 13:45:52'),
(288, 1, 'Admin', 'Admin', 1, '2015-02-09 07:09:06'),
(289, 1, 'Admin', 'Admin', 1, '2015-02-09 10:30:12'),
(290, 1, 'Admin', 'Admin', 1, '2015-02-09 15:05:55'),
(291, 1, 'Admin', 'Admin', 1, '2015-02-09 12:46:47'),
(292, 1, 'Admin', 'Admin', 1, '2015-02-10 12:25:01'),
(293, 2, 'John', 'ESS', 1, '2015-02-10 12:35:27'),
(294, 1, 'Admin', 'Admin', 1, '2015-02-10 12:38:15'),
(295, 2, 'John', 'ESS', 1, '2015-02-10 12:42:59'),
(296, 1, 'Admin', 'Admin', 1, '2015-02-10 12:48:49'),
(297, 1, 'Admin', 'Admin', 1, '2015-02-10 13:28:29'),
(298, 2, 'John', 'ESS', 1, '2015-02-10 13:28:40'),
(299, 2, 'John', 'ESS', 1, '2015-02-10 13:31:01'),
(300, 2, 'John', 'ESS', 1, '2015-02-10 13:33:43'),
(301, 2, 'John', 'ESS', 1, '2015-02-10 13:35:35'),
(302, 2, 'John', 'ESS', 1, '2015-02-10 13:36:34'),
(303, 2, 'John', 'ESS', 1, '2015-02-10 13:38:18'),
(304, 1, 'Admin', 'Admin', 1, '2015-02-11 09:23:41'),
(305, 2, 'John', 'ESS', 1, '2015-02-11 09:25:35'),
(306, 1, 'Admin', 'Admin', 1, '2015-02-11 12:59:38'),
(307, 2, 'John', 'ESS', 1, '2015-02-11 12:59:53'),
(308, 1, 'Admin', 'Admin', 1, '2015-02-11 13:06:49'),
(309, 2, 'John', 'ESS', 1, '2015-02-11 13:09:09'),
(310, 1, 'Admin', 'Admin', 1, '2015-02-11 13:21:08'),
(311, 2, 'John', 'ESS', 1, '2015-02-11 13:21:25'),
(312, 2, 'John', 'ESS', 1, '2015-02-11 13:33:44'),
(313, 2, 'John', 'ESS', 1, '2015-02-11 13:42:23'),
(314, 2, 'John', 'ESS', 1, '2015-02-11 13:43:00'),
(315, 2, 'John', 'ESS', 1, '2015-02-16 08:53:15'),
(316, 1, 'Admin', 'Admin', 1, '2015-02-16 08:59:33'),
(317, 1, 'Admin', 'Admin', 1, '2015-02-16 09:09:00'),
(318, 1, 'Admin', 'Admin', 1, '2015-02-19 13:34:36'),
(319, 1, 'Admin', 'Admin', 1, '2015-03-05 13:07:28'),
(320, 1, 'Admin', 'Admin', 1, '2015-03-07 06:16:11'),
(321, 1, 'Admin', 'Admin', 1, '2015-03-09 06:23:20'),
(322, 1, 'Admin', 'Admin', 1, '2015-03-09 06:24:19'),
(323, 2, 'John', 'ESS', 1, '2015-03-09 06:26:42'),
(324, 2, 'John', 'ESS', 1, '2015-03-09 06:30:09'),
(325, 1, 'Admin', 'Admin', 1, '2015-03-09 06:30:52'),
(326, 2, 'John', 'ESS', 1, '2015-03-09 06:47:52'),
(327, 2, 'John', 'ESS', 1, '2015-03-09 06:48:20'),
(328, 1, 'Admin', 'Admin', 1, '2015-03-09 12:47:48'),
(329, 1, 'Admin', 'Admin', 1, '2015-03-10 09:25:18'),
(330, 1, 'Admin', 'Admin', 1, '2015-03-10 11:06:04'),
(331, 1, 'Admin', 'Admin', 1, '2015-03-10 23:23:57'),
(332, 1, 'Admin', 'Admin', 1, '2015-03-10 23:38:01'),
(333, 1, 'Admin', 'Admin', 1, '2015-03-10 23:40:31'),
(334, 1, 'Admin', 'Admin', 1, '2015-03-10 23:41:51'),
(335, 1, 'Admin', 'Admin', 1, '2015-03-11 00:43:32'),
(336, 1, 'Admin', 'Admin', 1, '2015-03-11 00:48:50'),
(337, 1, 'Admin', 'Admin', 1, '2015-03-12 11:11:36'),
(338, 1, 'Admin', 'Admin', 1, '2015-03-12 11:32:30'),
(339, 1, 'Admin', 'Admin', 1, '2015-04-01 09:50:33'),
(340, 1, 'Admin', 'Admin', 1, '2015-04-10 15:50:45'),
(341, 1, 'Admin', 'Admin', 1, '2015-04-17 22:54:09'),
(342, 1, 'Admin', 'Admin', 1, '2015-04-17 23:29:26'),
(343, 1, 'Admin', 'Admin', 1, '2015-04-28 16:09:23'),
(344, 1, 'Admin', 'Admin', 1, '2015-05-26 05:37:45'),
(345, 1, 'Admin', 'Admin', 1, '2015-06-02 23:50:25'),
(346, 1, 'Admin', 'Admin', 1, '2015-06-15 11:57:12'),
(347, 1, 'Admin', 'Admin', 1, '2015-07-01 13:57:54'),
(348, 1, 'Admin', 'Admin', 1, '2015-08-27 19:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_membership`
--

CREATE TABLE IF NOT EXISTS `ohrm_membership` (
`id` int(6) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_menu_item`
--

CREATE TABLE IF NOT EXISTS `ohrm_menu_item` (
`id` int(11) NOT NULL,
  `menu_title` varchar(255) NOT NULL,
  `screen_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `level` tinyint(4) NOT NULL,
  `order_hint` int(11) NOT NULL,
  `url_extras` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_menu_item`
--

INSERT INTO `ohrm_menu_item` (`id`, `menu_title`, `screen_id`, `parent_id`, `level`, `order_hint`, `url_extras`, `status`) VALUES
(1, 'Admin', 74, NULL, 1, 100, NULL, 1),
(2, 'User Management', NULL, 1, 2, 100, NULL, 1),
(3, 'Project Info', NULL, 52, 2, 400, NULL, 0),
(4, 'Customers', 36, 3, 3, 100, NULL, 0),
(5, 'Projects', 37, 3, 3, 200, NULL, 0),
(6, 'Job', NULL, 1, 2, 300, NULL, 1),
(7, 'Job Titles', 23, 6, 3, 100, NULL, 1),
(8, 'Pay Grades', 24, 6, 3, 200, NULL, 1),
(9, 'Employment Status', 25, 6, 3, 300, NULL, 1),
(10, 'Job Categories', 26, 6, 3, 400, NULL, 1),
(11, 'Work Shifts', 27, 6, 3, 500, NULL, 1),
(12, 'Organization', NULL, 1, 2, 400, NULL, 1),
(13, 'General Information', 20, 12, 3, 100, NULL, 1),
(14, 'Locations', 21, 12, 3, 200, NULL, 1),
(15, 'Structure', 22, 12, 3, 300, NULL, 1),
(16, 'Qualifications', NULL, 1, 2, 500, NULL, 1),
(17, 'Skills', 28, 16, 3, 100, NULL, 1),
(18, 'Education', 29, 16, 3, 200, NULL, 1),
(19, 'Licenses', 30, 16, 3, 300, NULL, 1),
(20, 'Languages', 31, 16, 3, 400, NULL, 1),
(21, 'Memberships', 32, 16, 3, 500, NULL, 1),
(22, 'Nationalities', 33, 1, 2, 700, NULL, 1),
(23, 'Configuration', NULL, 1, 2, 900, NULL, 1),
(24, 'Email Configuration', 34, 23, 3, 100, NULL, 1),
(25, 'Email Subscriptions', 35, 23, 3, 200, NULL, 1),
(27, 'Localization', 38, 23, 3, 300, NULL, 1),
(28, 'Modules', 39, 23, 3, 400, NULL, 1),
(30, 'PIM', 75, NULL, 1, 200, NULL, 1),
(31, 'Configuration', NULL, 30, 2, 100, NULL, 1),
(32, 'Optional Fields', 40, 31, 3, 100, NULL, 1),
(33, 'Custom Fields', 41, 31, 3, 200, NULL, 1),
(34, 'Data Import', 42, 31, 3, 300, NULL, 1),
(35, 'Reporting Methods', 43, 31, 3, 400, NULL, 1),
(36, 'Termination Reasons', 44, 31, 3, 500, NULL, 1),
(37, 'Employee List', 5, 30, 2, 200, '/reset/1', 1),
(38, 'Add Employee', 4, 30, 2, 300, NULL, 1),
(39, 'Reports', 45, 30, 2, 400, '/reportGroup/3/reportType/PIM_DEFINED', 1),
(40, 'My Info', 46, NULL, 1, 700, NULL, 1),
(41, 'Leave', 68, NULL, 1, 300, NULL, 1),
(42, 'Configure', NULL, 41, 2, 500, NULL, 0),
(43, 'Leave Period', 47, 42, 3, 100, NULL, 1),
(44, 'Leave Types', 7, 42, 3, 200, NULL, 1),
(45, 'Work Week', 14, 42, 3, 300, NULL, 1),
(46, 'Holidays', 11, 42, 3, 400, NULL, 1),
(48, 'Leave List', 16, 41, 2, 600, '/reset/1', 1),
(49, 'Assign Leave', 17, 41, 2, 700, NULL, 1),
(50, 'My Leave', 48, 41, 2, 200, '/reset/1', 1),
(51, 'Apply', 49, 41, 2, 100, NULL, 1),
(52, 'Time', 67, NULL, 1, 400, NULL, 1),
(53, 'Timesheets', NULL, 52, 2, 100, NULL, 1),
(54, 'My Timesheets', 51, 53, 3, 100, NULL, 1),
(55, 'Employee Timesheets', 52, 53, 3, 200, NULL, 1),
(56, 'Attendance', NULL, 52, 2, 200, NULL, 1),
(57, 'My Records', 53, 56, 3, 100, NULL, 1),
(58, 'Punch In/Out', 54, 56, 3, 200, NULL, 1),
(59, 'Employee Records', 55, 56, 3, 300, NULL, 1),
(60, 'Configuration', 56, 56, 3, 400, NULL, 1),
(61, 'Reports', NULL, 52, 2, 300, NULL, 1),
(62, 'Project Reports', 57, 61, 3, 100, '?reportId=1', 1),
(63, 'Employee Reports', 58, 61, 3, 200, '?reportId=2', 1),
(64, 'Attendance Summary', 59, 61, 3, 300, '?reportId=4', 1),
(65, 'Recruitment', 76, NULL, 1, 500, NULL, 1),
(66, 'Candidates', 60, 65, 2, 100, NULL, 1),
(67, 'Vacancies', 61, 65, 2, 200, NULL, 1),
(74, 'Entitlements', NULL, 41, 2, 300, NULL, 0),
(75, 'Add Entitlements', 72, 74, 3, 100, NULL, 1),
(76, 'My Entitlements', 70, 74, 3, 300, '/reset/1', 1),
(77, 'Employee Entitlements', 69, 74, 3, 200, '/reset/1', 1),
(78, 'Reports', NULL, 41, 2, 400, NULL, 0),
(79, 'Leave Entitlements and Usage Report', 78, 78, 3, 100, NULL, 1),
(80, 'My Leave Entitlements and Usage Report', 79, 78, 3, 200, NULL, 1),
(81, 'Users', 1, 2, 3, 100, NULL, 1),
(82, 'Dashboard', 103, NULL, 1, 800, NULL, 1),
(83, 'Performance', NULL, NULL, 1, 700, '', 0),
(84, 'Configure', NULL, 83, 2, 100, '', 1),
(85, 'Manage Reviews', NULL, 83, 2, 200, '', 1),
(86, 'KPIs', 105, 84, 3, 100, '', 1),
(87, 'Manage Reviews', 111, 85, 3, 100, '', 1),
(88, 'My Reviews', 106, 85, 3, 200, '', 1),
(89, 'Review List', 110, 85, 3, 300, '', 1),
(90, 'Trackers', 112, 84, 3, 200, NULL, 0),
(91, 'Employee Trackers', 113, 83, 2, 800, NULL, 0),
(92, 'My Trackers', 114, 83, 2, 700, NULL, 1),
(93, 'Payroll', 116, NULL, 1, 900, NULL, 1),
(95, 'PaySlips', 118, 93, 2, 200, NULL, 1),
(99, 'Tax Rates', 120, 93, 2, 400, NULL, 1),
(101, 'Assign Pension', 122, 103, 3, 200, NULL, 1),
(102, 'Pensions List', 123, 103, 3, 100, NULL, 1),
(103, 'Pensions', NULL, 93, 2, 300, NULL, 1),
(104, 'Employees Pensions', 124, 103, 3, 400, NULL, 1),
(105, 'Run Payroll', 125, 95, 3, 100, NULL, 1),
(106, 'View Payslips', 126, 95, 3, 300, NULL, 1),
(109, 'Loans', NULL, 93, 2, 400, NULL, 1),
(110, 'Financial Organizations', 130, 109, 3, 100, NULL, 1),
(111, 'Loan List', 131, 109, 3, 200, NULL, 1),
(113, 'Employees Loans', 132, 109, 3, 300, NULL, 1),
(115, 'Deductions', NULL, 93, 2, 500, NULL, 1),
(116, 'Deductions', 134, 115, 3, 100, NULL, 1),
(118, 'Add Deduction', 136, 115, 3, 300, NULL, 1),
(119, 'Reports', NULL, 93, 2, 600, NULL, 1),
(120, 'Payroll', 137, 119, 3, 100, NULL, 1),
(121, 'Paye', 138, 119, 3, 200, NULL, 1),
(122, 'Pensions', 139, 119, 3, 300, NULL, 1),
(123, 'Loans', 140, 119, 3, 400, NULL, 1),
(124, 'Deductions', 141, 119, 3, 500, NULL, 1),
(125, 'Opras', 142, 83, 2, 300, NULL, 1),
(126, 'Forms', 143, 125, 3, 100, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_module`
--

CREATE TABLE IF NOT EXISTS `ohrm_module` (
`id` int(11) NOT NULL,
  `name` varchar(120) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_module`
--

INSERT INTO `ohrm_module` (`id`, `name`, `status`) VALUES
(1, 'core', 1),
(2, 'admin', 1),
(3, 'pim', 1),
(4, 'leave', 1),
(5, 'time', 1),
(6, 'attendance', 1),
(7, 'recruitment', 1),
(8, 'recruitmentApply', 1),
(9, 'communication', 1),
(10, 'dashboard', 1),
(11, 'performance', 1),
(12, 'performanceTracker', 1),
(13, 'payroll', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_module_default_page`
--

CREATE TABLE IF NOT EXISTS `ohrm_module_default_page` (
`id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `enable_class` varchar(100) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0' COMMENT 'lowest priority 0'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_module_default_page`
--

INSERT INTO `ohrm_module_default_page` (`id`, `module_id`, `user_role_id`, `action`, `enable_class`, `priority`) VALUES
(1, 2, 1, 'admin/viewSystemUsers', NULL, 20),
(2, 3, 1, 'pim/viewEmployeeList', NULL, 20),
(3, 3, 3, 'pim/viewEmployeeList', NULL, 10),
(4, 3, 2, 'pim/viewMyDetails', NULL, 0),
(5, 4, 1, 'leave/viewLeaveList/reset/1', NULL, 20),
(6, 4, 3, 'leave/viewLeaveList/reset/1', NULL, 10),
(7, 4, 2, 'leave/viewMyLeaveList', NULL, 0),
(8, 4, 1, 'leave/defineLeavePeriod', 'LeavePeriodDefinedHomePageEnabler', 100),
(9, 4, 2, 'leave/showLeavePeriodNotDefinedWarning', 'LeavePeriodDefinedHomePageEnabler', 90),
(10, 5, 1, 'time/viewEmployeeTimesheet', NULL, 20),
(11, 5, 2, 'time/viewMyTimesheet', NULL, 0),
(12, 5, 1, 'time/defineTimesheetPeriod', 'TimesheetPeriodDefinedHomePageEnabler', 100),
(13, 5, 2, 'time/timesheetPeriodNotDefined', 'TimesheetPeriodDefinedHomePageEnabler', 100),
(14, 7, 1, 'recruitment/viewCandidates', NULL, 20),
(15, 7, 5, 'recruitment/viewCandidates', NULL, 10),
(16, 7, 6, 'recruitment/viewCandidates', NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_nationality`
--

CREATE TABLE IF NOT EXISTS `ohrm_nationality` (
`id` int(6) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_nationality`
--

INSERT INTO `ohrm_nationality` (`id`, `name`) VALUES
(1, 'Afghan'),
(2, 'Albanian'),
(3, 'Algerian'),
(4, 'American'),
(5, 'Andorran'),
(6, 'Angolan'),
(7, 'Antiguans'),
(8, 'Argentinean'),
(9, 'Armenian'),
(10, 'Australian'),
(11, 'Austrian'),
(12, 'Azerbaijani'),
(13, 'Bahamian'),
(14, 'Bahraini'),
(15, 'Bangladeshi'),
(16, 'Barbadian'),
(17, 'Barbudans'),
(18, 'Batswana'),
(19, 'Belarusian'),
(20, 'Belgian'),
(21, 'Belizean'),
(22, 'Beninese'),
(23, 'Bhutanese'),
(24, 'Bolivian'),
(25, 'Bosnian'),
(26, 'Brazilian'),
(27, 'British'),
(28, 'Bruneian'),
(29, 'Bulgarian'),
(30, 'Burkinabe'),
(31, 'Burmese'),
(32, 'Burundian'),
(33, 'Cambodian'),
(34, 'Cameroonian'),
(35, 'Canadian'),
(36, 'Cape Verdean'),
(37, 'Central African'),
(38, 'Chadian'),
(39, 'Chilean'),
(40, 'Chinese'),
(41, 'Colombian'),
(42, 'Comoran'),
(43, 'Congolese'),
(44, 'Costa Rican'),
(45, 'Croatian'),
(46, 'Cuban'),
(47, 'Cypriot'),
(48, 'Czech'),
(49, 'Danish'),
(50, 'Djibouti'),
(51, 'Dominican'),
(52, 'Dutch'),
(53, 'East Timorese'),
(54, 'Ecuadorean'),
(55, 'Egyptian'),
(56, 'Emirian'),
(57, 'Equatorial Guinean'),
(58, 'Eritrean'),
(59, 'Estonian'),
(60, 'Ethiopian'),
(61, 'Fijian'),
(62, 'Filipino'),
(63, 'Finnish'),
(64, 'French'),
(65, 'Gabonese'),
(66, 'Gambian'),
(67, 'Georgian'),
(68, 'German'),
(69, 'Ghanaian'),
(70, 'Greek'),
(71, 'Grenadian'),
(72, 'Guatemalan'),
(73, 'Guinea-Bissauan'),
(74, 'Guinean'),
(75, 'Guyanese'),
(76, 'Haitian'),
(77, 'Herzegovinian'),
(78, 'Honduran'),
(79, 'Hungarian'),
(80, 'I-Kiribati'),
(81, 'Icelander'),
(82, 'Indian'),
(83, 'Indonesian'),
(84, 'Iranian'),
(85, 'Iraqi'),
(86, 'Irish'),
(87, 'Israeli'),
(88, 'Italian'),
(89, 'Ivorian'),
(90, 'Jamaican'),
(91, 'Japanese'),
(92, 'Jordanian'),
(93, 'Kazakhstani'),
(94, 'Kenyan'),
(95, 'Kittian and Nevisian'),
(96, 'Kuwaiti'),
(97, 'Kyrgyz'),
(98, 'Laotian'),
(99, 'Latvian'),
(100, 'Lebanese'),
(101, 'Liberian'),
(102, 'Libyan'),
(103, 'Liechtensteiner'),
(104, 'Lithuanian'),
(105, 'Luxembourger'),
(106, 'Macedonian'),
(107, 'Malagasy'),
(108, 'Malawian'),
(109, 'Malaysian'),
(110, 'Maldivan'),
(111, 'Malian'),
(112, 'Maltese'),
(113, 'Marshallese'),
(114, 'Mauritanian'),
(115, 'Mauritian'),
(116, 'Mexican'),
(117, 'Micronesian'),
(118, 'Moldovan'),
(119, 'Monacan'),
(120, 'Mongolian'),
(121, 'Moroccan'),
(122, 'Mosotho'),
(123, 'Motswana'),
(124, 'Mozambican'),
(125, 'Namibian'),
(126, 'Nauruan'),
(127, 'Nepalese'),
(128, 'New Zealander'),
(129, 'Nicaraguan'),
(130, 'Nigerian'),
(131, 'Nigerien'),
(132, 'North Korean'),
(133, 'Northern Irish'),
(134, 'Norwegian'),
(135, 'Omani'),
(136, 'Pakistani'),
(137, 'Palauan'),
(138, 'Panamanian'),
(139, 'Papua New Guinean'),
(140, 'Paraguayan'),
(141, 'Peruvian'),
(142, 'Polish'),
(143, 'Portuguese'),
(144, 'Qatari'),
(145, 'Romanian'),
(146, 'Russian'),
(147, 'Rwandan'),
(148, 'Saint Lucian'),
(149, 'Salvadoran'),
(150, 'Samoan'),
(151, 'San Marinese'),
(152, 'Sao Tomean'),
(153, 'Saudi'),
(154, 'Scottish'),
(155, 'Senegalese'),
(156, 'Serbian'),
(157, 'Seychellois'),
(158, 'Sierra Leonean'),
(159, 'Singaporean'),
(160, 'Slovakian'),
(161, 'Slovenian'),
(162, 'Solomon Islander'),
(163, 'Somali'),
(164, 'South African'),
(165, 'South Korean'),
(166, 'Spanish'),
(167, 'Sri Lankan'),
(168, 'Sudanese'),
(169, 'Surinamer'),
(170, 'Swazi'),
(171, 'Swedish'),
(172, 'Swiss'),
(173, 'Syrian'),
(174, 'Taiwanese'),
(175, 'Tajik'),
(176, 'Tanzanian'),
(177, 'Thai'),
(178, 'Togolese'),
(179, 'Tongan'),
(180, 'Trinidadian or Tobagonian'),
(181, 'Tunisian'),
(182, 'Turkish'),
(183, 'Tuvaluan'),
(184, 'Ugandan'),
(185, 'Ukrainian'),
(186, 'Uruguayan'),
(187, 'Uzbekistani'),
(188, 'Venezuelan'),
(189, 'Vietnamese'),
(190, 'Welsh'),
(191, 'Yemenite'),
(192, 'Zambian'),
(193, 'Zimbabwean');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_operational_country`
--

CREATE TABLE IF NOT EXISTS `ohrm_operational_country` (
`id` int(10) unsigned NOT NULL,
  `country_code` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_organization_gen_info`
--

CREATE TABLE IF NOT EXISTS `ohrm_organization_gen_info` (
`id` int(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `tax_id` varchar(30) DEFAULT NULL,
  `registration_number` varchar(30) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `province` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `zip_code` varchar(30) DEFAULT NULL,
  `street1` varchar(100) DEFAULT NULL,
  `street2` varchar(100) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_organization_gen_info`
--

INSERT INTO `ohrm_organization_gen_info` (`id`, `name`, `tax_id`, `registration_number`, `phone`, `fax`, `email`, `country`, `province`, `city`, `zip_code`, `street1`, `street2`, `note`) VALUES
(1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_pay_grade`
--

CREATE TABLE IF NOT EXISTS `ohrm_pay_grade` (
`id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_pay_grade`
--

INSERT INTO `ohrm_pay_grade` (`id`, `name`) VALUES
(1, 'Grade A'),
(2, 'Grade B');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_pay_grade_currency`
--

CREATE TABLE IF NOT EXISTS `ohrm_pay_grade_currency` (
  `pay_grade_id` int(11) NOT NULL,
  `currency_id` varchar(6) NOT NULL DEFAULT '',
  `min_salary` double DEFAULT NULL,
  `max_salary` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_pay_grade_currency`
--

INSERT INTO `ohrm_pay_grade_currency` (`pay_grade_id`, `currency_id`, `min_salary`, `max_salary`) VALUES
(1, 'TZS', 1, 2000000),
(2, 'TZS', 1, 800000);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_performance_review`
--

CREATE TABLE IF NOT EXISTS `ohrm_performance_review` (
`id` int(7) NOT NULL,
  `status_id` int(7) DEFAULT NULL,
  `employee_number` int(7) DEFAULT NULL,
  `work_period_start` date DEFAULT NULL,
  `work_period_end` date DEFAULT NULL,
  `job_title_code` int(7) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `completed_date` date DEFAULT NULL,
  `activated_date` datetime DEFAULT NULL,
  `final_comment` text CHARACTER SET utf8 COLLATE utf8_bin,
  `final_rate` decimal(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_performance_track`
--

CREATE TABLE IF NOT EXISTS `ohrm_performance_track` (
`id` int(11) NOT NULL,
  `emp_number` int(7) NOT NULL,
  `tracker_name` varchar(200) NOT NULL,
  `added_date` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_performance_track`
--

INSERT INTO `ohrm_performance_track` (`id`, `emp_number`, `tracker_name`, `added_date`, `added_by`, `status`, `modified_date`) VALUES
(1, 2, 'Jany', '2014-12-08 06:26:19', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_performance_tracker_log`
--

CREATE TABLE IF NOT EXISTS `ohrm_performance_tracker_log` (
`id` int(11) NOT NULL,
  `performance_track_id` int(11) DEFAULT NULL,
  `log` varchar(150) DEFAULT NULL,
  `comment` varchar(3000) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `added_date` timestamp NULL DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `reviewer_id` int(7) DEFAULT NULL,
  `achievement` varchar(45) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_performance_tracker_reviewer`
--

CREATE TABLE IF NOT EXISTS `ohrm_performance_tracker_reviewer` (
`id` int(11) NOT NULL,
  `performance_track_id` int(11) NOT NULL,
  `reviewer_id` int(7) NOT NULL,
  `added_date` timestamp NULL DEFAULT NULL,
  `status` int(2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_performance_tracker_reviewer`
--

INSERT INTO `ohrm_performance_tracker_reviewer` (`id`, `performance_track_id`, `reviewer_id`, `added_date`, `status`) VALUES
(1, 1, 1, '2014-12-08 06:26:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_plugin`
--

CREATE TABLE IF NOT EXISTS `ohrm_plugin` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `version` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_project`
--

CREATE TABLE IF NOT EXISTS `ohrm_project` (
`project_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_project`
--

INSERT INTO `ohrm_project` (`project_id`, `customer_id`, `name`, `description`, `is_deleted`) VALUES
(1, 1, 'Security', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_project_activity`
--

CREATE TABLE IF NOT EXISTS `ohrm_project_activity` (
`activity_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_project_activity`
--

INSERT INTO `ohrm_project_activity` (`activity_id`, `project_id`, `name`, `is_deleted`) VALUES
(1, 1, 'camera fittingt', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_project_admin`
--

CREATE TABLE IF NOT EXISTS `ohrm_project_admin` (
  `project_id` int(11) NOT NULL,
  `emp_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_project_admin`
--

INSERT INTO `ohrm_project_admin` (`project_id`, `emp_number`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_report`
--

CREATE TABLE IF NOT EXISTS `ohrm_report` (
`report_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `report_group_id` bigint(20) NOT NULL,
  `use_filter_field` tinyint(1) NOT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_report`
--

INSERT INTO `ohrm_report` (`report_id`, `name`, `report_group_id`, `use_filter_field`, `type`) VALUES
(1, 'Project Report', 1, 1, NULL),
(2, 'Employee Report', 1, 1, NULL),
(3, 'Project Activity Details', 1, 1, NULL),
(4, 'Attendance Total Summary Report', 2, 0, NULL),
(5, 'PIM Sample Report', 3, 1, 'PIM_DEFINED');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_report_group`
--

CREATE TABLE IF NOT EXISTS `ohrm_report_group` (
  `report_group_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `core_sql` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_report_group`
--

INSERT INTO `ohrm_report_group` (`report_group_id`, `name`, `core_sql`) VALUES
(1, 'timesheet', 'SELECT selectCondition FROM ohrm_project_activity LEFT JOIN (SELECT * FROM ohrm_timesheet_item WHERE whereCondition1) AS ohrm_timesheet_item  ON (ohrm_timesheet_item.activity_id = ohrm_project_activity.activity_id) LEFT JOIN ohrm_project ON (ohrm_project.project_id = ohrm_project_activity.project_id) LEFT JOIN hs_hr_employee ON (hs_hr_employee.emp_number = ohrm_timesheet_item.employee_id) LEFT JOIN ohrm_timesheet ON (ohrm_timesheet.timesheet_id = ohrm_timesheet_item.timesheet_id) LEFT JOIN ohrm_customer ON (ohrm_customer.customer_id = ohrm_project.customer_id) WHERE whereCondition2 groupByClause ORDER BY ohrm_customer.name, ohrm_project.name, ohrm_project_activity.name, hs_hr_employee.emp_lastname, hs_hr_employee.emp_firstname'),
(2, 'attendance', 'SELECT selectCondition FROM hs_hr_employee LEFT JOIN (SELECT * FROM ohrm_attendance_record WHERE ( ( ohrm_attendance_record.punch_in_user_time BETWEEN "#@fromDate@,@1970-01-01@#" AND #@"toDate"@,@CURDATE()@# ) AND ( ohrm_attendance_record.punch_out_user_time BETWEEN "#@fromDate@,@1970-01-01@#" AND #@"toDate"@,@CURDATE()@# ) ) ) AS ohrm_attendance_record ON (hs_hr_employee.emp_number = ohrm_attendance_record.employee_id) WHERE hs_hr_employee.emp_number = #@employeeId@,@hs_hr_employee.emp_number AND (hs_hr_employee.termination_id is null) @# AND (hs_hr_employee.job_title_code = #@"jobTitle")@,@hs_hr_employee.job_title_code OR hs_hr_employee.job_title_code is null)@# AND (hs_hr_employee.work_station IN (#@subUnit)@,@SELECT id FROM ohrm_subunit) OR hs_hr_employee.work_station is null@#) AND (hs_hr_employee.emp_status = #@"employeeStatus")@,@hs_hr_employee.emp_status OR hs_hr_employee.emp_status is null)@# groupByClause ORDER BY hs_hr_employee.emp_lastname, hs_hr_employee.emp_firstname'),
(3, 'pim', 'SELECT selectCondition FROM hs_hr_employee \n                    LEFT JOIN hs_hr_emp_emergency_contacts ON \n                        (hs_hr_employee.emp_number = hs_hr_emp_emergency_contacts.emp_number) \n                    LEFT JOIN ohrm_subunit ON \n                        (hs_hr_employee.work_station = ohrm_subunit.id) \n                    LEFT JOIN ohrm_employment_status ON \n                        (hs_hr_employee.emp_status = ohrm_employment_status.id) \n                    LEFT JOIN ohrm_job_title ON\n                        (hs_hr_employee.job_title_code = ohrm_job_title.id)\n                    LEFT JOIN ohrm_job_category ON \n                        (hs_hr_employee.eeo_cat_code = ohrm_job_category.id) \n                    LEFT JOIN ohrm_nationality ON\n                        (hs_hr_employee.nation_code = ohrm_nationality.id)\n                    LEFT JOIN hs_hr_emp_dependents ON \n                        (hs_hr_employee.emp_number = hs_hr_emp_dependents.emp_number)\n                    LEFT JOIN hs_hr_emp_locations AS emp_location ON\n                        (hs_hr_employee.emp_number = emp_location.emp_number)\n                    LEFT JOIN ohrm_location ON\n                        (emp_location.location_id = ohrm_location.id)\n                    LEFT JOIN hs_hr_emp_contract_extend ON \n                        (hs_hr_employee.emp_number = hs_hr_emp_contract_extend.emp_number) \n                    LEFT JOIN hs_hr_emp_basicsalary ON \n                        (hs_hr_employee.emp_number = hs_hr_emp_basicsalary.emp_number) \n                    LEFT JOIN ohrm_pay_grade ON \n                        (hs_hr_emp_basicsalary.sal_grd_code = ohrm_pay_grade.id) \n                    LEFT JOIN hs_hr_currency_type ON \n                        (hs_hr_emp_basicsalary.currency_id = hs_hr_currency_type.currency_id) \n                    LEFT JOIN hs_hr_payperiod ON \n                        (hs_hr_emp_basicsalary.payperiod_code = hs_hr_payperiod.payperiod_code) \n                    LEFT JOIN hs_hr_emp_passport ON \n                        (hs_hr_employee.emp_number = hs_hr_emp_passport.emp_number) \n                    LEFT JOIN hs_hr_emp_reportto AS subordinate_list ON \n                        (hs_hr_employee.emp_number = subordinate_list.erep_sup_emp_number) \n                    LEFT JOIN hs_hr_employee AS subordinate ON\n                        (subordinate.emp_number = subordinate_list.erep_sub_emp_number)\n                    LEFT JOIN ohrm_emp_reporting_method AS subordinate_reporting_method ON \n                        (subordinate_list.erep_reporting_mode = subordinate_reporting_method.reporting_method_id) \n                    LEFT JOIN hs_hr_emp_work_experience ON \n                        (hs_hr_employee.emp_number = hs_hr_emp_work_experience.emp_number) \n                    LEFT JOIN ohrm_emp_education ON \n                        (hs_hr_employee.emp_number = ohrm_emp_education.emp_number) \n                    LEFT JOIN ohrm_education ON \n                        (ohrm_emp_education.education_id = ohrm_education.id) \n                    LEFT JOIN hs_hr_emp_skill ON \n                        (hs_hr_employee.emp_number = hs_hr_emp_skill.emp_number) \n                    LEFT JOIN ohrm_skill ON \n                        (hs_hr_emp_skill.skill_id = ohrm_skill.id) \n                    LEFT JOIN hs_hr_emp_language ON \n                        (hs_hr_employee.emp_number = hs_hr_emp_language.emp_number) \n                    LEFT JOIN ohrm_language ON \n                        (hs_hr_emp_language.lang_id = ohrm_language.id) \n                    LEFT JOIN ohrm_emp_license ON \n                        (hs_hr_employee.emp_number = ohrm_emp_license.emp_number) \n                    LEFT JOIN ohrm_license ON \n                        (ohrm_emp_license.license_id = ohrm_license.id) \n                    LEFT JOIN hs_hr_emp_member_detail ON \n                        (hs_hr_employee.emp_number = hs_hr_emp_member_detail.emp_number) \n                    LEFT JOIN ohrm_membership ON\n                        (hs_hr_emp_member_detail.membship_code = ohrm_membership.id)\n                    LEFT JOIN hs_hr_country ON \n                        (hs_hr_employee.coun_code = hs_hr_country.cou_code) \n                    LEFT JOIN hs_hr_emp_directdebit ON \n                        (hs_hr_emp_basicsalary.id = hs_hr_emp_directdebit.salary_id) \n                    LEFT JOIN hs_hr_emp_reportto AS supervisor_list ON \n                        (hs_hr_employee.emp_number = supervisor_list.erep_sub_emp_number) \n                    LEFT JOIN hs_hr_employee AS supervisor ON\n                        (supervisor.emp_number = supervisor_list.erep_sup_emp_number)\n                    LEFT JOIN ohrm_emp_reporting_method AS supervisor_reporting_method ON \n                        (supervisor_list.erep_reporting_mode = supervisor_reporting_method.reporting_method_id) \n                    LEFT JOIN ohrm_emp_termination ON\n                        (hs_hr_employee.termination_id = ohrm_emp_termination.id)\n                    LEFT JOIN ohrm_emp_termination_reason ON\n                        (ohrm_emp_termination.reason_id = ohrm_emp_termination_reason.id)\n                WHERE hs_hr_employee.emp_number in (\n                    SELECT hs_hr_employee.emp_number FROM hs_hr_employee\n                        LEFT JOIN hs_hr_emp_basicsalary ON \n                            (hs_hr_employee.emp_number = hs_hr_emp_basicsalary.emp_number) \n                        LEFT JOIN ohrm_emp_education ON \n                            (hs_hr_employee.emp_number = ohrm_emp_education.emp_number) \n                        LEFT JOIN hs_hr_emp_skill ON \n                            (hs_hr_employee.emp_number = hs_hr_emp_skill.emp_number) \n                        LEFT JOIN hs_hr_emp_language ON \n                            (hs_hr_employee.emp_number = hs_hr_emp_language.emp_number) \n                    WHERE whereCondition1\n                )\n                GROUP BY \n                     hs_hr_employee.emp_number,\n                     hs_hr_employee.emp_lastname,\n                     hs_hr_employee.emp_firstname,\n                     hs_hr_employee.emp_middle_name,\n                     hs_hr_employee.emp_birthday,\n                     ohrm_nationality.name,\n                     hs_hr_employee.emp_gender,\n                     hs_hr_employee.emp_marital_status,\n                     hs_hr_employee.emp_dri_lice_num,\n                     hs_hr_employee.emp_dri_lice_exp_date,\n                     hs_hr_employee.emp_street1,\n                     hs_hr_employee.emp_street2,\n                     hs_hr_employee.city_code,\n                     hs_hr_employee.provin_code,\n                     hs_hr_employee.emp_zipcode,\n                     hs_hr_country.cou_code,\n                     hs_hr_employee.emp_hm_telephone,\n                     hs_hr_employee.emp_mobile,\n                     hs_hr_employee.emp_work_telephone,\n                     hs_hr_employee.emp_work_email,\n                     hs_hr_employee.emp_oth_email\n\nORDER BY hs_hr_employee.emp_lastname\n');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_reviewer`
--

CREATE TABLE IF NOT EXISTS `ohrm_reviewer` (
`id` int(7) NOT NULL,
  `review_id` int(7) DEFAULT NULL,
  `employee_number` int(7) DEFAULT NULL,
  `status` int(7) DEFAULT NULL,
  `reviewer_group_id` int(7) DEFAULT NULL,
  `completed_date` datetime DEFAULT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_reviewer_group`
--

CREATE TABLE IF NOT EXISTS `ohrm_reviewer_group` (
`id` int(7) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `piority` int(7) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_reviewer_group`
--

INSERT INTO `ohrm_reviewer_group` (`id`, `name`, `piority`) VALUES
(1, 'Supervisor', 1),
(2, 'Employee', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_reviewer_rating`
--

CREATE TABLE IF NOT EXISTS `ohrm_reviewer_rating` (
`id` int(11) NOT NULL,
  `rating` decimal(18,2) DEFAULT NULL,
  `kpi_id` int(7) DEFAULT NULL,
  `review_id` int(7) DEFAULT NULL,
  `reviewer_id` int(7) NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_role_user_selection_rule`
--

CREATE TABLE IF NOT EXISTS `ohrm_role_user_selection_rule` (
  `user_role_id` int(10) NOT NULL,
  `selection_rule_id` int(10) NOT NULL,
  `configurable_params` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_screen`
--

CREATE TABLE IF NOT EXISTS `ohrm_screen` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `module_id` int(11) NOT NULL,
  `action_url` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_screen`
--

INSERT INTO `ohrm_screen` (`id`, `name`, `module_id`, `action_url`) VALUES
(1, 'User List', 2, 'viewSystemUsers'),
(2, 'Add/Edit System User', 2, 'saveSystemUser'),
(3, 'Delete System Users', 2, 'deleteSystemUsers'),
(4, 'Add Employee', 3, 'addEmployee'),
(5, 'View Employee List', 3, 'viewEmployeeList'),
(6, 'Delete Employees', 3, 'deleteEmployees'),
(7, 'Leave Type List', 4, 'leaveTypeList'),
(8, 'Define Leave Type', 4, 'defineLeaveType'),
(9, 'Undelete Leave Type', 4, 'undeleteLeaveType'),
(10, 'Delete Leave Type', 4, 'deleteLeaveType'),
(11, 'View Holiday List', 4, 'viewHolidayList'),
(12, 'Define Holiday', 4, 'defineHoliday'),
(13, 'Delete Holiday', 4, 'deleteHoliday'),
(14, 'Define WorkWeek', 4, 'defineWorkWeek'),
(16, 'Leave List', 4, 'viewLeaveList'),
(17, 'Assign Leave', 4, 'assignLeave'),
(18, 'View Leave Summary', 4, 'viewLeaveSummary'),
(19, 'Save Leave Entitlements', 4, 'saveLeaveEntitlements'),
(20, 'General Information', 2, 'viewOrganizationGeneralInformation'),
(21, 'Location List', 2, 'viewLocations'),
(22, 'View Company Structure', 2, 'viewCompanyStructure'),
(23, 'Job Title List', 2, 'viewJobTitleList'),
(24, 'Pay Grade List', 2, 'viewPayGrades'),
(25, 'Employment Status List', 2, 'employmentStatus'),
(26, 'Job Category List', 2, 'jobCategory'),
(27, 'Work Shift List', 2, 'workShift'),
(28, 'Skill List', 2, 'viewSkills'),
(29, 'Education List', 2, 'viewEducation'),
(30, 'License List', 2, 'viewLicenses'),
(31, 'Language List', 2, 'viewLanguages'),
(32, 'Membership List', 2, 'membership'),
(33, 'Nationality List', 2, 'nationality'),
(34, 'Add/Edit Mail Configuration', 2, 'listMailConfiguration'),
(35, 'Notification List', 2, 'viewEmailNotification'),
(36, 'Customer List', 2, 'viewCustomers'),
(37, 'Project List', 2, 'viewProjects'),
(38, 'Localization', 2, 'localization'),
(39, 'Module Configuration', 2, 'viewModules'),
(40, 'Configure PIM', 3, 'configurePim'),
(41, 'Custom Field List', 3, 'listCustomFields'),
(42, 'Data Import', 2, 'pimCsvImport'),
(43, 'Reporting Method List', 3, 'viewReportingMethods'),
(44, 'Termination Reason List', 3, 'viewTerminationReasons'),
(45, 'PIM Reports List', 1, 'viewDefinedPredefinedReports'),
(46, 'View MyInfo', 3, 'viewMyDetails'),
(47, 'Define Leave Period', 4, 'defineLeavePeriod'),
(48, 'View My Leave List', 4, 'viewMyLeaveList'),
(49, 'Apply Leave', 4, 'applyLeave'),
(50, 'Define Timesheet Start Date', 5, 'defineTimesheetPeriod'),
(51, 'View My Timesheet', 5, 'viewMyTimesheet'),
(52, 'View Employee Timesheet', 5, 'viewEmployeeTimesheet'),
(53, 'View My Attendance', 6, 'viewMyAttendanceRecord'),
(54, 'Punch In/Out', 6, 'punchIn'),
(55, 'View Employee Attendance', 6, 'viewAttendanceRecord'),
(56, 'Attendance Configuration', 6, 'configure'),
(57, 'View Project Report Criteria', 5, 'displayProjectReportCriteria'),
(58, 'View Employee Report Criteria', 5, 'displayEmployeeReportCriteria'),
(59, 'View Attendance Report Criteria', 5, 'displayAttendanceSummaryReportCriteria'),
(60, 'Candidate List', 7, 'viewCandidates'),
(61, 'Vacancy List', 7, 'viewJobVacancy'),
(67, 'View Time Module', 5, 'viewTimeModule'),
(68, 'View Leave Module', 4, 'viewLeaveModule'),
(69, 'Leave Entitlements', 4, 'viewLeaveEntitlements'),
(70, 'My Leave Entitlements', 4, 'viewMyLeaveEntitlements'),
(71, 'Delete Leave Entitlements', 4, 'deleteLeaveEntitlements'),
(72, 'Add Leave Entitlement', 4, 'addLeaveEntitlement'),
(73, 'Edit Leave Entitlement', 4, 'editLeaveEntitlement'),
(74, 'View Admin Module', 2, 'viewAdminModule'),
(75, 'View PIM Module', 3, 'viewPimModule'),
(76, 'View Recruitment Module', 7, 'viewRecruitmentModule'),
(78, 'Leave Balance Report', 4, 'viewLeaveBalanceReport'),
(79, 'My Leave Balance Report', 4, 'viewMyLeaveBalanceReport'),
(80, 'Save Job Title', 2, 'saveJobTitle'),
(81, 'Delete Job Title', 2, 'deleteJobTitle'),
(82, 'Save Pay Grade', 2, 'payGrade'),
(83, 'Delete Pay Grade', 2, 'deletePayGrades'),
(84, 'Save Pay Grade Currency', 2, 'savePayGradeCurrency'),
(85, 'Delete Pay Grade Currency', 2, 'deletePayGradeCurrency'),
(86, 'Add Customer', 2, 'addCustomer'),
(87, 'Delete Customer', 2, 'deleteCustomer'),
(88, 'Save Project', 2, 'saveProject'),
(89, 'Delete Project', 2, 'deleteProject'),
(90, 'Add Project Adtivity', 2, 'addProjectActivity'),
(91, 'Delete Project Adtivity', 2, 'deleteProjectActivity'),
(92, 'Define PIM reports', 1, 'definePredefinedReport'),
(93, 'Display PIM reports', 1, 'displayPredefinedReport'),
(94, 'Add Job Vacancy', 7, 'addJobVacancy'),
(95, 'Delete Job Vacancy', 7, 'deleteJobVacancy'),
(96, 'Add Candidate', 7, 'addCandidate'),
(97, 'Delete Candidate', 7, 'deleteCandidateVacancies'),
(98, 'View Leave Request', 4, 'viewLeaveRequest'),
(99, 'Change Leave Status', 4, 'changeLeaveStatus'),
(100, 'Terminate Employment', 3, 'terminateEmployement'),
(101, 'View Attendance Summary Report', 5, 'displayAttendanceSummaryReport'),
(102, 'View Project Activity Details Report', 5, 'displayProjectActivityDetailsReport'),
(103, 'Dashboard', 10, 'index'),
(104, 'Save KPI', 11, 'saveKpi'),
(105, 'Saearch KPI', 11, 'searchKpi'),
(106, 'My Reviews', 11, 'myPerformanceReview'),
(107, 'Add Review', 11, 'saveReview'),
(108, 'Review Evaluate', 11, 'reviewEvaluate'),
(109, 'Review Evaluate By Admin', 11, 'reviewEvaluateByAdmin'),
(110, 'Search Evaluate Performance', 11, 'searchEvaluatePerformancReview'),
(111, 'Search Performance Review', 11, 'searchPerformancReview'),
(112, 'Manage_Trackers', 12, 'addPerformanceTracker'),
(113, 'Employee_Trackers', 12, 'viewEmployeePerformanceTrackerList'),
(114, 'My_Trackers', 12, 'viewMyPerformanceTrackerList'),
(115, 'Employee_Tracker_Logs', 12, 'addPerformanceTrackerLog'),
(116, 'Payroll', 13, 'viewPayslips'),
(117, 'salary', 13, 'viewSalary'),
(118, 'PaySlips', 13, 'viewPayslips'),
(119, 'Allowances', 13, 'viewAllowanceList'),
(120, 'Tax Rates', 13, 'viewTaxRates'),
(122, 'Assign Pension', 13, 'assignPension'),
(123, 'Pensions', 13, 'viewPensions'),
(124, 'Employees Pensions', 13, 'viewEmployeesPensions'),
(125, 'Run Payroll', 13, 'runPayroll'),
(126, 'Payslips', 13, 'viewPayslips'),
(129, 'Loans', 13, '#'),
(130, 'Financial Organizations', 13, 'viewFinancialOrganizations'),
(131, 'Loans', 13, 'viewLoans'),
(132, 'Employee Loans', 13, 'viewEmployeesLoans'),
(134, 'Deductions', 13, 'viewDeductions'),
(136, 'Add Deduction', 13, 'addDeduction'),
(137, 'Payroll', 13, 'payrollReport'),
(138, 'Paye', 13, 'payeReport'),
(139, 'Pensions', 13, 'pensionsReport'),
(140, 'Loans', 13, 'loansReport'),
(141, 'Deductions', 13, 'deductionsReport'),
(142, 'OPRAS', 11, ''),
(143, 'Forms', 11, 'viewOprasForms');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_selected_composite_display_field`
--

CREATE TABLE IF NOT EXISTS `ohrm_selected_composite_display_field` (
  `id` bigint(20) NOT NULL,
  `composite_display_field_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_selected_composite_display_field`
--

INSERT INTO `ohrm_selected_composite_display_field` (`id`, `composite_display_field_id`, `report_id`) VALUES
(1, 1, 3),
(2, 1, 4),
(3, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_selected_display_field`
--

CREATE TABLE IF NOT EXISTS `ohrm_selected_display_field` (
`id` bigint(20) NOT NULL,
  `display_field_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_selected_display_field`
--

INSERT INTO `ohrm_selected_display_field` (`id`, `display_field_id`, `report_id`) VALUES
(2, 2, 1),
(4, 8, 2),
(5, 9, 5),
(6, 10, 5),
(7, 11, 5),
(8, 12, 5),
(9, 13, 5),
(10, 14, 5),
(11, 15, 5),
(13, 17, 5),
(14, 18, 5),
(15, 19, 5),
(16, 20, 5),
(17, 21, 5),
(18, 22, 5),
(19, 23, 5),
(20, 24, 5),
(21, 25, 5),
(22, 26, 5),
(23, 27, 5),
(24, 28, 5),
(25, 29, 5),
(26, 30, 5),
(27, 31, 5),
(28, 32, 5),
(29, 33, 5),
(31, 35, 5),
(32, 36, 5),
(33, 37, 5),
(34, 38, 5),
(35, 39, 5),
(36, 40, 5),
(37, 41, 5),
(38, 42, 5),
(39, 43, 5),
(40, 44, 5),
(41, 45, 5),
(43, 47, 5),
(44, 48, 5),
(45, 49, 5),
(48, 52, 5),
(49, 53, 5),
(50, 54, 5),
(51, 55, 5),
(53, 57, 5),
(54, 58, 5),
(55, 59, 5),
(56, 60, 5),
(57, 61, 5),
(58, 62, 5),
(59, 63, 5),
(60, 64, 5),
(61, 65, 5),
(62, 66, 5),
(63, 67, 5),
(64, 68, 5),
(65, 69, 5),
(66, 70, 5),
(67, 71, 5),
(68, 72, 5),
(69, 73, 5),
(70, 74, 5),
(71, 75, 5),
(72, 76, 5),
(73, 77, 5),
(74, 78, 5),
(76, 80, 5),
(77, 81, 5),
(78, 82, 5),
(79, 83, 5),
(80, 84, 5),
(81, 85, 5),
(82, 86, 5),
(83, 87, 5),
(84, 88, 5),
(85, 89, 5),
(86, 90, 5),
(87, 91, 5),
(88, 92, 5),
(89, 93, 5),
(90, 94, 5),
(91, 95, 5),
(93, 97, 5);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_selected_display_field_group`
--

CREATE TABLE IF NOT EXISTS `ohrm_selected_display_field_group` (
`id` int(10) unsigned NOT NULL,
  `report_id` bigint(20) NOT NULL,
  `display_field_group_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_selected_display_field_group`
--

INSERT INTO `ohrm_selected_display_field_group` (`id`, `report_id`, `display_field_group_id`) VALUES
(1, 5, 1),
(2, 5, 2),
(3, 5, 3),
(4, 5, 4),
(5, 5, 5),
(6, 5, 6),
(7, 5, 7),
(8, 5, 8),
(9, 5, 9),
(10, 5, 10),
(11, 5, 11),
(12, 5, 12),
(13, 5, 13),
(14, 5, 14),
(15, 5, 15);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_selected_filter_field`
--

CREATE TABLE IF NOT EXISTS `ohrm_selected_filter_field` (
  `report_id` bigint(20) NOT NULL,
  `filter_field_id` bigint(20) NOT NULL,
  `filter_field_order` bigint(20) NOT NULL,
  `value1` varchar(255) DEFAULT NULL,
  `value2` varchar(255) DEFAULT NULL,
  `where_condition` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_selected_filter_field`
--

INSERT INTO `ohrm_selected_filter_field` (`report_id`, `filter_field_id`, `filter_field_order`, `value1`, `value2`, `where_condition`, `type`) VALUES
(1, 1, 1, NULL, NULL, NULL, 'Runtime'),
(1, 3, 2, NULL, NULL, NULL, 'Runtime'),
(1, 7, 3, NULL, NULL, NULL, 'Runtime'),
(1, 21, 4, '0', NULL, '=', 'Predefined'),
(2, 3, 4, NULL, NULL, NULL, 'Runtime'),
(2, 4, 1, NULL, NULL, NULL, 'Runtime'),
(2, 5, 3, NULL, NULL, NULL, 'Runtime'),
(2, 6, 2, NULL, NULL, NULL, 'Runtime'),
(2, 7, 5, NULL, NULL, NULL, 'Runtime'),
(3, 3, 2, NULL, NULL, NULL, 'Runtime'),
(3, 5, 1, NULL, NULL, NULL, 'Runtime'),
(3, 7, 3, NULL, NULL, NULL, 'Runtime'),
(5, 22, 1, NULL, NULL, 'IS NULL', 'Predefined');

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_selected_group_field`
--

CREATE TABLE IF NOT EXISTS `ohrm_selected_group_field` (
  `group_field_id` bigint(20) NOT NULL,
  `summary_display_field_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_selected_group_field`
--

INSERT INTO `ohrm_selected_group_field` (`group_field_id`, `summary_display_field_id`, `report_id`) VALUES
(1, 1, 1),
(1, 1, 2),
(2, 1, 3),
(2, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_skill`
--

CREATE TABLE IF NOT EXISTS `ohrm_skill` (
`id` int(11) NOT NULL,
  `name` varchar(120) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_subunit`
--

CREATE TABLE IF NOT EXISTS `ohrm_subunit` (
`id` int(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `unit_id` varchar(100) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `lft` smallint(6) unsigned DEFAULT NULL,
  `rgt` smallint(6) unsigned DEFAULT NULL,
  `level` smallint(6) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_subunit`
--

INSERT INTO `ohrm_subunit` (`id`, `name`, `unit_id`, `description`, `lft`, `rgt`, `level`) VALUES
(1, 'Organization', '', '', 1, 12, 0),
(2, 'Finance', '', '', 2, 3, 1),
(3, 'IT', '', '', 4, 5, 1),
(4, 'Operations', '', '', 6, 7, 1),
(5, 'HR', '', '', 8, 9, 1),
(6, 'R&D', '', '', 10, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_summary_display_field`
--

CREATE TABLE IF NOT EXISTS `ohrm_summary_display_field` (
  `summary_display_field_id` bigint(20) NOT NULL,
  `function` varchar(1000) NOT NULL,
  `label` varchar(255) NOT NULL,
  `field_alias` varchar(255) DEFAULT NULL,
  `is_sortable` varchar(10) NOT NULL,
  `sort_order` varchar(255) DEFAULT NULL,
  `sort_field` varchar(255) DEFAULT NULL,
  `element_type` varchar(255) NOT NULL,
  `element_property` varchar(1000) NOT NULL,
  `width` varchar(255) NOT NULL,
  `is_exportable` varchar(10) DEFAULT NULL,
  `text_alignment_style` varchar(20) DEFAULT NULL,
  `is_value_list` tinyint(1) NOT NULL DEFAULT '0',
  `display_field_group_id` int(10) unsigned DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_summary_display_field`
--

INSERT INTO `ohrm_summary_display_field` (`summary_display_field_id`, `function`, `label`, `field_alias`, `is_sortable`, `sort_order`, `sort_field`, `element_type`, `element_property`, `width`, `is_exportable`, `text_alignment_style`, `is_value_list`, `display_field_group_id`, `default_value`) VALUES
(1, 'ROUND(COALESCE(sum(duration)/3600, 0),2)', 'Time (Hours)', 'totalduration', 'false', NULL, NULL, 'label', '<xml><getter>totalduration</getter></xml>', '100', 'false', 'right', 0, NULL, NULL),
(2, 'ROUND(COALESCE(sum(TIMESTAMPDIFF(SECOND , ohrm_attendance_record.punch_in_utc_time , ohrm_attendance_record.punch_out_utc_time))/3600, 0),2)', 'Time (Hours)', 'totalduration', 'false', NULL, NULL, 'label', '<xml><getter>totalduration</getter></xml>', '100', 'false', 'right', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_timesheet`
--

CREATE TABLE IF NOT EXISTS `ohrm_timesheet` (
  `timesheet_id` bigint(20) NOT NULL,
  `state` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `employee_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_timesheet`
--

INSERT INTO `ohrm_timesheet` (`timesheet_id`, `state`, `start_date`, `end_date`, `employee_id`) VALUES
(1, 'NOT SUBMITTED', '2014-10-20', '2014-10-26', 1),
(2, 'NOT SUBMITTED', '2014-11-17', '2014-11-23', 2),
(3, 'REJECTED', '2014-12-08', '2014-12-14', 1),
(4, 'NOT SUBMITTED', '2014-12-01', '2014-12-07', 1),
(5, 'NOT SUBMITTED', '2014-11-24', '2014-11-30', 1),
(6, 'NOT SUBMITTED', '2014-11-17', '2014-11-23', 1),
(7, 'NOT SUBMITTED', '2014-11-10', '2014-11-16', 1),
(8, 'NOT SUBMITTED', '2014-11-03', '2014-11-09', 1),
(9, 'NOT SUBMITTED', '2014-10-27', '2014-11-02', 1),
(10, 'SUBMITTED', '2015-03-09', '2015-03-15', 1),
(11, 'NOT SUBMITTED', '2015-03-02', '2015-03-08', 1),
(12, 'NOT SUBMITTED', '2015-02-23', '2015-03-01', 1),
(13, 'NOT SUBMITTED', '2015-02-16', '2015-02-22', 1),
(14, 'NOT SUBMITTED', '2015-02-09', '2015-02-15', 1),
(15, 'NOT SUBMITTED', '2015-02-02', '2015-02-08', 1),
(16, 'NOT SUBMITTED', '2015-01-26', '2015-02-01', 1),
(17, 'NOT SUBMITTED', '2015-01-19', '2015-01-25', 1),
(18, 'NOT SUBMITTED', '2015-01-12', '2015-01-18', 1),
(19, 'NOT SUBMITTED', '2015-01-05', '2015-01-11', 1),
(20, 'NOT SUBMITTED', '2014-12-29', '2015-01-04', 1),
(21, 'NOT SUBMITTED', '2014-12-22', '2014-12-28', 1),
(22, 'NOT SUBMITTED', '2014-12-15', '2014-12-21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_timesheet_action_log`
--

CREATE TABLE IF NOT EXISTS `ohrm_timesheet_action_log` (
  `timesheet_action_log_id` bigint(20) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `date_time` date NOT NULL,
  `performed_by` int(20) NOT NULL,
  `timesheet_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_timesheet_action_log`
--

INSERT INTO `ohrm_timesheet_action_log` (`timesheet_action_log_id`, `comment`, `action`, `date_time`, `performed_by`, `timesheet_id`) VALUES
(1, NULL, 'SUBMITTED', '2014-12-08', 2, 3),
(2, NULL, 'SUBMITTED', '2015-03-09', 1, 10),
(3, '', 'REJECTED', '2015-03-09', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_timesheet_item`
--

CREATE TABLE IF NOT EXISTS `ohrm_timesheet_item` (
  `timesheet_item_id` bigint(20) NOT NULL,
  `timesheet_id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `duration` bigint(20) DEFAULT NULL,
  `comment` text,
  `project_id` bigint(20) NOT NULL,
  `employee_id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_timesheet_item`
--

INSERT INTO `ohrm_timesheet_item` (`timesheet_item_id`, `timesheet_id`, `date`, `duration`, `comment`, `project_id`, `employee_id`, `activity_id`) VALUES
(1, 3, '2014-12-08', 28800, 'cameras are all not working properly', 1, 1, 1),
(2, 10, '2015-03-09', 10800, 'Couldn''t finish', 1, 1, 1),
(3, 10, '2015-03-10', 14400, NULL, 1, 1, 1),
(4, 10, '2015-03-11', 18000, NULL, 1, 1, 1),
(5, 10, '2015-03-12', 22200, NULL, 1, 1, 1),
(6, 10, '2015-03-13', 14400, NULL, 1, 1, 1),
(7, 10, '2015-03-14', 21600, NULL, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_upgrade_history`
--

CREATE TABLE IF NOT EXISTS `ohrm_upgrade_history` (
`id` int(10) NOT NULL,
  `start_version` varchar(30) DEFAULT NULL,
  `end_version` varchar(30) DEFAULT NULL,
  `start_increment` int(11) NOT NULL,
  `end_increment` int(11) NOT NULL,
  `upgraded_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_user`
--

CREATE TABLE IF NOT EXISTS `ohrm_user` (
`id` int(10) NOT NULL,
  `user_role_id` int(10) NOT NULL,
  `emp_number` int(13) DEFAULT NULL,
  `user_name` varchar(40) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` int(10) DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_user`
--

INSERT INTO `ohrm_user` (`id`, `user_role_id`, `emp_number`, `user_name`, `user_password`, `deleted`, `status`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`) VALUES
(1, 1, NULL, 'admin', '$2a$12$F30mRQVW8TV2AWOKBIKbk.m6dBZ7xOLuzH93RFpxLCG0p/u46qLoq', 0, 1, NULL, NULL, NULL, NULL),
(2, 2, 1, 'johnd', '$2a$12$JLf1QSMvPEYpmozcFuOequuhEXN54MONS/9Bd4kzcYHs/nl2jrhQC', 0, 1, '2014-11-20 13:20:32', '2014-12-08 09:21:15', 1, 1),
(3, 2, 4, 'smith', '$2a$12$q1LaQv8UzENiSJULAZ4A2Oj7nNqYd7P4WSbVfoadQ5tQ6wyjsOqTW', 0, 1, '2015-01-22 09:31:37', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_user_role`
--

CREATE TABLE IF NOT EXISTS `ohrm_user_role` (
`id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `is_assignable` tinyint(1) DEFAULT '0',
  `is_predefined` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_user_role`
--

INSERT INTO `ohrm_user_role` (`id`, `name`, `display_name`, `is_assignable`, `is_predefined`) VALUES
(1, 'Admin', 'Admin', 1, 1),
(2, 'ESS', 'ESS', 1, 1),
(3, 'Supervisor', 'Supervisor', 0, 1),
(4, 'ProjectAdmin', 'ProjectAdmin', 0, 1),
(5, 'Interviewer', 'Interviewer', 0, 1),
(6, 'HiringManager', 'HiringManager', 0, 1),
(7, 'Reviewer', 'Reviewer', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_user_role_data_group`
--

CREATE TABLE IF NOT EXISTS `ohrm_user_role_data_group` (
`id` int(11) NOT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  `data_group_id` int(11) DEFAULT NULL,
  `can_read` tinyint(4) DEFAULT NULL,
  `can_create` tinyint(4) DEFAULT NULL,
  `can_update` tinyint(4) DEFAULT NULL,
  `can_delete` tinyint(4) DEFAULT NULL,
  `self` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_user_role_data_group`
--

INSERT INTO `ohrm_user_role_data_group` (`id`, `user_role_id`, `data_group_id`, `can_read`, `can_create`, `can_update`, `can_delete`, `self`) VALUES
(1, 1, 1, 1, NULL, 1, NULL, 0),
(2, 1, 2, 1, 1, 1, 1, 0),
(3, 1, 3, 1, NULL, 1, NULL, 0),
(4, 1, 4, 1, NULL, 1, NULL, 0),
(5, 1, 5, 1, 1, 1, 1, 0),
(6, 1, 6, 1, NULL, 1, NULL, 0),
(7, 1, 7, 1, 1, 1, 1, 0),
(8, 1, 8, 1, 1, 1, 1, 0),
(9, 1, 9, 1, NULL, 1, NULL, 0),
(10, 1, 10, 1, 1, 1, 1, 0),
(11, 1, 11, 1, 1, 1, 1, 0),
(12, 1, 12, 1, NULL, 1, NULL, 0),
(13, 1, 13, 1, 1, 1, 1, 0),
(14, 1, 14, 1, 1, 1, 1, 0),
(15, 1, 15, 1, NULL, 1, NULL, 0),
(16, 1, 16, 1, NULL, 1, NULL, 0),
(17, 1, 17, 1, 1, 1, 1, 0),
(18, 1, 18, 1, NULL, 1, NULL, 0),
(19, 1, 19, 1, 1, 1, 1, 0),
(20, 1, 20, 1, 1, 1, 1, 0),
(21, 1, 21, 1, NULL, 1, NULL, 0),
(22, 1, 22, 1, NULL, 1, NULL, 0),
(23, 1, 23, 1, 1, 1, 1, 0),
(24, 1, 24, 1, NULL, 1, NULL, 0),
(25, 1, 25, 1, 1, 1, 1, 0),
(26, 1, 26, 1, 1, 1, 1, 0),
(27, 1, 27, 1, 1, 1, 1, 0),
(28, 1, 28, 1, NULL, 1, NULL, 0),
(29, 1, 29, 1, 1, 1, 1, 0),
(30, 1, 30, 1, 1, 1, 1, 0),
(31, 1, 31, 1, 1, 1, 1, 0),
(32, 1, 32, 1, 1, 1, 1, 0),
(33, 1, 33, 1, 1, 1, 1, 0),
(34, 1, 34, 1, 1, 1, 1, 0),
(35, 1, 35, 1, NULL, 1, NULL, 0),
(36, 1, 36, 1, 1, 1, 1, 0),
(37, 1, 37, 1, 1, 1, 1, 0),
(38, 1, 38, 1, NULL, 1, NULL, 0),
(39, 1, 39, 1, NULL, 1, 1, 0),
(40, 1, 40, 1, 1, 1, 1, 0),
(41, 1, 41, 1, NULL, NULL, NULL, 0),
(42, 1, 40, 1, 1, 1, 1, 1),
(43, 2, 1, 1, NULL, 1, NULL, 1),
(44, 2, 2, 1, 1, 1, 1, 1),
(45, 2, 3, 1, NULL, 1, NULL, 1),
(46, 2, 4, 1, NULL, 1, NULL, 1),
(47, 2, 5, 1, 1, 1, 1, 1),
(48, 2, 6, 1, NULL, 1, NULL, 1),
(49, 2, 7, 1, 1, 1, 1, 1),
(50, 2, 8, 1, 1, 1, 1, 1),
(51, 2, 9, 1, NULL, 1, NULL, 1),
(52, 2, 10, 1, 1, 1, 1, 1),
(53, 2, 11, 1, 1, 1, 1, 1),
(54, 2, 12, 1, NULL, 1, NULL, 1),
(55, 2, 13, 1, 1, 1, 1, 1),
(56, 2, 14, 1, 1, 1, 1, 1),
(57, 2, 15, 1, NULL, 1, NULL, 1),
(58, 2, 16, 1, NULL, NULL, NULL, 1),
(59, 2, 17, 1, 0, 0, 0, 1),
(60, 2, 18, 1, 0, 0, 0, 1),
(61, 2, 19, 1, NULL, NULL, NULL, 1),
(62, 2, 20, 1, 0, 0, 0, 1),
(63, 2, 21, 1, 0, 0, 0, 1),
(64, 2, 22, 1, NULL, NULL, NULL, 1),
(65, 2, 23, 1, 0, 0, 0, 1),
(66, 2, 24, 1, 0, 0, 0, 1),
(67, 2, 25, 1, NULL, NULL, NULL, 1),
(68, 2, 26, 1, NULL, NULL, NULL, 1),
(69, 2, 27, 1, 0, 0, 0, 1),
(70, 2, 28, 1, 0, 0, 0, 1),
(71, 2, 29, 1, 1, 1, 1, 1),
(72, 2, 30, 1, 1, 1, 1, 1),
(73, 2, 31, 1, 1, 1, 1, 1),
(74, 2, 32, 1, 1, 1, 1, 1),
(75, 2, 33, 1, 1, 1, 1, 1),
(76, 2, 34, 1, 1, 1, 1, 1),
(77, 2, 35, 1, NULL, 1, NULL, 1),
(78, 2, 36, 1, 1, 1, 1, 1),
(79, 2, 37, 1, 1, 1, 1, 1),
(80, 2, 38, 1, NULL, 1, NULL, 1),
(81, 2, 39, 1, NULL, 1, 1, 1),
(82, 2, 40, 1, 0, 0, 0, 1),
(83, 2, 41, 1, NULL, NULL, NULL, 1),
(84, 3, 1, 1, NULL, 1, NULL, 0),
(85, 3, 2, 1, 1, 1, 1, 0),
(86, 3, 3, 1, NULL, 1, NULL, 0),
(87, 3, 4, 1, NULL, 1, NULL, 0),
(88, 3, 5, 1, 1, 1, 1, 0),
(89, 3, 6, 1, NULL, 1, NULL, 0),
(90, 3, 7, 1, 1, 1, 1, 0),
(91, 3, 8, 1, 1, 1, 1, 0),
(92, 3, 9, 1, NULL, 1, NULL, 0),
(93, 3, 10, 1, 1, 1, 1, 0),
(94, 3, 11, 1, 1, 1, 1, 0),
(95, 3, 12, 1, NULL, 1, NULL, 0),
(96, 3, 13, 1, 1, 1, 1, 0),
(97, 3, 14, 1, 1, 1, 1, 0),
(98, 3, 15, 1, NULL, 1, NULL, 0),
(99, 3, 16, 1, NULL, NULL, NULL, 0),
(100, 3, 17, 1, 0, 0, 0, 0),
(101, 3, 18, 1, 0, 0, 0, 0),
(102, 3, 19, 0, 0, 0, 0, 0),
(103, 3, 20, 0, 0, 0, 0, 0),
(104, 3, 21, 0, 0, 0, 0, 0),
(105, 3, 22, 1, NULL, NULL, NULL, 0),
(106, 3, 23, 1, 0, 0, 0, 0),
(107, 3, 24, 1, 0, 0, 0, 0),
(108, 3, 25, 1, NULL, NULL, NULL, 0),
(109, 3, 26, 1, NULL, NULL, NULL, 0),
(110, 3, 27, 1, 0, 0, 0, 0),
(111, 3, 28, 1, 0, 0, 0, 0),
(112, 3, 29, 1, 1, 1, 1, 0),
(113, 3, 30, 1, 1, 1, 1, 0),
(114, 3, 31, 1, 1, 1, 1, 0),
(115, 3, 32, 1, 1, 1, 1, 0),
(116, 3, 33, 1, 1, 1, 1, 0),
(117, 3, 34, 1, 1, 1, 1, 0),
(118, 3, 35, 1, NULL, 1, NULL, 0),
(119, 3, 36, 1, 1, 1, 1, 0),
(120, 3, 37, 1, 1, 1, 1, 0),
(121, 3, 38, 1, NULL, 1, NULL, 0),
(122, 3, 39, 1, NULL, 1, 1, 0),
(123, 3, 40, 1, 0, 0, 0, 0),
(124, 3, 41, 1, NULL, NULL, NULL, 0),
(125, 3, 1, 1, NULL, 1, NULL, 1),
(126, 3, 2, 1, 1, 1, 1, 1),
(127, 3, 3, 1, NULL, 1, NULL, 1),
(128, 3, 4, 1, NULL, 1, NULL, 1),
(129, 3, 5, 1, 1, 1, 1, 1),
(130, 3, 6, 1, NULL, 1, NULL, 1),
(131, 3, 7, 1, 1, 1, 1, 1),
(132, 3, 8, 1, 1, 1, 1, 1),
(133, 3, 9, 1, NULL, 1, NULL, 1),
(134, 3, 10, 1, 1, 1, 1, 1),
(135, 3, 11, 1, 1, 1, 1, 1),
(136, 3, 12, 1, NULL, 1, NULL, 1),
(137, 3, 13, 1, 1, 1, 1, 1),
(138, 3, 14, 1, 1, 1, 1, 1),
(139, 3, 15, 1, NULL, 1, NULL, 1),
(140, 3, 16, 1, NULL, NULL, NULL, 1),
(141, 3, 17, 1, 0, 0, 0, 1),
(142, 3, 18, 1, 0, 0, 0, 1),
(143, 3, 19, 1, 0, 0, 0, 1),
(144, 3, 20, 1, 0, 0, 0, 1),
(145, 3, 21, 1, 0, 0, 0, 1),
(146, 3, 22, 1, NULL, NULL, NULL, 1),
(147, 3, 23, 1, 0, 0, 0, 1),
(148, 3, 24, 1, 0, 0, 0, 1),
(149, 3, 25, 1, NULL, NULL, NULL, 1),
(150, 3, 26, 1, NULL, NULL, NULL, 1),
(151, 3, 27, 1, 0, 0, 0, 1),
(152, 3, 28, 1, 0, 0, 0, 1),
(153, 3, 29, 1, 1, 1, 1, 1),
(154, 3, 30, 1, 1, 1, 1, 1),
(155, 3, 31, 1, 1, 1, 1, 1),
(156, 3, 32, 1, 1, 1, 1, 1),
(157, 3, 33, 1, 1, 1, 1, 1),
(158, 3, 34, 1, 1, 1, 1, 1),
(159, 3, 35, 1, NULL, 1, NULL, 1),
(160, 3, 36, 1, 1, 1, 1, 1),
(161, 3, 37, 1, 1, 1, 1, 1),
(162, 3, 38, 1, NULL, 1, NULL, 1),
(163, 3, 39, 1, NULL, 1, 1, 1),
(164, 3, 40, 1, 0, 0, 0, 1),
(165, 3, 41, 1, NULL, NULL, NULL, 1),
(166, 1, 42, 1, 1, 1, 1, 0),
(167, 2, 42, 0, 0, 0, 0, 0),
(168, 3, 42, 0, 0, 0, 0, 0),
(169, 1, 43, 1, 1, 1, 1, 0),
(170, 2, 43, 0, 0, 0, 0, 0),
(171, 3, 43, 0, 0, 0, 0, 0),
(172, 1, 44, 1, 1, 1, 1, 0),
(173, 2, 44, 0, 0, 0, 0, 0),
(174, 3, 44, 0, 0, 0, 0, 0),
(175, 1, 45, 1, 1, 1, 1, 0),
(176, 2, 45, 0, 0, 0, 0, 0),
(177, 3, 45, 0, 0, 0, 0, 0),
(178, 4, 45, 1, 0, 1, 0, 0),
(179, 1, 46, 1, 1, 1, 1, 0),
(180, 2, 46, 0, 0, 0, 0, 0),
(181, 3, 46, 0, 0, 0, 0, 0),
(182, 1, 47, 1, NULL, 1, NULL, 0),
(183, 2, 47, 0, 0, 0, 0, 0),
(184, 3, 47, 0, 0, 0, 0, 0),
(185, 1, 48, 1, 0, 0, 0, 0),
(186, 2, 48, 0, 0, 0, 0, 0),
(187, 2, 48, 1, 0, 0, 0, 1),
(188, 3, 48, 1, 0, 0, 0, 0),
(189, 1, 49, 1, 0, 0, 0, 0),
(190, 2, 49, 0, 0, 0, 0, 0),
(191, 3, 49, 0, 0, 0, 0, 0),
(192, 4, 49, 1, 0, 0, 0, 0),
(193, 1, 50, 1, 0, 0, 0, 0),
(194, 2, 50, 0, 0, 0, 0, 0),
(195, 3, 50, 1, 0, 0, 0, 0),
(196, 1, 51, 1, 0, 0, 0, 0),
(197, 2, 51, 0, 0, 0, 0, 0),
(198, 3, 51, 1, 0, 0, 0, 0),
(199, 1, 52, 1, NULL, 1, NULL, 0),
(200, 2, 52, 0, 0, 0, 0, 0),
(201, 3, 52, 0, 0, 0, 0, 0),
(202, 1, 53, 1, 1, 1, 1, 0),
(203, 2, 53, 0, 0, 0, 0, 0),
(204, 3, 53, 0, 0, 0, 0, 0),
(205, 1, 54, 1, 0, 1, 0, 0),
(206, 2, 54, 0, 0, 0, 0, 0),
(207, 3, 54, 0, 0, 0, 0, 0),
(208, 1, 55, 1, 1, 1, 1, 0),
(209, 2, 55, 0, 0, 0, 0, 0),
(210, 3, 55, 0, 0, 0, 0, 0),
(211, 1, 56, 1, 1, 1, 1, 0),
(212, 2, 56, 0, 0, 0, 0, 0),
(213, 3, 56, 0, 0, 0, 0, 0),
(214, 1, 57, 1, 1, 1, 1, 0),
(215, 6, 57, 1, 1, 1, 1, 0),
(216, 5, 57, 1, 0, 1, 0, 0),
(217, 1, 58, 1, 0, 0, 0, 0),
(218, 2, 58, 0, 0, 0, 0, 0),
(219, 2, 58, 1, 0, 0, 0, 1),
(220, 3, 58, 1, 0, 0, 0, 0),
(221, 1, 59, 1, 0, 0, 0, 0),
(222, 2, 59, 1, 0, 0, 0, 1),
(223, 3, 59, 1, 0, 0, 0, 0),
(224, 1, 60, 0, 1, 0, 0, 0),
(225, 2, 60, 0, 1, 0, 0, 1),
(226, 3, 60, 0, 1, 0, 0, 0),
(227, 1, 61, 1, 1, 1, 1, 0),
(228, 1, 62, 1, 1, 1, 1, 0),
(229, 1, 63, 1, 1, 1, 1, 0),
(230, 1, 64, 1, 1, 1, 1, 0),
(231, 2, 63, 1, 0, 0, 0, 0),
(232, 2, 64, 1, 0, 0, 0, 0),
(233, 2, 65, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_user_role_screen`
--

CREATE TABLE IF NOT EXISTS `ohrm_user_role_screen` (
`id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `screen_id` int(11) NOT NULL,
  `can_read` tinyint(1) NOT NULL DEFAULT '0',
  `can_create` tinyint(1) NOT NULL DEFAULT '0',
  `can_update` tinyint(1) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_user_role_screen`
--

INSERT INTO `ohrm_user_role_screen` (`id`, `user_role_id`, `screen_id`, `can_read`, `can_create`, `can_update`, `can_delete`) VALUES
(1, 1, 1, 1, 1, 1, 1),
(2, 1, 2, 1, 1, 1, 1),
(3, 2, 2, 0, 0, 0, 0),
(4, 3, 2, 0, 0, 0, 0),
(5, 1, 3, 1, 1, 1, 1),
(6, 2, 3, 0, 0, 0, 0),
(7, 3, 3, 0, 0, 0, 0),
(8, 1, 4, 1, 1, 1, 1),
(9, 1, 5, 1, 1, 1, 1),
(10, 3, 5, 1, 0, 0, 0),
(11, 1, 6, 1, 0, 0, 1),
(12, 1, 7, 1, 1, 1, 1),
(13, 1, 8, 1, 1, 1, 1),
(14, 1, 9, 1, 1, 1, 1),
(15, 1, 10, 1, 1, 1, 1),
(16, 1, 11, 1, 1, 1, 1),
(17, 1, 12, 1, 1, 1, 1),
(18, 1, 13, 1, 1, 1, 1),
(19, 1, 14, 1, 1, 1, 1),
(20, 1, 16, 1, 1, 1, 0),
(21, 3, 16, 1, 1, 1, 0),
(22, 1, 17, 1, 1, 1, 0),
(23, 3, 17, 1, 1, 1, 0),
(24, 1, 18, 1, 1, 1, 0),
(25, 2, 18, 1, 0, 0, 0),
(26, 3, 18, 1, 0, 0, 0),
(27, 1, 19, 1, 1, 1, 1),
(28, 1, 20, 1, 1, 1, 1),
(29, 1, 21, 1, 1, 1, 1),
(30, 1, 22, 1, 1, 1, 1),
(31, 1, 23, 1, 1, 1, 1),
(32, 1, 24, 1, 1, 1, 1),
(33, 1, 25, 1, 1, 1, 1),
(34, 1, 26, 1, 1, 1, 1),
(35, 1, 27, 1, 1, 1, 1),
(36, 1, 28, 1, 1, 1, 1),
(37, 1, 29, 1, 1, 1, 1),
(38, 1, 30, 1, 1, 1, 1),
(39, 1, 31, 1, 1, 1, 1),
(40, 1, 32, 1, 1, 1, 1),
(41, 1, 33, 1, 1, 1, 1),
(42, 1, 34, 1, 1, 1, 1),
(43, 1, 35, 1, 1, 1, 1),
(44, 1, 36, 1, 1, 1, 1),
(45, 1, 37, 1, 1, 1, 1),
(46, 4, 37, 1, 0, 0, 0),
(47, 1, 38, 1, 1, 1, 1),
(48, 1, 39, 1, 1, 1, 1),
(49, 1, 40, 1, 1, 1, 1),
(50, 1, 41, 1, 1, 1, 1),
(51, 1, 42, 1, 1, 1, 1),
(52, 1, 43, 1, 1, 1, 1),
(53, 1, 44, 1, 1, 1, 1),
(54, 1, 45, 1, 1, 1, 1),
(55, 2, 46, 1, 1, 1, 1),
(56, 1, 47, 1, 1, 1, 1),
(57, 2, 48, 1, 1, 1, 0),
(58, 2, 49, 1, 1, 1, 1),
(59, 1, 50, 1, 1, 1, 1),
(60, 2, 51, 1, 1, 1, 1),
(61, 1, 52, 1, 1, 1, 1),
(62, 3, 52, 1, 1, 1, 1),
(63, 2, 53, 1, 1, 0, 0),
(64, 2, 54, 1, 1, 1, 1),
(65, 1, 55, 1, 1, 0, 1),
(66, 3, 55, 1, 1, 0, 0),
(67, 1, 56, 1, 1, 1, 1),
(68, 1, 57, 1, 1, 1, 1),
(69, 4, 57, 1, 1, 1, 1),
(70, 1, 58, 1, 1, 1, 1),
(71, 3, 58, 1, 1, 1, 1),
(72, 1, 59, 1, 1, 1, 1),
(73, 3, 59, 1, 1, 1, 1),
(74, 1, 60, 1, 1, 1, 1),
(75, 6, 60, 1, 1, 1, 1),
(76, 5, 60, 1, 0, 1, 0),
(77, 1, 61, 1, 1, 1, 1),
(78, 1, 67, 1, 1, 1, 1),
(79, 2, 67, 1, 0, 1, 0),
(80, 3, 67, 1, 0, 1, 0),
(81, 1, 68, 1, 1, 1, 1),
(82, 2, 68, 1, 0, 1, 0),
(83, 3, 68, 1, 0, 1, 0),
(84, 1, 69, 1, 1, 1, 1),
(85, 3, 69, 1, 0, 0, 0),
(86, 2, 70, 1, 0, 0, 0),
(87, 1, 71, 1, 0, 0, 1),
(88, 1, 72, 1, 1, 1, 0),
(89, 1, 73, 1, 0, 1, 0),
(90, 1, 74, 1, 1, 1, 1),
(91, 1, 75, 1, 1, 1, 1),
(92, 3, 75, 1, 1, 1, 1),
(93, 1, 76, 1, 1, 1, 1),
(94, 5, 76, 1, 1, 1, 1),
(95, 6, 76, 1, 1, 1, 1),
(96, 1, 78, 1, 0, 0, 0),
(97, 3, 78, 1, 0, 0, 0),
(98, 2, 79, 1, 0, 0, 0),
(99, 1, 80, 1, 1, 1, 1),
(100, 1, 81, 1, 1, 1, 1),
(101, 1, 82, 1, 1, 1, 1),
(102, 1, 83, 1, 1, 1, 1),
(103, 1, 84, 1, 1, 1, 1),
(104, 1, 85, 1, 1, 1, 1),
(105, 1, 86, 1, 1, 1, 1),
(106, 1, 87, 1, 1, 1, 1),
(107, 1, 88, 1, 1, 1, 1),
(108, 4, 88, 1, 1, 1, 1),
(109, 1, 89, 1, 1, 1, 1),
(110, 1, 90, 1, 1, 1, 1),
(111, 4, 90, 1, 1, 1, 1),
(112, 1, 91, 1, 1, 1, 1),
(113, 4, 91, 1, 1, 1, 1),
(114, 1, 92, 1, 1, 1, 1),
(115, 1, 93, 1, 1, 1, 1),
(116, 1, 94, 1, 1, 1, 1),
(117, 1, 95, 1, 1, 1, 1),
(118, 1, 96, 1, 1, 1, 1),
(119, 5, 96, 1, 1, 1, 1),
(120, 6, 96, 1, 1, 1, 1),
(121, 1, 97, 1, 1, 1, 1),
(122, 6, 97, 1, 1, 1, 1),
(123, 1, 98, 1, 1, 1, 1),
(124, 2, 98, 1, 1, 1, 1),
(125, 3, 98, 1, 1, 1, 1),
(126, 1, 99, 1, 0, 1, 0),
(127, 2, 99, 1, 0, 1, 0),
(128, 3, 99, 1, 0, 1, 0),
(129, 1, 100, 1, 0, 0, 0),
(130, 1, 101, 1, 1, 1, 1),
(131, 3, 101, 1, 1, 1, 1),
(132, 1, 102, 1, 1, 1, 1),
(133, 4, 102, 1, 1, 1, 1),
(134, 1, 103, 1, 0, 0, 0),
(135, 2, 103, 1, 0, 0, 0),
(136, 1, 104, 1, 1, 1, 0),
(137, 1, 105, 1, 1, 1, 1),
(138, 1, 107, 1, 1, 1, 0),
(139, 1, 109, 1, 1, 1, 0),
(140, 1, 111, 1, 1, 1, 1),
(141, 2, 110, 1, 0, 1, 0),
(142, 2, 108, 1, 1, 1, 0),
(143, 2, 106, 1, 0, 1, 0),
(144, 3, 109, 1, 1, 1, 0),
(145, 2, 109, 1, 1, 1, 0),
(146, 1, 112, 1, 1, 1, 1),
(147, 2, 112, 0, 0, 0, 0),
(148, 1, 113, 1, 1, 1, 1),
(149, 2, 113, 1, 1, 1, 0),
(150, 1, 114, 0, 0, 0, 0),
(151, 2, 114, 1, 0, 1, 0),
(152, 1, 115, 1, 1, 1, 0),
(153, 2, 115, 1, 0, 0, 0),
(154, 1, 116, 1, 1, 1, 1),
(155, 1, 117, 1, 1, 1, 1),
(156, 1, 118, 1, 1, 1, 1),
(157, 1, 119, 1, 1, 1, 1),
(158, 1, 120, 1, 1, 1, 1),
(160, 1, 122, 1, 1, 1, 1),
(161, 1, 123, 1, 1, 1, 1),
(162, 1, 124, 1, 1, 1, 1),
(163, 1, 125, 1, 1, 1, 1),
(164, 1, 126, 1, 1, 1, 1),
(167, 1, 129, 1, 1, 1, 1),
(168, 1, 130, 1, 1, 1, 1),
(169, 1, 131, 1, 1, 1, 1),
(170, 1, 132, 1, 1, 1, 1),
(171, 1, 132, 1, 1, 1, 1),
(172, 1, 134, 1, 1, 1, 1),
(173, 1, 134, 1, 1, 1, 1),
(175, 1, 136, 1, 1, 1, 1),
(176, 1, 137, 1, 1, 1, 1),
(177, 1, 138, 1, 1, 1, 1),
(178, 1, 139, 1, 1, 1, 1),
(179, 1, 140, 1, 1, 1, 1),
(180, 1, 141, 1, 1, 1, 1),
(181, 2, 116, 1, 0, 0, 0),
(182, 2, 120, 1, 0, 0, 0),
(183, 2, 123, 1, 0, 0, 0),
(184, 2, 131, 1, 0, 0, 0),
(185, 2, 132, 1, 0, 0, 0),
(186, 2, 126, 1, 0, 0, 0),
(187, 2, 118, 1, 0, 0, 0),
(188, 1, 142, 1, 1, 1, 1),
(189, 1, 143, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_user_selection_rule`
--

CREATE TABLE IF NOT EXISTS `ohrm_user_selection_rule` (
`id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `implementation_class` varchar(255) NOT NULL,
  `rule_xml_data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_workflow_state_machine`
--

CREATE TABLE IF NOT EXISTS `ohrm_workflow_state_machine` (
`id` bigint(20) NOT NULL,
  `workflow` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `resulting_state` varchar(255) NOT NULL,
  `roles_to_notify` text,
  `priority` int(11) NOT NULL DEFAULT '0' COMMENT 'lowest priority 0'
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ohrm_workflow_state_machine`
--

INSERT INTO `ohrm_workflow_state_machine` (`id`, `workflow`, `state`, `role`, `action`, `resulting_state`, `roles_to_notify`, `priority`) VALUES
(1, '0', 'INITIAL', 'SYSTEM', '7', 'NOT SUBMITTED', '', 0),
(2, '0', 'SUBMITTED', 'ADMIN', '2', 'APPROVED', '', 0),
(3, '0', 'SUBMITTED', 'ADMIN', '3', 'REJECTED', '', 0),
(4, '0', 'SUBMITTED', 'ADMIN', '0', 'SUBMITTED', '', 0),
(5, '0', 'SUBMITTED', 'ADMIN', '5', 'SUBMITTED', '', 0),
(6, '0', 'SUBMITTED', 'SUPERVISOR', '2', 'APPROVED', '', 0),
(7, '0', 'SUBMITTED', 'SUPERVISOR', '3', 'REJECTED', '', 0),
(8, '0', 'SUBMITTED', 'SUPERVISOR', '5', 'SUBMITTED', '', 0),
(9, '0', 'SUBMITTED', 'SUPERVISOR', '0', 'SUBMITTED', '', 0),
(10, '0', 'SUBMITTED', 'ESS USER', '0', 'SUBMITTED', '', 0),
(11, '0', 'SUBMITTED', 'ESS USER', '5', 'SUBMITTED', '', 0),
(12, '0', 'NOT SUBMITTED', 'ESS USER', '1', 'SUBMITTED', '', 0),
(13, '0', 'NOT SUBMITTED', 'ESS USER', '5', 'NOT SUBMITTED', '', 0),
(15, '0', 'NOT SUBMITTED', 'ESS USER', '0', 'NOT SUBMITTED', '', 0),
(16, '0', 'NOT SUBMITTED', 'SUPERVISOR', '0', 'NOT SUBMITTED', '', 0),
(17, '0', 'NOT SUBMITTED', 'SUPERVISOR', '5', 'NOT SUBMITTED', '', 0),
(18, '0', 'NOT SUBMITTED', 'SUPERVISOR', '1', 'SUBMITTED', '', 0),
(19, '0', 'NOT SUBMITTED', 'ADMIN', '0', 'NOT SUBMITTED', '', 0),
(20, '0', 'NOT SUBMITTED', 'ADMIN', '5', 'NOT SUBMITTED', '', 0),
(21, '0', 'NOT SUBMITTED', 'ADMIN', '1', 'SUBMITTED', '', 0),
(22, '0', 'REJECTED', 'ESS USER', '1', 'SUBMITTED', '', 0),
(23, '0', 'REJECTED', 'ESS USER', '0', 'REJECTED', '', 0),
(24, '0', 'REJECTED', 'ESS USER', '5', 'REJECTED', '', 0),
(25, '0', 'REJECTED', 'SUPERVISOR', '1', 'SUBMITTED', '', 0),
(26, '0', 'REJECTED', 'SUPERVISOR', '0', 'REJECTED', '', 0),
(27, '0', 'REJECTED', 'SUPERVISOR', '5', 'REJECTED', '', 0),
(28, '0', 'REJECTED', 'ADMIN', '0', 'REJECTED', '', 0),
(29, '0', 'REJECTED', 'ADMIN', '5', 'SUBMITTED', '', 0),
(30, '0', 'REJECTED', 'ADMIN', '1', 'SUBMITTED', '', 0),
(31, '0', 'APPROVED', 'ESS USER', '0', 'APPROVED', '', 0),
(32, '0', 'APPROVED', 'SUPERVISOR', '0', 'APPROVED', '', 0),
(33, '0', 'APPROVED', 'ADMIN', '0', 'APPROVED', '', 0),
(34, '0', 'APPROVED', 'ADMIN', '4', 'SUBMITTED', '', 0),
(35, '1', 'PUNCHED IN', 'ESS USER', '1', 'PUNCHED OUT', '', 0),
(36, '1', 'INITIAL', 'ESS USER', '0', 'PUNCHED IN', '', 0),
(37, '2', 'INITIAL', 'ADMIN', '1', 'APPLICATION INITIATED', '', 0),
(38, '2', 'APPLICATION INITIATED', 'ADMIN', '2', 'SHORTLISTED', '', 0),
(39, '2', 'APPLICATION INITIATED', 'ADMIN', '3', 'REJECTED', '', 0),
(40, '2', 'SHORTLISTED', 'ADMIN', '4', 'INTERVIEW SCHEDULED', '', 0),
(41, '2', 'SHORTLISTED', 'ADMIN', '3', 'REJECTED', '', 0),
(42, '2', 'INTERVIEW SCHEDULED', 'ADMIN', '3', 'REJECTED', '', 0),
(43, '2', 'INTERVIEW SCHEDULED', 'ADMIN', '5', 'INTERVIEW PASSED', '', 0),
(44, '2', 'INTERVIEW SCHEDULED', 'ADMIN', '6', 'INTERVIEW FAILED', '', 0),
(45, '2', 'INTERVIEW PASSED', 'ADMIN', '4', 'INTERVIEW SCHEDULED', '', 0),
(46, '2', 'INTERVIEW PASSED', 'ADMIN', '7', 'JOB OFFERED', '', 0),
(47, '2', 'INTERVIEW PASSED', 'ADMIN', '3', 'REJECTED', '', 0),
(48, '2', 'INTERVIEW FAILED', 'ADMIN', '3', 'REJECTED', '', 0),
(49, '2', 'JOB OFFERED', 'ADMIN', '8', 'OFFER DECLINED', '', 0),
(50, '2', 'JOB OFFERED', 'ADMIN', '3', 'REJECTED', '', 0),
(51, '2', 'JOB OFFERED', 'ADMIN', '9', 'HIRED', '', 0),
(52, '2', 'OFFER DECLINED', 'ADMIN', '3', 'REJECTED', '', 0),
(53, '2', 'INITIAL', 'HIRING MANAGER', '1', 'APPLICATION INITIATED', '', 0),
(54, '2', 'APPLICATION INITIATED', 'HIRING MANAGER', '2', 'SHORTLISTED', '', 0),
(55, '2', 'APPLICATION INITIATED', 'HIRING MANAGER', '3', 'REJECTED', '', 0),
(56, '2', 'SHORTLISTED', 'HIRING MANAGER', '4', 'INTERVIEW SCHEDULED', '', 0),
(57, '2', 'SHORTLISTED', 'HIRING MANAGER', '3', 'REJECTED', '', 0),
(58, '2', 'INTERVIEW SCHEDULED', 'HIRING MANAGER', '3', 'REJECTED', '', 0),
(59, '2', 'INTERVIEW SCHEDULED', 'HIRING MANAGER', '5', 'INTERVIEW PASSED', '', 0),
(60, '2', 'INTERVIEW SCHEDULED', 'HIRING MANAGER', '6', 'INTERVIEW FAILED', '', 0),
(61, '2', 'INTERVIEW PASSED', 'HIRING MANAGER', '4', 'INTERVIEW SCHEDULED', '', 0),
(62, '2', 'INTERVIEW PASSED', 'HIRING MANAGER', '7', 'JOB OFFERED', '', 0),
(63, '2', 'INTERVIEW PASSED', 'HIRING MANAGER', '3', 'REJECTED', '', 0),
(64, '2', 'INTERVIEW FAILED', 'HIRING MANAGER', '3', 'REJECTED', '', 0),
(65, '2', 'JOB OFFERED', 'HIRING MANAGER', '8', 'OFFER DECLINED', '', 0),
(66, '2', 'JOB OFFERED', 'HIRING MANAGER', '3', 'REJECTED', '', 0),
(67, '2', 'JOB OFFERED', 'HIRING MANAGER', '9', 'HIRED', '', 0),
(68, '2', 'OFFER DECLINED', 'HIRING MANAGER', '3', 'REJECTED', '', 0),
(69, '2', 'INTERVIEW SCHEDULED', 'INTERVIEWER', '5', 'INTERVIEW PASSED', '', 0),
(70, '2', 'INTERVIEW SCHEDULED', 'INTERVIEWER', '6', 'INTERVIEW FAILED', '', 0),
(71, '1', 'INITIAL', 'ADMIN', '5', 'PUNCHED IN', '', 0),
(72, '1', 'PUNCHED IN', 'ADMIN', '6', 'PUNCHED OUT', '', 0),
(73, '1', 'PUNCHED IN', 'ADMIN', '2', 'PUNCHED IN', '', 0),
(74, '1', 'PUNCHED IN', 'ADMIN', '7', 'N/A', '', 0),
(75, '1', 'PUNCHED OUT', 'ADMIN', '2', 'PUNCHED OUT', '', 0),
(76, '1', 'PUNCHED OUT', 'ADMIN', '3', 'PUNCHED OUT', '', 0),
(77, '1', 'PUNCHED OUT', 'ADMIN', '7', 'N/A', '', 0),
(78, '0', 'INITIAL', 'ADMIN', '7', 'NOT SUBMITTED', '', 0),
(79, '0', 'INITIAL', 'ESS USER', '7', 'NOT SUBMITTED', '', 0),
(80, '0', 'INITIAL', 'SUPERVISOR', '7', 'NOT SUBMITTED', '', 0),
(81, '3', 'NOT_EXIST', 'ADMIN', '1', 'ACTIVE', '', 0),
(82, '3', 'ACTIVE', 'ADMIN', '2', 'NOT_EXIST', '', 0),
(83, '3', 'ACTIVE', 'ADMIN', '3', 'TERMINATED', '', 0),
(84, '3', 'TERMINATED', 'ADMIN', '4', 'ACTIVE', '', 0),
(85, '3', 'TERMINATED', 'ADMIN', '5', 'NOT_EXIST', '', 0),
(86, '4', 'INITIAL', 'ESS', 'APPLY', 'PENDING APPROVAL', 'supervisor,subscriber', 0),
(87, '4', 'INITIAL', 'ADMIN', 'ASSIGN', 'SCHEDULED', 'ess,supervisor,subscriber', 0),
(88, '4', 'INITIAL', 'SUPERVISOR', 'ASSIGN', 'SCHEDULED', 'ess,supervisor,subscriber', 0),
(89, '4', 'PENDING APPROVAL', 'ADMIN', 'APPROVE', 'SCHEDULED', 'ess,subscriber', 0),
(90, '4', 'PENDING APPROVAL', 'SUPERVISOR', 'APPROVE', 'SCHEDULED', 'ess,subscriber', 0),
(91, '4', 'PENDING APPROVAL', 'ESS', 'CANCEL', 'CANCELLED', 'supervisor,subscriber', 0),
(92, '4', 'PENDING APPROVAL', 'ADMIN', 'CANCEL', 'CANCELLED', 'ess,subscriber', 0),
(93, '4', 'PENDING APPROVAL', 'SUPERVISOR', 'CANCEL', 'CANCELLED', 'ess,subscriber', 0),
(94, '4', 'PENDING APPROVAL', 'ADMIN', 'REJECT', 'REJECTED', 'ess,subscriber', 0),
(95, '4', 'PENDING APPROVAL', 'SUPERVISOR', 'REJECT', 'REJECTED', 'ess,subscriber', 0),
(96, '4', 'SCHEDULED', 'ESS', 'CANCEL', 'CANCELLED', 'supervisor,subscriber', 0),
(97, '4', 'SCHEDULED', 'ADMIN', 'CANCEL', 'CANCELLED', 'ess,subscriber', 0),
(98, '4', 'SCHEDULED', 'SUPERVISOR', 'CANCEL', 'CANCELLED', 'ess,subscriber', 0),
(99, '4', 'TAKEN', 'ADMIN', 'CANCEL', 'CANCELLED', 'ess,subscriber', 0),
(100, '4', 'LEAVE TYPE DELETED PENDING APPROVAL', 'ESS', 'CANCEL', 'CANCELLED', 'supervisor,subscriber', 0),
(101, '4', 'LEAVE TYPE DELETED PENDING APPROVAL', 'ADMIN', 'CANCEL', 'CANCELLED', 'ess,subscriber', 0),
(102, '4', 'LEAVE TYPE DELETED PENDING APPROVAL', 'SUPERVISOR', 'CANCEL', 'CANCELLED', 'ess,subscriber', 0),
(103, '4', 'LEAVE TYPE DELETED SCHEDULED', 'ESS', 'CANCEL', 'CANCELLED', 'supervisor,subscriber', 0),
(104, '4', 'LEAVE TYPE DELETED SCHEDULED', 'ADMIN', 'CANCEL', 'CANCELLED', 'ess,subscriber', 0),
(105, '4', 'LEAVE TYPE DELETED SCHEDULED', 'SUPERVISOR', 'CANCEL', 'CANCELLED', 'ess,subscriber', 0),
(106, '4', 'LEAVE TYPE DELETED TAKEN', 'ADMIN', 'CANCEL', 'CANCELLED', 'ess,subscriber', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_work_shift`
--

CREATE TABLE IF NOT EXISTS `ohrm_work_shift` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `hours_per_day` decimal(4,2) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ohrm_work_week`
--

CREATE TABLE IF NOT EXISTS `ohrm_work_week` (
`id` int(10) unsigned NOT NULL,
  `operational_country_id` int(10) unsigned DEFAULT NULL,
  `mon` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tue` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `wed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `thu` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `fri` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sat` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sun` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ohrm_work_week`
--

INSERT INTO `ohrm_work_week` (`id`, `operational_country_id`, `mon`, `tue`, `wed`, `thu`, `fri`, `sat`, `sun`) VALUES
(1, NULL, 0, 0, 0, 0, 0, 8, 8);

-- --------------------------------------------------------

--
-- Table structure for table `opras_form`
--

CREATE TABLE IF NOT EXISTS `opras_form` (
`form_id` int(11) NOT NULL,
  `description` text,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `appraisee` int(7) NOT NULL,
  `supervisor` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE IF NOT EXISTS `payroll` (
`payroll_id` int(11) NOT NULL,
  `pay_date` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll`
--

INSERT INTO `payroll` (`payroll_id`, `pay_date`) VALUES
(41, '2014-12-24'),
(58, '2015-01-05'),
(59, '2015-02-06'),
(60, '2015-03-06'),
(61, '2015-04-01'),
(62, '2015-05-26'),
(63, '2015-06-15');

-- --------------------------------------------------------

--
-- Table structure for table `payslip`
--

CREATE TABLE IF NOT EXISTS `payslip` (
`payslip_id` int(11) NOT NULL,
  `is_committed` tinyint(4) DEFAULT '0',
  `date_commited` date DEFAULT NULL,
  `emp_number` int(7) NOT NULL,
  `payroll_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payslip`
--

INSERT INTO `payslip` (`payslip_id`, `is_committed`, `date_commited`, `emp_number`, `payroll_id`) VALUES
(18, 1, '2014-12-24', 1, 41),
(19, 1, '2014-12-24', 2, 41),
(20, 1, '2014-12-24', 4, 41),
(21, 1, '2015-02-06', 1, 59),
(22, 1, '2015-02-06', 2, 59),
(23, 1, '2015-02-06', 4, 59),
(24, 1, '2015-03-06', 1, 60),
(25, 1, '2015-03-06', 2, 60),
(26, 1, '2015-03-06', 4, 60);

-- --------------------------------------------------------

--
-- Table structure for table `pa_line`
--

CREATE TABLE IF NOT EXISTS `pa_line` (
`line_id` int(11) NOT NULL,
  `a_objectives` text,
  `ap_targets` text,
  `ap_criteria` text,
  `ap_resources` text,
  `pa_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pension`
--

CREATE TABLE IF NOT EXISTS `pension` (
`pension_id` int(11) NOT NULL,
  `pension_name` varchar(45) DEFAULT NULL,
  `percent_employer` float DEFAULT NULL COMMENT '				',
  `percent_employee` float DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pension`
--

INSERT INTO `pension` (`pension_id`, `pension_name`, `percent_employer`, `percent_employee`) VALUES
(1, 'PPF', 10, 10),
(2, 'NSSF', 10, 10),
(8, 'PSPF', 12, 8);

-- --------------------------------------------------------

--
-- Table structure for table `pension_deduction`
--

CREATE TABLE IF NOT EXISTS `pension_deduction` (
`deduction_id` int(11) NOT NULL,
  `amount` float DEFAULT NULL,
  `emp_number` int(7) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `pension_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pension_deduction`
--

INSERT INTO `pension_deduction` (`deduction_id`, `amount`, `emp_number`, `payroll_id`, `pension_id`) VALUES
(18, NULL, 1, 41, 1),
(19, NULL, 2, 41, 2),
(20, NULL, 4, 41, 2),
(21, NULL, 1, 59, 2),
(22, NULL, 2, 59, 1),
(23, NULL, 4, 59, 2),
(24, NULL, 1, 60, 2),
(25, NULL, 2, 60, 1),
(26, NULL, 4, 60, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pension_employee`
--

CREATE TABLE IF NOT EXISTS `pension_employee` (
  `pension_id` int(11) NOT NULL,
  `emp_number` int(7) NOT NULL,
  `date_created` date DEFAULT NULL,
  `date_joined` varchar(45) DEFAULT NULL,
`pen_emp_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pension_employee`
--

INSERT INTO `pension_employee` (`pension_id`, `emp_number`, `date_created`, `date_joined`, `pen_emp_id`) VALUES
(1, 2, NULL, '2014-05-13', 1),
(2, 1, NULL, '2014-10-22', 1),
(2, 4, NULL, '2015-01-22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `performance_agreement`
--

CREATE TABLE IF NOT EXISTS `performance_agreement` (
`pa_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` text,
  `date_created` date DEFAULT NULL,
  `form_id` int(11) NOT NULL,
  `appraisee` int(7) NOT NULL,
  `supervisor` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tax_deduction`
--

CREATE TABLE IF NOT EXISTS `tax_deduction` (
`deduction_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `emp_number` int(7) NOT NULL,
  `payroll_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax_deduction`
--

INSERT INTO `tax_deduction` (`deduction_id`, `tax_rate_id`, `emp_number`, `payroll_id`) VALUES
(18, 5, 1, 41),
(19, 4, 2, 41),
(20, 5, 4, 41),
(21, 5, 1, 59),
(22, 4, 2, 59),
(23, 5, 4, 59),
(24, 5, 1, 60),
(25, 4, 2, 60),
(26, 5, 4, 60);

-- --------------------------------------------------------

--
-- Table structure for table `tax_rate`
--

CREATE TABLE IF NOT EXISTS `tax_rate` (
`rate_id` int(11) NOT NULL,
  `salary_from` float DEFAULT NULL,
  `salary_to` float DEFAULT NULL,
  `rate_amount` float DEFAULT '0',
  `as_of` varchar(20) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `rate_percent` float DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax_rate`
--

INSERT INTO `tax_rate` (`rate_id`, `salary_from`, `salary_to`, `rate_amount`, `as_of`, `active`, `rate_percent`) VALUES
(1, 0, 170000, 0, '2014/2015', 1, 0),
(2, 170000, 360000, 0, '2014/2015', 1, 13),
(3, 360000, 540000, 24700, '2014/2015', 1, 20),
(4, 540000, 720000, 60700, '2014/2015', 1, 25),
(5, 720000, 0, 105700, '2014/2015', 1, 30);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allowance`
--
ALTER TABLE `allowance`
 ADD PRIMARY KEY (`allowance_id`);

--
-- Indexes for table `deduction`
--
ALTER TABLE `deduction`
 ADD PRIMARY KEY (`deduction_id`,`emp_number`), ADD KEY `fk_deductions_hs_hr_employee1_idx` (`emp_number`);

--
-- Indexes for table `deduction_line`
--
ALTER TABLE `deduction_line`
 ADD PRIMARY KEY (`line_id`,`deduction_id`,`emp_number`,`payroll_id`), ADD KEY `fk_deduction_lines_deductions1_idx` (`deduction_id`,`emp_number`), ADD KEY `fk_deduction_lines_payroll1_idx` (`payroll_id`);

--
-- Indexes for table `employee_loan`
--
ALTER TABLE `employee_loan`
 ADD PRIMARY KEY (`employee_loan_id`,`emp_number`,`loan_id`), ADD KEY `fk_employee_loan_hs_hr_employee1_idx` (`emp_number`), ADD KEY `fk_employee_loan_loan1_idx` (`loan_id`);

--
-- Indexes for table `financial_organization`
--
ALTER TABLE `financial_organization`
 ADD PRIMARY KEY (`organization_id`);

--
-- Indexes for table `hs_hr_config`
--
ALTER TABLE `hs_hr_config`
 ADD PRIMARY KEY (`key`);

--
-- Indexes for table `hs_hr_country`
--
ALTER TABLE `hs_hr_country`
 ADD PRIMARY KEY (`cou_code`);

--
-- Indexes for table `hs_hr_currency_type`
--
ALTER TABLE `hs_hr_currency_type`
 ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `hs_hr_custom_export`
--
ALTER TABLE `hs_hr_custom_export`
 ADD PRIMARY KEY (`export_id`), ADD KEY `emp_number` (`export_id`);

--
-- Indexes for table `hs_hr_custom_fields`
--
ALTER TABLE `hs_hr_custom_fields`
 ADD PRIMARY KEY (`field_num`), ADD KEY `emp_number` (`field_num`), ADD KEY `screen` (`screen`);

--
-- Indexes for table `hs_hr_custom_import`
--
ALTER TABLE `hs_hr_custom_import`
 ADD PRIMARY KEY (`import_id`), ADD KEY `emp_number` (`import_id`);

--
-- Indexes for table `hs_hr_district`
--
ALTER TABLE `hs_hr_district`
 ADD PRIMARY KEY (`district_code`);

--
-- Indexes for table `hs_hr_employee`
--
ALTER TABLE `hs_hr_employee`
 ADD PRIMARY KEY (`emp_number`), ADD KEY `work_station` (`work_station`), ADD KEY `nation_code` (`nation_code`), ADD KEY `job_title_code` (`job_title_code`), ADD KEY `emp_status` (`emp_status`), ADD KEY `eeo_cat_code` (`eeo_cat_code`), ADD KEY `termination_id` (`termination_id`);

--
-- Indexes for table `hs_hr_emp_attachment`
--
ALTER TABLE `hs_hr_emp_attachment`
 ADD PRIMARY KEY (`emp_number`,`eattach_id`), ADD KEY `screen` (`screen`);

--
-- Indexes for table `hs_hr_emp_basicsalary`
--
ALTER TABLE `hs_hr_emp_basicsalary`
 ADD PRIMARY KEY (`id`), ADD KEY `sal_grd_code` (`sal_grd_code`), ADD KEY `currency_id` (`currency_id`), ADD KEY `emp_number` (`emp_number`), ADD KEY `payperiod_code` (`payperiod_code`);

--
-- Indexes for table `hs_hr_emp_children`
--
ALTER TABLE `hs_hr_emp_children`
 ADD PRIMARY KEY (`emp_number`,`ec_seqno`);

--
-- Indexes for table `hs_hr_emp_contract_extend`
--
ALTER TABLE `hs_hr_emp_contract_extend`
 ADD PRIMARY KEY (`emp_number`,`econ_extend_id`);

--
-- Indexes for table `hs_hr_emp_dependents`
--
ALTER TABLE `hs_hr_emp_dependents`
 ADD PRIMARY KEY (`emp_number`,`ed_seqno`);

--
-- Indexes for table `hs_hr_emp_directdebit`
--
ALTER TABLE `hs_hr_emp_directdebit`
 ADD PRIMARY KEY (`id`), ADD KEY `salary_id` (`salary_id`);

--
-- Indexes for table `hs_hr_emp_emergency_contacts`
--
ALTER TABLE `hs_hr_emp_emergency_contacts`
 ADD PRIMARY KEY (`emp_number`,`eec_seqno`);

--
-- Indexes for table `hs_hr_emp_history_of_ealier_pos`
--
ALTER TABLE `hs_hr_emp_history_of_ealier_pos`
 ADD PRIMARY KEY (`emp_number`,`emp_seqno`);

--
-- Indexes for table `hs_hr_emp_language`
--
ALTER TABLE `hs_hr_emp_language`
 ADD PRIMARY KEY (`emp_number`,`lang_id`,`fluency`), ADD KEY `lang_id` (`lang_id`);

--
-- Indexes for table `hs_hr_emp_locations`
--
ALTER TABLE `hs_hr_emp_locations`
 ADD PRIMARY KEY (`emp_number`,`location_id`), ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `hs_hr_emp_member_detail`
--
ALTER TABLE `hs_hr_emp_member_detail`
 ADD PRIMARY KEY (`emp_number`,`membship_code`), ADD KEY `membship_code` (`membship_code`);

--
-- Indexes for table `hs_hr_emp_passport`
--
ALTER TABLE `hs_hr_emp_passport`
 ADD PRIMARY KEY (`emp_number`,`ep_seqno`);

--
-- Indexes for table `hs_hr_emp_picture`
--
ALTER TABLE `hs_hr_emp_picture`
 ADD PRIMARY KEY (`emp_number`);

--
-- Indexes for table `hs_hr_emp_reportto`
--
ALTER TABLE `hs_hr_emp_reportto`
 ADD PRIMARY KEY (`erep_sup_emp_number`,`erep_sub_emp_number`,`erep_reporting_mode`), ADD KEY `erep_sub_emp_number` (`erep_sub_emp_number`), ADD KEY `erep_reporting_mode` (`erep_reporting_mode`);

--
-- Indexes for table `hs_hr_emp_skill`
--
ALTER TABLE `hs_hr_emp_skill`
 ADD KEY `emp_number` (`emp_number`), ADD KEY `skill_id` (`skill_id`);

--
-- Indexes for table `hs_hr_emp_us_tax`
--
ALTER TABLE `hs_hr_emp_us_tax`
 ADD PRIMARY KEY (`emp_number`);

--
-- Indexes for table `hs_hr_emp_work_experience`
--
ALTER TABLE `hs_hr_emp_work_experience`
 ADD PRIMARY KEY (`emp_number`,`eexp_seqno`);

--
-- Indexes for table `hs_hr_jobtit_empstat`
--
ALTER TABLE `hs_hr_jobtit_empstat`
 ADD PRIMARY KEY (`jobtit_code`,`estat_code`), ADD KEY `estat_code` (`estat_code`);

--
-- Indexes for table `hs_hr_mailnotifications`
--
ALTER TABLE `hs_hr_mailnotifications`
 ADD KEY `user_id` (`user_id`), ADD KEY `notification_type_id` (`notification_type_id`);

--
-- Indexes for table `hs_hr_module`
--
ALTER TABLE `hs_hr_module`
 ADD PRIMARY KEY (`mod_id`);

--
-- Indexes for table `hs_hr_payperiod`
--
ALTER TABLE `hs_hr_payperiod`
 ADD PRIMARY KEY (`payperiod_code`);

--
-- Indexes for table `hs_hr_pay_period`
--
ALTER TABLE `hs_hr_pay_period`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hs_hr_province`
--
ALTER TABLE `hs_hr_province`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hs_hr_unique_id`
--
ALTER TABLE `hs_hr_unique_id`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `table_field` (`table_name`,`field_name`);

--
-- Indexes for table `loan`
--
ALTER TABLE `loan`
 ADD PRIMARY KEY (`id`,`organization_id`), ADD KEY `fk_loan_financial_organization1_idx` (`organization_id`);

--
-- Indexes for table `loan_deduction`
--
ALTER TABLE `loan_deduction`
 ADD PRIMARY KEY (`loan_deduction_id`,`payroll_id`,`emp_number`,`loan_id`), ADD KEY `fk_loan_deduction_payroll1_idx` (`payroll_id`), ADD KEY `fk_loan_deduction_hs_hr_employee1_idx` (`emp_number`), ADD KEY `fk_loan_deduction_loan1_idx` (`loan_id`);

--
-- Indexes for table `ohrm_advanced_report`
--
ALTER TABLE `ohrm_advanced_report`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_attendance_record`
--
ALTER TABLE `ohrm_attendance_record`
 ADD PRIMARY KEY (`id`), ADD KEY `emp_id_state` (`employee_id`,`state`), ADD KEY `emp_id_time` (`employee_id`,`punch_in_utc_time`,`punch_out_utc_time`);

--
-- Indexes for table `ohrm_available_group_field`
--
ALTER TABLE `ohrm_available_group_field`
 ADD PRIMARY KEY (`report_group_id`,`group_field_id`), ADD KEY `report_group_id` (`report_group_id`), ADD KEY `group_field_id` (`group_field_id`);

--
-- Indexes for table `ohrm_beacon_notification`
--
ALTER TABLE `ohrm_beacon_notification`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_composite_display_field`
--
ALTER TABLE `ohrm_composite_display_field`
 ADD PRIMARY KEY (`composite_display_field_id`), ADD KEY `report_group_id` (`report_group_id`), ADD KEY `display_field_group_id` (`display_field_group_id`);

--
-- Indexes for table `ohrm_customer`
--
ALTER TABLE `ohrm_customer`
 ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `ohrm_datapoint`
--
ALTER TABLE `ohrm_datapoint`
 ADD PRIMARY KEY (`id`), ADD KEY `datapoint_type_id` (`datapoint_type_id`);

--
-- Indexes for table `ohrm_datapoint_type`
--
ALTER TABLE `ohrm_datapoint_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_data_group`
--
ALTER TABLE `ohrm_data_group`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `ohrm_data_group_screen`
--
ALTER TABLE `ohrm_data_group_screen`
 ADD PRIMARY KEY (`id`), ADD KEY `data_group_id` (`data_group_id`), ADD KEY `screen_id` (`screen_id`);

--
-- Indexes for table `ohrm_display_field`
--
ALTER TABLE `ohrm_display_field`
 ADD PRIMARY KEY (`display_field_id`), ADD KEY `report_group_id` (`report_group_id`), ADD KEY `display_field_group_id` (`display_field_group_id`);

--
-- Indexes for table `ohrm_display_field_group`
--
ALTER TABLE `ohrm_display_field_group`
 ADD PRIMARY KEY (`id`), ADD KEY `report_group_id` (`report_group_id`);

--
-- Indexes for table `ohrm_education`
--
ALTER TABLE `ohrm_education`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_email`
--
ALTER TABLE `ohrm_email`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`), ADD UNIQUE KEY `ohrm_email_name` (`name`);

--
-- Indexes for table `ohrm_email_configuration`
--
ALTER TABLE `ohrm_email_configuration`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_email_notification`
--
ALTER TABLE `ohrm_email_notification`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_email_processor`
--
ALTER TABLE `ohrm_email_processor`
 ADD PRIMARY KEY (`id`), ADD KEY `email_id` (`email_id`);

--
-- Indexes for table `ohrm_email_subscriber`
--
ALTER TABLE `ohrm_email_subscriber`
 ADD PRIMARY KEY (`id`), ADD KEY `notification_id` (`notification_id`);

--
-- Indexes for table `ohrm_email_template`
--
ALTER TABLE `ohrm_email_template`
 ADD PRIMARY KEY (`id`), ADD KEY `email_id` (`email_id`);

--
-- Indexes for table `ohrm_employee_work_shift`
--
ALTER TABLE `ohrm_employee_work_shift`
 ADD PRIMARY KEY (`work_shift_id`,`emp_number`), ADD KEY `emp_number` (`emp_number`);

--
-- Indexes for table `ohrm_employment_status`
--
ALTER TABLE `ohrm_employment_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_emp_education`
--
ALTER TABLE `ohrm_emp_education`
 ADD PRIMARY KEY (`id`), ADD KEY `emp_number` (`emp_number`), ADD KEY `education_id` (`education_id`);

--
-- Indexes for table `ohrm_emp_license`
--
ALTER TABLE `ohrm_emp_license`
 ADD PRIMARY KEY (`emp_number`,`license_id`), ADD KEY `license_id` (`license_id`);

--
-- Indexes for table `ohrm_emp_reporting_method`
--
ALTER TABLE `ohrm_emp_reporting_method`
 ADD PRIMARY KEY (`reporting_method_id`);

--
-- Indexes for table `ohrm_emp_termination`
--
ALTER TABLE `ohrm_emp_termination`
 ADD PRIMARY KEY (`id`), ADD KEY `reason_id` (`reason_id`), ADD KEY `emp_number` (`emp_number`);

--
-- Indexes for table `ohrm_emp_termination_reason`
--
ALTER TABLE `ohrm_emp_termination_reason`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_filter_field`
--
ALTER TABLE `ohrm_filter_field`
 ADD PRIMARY KEY (`filter_field_id`), ADD KEY `report_group_id` (`report_group_id`);

--
-- Indexes for table `ohrm_group_field`
--
ALTER TABLE `ohrm_group_field`
 ADD PRIMARY KEY (`group_field_id`);

--
-- Indexes for table `ohrm_holiday`
--
ALTER TABLE `ohrm_holiday`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_ohrm_holiday_ohrm_operational_country` (`operational_country_id`);

--
-- Indexes for table `ohrm_home_page`
--
ALTER TABLE `ohrm_home_page`
 ADD PRIMARY KEY (`id`), ADD KEY `user_role_id` (`user_role_id`);

--
-- Indexes for table `ohrm_job_candidate`
--
ALTER TABLE `ohrm_job_candidate`
 ADD PRIMARY KEY (`id`), ADD KEY `added_person` (`added_person`);

--
-- Indexes for table `ohrm_job_candidate_attachment`
--
ALTER TABLE `ohrm_job_candidate_attachment`
 ADD PRIMARY KEY (`id`), ADD KEY `candidate_id` (`candidate_id`);

--
-- Indexes for table `ohrm_job_candidate_history`
--
ALTER TABLE `ohrm_job_candidate_history`
 ADD PRIMARY KEY (`id`), ADD KEY `candidate_id` (`candidate_id`), ADD KEY `vacancy_id` (`vacancy_id`), ADD KEY `interview_id` (`interview_id`), ADD KEY `performed_by` (`performed_by`);

--
-- Indexes for table `ohrm_job_candidate_vacancy`
--
ALTER TABLE `ohrm_job_candidate_vacancy`
 ADD PRIMARY KEY (`candidate_id`,`vacancy_id`), ADD UNIQUE KEY `id` (`id`), ADD KEY `vacancy_id` (`vacancy_id`);

--
-- Indexes for table `ohrm_job_category`
--
ALTER TABLE `ohrm_job_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_job_interview`
--
ALTER TABLE `ohrm_job_interview`
 ADD PRIMARY KEY (`id`), ADD KEY `candidate_vacancy_id` (`candidate_vacancy_id`), ADD KEY `candidate_id` (`candidate_id`);

--
-- Indexes for table `ohrm_job_interview_attachment`
--
ALTER TABLE `ohrm_job_interview_attachment`
 ADD PRIMARY KEY (`id`), ADD KEY `interview_id` (`interview_id`);

--
-- Indexes for table `ohrm_job_interview_interviewer`
--
ALTER TABLE `ohrm_job_interview_interviewer`
 ADD PRIMARY KEY (`interview_id`,`interviewer_id`), ADD KEY `interviewer_id` (`interviewer_id`);

--
-- Indexes for table `ohrm_job_specification_attachment`
--
ALTER TABLE `ohrm_job_specification_attachment`
 ADD PRIMARY KEY (`id`), ADD KEY `job_title_id` (`job_title_id`);

--
-- Indexes for table `ohrm_job_title`
--
ALTER TABLE `ohrm_job_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_job_vacancy`
--
ALTER TABLE `ohrm_job_vacancy`
 ADD PRIMARY KEY (`id`), ADD KEY `job_title_code` (`job_title_code`), ADD KEY `hiring_manager_id` (`hiring_manager_id`);

--
-- Indexes for table `ohrm_job_vacancy_attachment`
--
ALTER TABLE `ohrm_job_vacancy_attachment`
 ADD PRIMARY KEY (`id`), ADD KEY `vacancy_id` (`vacancy_id`);

--
-- Indexes for table `ohrm_kpi`
--
ALTER TABLE `ohrm_kpi`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_language`
--
ALTER TABLE `ohrm_language`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_leave`
--
ALTER TABLE `ohrm_leave`
 ADD PRIMARY KEY (`id`), ADD KEY `leave_request_type_emp` (`leave_request_id`,`leave_type_id`,`emp_number`), ADD KEY `request_status` (`leave_request_id`,`status`), ADD KEY `leave_type_id` (`leave_type_id`);

--
-- Indexes for table `ohrm_leave_adjustment`
--
ALTER TABLE `ohrm_leave_adjustment`
 ADD PRIMARY KEY (`id`), ADD KEY `leave_type_id` (`leave_type_id`), ADD KEY `emp_number` (`emp_number`), ADD KEY `created_by_id` (`created_by_id`), ADD KEY `adjustment_type` (`adjustment_type`);

--
-- Indexes for table `ohrm_leave_comment`
--
ALTER TABLE `ohrm_leave_comment`
 ADD PRIMARY KEY (`id`), ADD KEY `leave_id` (`leave_id`), ADD KEY `created_by_id` (`created_by_id`), ADD KEY `created_by_emp_number` (`created_by_emp_number`);

--
-- Indexes for table `ohrm_leave_entitlement`
--
ALTER TABLE `ohrm_leave_entitlement`
 ADD PRIMARY KEY (`id`), ADD KEY `leave_type_id` (`leave_type_id`), ADD KEY `emp_number` (`emp_number`), ADD KEY `entitlement_type` (`entitlement_type`), ADD KEY `created_by_id` (`created_by_id`);

--
-- Indexes for table `ohrm_leave_entitlement_adjustment`
--
ALTER TABLE `ohrm_leave_entitlement_adjustment`
 ADD PRIMARY KEY (`id`), ADD KEY `entitlement_id` (`entitlement_id`), ADD KEY `adjustment_id` (`adjustment_id`);

--
-- Indexes for table `ohrm_leave_entitlement_type`
--
ALTER TABLE `ohrm_leave_entitlement_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_leave_leave_entitlement`
--
ALTER TABLE `ohrm_leave_leave_entitlement`
 ADD PRIMARY KEY (`id`), ADD KEY `entitlement_id` (`entitlement_id`), ADD KEY `leave_id` (`leave_id`);

--
-- Indexes for table `ohrm_leave_period_history`
--
ALTER TABLE `ohrm_leave_period_history`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_leave_request`
--
ALTER TABLE `ohrm_leave_request`
 ADD PRIMARY KEY (`id`), ADD KEY `emp_number` (`emp_number`), ADD KEY `leave_type_id` (`leave_type_id`);

--
-- Indexes for table `ohrm_leave_request_comment`
--
ALTER TABLE `ohrm_leave_request_comment`
 ADD PRIMARY KEY (`id`), ADD KEY `leave_request_id` (`leave_request_id`), ADD KEY `created_by_id` (`created_by_id`), ADD KEY `created_by_emp_number` (`created_by_emp_number`);

--
-- Indexes for table `ohrm_leave_status`
--
ALTER TABLE `ohrm_leave_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_leave_type`
--
ALTER TABLE `ohrm_leave_type`
 ADD PRIMARY KEY (`id`), ADD KEY `operational_country_id` (`operational_country_id`);

--
-- Indexes for table `ohrm_license`
--
ALTER TABLE `ohrm_license`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_location`
--
ALTER TABLE `ohrm_location`
 ADD PRIMARY KEY (`id`), ADD KEY `country_code` (`country_code`);

--
-- Indexes for table `ohrm_login`
--
ALTER TABLE `ohrm_login`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_membership`
--
ALTER TABLE `ohrm_membership`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_menu_item`
--
ALTER TABLE `ohrm_menu_item`
 ADD PRIMARY KEY (`id`), ADD KEY `screen_id` (`screen_id`);

--
-- Indexes for table `ohrm_module`
--
ALTER TABLE `ohrm_module`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_module_default_page`
--
ALTER TABLE `ohrm_module_default_page`
 ADD PRIMARY KEY (`id`), ADD KEY `user_role_id` (`user_role_id`), ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `ohrm_nationality`
--
ALTER TABLE `ohrm_nationality`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_operational_country`
--
ALTER TABLE `ohrm_operational_country`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_ohrm_operational_country_hs_hr_country` (`country_code`);

--
-- Indexes for table `ohrm_organization_gen_info`
--
ALTER TABLE `ohrm_organization_gen_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_pay_grade`
--
ALTER TABLE `ohrm_pay_grade`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `ohrm_pay_grade_currency`
--
ALTER TABLE `ohrm_pay_grade_currency`
 ADD PRIMARY KEY (`pay_grade_id`,`currency_id`), ADD KEY `currency_id` (`currency_id`);

--
-- Indexes for table `ohrm_performance_review`
--
ALTER TABLE `ohrm_performance_review`
 ADD PRIMARY KEY (`id`), ADD KEY `employee_number` (`employee_number`);

--
-- Indexes for table `ohrm_performance_track`
--
ALTER TABLE `ohrm_performance_track`
 ADD PRIMARY KEY (`id`), ADD KEY `ohrm_performance_track_fk1_idx` (`emp_number`), ADD KEY `ohrm_performance_track_fk2_idx` (`added_by`);

--
-- Indexes for table `ohrm_performance_tracker_log`
--
ALTER TABLE `ohrm_performance_tracker_log`
 ADD PRIMARY KEY (`id`), ADD KEY `ohrm_performance_tracker_log_fk1_idx` (`performance_track_id`), ADD KEY `ohrm_performance_tracker_log_fk2_idx` (`reviewer_id`), ADD KEY `fk_ohrm_performance_tracker_log_1` (`user_id`);

--
-- Indexes for table `ohrm_performance_tracker_reviewer`
--
ALTER TABLE `ohrm_performance_tracker_reviewer`
 ADD PRIMARY KEY (`id`), ADD KEY `ohrm_performance_tracker_reviewer_fk1_idx` (`performance_track_id`), ADD KEY `ohrm_performance_tracker_reviewer_fk2_idx` (`reviewer_id`);

--
-- Indexes for table `ohrm_plugin`
--
ALTER TABLE `ohrm_plugin`
 ADD PRIMARY KEY (`id`), ADD KEY `name` (`name`);

--
-- Indexes for table `ohrm_project`
--
ALTER TABLE `ohrm_project`
 ADD PRIMARY KEY (`project_id`,`customer_id`), ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `ohrm_project_activity`
--
ALTER TABLE `ohrm_project_activity`
 ADD PRIMARY KEY (`activity_id`), ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `ohrm_project_admin`
--
ALTER TABLE `ohrm_project_admin`
 ADD PRIMARY KEY (`project_id`,`emp_number`), ADD KEY `emp_number` (`emp_number`);

--
-- Indexes for table `ohrm_report`
--
ALTER TABLE `ohrm_report`
 ADD PRIMARY KEY (`report_id`), ADD KEY `report_group_id` (`report_group_id`);

--
-- Indexes for table `ohrm_report_group`
--
ALTER TABLE `ohrm_report_group`
 ADD PRIMARY KEY (`report_group_id`);

--
-- Indexes for table `ohrm_reviewer`
--
ALTER TABLE `ohrm_reviewer`
 ADD PRIMARY KEY (`id`), ADD KEY `review_id` (`review_id`);

--
-- Indexes for table `ohrm_reviewer_group`
--
ALTER TABLE `ohrm_reviewer_group`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_reviewer_rating`
--
ALTER TABLE `ohrm_reviewer_rating`
 ADD PRIMARY KEY (`id`), ADD KEY `review_id` (`review_id`), ADD KEY `reviewer_id` (`reviewer_id`);

--
-- Indexes for table `ohrm_role_user_selection_rule`
--
ALTER TABLE `ohrm_role_user_selection_rule`
 ADD PRIMARY KEY (`user_role_id`,`selection_rule_id`);

--
-- Indexes for table `ohrm_screen`
--
ALTER TABLE `ohrm_screen`
 ADD PRIMARY KEY (`id`), ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `ohrm_selected_composite_display_field`
--
ALTER TABLE `ohrm_selected_composite_display_field`
 ADD PRIMARY KEY (`id`,`composite_display_field_id`,`report_id`), ADD KEY `composite_display_field_id` (`composite_display_field_id`), ADD KEY `report_id` (`report_id`);

--
-- Indexes for table `ohrm_selected_display_field`
--
ALTER TABLE `ohrm_selected_display_field`
 ADD PRIMARY KEY (`id`,`display_field_id`,`report_id`), ADD KEY `display_field_id` (`display_field_id`), ADD KEY `report_id` (`report_id`);

--
-- Indexes for table `ohrm_selected_display_field_group`
--
ALTER TABLE `ohrm_selected_display_field_group`
 ADD PRIMARY KEY (`id`), ADD KEY `report_id` (`report_id`), ADD KEY `display_field_group_id` (`display_field_group_id`);

--
-- Indexes for table `ohrm_selected_filter_field`
--
ALTER TABLE `ohrm_selected_filter_field`
 ADD PRIMARY KEY (`report_id`,`filter_field_id`), ADD KEY `report_id` (`report_id`), ADD KEY `filter_field_id` (`filter_field_id`);

--
-- Indexes for table `ohrm_selected_group_field`
--
ALTER TABLE `ohrm_selected_group_field`
 ADD PRIMARY KEY (`group_field_id`,`summary_display_field_id`,`report_id`), ADD KEY `group_field_id` (`group_field_id`), ADD KEY `summary_display_field_id` (`summary_display_field_id`), ADD KEY `report_id` (`report_id`);

--
-- Indexes for table `ohrm_skill`
--
ALTER TABLE `ohrm_skill`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_subunit`
--
ALTER TABLE `ohrm_subunit`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `ohrm_summary_display_field`
--
ALTER TABLE `ohrm_summary_display_field`
 ADD PRIMARY KEY (`summary_display_field_id`), ADD KEY `display_field_group_id` (`display_field_group_id`);

--
-- Indexes for table `ohrm_timesheet`
--
ALTER TABLE `ohrm_timesheet`
 ADD PRIMARY KEY (`timesheet_id`);

--
-- Indexes for table `ohrm_timesheet_action_log`
--
ALTER TABLE `ohrm_timesheet_action_log`
 ADD PRIMARY KEY (`timesheet_action_log_id`), ADD KEY `timesheet_id` (`timesheet_id`), ADD KEY `performed_by` (`performed_by`);

--
-- Indexes for table `ohrm_timesheet_item`
--
ALTER TABLE `ohrm_timesheet_item`
 ADD PRIMARY KEY (`timesheet_item_id`), ADD KEY `timesheet_id` (`timesheet_id`), ADD KEY `activity_id` (`activity_id`);

--
-- Indexes for table `ohrm_upgrade_history`
--
ALTER TABLE `ohrm_upgrade_history`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_user`
--
ALTER TABLE `ohrm_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_name` (`user_name`), ADD KEY `user_role_id` (`user_role_id`), ADD KEY `emp_number` (`emp_number`), ADD KEY `modified_user_id` (`modified_user_id`), ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `ohrm_user_role`
--
ALTER TABLE `ohrm_user_role`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_role_name` (`name`);

--
-- Indexes for table `ohrm_user_role_data_group`
--
ALTER TABLE `ohrm_user_role_data_group`
 ADD PRIMARY KEY (`id`), ADD KEY `user_role_id` (`user_role_id`), ADD KEY `data_group_id` (`data_group_id`);

--
-- Indexes for table `ohrm_user_role_screen`
--
ALTER TABLE `ohrm_user_role_screen`
 ADD PRIMARY KEY (`id`), ADD KEY `user_role_id` (`user_role_id`), ADD KEY `screen_id` (`screen_id`);

--
-- Indexes for table `ohrm_user_selection_rule`
--
ALTER TABLE `ohrm_user_selection_rule`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_workflow_state_machine`
--
ALTER TABLE `ohrm_workflow_state_machine`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_work_shift`
--
ALTER TABLE `ohrm_work_shift`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ohrm_work_week`
--
ALTER TABLE `ohrm_work_week`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_ohrm_work_week_ohrm_operational_country` (`operational_country_id`);

--
-- Indexes for table `opras_form`
--
ALTER TABLE `opras_form`
 ADD PRIMARY KEY (`form_id`,`appraisee`,`supervisor`), ADD KEY `fk_opras_form_hs_hr_employee1_idx` (`appraisee`), ADD KEY `fk_opras_form_hs_hr_employee2_idx` (`supervisor`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
 ADD PRIMARY KEY (`payroll_id`);

--
-- Indexes for table `payslip`
--
ALTER TABLE `payslip`
 ADD PRIMARY KEY (`payslip_id`,`emp_number`,`payroll_id`), ADD KEY `fk_payslip_hs_hr_employee1_idx` (`emp_number`), ADD KEY `fk_payslip_payroll1_idx` (`payroll_id`);

--
-- Indexes for table `pa_line`
--
ALTER TABLE `pa_line`
 ADD PRIMARY KEY (`line_id`,`pa_id`,`form_id`), ADD KEY `fk_pa_line_performance_agreement1_idx` (`pa_id`,`form_id`);

--
-- Indexes for table `pension`
--
ALTER TABLE `pension`
 ADD PRIMARY KEY (`pension_id`);

--
-- Indexes for table `pension_deduction`
--
ALTER TABLE `pension_deduction`
 ADD PRIMARY KEY (`deduction_id`,`emp_number`,`pension_id`), ADD KEY `fk_pension_deduction_hs_hr_employee1_idx` (`emp_number`), ADD KEY `fk_pension_deduction_payroll1_idx` (`payroll_id`), ADD KEY `fk_pension_deduction_pension1_idx` (`pension_id`);

--
-- Indexes for table `pension_employee`
--
ALTER TABLE `pension_employee`
 ADD PRIMARY KEY (`pen_emp_id`,`pension_id`,`emp_number`), ADD KEY `fk_pension_has_hs_hr_employee_hs_hr_employee1_idx` (`emp_number`), ADD KEY `fk_pension_has_hs_hr_employee_pension1_idx` (`pension_id`);

--
-- Indexes for table `performance_agreement`
--
ALTER TABLE `performance_agreement`
 ADD PRIMARY KEY (`pa_id`,`form_id`,`appraisee`,`supervisor`), ADD KEY `fk_performance_agreement_opras_form1_idx` (`form_id`,`appraisee`,`supervisor`);

--
-- Indexes for table `tax_deduction`
--
ALTER TABLE `tax_deduction`
 ADD PRIMARY KEY (`deduction_id`,`tax_rate_id`,`emp_number`), ADD KEY `fk_tax_deduction_tax_rate1_idx` (`tax_rate_id`), ADD KEY `fk_tax_deduction_hs_hr_employee1_idx` (`emp_number`), ADD KEY `fk_tax_deduction_payroll1_idx` (`payroll_id`);

--
-- Indexes for table `tax_rate`
--
ALTER TABLE `tax_rate`
 ADD PRIMARY KEY (`rate_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allowance`
--
ALTER TABLE `allowance`
MODIFY `allowance_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `deduction`
--
ALTER TABLE `deduction`
MODIFY `deduction_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `deduction_line`
--
ALTER TABLE `deduction_line`
MODIFY `line_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `financial_organization`
--
ALTER TABLE `financial_organization`
MODIFY `organization_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hs_hr_emp_basicsalary`
--
ALTER TABLE `hs_hr_emp_basicsalary`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hs_hr_emp_directdebit`
--
ALTER TABLE `hs_hr_emp_directdebit`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hs_hr_province`
--
ALTER TABLE `hs_hr_province`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `hs_hr_unique_id`
--
ALTER TABLE `hs_hr_unique_id`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `loan`
--
ALTER TABLE `loan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `loan_deduction`
--
ALTER TABLE `loan_deduction`
MODIFY `loan_deduction_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `ohrm_beacon_notification`
--
ALTER TABLE `ohrm_beacon_notification`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_composite_display_field`
--
ALTER TABLE `ohrm_composite_display_field`
MODIFY `composite_display_field_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ohrm_customer`
--
ALTER TABLE `ohrm_customer`
MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_datapoint`
--
ALTER TABLE `ohrm_datapoint`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_datapoint_type`
--
ALTER TABLE `ohrm_datapoint_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ohrm_data_group`
--
ALTER TABLE `ohrm_data_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `ohrm_data_group_screen`
--
ALTER TABLE `ohrm_data_group_screen`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `ohrm_display_field`
--
ALTER TABLE `ohrm_display_field`
MODIFY `display_field_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `ohrm_display_field_group`
--
ALTER TABLE `ohrm_display_field_group`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `ohrm_education`
--
ALTER TABLE `ohrm_education`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_email`
--
ALTER TABLE `ohrm_email`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ohrm_email_configuration`
--
ALTER TABLE `ohrm_email_configuration`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_email_notification`
--
ALTER TABLE `ohrm_email_notification`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ohrm_email_processor`
--
ALTER TABLE `ohrm_email_processor`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ohrm_email_subscriber`
--
ALTER TABLE `ohrm_email_subscriber`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_email_template`
--
ALTER TABLE `ohrm_email_template`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `ohrm_employee_work_shift`
--
ALTER TABLE `ohrm_employee_work_shift`
MODIFY `work_shift_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_employment_status`
--
ALTER TABLE `ohrm_employment_status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_emp_education`
--
ALTER TABLE `ohrm_emp_education`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_emp_reporting_method`
--
ALTER TABLE `ohrm_emp_reporting_method`
MODIFY `reporting_method_id` int(7) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ohrm_emp_termination`
--
ALTER TABLE `ohrm_emp_termination`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_emp_termination_reason`
--
ALTER TABLE `ohrm_emp_termination_reason`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `ohrm_holiday`
--
ALTER TABLE `ohrm_holiday`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_home_page`
--
ALTER TABLE `ohrm_home_page`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ohrm_job_candidate_attachment`
--
ALTER TABLE `ohrm_job_candidate_attachment`
MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_job_candidate_history`
--
ALTER TABLE `ohrm_job_candidate_history`
MODIFY `id` int(13) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ohrm_job_category`
--
ALTER TABLE `ohrm_job_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ohrm_job_interview`
--
ALTER TABLE `ohrm_job_interview`
MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_job_interview_attachment`
--
ALTER TABLE `ohrm_job_interview_attachment`
MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_job_specification_attachment`
--
ALTER TABLE `ohrm_job_specification_attachment`
MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_job_title`
--
ALTER TABLE `ohrm_job_title`
MODIFY `id` int(13) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ohrm_job_vacancy_attachment`
--
ALTER TABLE `ohrm_job_vacancy_attachment`
MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_kpi`
--
ALTER TABLE `ohrm_kpi`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_language`
--
ALTER TABLE `ohrm_language`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_leave`
--
ALTER TABLE `ohrm_leave`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ohrm_leave_adjustment`
--
ALTER TABLE `ohrm_leave_adjustment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_leave_comment`
--
ALTER TABLE `ohrm_leave_comment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_leave_entitlement`
--
ALTER TABLE `ohrm_leave_entitlement`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_leave_entitlement_adjustment`
--
ALTER TABLE `ohrm_leave_entitlement_adjustment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_leave_entitlement_type`
--
ALTER TABLE `ohrm_leave_entitlement_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_leave_leave_entitlement`
--
ALTER TABLE `ohrm_leave_leave_entitlement`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ohrm_leave_period_history`
--
ALTER TABLE `ohrm_leave_period_history`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ohrm_leave_request`
--
ALTER TABLE `ohrm_leave_request`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_leave_request_comment`
--
ALTER TABLE `ohrm_leave_request_comment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_leave_status`
--
ALTER TABLE `ohrm_leave_status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ohrm_leave_type`
--
ALTER TABLE `ohrm_leave_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ohrm_license`
--
ALTER TABLE `ohrm_license`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_location`
--
ALTER TABLE `ohrm_location`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_login`
--
ALTER TABLE `ohrm_login`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=349;
--
-- AUTO_INCREMENT for table `ohrm_membership`
--
ALTER TABLE `ohrm_membership`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_menu_item`
--
ALTER TABLE `ohrm_menu_item`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT for table `ohrm_module`
--
ALTER TABLE `ohrm_module`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ohrm_module_default_page`
--
ALTER TABLE `ohrm_module_default_page`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `ohrm_nationality`
--
ALTER TABLE `ohrm_nationality`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=194;
--
-- AUTO_INCREMENT for table `ohrm_operational_country`
--
ALTER TABLE `ohrm_operational_country`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_organization_gen_info`
--
ALTER TABLE `ohrm_organization_gen_info`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_pay_grade`
--
ALTER TABLE `ohrm_pay_grade`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ohrm_performance_review`
--
ALTER TABLE `ohrm_performance_review`
MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_performance_track`
--
ALTER TABLE `ohrm_performance_track`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_performance_tracker_log`
--
ALTER TABLE `ohrm_performance_tracker_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_performance_tracker_reviewer`
--
ALTER TABLE `ohrm_performance_tracker_reviewer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_plugin`
--
ALTER TABLE `ohrm_plugin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_project`
--
ALTER TABLE `ohrm_project`
MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_project_activity`
--
ALTER TABLE `ohrm_project_activity`
MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ohrm_report`
--
ALTER TABLE `ohrm_report`
MODIFY `report_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ohrm_reviewer`
--
ALTER TABLE `ohrm_reviewer`
MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_reviewer_group`
--
ALTER TABLE `ohrm_reviewer_group`
MODIFY `id` int(7) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ohrm_reviewer_rating`
--
ALTER TABLE `ohrm_reviewer_rating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_screen`
--
ALTER TABLE `ohrm_screen`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=144;
--
-- AUTO_INCREMENT for table `ohrm_selected_display_field`
--
ALTER TABLE `ohrm_selected_display_field`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `ohrm_selected_display_field_group`
--
ALTER TABLE `ohrm_selected_display_field_group`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `ohrm_skill`
--
ALTER TABLE `ohrm_skill`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_subunit`
--
ALTER TABLE `ohrm_subunit`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ohrm_upgrade_history`
--
ALTER TABLE `ohrm_upgrade_history`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_user`
--
ALTER TABLE `ohrm_user`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ohrm_user_role`
--
ALTER TABLE `ohrm_user_role`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ohrm_user_role_data_group`
--
ALTER TABLE `ohrm_user_role_data_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=234;
--
-- AUTO_INCREMENT for table `ohrm_user_role_screen`
--
ALTER TABLE `ohrm_user_role_screen`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT for table `ohrm_user_selection_rule`
--
ALTER TABLE `ohrm_user_selection_rule`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_workflow_state_machine`
--
ALTER TABLE `ohrm_workflow_state_machine`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `ohrm_work_shift`
--
ALTER TABLE `ohrm_work_shift`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ohrm_work_week`
--
ALTER TABLE `ohrm_work_week`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `opras_form`
--
ALTER TABLE `opras_form`
MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
MODIFY `payroll_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `payslip`
--
ALTER TABLE `payslip`
MODIFY `payslip_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `pa_line`
--
ALTER TABLE `pa_line`
MODIFY `line_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pension`
--
ALTER TABLE `pension`
MODIFY `pension_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pension_deduction`
--
ALTER TABLE `pension_deduction`
MODIFY `deduction_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `pension_employee`
--
ALTER TABLE `pension_employee`
MODIFY `pen_emp_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `performance_agreement`
--
ALTER TABLE `performance_agreement`
MODIFY `pa_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tax_deduction`
--
ALTER TABLE `tax_deduction`
MODIFY `deduction_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tax_rate`
--
ALTER TABLE `tax_rate`
MODIFY `rate_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `deduction`
--
ALTER TABLE `deduction`
ADD CONSTRAINT `fk_deductions_hs_hr_employee1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `deduction_line`
--
ALTER TABLE `deduction_line`
ADD CONSTRAINT `fk_deduction_lines_deductions1` FOREIGN KEY (`deduction_id`, `emp_number`) REFERENCES `deduction` (`deduction_id`, `emp_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_deduction_lines_payroll1` FOREIGN KEY (`payroll_id`) REFERENCES `payroll` (`payroll_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_loan`
--
ALTER TABLE `employee_loan`
ADD CONSTRAINT `fk_employee_loan_hs_hr_employee1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_employee_loan_loan1` FOREIGN KEY (`loan_id`) REFERENCES `loan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hs_hr_employee`
--
ALTER TABLE `hs_hr_employee`
ADD CONSTRAINT `hs_hr_employee_ibfk_1` FOREIGN KEY (`work_station`) REFERENCES `ohrm_subunit` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `hs_hr_employee_ibfk_2` FOREIGN KEY (`nation_code`) REFERENCES `ohrm_nationality` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `hs_hr_employee_ibfk_3` FOREIGN KEY (`job_title_code`) REFERENCES `ohrm_job_title` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `hs_hr_employee_ibfk_4` FOREIGN KEY (`emp_status`) REFERENCES `ohrm_employment_status` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `hs_hr_employee_ibfk_5` FOREIGN KEY (`eeo_cat_code`) REFERENCES `ohrm_job_category` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `hs_hr_employee_ibfk_6` FOREIGN KEY (`termination_id`) REFERENCES `ohrm_emp_termination` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `hs_hr_emp_attachment`
--
ALTER TABLE `hs_hr_emp_attachment`
ADD CONSTRAINT `hs_hr_emp_attachment_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_basicsalary`
--
ALTER TABLE `hs_hr_emp_basicsalary`
ADD CONSTRAINT `hs_hr_emp_basicsalary_ibfk_1` FOREIGN KEY (`sal_grd_code`) REFERENCES `ohrm_pay_grade` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `hs_hr_emp_basicsalary_ibfk_2` FOREIGN KEY (`currency_id`) REFERENCES `hs_hr_currency_type` (`currency_id`) ON DELETE CASCADE,
ADD CONSTRAINT `hs_hr_emp_basicsalary_ibfk_3` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `hs_hr_emp_basicsalary_ibfk_4` FOREIGN KEY (`payperiod_code`) REFERENCES `hs_hr_payperiod` (`payperiod_code`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_children`
--
ALTER TABLE `hs_hr_emp_children`
ADD CONSTRAINT `hs_hr_emp_children_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_contract_extend`
--
ALTER TABLE `hs_hr_emp_contract_extend`
ADD CONSTRAINT `hs_hr_emp_contract_extend_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_dependents`
--
ALTER TABLE `hs_hr_emp_dependents`
ADD CONSTRAINT `hs_hr_emp_dependents_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_directdebit`
--
ALTER TABLE `hs_hr_emp_directdebit`
ADD CONSTRAINT `hs_hr_emp_directdebit_ibfk_1` FOREIGN KEY (`salary_id`) REFERENCES `hs_hr_emp_basicsalary` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_emergency_contacts`
--
ALTER TABLE `hs_hr_emp_emergency_contacts`
ADD CONSTRAINT `hs_hr_emp_emergency_contacts_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_history_of_ealier_pos`
--
ALTER TABLE `hs_hr_emp_history_of_ealier_pos`
ADD CONSTRAINT `hs_hr_emp_history_of_ealier_pos_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_language`
--
ALTER TABLE `hs_hr_emp_language`
ADD CONSTRAINT `hs_hr_emp_language_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `hs_hr_emp_language_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `ohrm_language` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_locations`
--
ALTER TABLE `hs_hr_emp_locations`
ADD CONSTRAINT `hs_hr_emp_locations_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `ohrm_location` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `hs_hr_emp_locations_ibfk_2` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_member_detail`
--
ALTER TABLE `hs_hr_emp_member_detail`
ADD CONSTRAINT `hs_hr_emp_member_detail_ibfk_1` FOREIGN KEY (`membship_code`) REFERENCES `ohrm_membership` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `hs_hr_emp_member_detail_ibfk_2` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_passport`
--
ALTER TABLE `hs_hr_emp_passport`
ADD CONSTRAINT `hs_hr_emp_passport_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_picture`
--
ALTER TABLE `hs_hr_emp_picture`
ADD CONSTRAINT `hs_hr_emp_picture_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_reportto`
--
ALTER TABLE `hs_hr_emp_reportto`
ADD CONSTRAINT `hs_hr_emp_reportto_ibfk_1` FOREIGN KEY (`erep_sup_emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `hs_hr_emp_reportto_ibfk_2` FOREIGN KEY (`erep_sub_emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `hs_hr_emp_reportto_ibfk_3` FOREIGN KEY (`erep_reporting_mode`) REFERENCES `ohrm_emp_reporting_method` (`reporting_method_id`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_skill`
--
ALTER TABLE `hs_hr_emp_skill`
ADD CONSTRAINT `hs_hr_emp_skill_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `hs_hr_emp_skill_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `ohrm_skill` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_us_tax`
--
ALTER TABLE `hs_hr_emp_us_tax`
ADD CONSTRAINT `hs_hr_emp_us_tax_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_emp_work_experience`
--
ALTER TABLE `hs_hr_emp_work_experience`
ADD CONSTRAINT `hs_hr_emp_work_experience_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_jobtit_empstat`
--
ALTER TABLE `hs_hr_jobtit_empstat`
ADD CONSTRAINT `hs_hr_jobtit_empstat_ibfk_1` FOREIGN KEY (`jobtit_code`) REFERENCES `ohrm_job_title` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `hs_hr_jobtit_empstat_ibfk_2` FOREIGN KEY (`estat_code`) REFERENCES `ohrm_employment_status` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hs_hr_mailnotifications`
--
ALTER TABLE `hs_hr_mailnotifications`
ADD CONSTRAINT `hs_hr_mailnotifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ohrm_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `loan`
--
ALTER TABLE `loan`
ADD CONSTRAINT `fk_loan_financial_organization1` FOREIGN KEY (`organization_id`) REFERENCES `financial_organization` (`organization_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `loan_deduction`
--
ALTER TABLE `loan_deduction`
ADD CONSTRAINT `fk_loan_deduction_hs_hr_employee1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_loan_deduction_loan1` FOREIGN KEY (`loan_id`) REFERENCES `loan` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `fk_loan_deduction_payroll1` FOREIGN KEY (`payroll_id`) REFERENCES `payroll` (`payroll_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ohrm_available_group_field`
--
ALTER TABLE `ohrm_available_group_field`
ADD CONSTRAINT `ohrm_available_group_field_ibfk_1` FOREIGN KEY (`group_field_id`) REFERENCES `ohrm_group_field` (`group_field_id`);

--
-- Constraints for table `ohrm_composite_display_field`
--
ALTER TABLE `ohrm_composite_display_field`
ADD CONSTRAINT `ohrm_composite_display_field_ibfk_1` FOREIGN KEY (`report_group_id`) REFERENCES `ohrm_report_group` (`report_group_id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_composite_display_field_ibfk_2` FOREIGN KEY (`display_field_group_id`) REFERENCES `ohrm_display_field_group` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `ohrm_datapoint`
--
ALTER TABLE `ohrm_datapoint`
ADD CONSTRAINT `ohrm_datapoint_ibfk_1` FOREIGN KEY (`datapoint_type_id`) REFERENCES `ohrm_datapoint_type` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_data_group_screen`
--
ALTER TABLE `ohrm_data_group_screen`
ADD CONSTRAINT `ohrm_data_group_screen_ibfk_1` FOREIGN KEY (`data_group_id`) REFERENCES `ohrm_data_group` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_data_group_screen_ibfk_2` FOREIGN KEY (`screen_id`) REFERENCES `ohrm_screen` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_display_field`
--
ALTER TABLE `ohrm_display_field`
ADD CONSTRAINT `ohrm_display_field_ibfk_1` FOREIGN KEY (`report_group_id`) REFERENCES `ohrm_report_group` (`report_group_id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_display_field_ibfk_2` FOREIGN KEY (`display_field_group_id`) REFERENCES `ohrm_display_field_group` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `ohrm_display_field_group`
--
ALTER TABLE `ohrm_display_field_group`
ADD CONSTRAINT `ohrm_display_field_group_ibfk_1` FOREIGN KEY (`report_group_id`) REFERENCES `ohrm_report_group` (`report_group_id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_email_processor`
--
ALTER TABLE `ohrm_email_processor`
ADD CONSTRAINT `ohrm_email_processor_ibfk_1` FOREIGN KEY (`email_id`) REFERENCES `ohrm_email` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_email_subscriber`
--
ALTER TABLE `ohrm_email_subscriber`
ADD CONSTRAINT `ohrm_email_subscriber_ibfk_1` FOREIGN KEY (`notification_id`) REFERENCES `ohrm_email_notification` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_email_template`
--
ALTER TABLE `ohrm_email_template`
ADD CONSTRAINT `ohrm_email_template_ibfk_1` FOREIGN KEY (`email_id`) REFERENCES `ohrm_email` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_employee_work_shift`
--
ALTER TABLE `ohrm_employee_work_shift`
ADD CONSTRAINT `ohrm_employee_work_shift_ibfk_1` FOREIGN KEY (`work_shift_id`) REFERENCES `ohrm_work_shift` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_employee_work_shift_ibfk_2` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_emp_education`
--
ALTER TABLE `ohrm_emp_education`
ADD CONSTRAINT `ohrm_emp_education_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_emp_education_ibfk_2` FOREIGN KEY (`education_id`) REFERENCES `ohrm_education` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_emp_license`
--
ALTER TABLE `ohrm_emp_license`
ADD CONSTRAINT `ohrm_emp_license_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_emp_license_ibfk_2` FOREIGN KEY (`license_id`) REFERENCES `ohrm_license` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_emp_termination`
--
ALTER TABLE `ohrm_emp_termination`
ADD CONSTRAINT `ohrm_emp_termination_ibfk_1` FOREIGN KEY (`reason_id`) REFERENCES `ohrm_emp_termination_reason` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `ohrm_emp_termination_ibfk_2` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_filter_field`
--
ALTER TABLE `ohrm_filter_field`
ADD CONSTRAINT `ohrm_filter_field_ibfk_1` FOREIGN KEY (`report_group_id`) REFERENCES `ohrm_report_group` (`report_group_id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_holiday`
--
ALTER TABLE `ohrm_holiday`
ADD CONSTRAINT `fk_ohrm_holiday_ohrm_operational_country` FOREIGN KEY (`operational_country_id`) REFERENCES `ohrm_operational_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ohrm_home_page`
--
ALTER TABLE `ohrm_home_page`
ADD CONSTRAINT `ohrm_home_page_ibfk_1` FOREIGN KEY (`user_role_id`) REFERENCES `ohrm_user_role` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_job_candidate`
--
ALTER TABLE `ohrm_job_candidate`
ADD CONSTRAINT `ohrm_job_candidate_ibfk_1` FOREIGN KEY (`added_person`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE SET NULL;

--
-- Constraints for table `ohrm_job_candidate_attachment`
--
ALTER TABLE `ohrm_job_candidate_attachment`
ADD CONSTRAINT `ohrm_job_candidate_attachment_ibfk_1` FOREIGN KEY (`candidate_id`) REFERENCES `ohrm_job_candidate` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_job_candidate_history`
--
ALTER TABLE `ohrm_job_candidate_history`
ADD CONSTRAINT `ohrm_job_candidate_history_ibfk_1` FOREIGN KEY (`candidate_id`) REFERENCES `ohrm_job_candidate` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_job_candidate_history_ibfk_2` FOREIGN KEY (`vacancy_id`) REFERENCES `ohrm_job_vacancy` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `ohrm_job_candidate_history_ibfk_3` FOREIGN KEY (`interview_id`) REFERENCES `ohrm_job_interview` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `ohrm_job_candidate_history_ibfk_4` FOREIGN KEY (`performed_by`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE SET NULL;

--
-- Constraints for table `ohrm_job_candidate_vacancy`
--
ALTER TABLE `ohrm_job_candidate_vacancy`
ADD CONSTRAINT `ohrm_job_candidate_vacancy_ibfk_1` FOREIGN KEY (`candidate_id`) REFERENCES `ohrm_job_candidate` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_job_candidate_vacancy_ibfk_2` FOREIGN KEY (`vacancy_id`) REFERENCES `ohrm_job_vacancy` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_job_interview`
--
ALTER TABLE `ohrm_job_interview`
ADD CONSTRAINT `ohrm_job_interview_ibfk_1` FOREIGN KEY (`candidate_vacancy_id`) REFERENCES `ohrm_job_candidate_vacancy` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `ohrm_job_interview_ibfk_2` FOREIGN KEY (`candidate_id`) REFERENCES `ohrm_job_candidate` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_job_interview_attachment`
--
ALTER TABLE `ohrm_job_interview_attachment`
ADD CONSTRAINT `ohrm_job_interview_attachment_ibfk_1` FOREIGN KEY (`interview_id`) REFERENCES `ohrm_job_interview` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_job_interview_interviewer`
--
ALTER TABLE `ohrm_job_interview_interviewer`
ADD CONSTRAINT `ohrm_job_interview_interviewer_ibfk_1` FOREIGN KEY (`interview_id`) REFERENCES `ohrm_job_interview` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_job_interview_interviewer_ibfk_2` FOREIGN KEY (`interviewer_id`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_job_specification_attachment`
--
ALTER TABLE `ohrm_job_specification_attachment`
ADD CONSTRAINT `ohrm_job_specification_attachment_ibfk_1` FOREIGN KEY (`job_title_id`) REFERENCES `ohrm_job_title` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_job_vacancy`
--
ALTER TABLE `ohrm_job_vacancy`
ADD CONSTRAINT `ohrm_job_vacancy_ibfk_1` FOREIGN KEY (`job_title_code`) REFERENCES `ohrm_job_title` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_job_vacancy_ibfk_2` FOREIGN KEY (`hiring_manager_id`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE SET NULL;

--
-- Constraints for table `ohrm_job_vacancy_attachment`
--
ALTER TABLE `ohrm_job_vacancy_attachment`
ADD CONSTRAINT `ohrm_job_vacancy_attachment_ibfk_1` FOREIGN KEY (`vacancy_id`) REFERENCES `ohrm_job_vacancy` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_leave`
--
ALTER TABLE `ohrm_leave`
ADD CONSTRAINT `ohrm_leave_ibfk_1` FOREIGN KEY (`leave_request_id`) REFERENCES `ohrm_leave_request` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_ibfk_2` FOREIGN KEY (`leave_type_id`) REFERENCES `ohrm_leave_type` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_leave_adjustment`
--
ALTER TABLE `ohrm_leave_adjustment`
ADD CONSTRAINT `ohrm_leave_adjustment_ibfk_1` FOREIGN KEY (`leave_type_id`) REFERENCES `ohrm_leave_type` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_adjustment_ibfk_2` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_adjustment_ibfk_3` FOREIGN KEY (`created_by_id`) REFERENCES `ohrm_user` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `ohrm_leave_adjustment_ibfk_4` FOREIGN KEY (`adjustment_type`) REFERENCES `ohrm_leave_entitlement_type` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_leave_comment`
--
ALTER TABLE `ohrm_leave_comment`
ADD CONSTRAINT `ohrm_leave_comment_ibfk_1` FOREIGN KEY (`leave_id`) REFERENCES `ohrm_leave` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_comment_ibfk_2` FOREIGN KEY (`created_by_id`) REFERENCES `ohrm_user` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `ohrm_leave_comment_ibfk_3` FOREIGN KEY (`created_by_emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_leave_entitlement`
--
ALTER TABLE `ohrm_leave_entitlement`
ADD CONSTRAINT `ohrm_leave_entitlement_ibfk_1` FOREIGN KEY (`leave_type_id`) REFERENCES `ohrm_leave_type` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_entitlement_ibfk_2` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_entitlement_ibfk_3` FOREIGN KEY (`entitlement_type`) REFERENCES `ohrm_leave_entitlement_type` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_entitlement_ibfk_4` FOREIGN KEY (`created_by_id`) REFERENCES `ohrm_user` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `ohrm_leave_entitlement_adjustment`
--
ALTER TABLE `ohrm_leave_entitlement_adjustment`
ADD CONSTRAINT `ohrm_leave_entitlement_adjustment_ibfk_1` FOREIGN KEY (`entitlement_id`) REFERENCES `ohrm_leave_entitlement` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_entitlement_adjustment_ibfk_2` FOREIGN KEY (`adjustment_id`) REFERENCES `ohrm_leave_adjustment` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_leave_leave_entitlement`
--
ALTER TABLE `ohrm_leave_leave_entitlement`
ADD CONSTRAINT `ohrm_leave_leave_entitlement_ibfk_1` FOREIGN KEY (`entitlement_id`) REFERENCES `ohrm_leave_entitlement` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_leave_entitlement_ibfk_2` FOREIGN KEY (`leave_id`) REFERENCES `ohrm_leave` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_leave_request`
--
ALTER TABLE `ohrm_leave_request`
ADD CONSTRAINT `ohrm_leave_request_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_request_ibfk_2` FOREIGN KEY (`leave_type_id`) REFERENCES `ohrm_leave_type` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_leave_request_comment`
--
ALTER TABLE `ohrm_leave_request_comment`
ADD CONSTRAINT `ohrm_leave_request_comment_ibfk_1` FOREIGN KEY (`leave_request_id`) REFERENCES `ohrm_leave_request` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_leave_request_comment_ibfk_2` FOREIGN KEY (`created_by_id`) REFERENCES `ohrm_user` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `ohrm_leave_request_comment_ibfk_3` FOREIGN KEY (`created_by_emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_leave_type`
--
ALTER TABLE `ohrm_leave_type`
ADD CONSTRAINT `ohrm_leave_type_ibfk_1` FOREIGN KEY (`operational_country_id`) REFERENCES `ohrm_operational_country` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `ohrm_location`
--
ALTER TABLE `ohrm_location`
ADD CONSTRAINT `ohrm_location_ibfk_1` FOREIGN KEY (`country_code`) REFERENCES `hs_hr_country` (`cou_code`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_menu_item`
--
ALTER TABLE `ohrm_menu_item`
ADD CONSTRAINT `ohrm_menu_item_ibfk_1` FOREIGN KEY (`screen_id`) REFERENCES `ohrm_screen` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_module_default_page`
--
ALTER TABLE `ohrm_module_default_page`
ADD CONSTRAINT `ohrm_module_default_page_ibfk_1` FOREIGN KEY (`user_role_id`) REFERENCES `ohrm_user_role` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_module_default_page_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `ohrm_module` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_operational_country`
--
ALTER TABLE `ohrm_operational_country`
ADD CONSTRAINT `fk_ohrm_operational_country_hs_hr_country` FOREIGN KEY (`country_code`) REFERENCES `hs_hr_country` (`cou_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ohrm_pay_grade_currency`
--
ALTER TABLE `ohrm_pay_grade_currency`
ADD CONSTRAINT `ohrm_pay_grade_currency_ibfk_1` FOREIGN KEY (`currency_id`) REFERENCES `hs_hr_currency_type` (`currency_id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_pay_grade_currency_ibfk_2` FOREIGN KEY (`pay_grade_id`) REFERENCES `ohrm_pay_grade` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_performance_review`
--
ALTER TABLE `ohrm_performance_review`
ADD CONSTRAINT `ohrm_performance_review_ibfk_1` FOREIGN KEY (`employee_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_performance_track`
--
ALTER TABLE `ohrm_performance_track`
ADD CONSTRAINT `ohrm_performance_track_fk1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ohrm_performance_track_fk2` FOREIGN KEY (`added_by`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ohrm_performance_tracker_log`
--
ALTER TABLE `ohrm_performance_tracker_log`
ADD CONSTRAINT `fk_ohrm_performance_tracker_log_1` FOREIGN KEY (`user_id`) REFERENCES `ohrm_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ohrm_performance_tracker_log_fk1` FOREIGN KEY (`performance_track_id`) REFERENCES `ohrm_performance_track` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ohrm_performance_tracker_log_fk2` FOREIGN KEY (`reviewer_id`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ohrm_performance_tracker_reviewer`
--
ALTER TABLE `ohrm_performance_tracker_reviewer`
ADD CONSTRAINT `ohrm_performance_tracker_reviewer_fk1` FOREIGN KEY (`performance_track_id`) REFERENCES `ohrm_performance_track` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ohrm_performance_tracker_reviewer_fk2` FOREIGN KEY (`reviewer_id`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ohrm_project_activity`
--
ALTER TABLE `ohrm_project_activity`
ADD CONSTRAINT `ohrm_project_activity_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `ohrm_project` (`project_id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_project_admin`
--
ALTER TABLE `ohrm_project_admin`
ADD CONSTRAINT `ohrm_project_admin_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `ohrm_project` (`project_id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_project_admin_ibfk_2` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_report`
--
ALTER TABLE `ohrm_report`
ADD CONSTRAINT `ohrm_report_ibfk_1` FOREIGN KEY (`report_group_id`) REFERENCES `ohrm_report_group` (`report_group_id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_reviewer`
--
ALTER TABLE `ohrm_reviewer`
ADD CONSTRAINT `ohrm_reviewer_ibfk_1` FOREIGN KEY (`review_id`) REFERENCES `ohrm_performance_review` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_reviewer_rating`
--
ALTER TABLE `ohrm_reviewer_rating`
ADD CONSTRAINT `ohrm_reviewer_rating_ibfk_1` FOREIGN KEY (`reviewer_id`) REFERENCES `ohrm_reviewer` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_reviewer_rating_ibfk_2` FOREIGN KEY (`review_id`) REFERENCES `ohrm_performance_review` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_screen`
--
ALTER TABLE `ohrm_screen`
ADD CONSTRAINT `ohrm_screen_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `ohrm_module` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_selected_composite_display_field`
--
ALTER TABLE `ohrm_selected_composite_display_field`
ADD CONSTRAINT `ohrm_selected_composite_display_field_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `ohrm_report` (`report_id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_selected_composite_display_field_ibfk_2` FOREIGN KEY (`composite_display_field_id`) REFERENCES `ohrm_composite_display_field` (`composite_display_field_id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_selected_display_field`
--
ALTER TABLE `ohrm_selected_display_field`
ADD CONSTRAINT `ohrm_selected_display_field_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `ohrm_report` (`report_id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_selected_display_field_ibfk_2` FOREIGN KEY (`display_field_id`) REFERENCES `ohrm_display_field` (`display_field_id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_selected_display_field_group`
--
ALTER TABLE `ohrm_selected_display_field_group`
ADD CONSTRAINT `ohrm_selected_display_field_group_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `ohrm_report` (`report_id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_selected_display_field_group_ibfk_2` FOREIGN KEY (`display_field_group_id`) REFERENCES `ohrm_display_field_group` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_selected_filter_field`
--
ALTER TABLE `ohrm_selected_filter_field`
ADD CONSTRAINT `ohrm_selected_filter_field_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `ohrm_report` (`report_id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_selected_filter_field_ibfk_2` FOREIGN KEY (`filter_field_id`) REFERENCES `ohrm_filter_field` (`filter_field_id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_selected_group_field`
--
ALTER TABLE `ohrm_selected_group_field`
ADD CONSTRAINT `ohrm_selected_group_field_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `ohrm_report` (`report_id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_selected_group_field_ibfk_2` FOREIGN KEY (`group_field_id`) REFERENCES `ohrm_group_field` (`group_field_id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_selected_group_field_ibfk_3` FOREIGN KEY (`summary_display_field_id`) REFERENCES `ohrm_summary_display_field` (`summary_display_field_id`);

--
-- Constraints for table `ohrm_summary_display_field`
--
ALTER TABLE `ohrm_summary_display_field`
ADD CONSTRAINT `ohrm_summary_display_field_ibfk_1` FOREIGN KEY (`display_field_group_id`) REFERENCES `ohrm_display_field_group` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `ohrm_timesheet_action_log`
--
ALTER TABLE `ohrm_timesheet_action_log`
ADD CONSTRAINT `ohrm_timesheet_action_log_ibfk_1` FOREIGN KEY (`performed_by`) REFERENCES `ohrm_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_user`
--
ALTER TABLE `ohrm_user`
ADD CONSTRAINT `ohrm_user_ibfk_1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_user_ibfk_2` FOREIGN KEY (`user_role_id`) REFERENCES `ohrm_user_role` (`id`);

--
-- Constraints for table `ohrm_user_role_data_group`
--
ALTER TABLE `ohrm_user_role_data_group`
ADD CONSTRAINT `ohrm_user_role_data_group_ibfk_1` FOREIGN KEY (`user_role_id`) REFERENCES `ohrm_user_role` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_user_role_data_group_ibfk_2` FOREIGN KEY (`data_group_id`) REFERENCES `ohrm_data_group` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_user_role_screen`
--
ALTER TABLE `ohrm_user_role_screen`
ADD CONSTRAINT `ohrm_user_role_screen_ibfk_1` FOREIGN KEY (`user_role_id`) REFERENCES `ohrm_user_role` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `ohrm_user_role_screen_ibfk_2` FOREIGN KEY (`screen_id`) REFERENCES `ohrm_screen` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ohrm_work_week`
--
ALTER TABLE `ohrm_work_week`
ADD CONSTRAINT `fk_ohrm_work_week_ohrm_operational_country` FOREIGN KEY (`operational_country_id`) REFERENCES `ohrm_operational_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `opras_form`
--
ALTER TABLE `opras_form`
ADD CONSTRAINT `fk_opras_form_hs_hr_employee1` FOREIGN KEY (`appraisee`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_opras_form_hs_hr_employee2` FOREIGN KEY (`supervisor`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payslip`
--
ALTER TABLE `payslip`
ADD CONSTRAINT `fk_payslip_hs_hr_employee1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_payslip_payroll1` FOREIGN KEY (`payroll_id`) REFERENCES `payroll` (`payroll_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pa_line`
--
ALTER TABLE `pa_line`
ADD CONSTRAINT `fk_pa_line_performance_agreement1` FOREIGN KEY (`pa_id`, `form_id`) REFERENCES `performance_agreement` (`pa_id`, `form_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pension_deduction`
--
ALTER TABLE `pension_deduction`
ADD CONSTRAINT `fk_pension_deduction_hs_hr_employee1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_pension_deduction_payroll1` FOREIGN KEY (`payroll_id`) REFERENCES `payroll` (`payroll_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_pension_deduction_pension1` FOREIGN KEY (`pension_id`) REFERENCES `pension` (`pension_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pension_employee`
--
ALTER TABLE `pension_employee`
ADD CONSTRAINT `fk_pension_has_hs_hr_employee_hs_hr_employee1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_pension_has_hs_hr_employee_pension1` FOREIGN KEY (`pension_id`) REFERENCES `pension` (`pension_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `performance_agreement`
--
ALTER TABLE `performance_agreement`
ADD CONSTRAINT `fk_performance_agreement_opras_form1` FOREIGN KEY (`form_id`, `appraisee`, `supervisor`) REFERENCES `opras_form` (`form_id`, `appraisee`, `supervisor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tax_deduction`
--
ALTER TABLE `tax_deduction`
ADD CONSTRAINT `fk_tax_deduction_hs_hr_employee1` FOREIGN KEY (`emp_number`) REFERENCES `hs_hr_employee` (`emp_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tax_deduction_payroll1` FOREIGN KEY (`payroll_id`) REFERENCES `payroll` (`payroll_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tax_deduction_tax_rate1` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rate` (`rate_id`) ON UPDATE CASCADE;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `leave_taken_status_change` ON SCHEDULE EVERY 1 HOUR STARTS '2014-10-20 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
                        UPDATE hs_hr_leave SET leave_status = 3 WHERE leave_status = 2 AND leave_date < DATE(NOW());
                      END$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
