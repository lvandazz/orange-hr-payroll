<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class PensionReport extends  PayrollResult{
    	private $employerShare;
		private $totalEmployeesPension = 0;
		private $totalEmployersPension = 0;
		private $totalLinePension = 0;
		private $totalPension = 0;
		
		function setEmployerShare( $share ){
			$this -> employerShare = $share;
		}
		/*
		 * 
		 */
		function setTotEmpPension( $total ){
			$this -> totalEmployeesPension = $total;
		}
		/*
		 * 
		 */
		function setTotEmplyrPension( $total ){
			$this -> totalEmployersPension = $total;
		}
		/*
		 * 
		 */
		function setTotLinePension( $total ){
			$this -> totalLinePension = $total;
		}
		
		/*
		 * 
		 */
		function getEmployerShare( $raw = true ){
			if( $raw == true ){
				return $this -> employerShare;
			}
			return number_format( $this -> employerShare );
		}
		/*
		 * 
		 */
		function getTotEmpPension( $raw = true ){
			if( $raw == true ){
				return $this -> totalEmployeesPension;
			}
			return number_format( $this -> totalEmployeesPension );
		}
		/*
		 * 
		 */
		function getTotEmplyrPension( $raw = true ){
			if( $raw == true ){
				return $this -> totalEmployersPension;
			}
			return number_format( $this -> totalEmployersPension );
		}
		/*
		 * 
		 */
		 function getTotLinePension( $raw = true ){
		 	if( $raw == true ){
		 		return $this -> totalLinePension; 
		 	}
			return number_format( $this -> totalLinePension );
		 }
		 /*
		  * 
		  */
		 function setTotalPension( $total ){
		 	$this -> totalPension = $total;
		 }
		 /*
		  * 
		  */
		 function getTotalPension ( $raw = true ){
		 	if( $raw == true ){
		 		return $this -> totalPension;
		 	}
			return number_format( $this -> totalPension );
		 }
		
		
    }
?>