<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class LoanReport extends PayrollResult{
    	
		private $loanPrincipal;
		private $loanAccAmount;
		private $totalPrincipal;
		private $totalAccAmount;
		private $totalDeduction;
		private $totalBalance;
		
		function setLoanPrincipal( $p ){
			$this -> loanPrincipal = $p;
		}
		/*
		 * 
		 */
		function getLoanPrincipal( $raw = true ){
			if( $raw == false ){
				return number_format( $this -> loanPrincipal );
			}
			return $this -> loanPrincipal;
		}
		/*
		 * 
		 */
		function setLoanAccAmount( $amount ){
			$this -> loanAccAmount = $amount;
		}
		/*
		 * 
		 */
		function getLoanAccAmount( $raw = true ){
			if( $raw == false ){
				return number_format( $this -> loanAccAmount );
			}
			return $this -> loanAccAmount;
		}
		
		/*
		 * 
		 */
		function setTotalPrincipal( $tp ){
			$this -> totalPrincipal = $tp;
		}
		/*
		 * 
		 */
		 function setTotalAccAmount( $amount ){
		 	$this -> totalAccAmount = $amount;
		 }
		 /*
		  * 
		  */
		 function setTotalLoanDeduction( $total ){
		 	$this -> totalDeduction = $total;
		 }
		 /*
		  * 
		  */
		 function getTotalPrincipal( $raw = true ){
		 	if( $raw == false ){
		 		return number_format( $this -> totalPrincipal);
		 	}
			return $this -> totalPrincipal;
		 }
		 /*
		  * 
		  */
		 function getTotalAcc( $raw = true ){
		 	if( $raw == false ){
		 		return number_format( $this -> totalAccAmount );
		 	}
			return $this -> totalAccAmount;
		 }
		 /*
		  * 
		  */
		 function  getTotalLoanDeduction( $raw = true ){
		 	if( $raw == false ){
		 		return number_format( $this -> totalDeduction );
		 	}
			return $this -> totalDeduction;
		 }
		/*
		 * 
		 */
		  function getTotalBalance( $raw = true ){
		 	if( $raw == false ){
		 		return number_format( $this -> totalBalance );
		 	}
			return $this -> totalAccAmount;
		 }
		 /*
		  * 
		  */
		 function  setTotalBalance( $total ) {
		 	$this -> totalBalance = $total;
		 }
    }
    
?>