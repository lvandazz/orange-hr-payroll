<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
  class DeductionReport extends  PayrollResult{
  	
	private $totalAmount = 0;
	private $totalBalance = 0;
	private $totalDeductionLine = 0; 
	private $title;
	private $amount;
	private $balance;
	private $deductionAmount;
	
	
	function setTotalAmount( $total ){
		$this -> totalAmount = $total;
	}
	/*
	 * 
	 */
	function setTotalBalance( $balance ){
		$this -> totalBalance = $balance;
	}
	/*
	 * 
	 */
	function setTotalLine( $total ){
		$this -> totalDeductionLine = $total;
	}
	/*
	 * 
	 */
	function getTotalAmount( $raw = true ){
		if( $raw == false ){
			return number_format( $this -> totalAmount );
		}
		return $this -> totalAmount;
	}
	/*
	 * 
	 */
	function getTotalBalance( $raw = true ){
		if( $raw == false ){
			return number_format( $this -> totalBalance );
		}
		return $this -> totalBalance;
	}
	/*
	 * 
	 */
	 function getTotalDeductionLine( $raw = true ){
	 	if( $raw == false ){
	 		return number_format( $this -> totalDeductionLine );
	 	}
		return $this -> totalDeductionLine;
	 }
	 /*
	  * 
	  */
	 function setTitle( $title ){
	 	$this -> title = $title;
	 }
	 /*
	  * 
	  */
	 function setBalance( $balance ){
	 	$this -> balance = $balance;
	 }
	 /*
	  * 
	  */
	 function setAmount( $amount ){
	 	$this -> amount = $amount;
	 }
	 /*
	  * 
	  */
	 function getTitle( ){
	 	return $this -> title;
	 }
	 /*
	  * 
	  */
	function getBalance( $raw = true ){
		if( $raw == false )
			return number_format( $this -> balance );
		
		return $this -> balance;
	}
	/*
	 * 
	 */
	function getAmount( $raw = true ){
		if( $raw == false )
			return number_format( $this -> amount );
		return $this -> amount;
	} 
	/*
	 * 
	 */
	function getDeductionAmount( $raw = true ){
		if( $raw == false ){
			return  number_format( $this -> deductionAmount );
		}
		return $this -> deductionAmount;
	}
	/*
	 * 
	 */
	function setDeductionAmount( $amount ){
		$this -> deductionAmount = $amount;
	}
  } 
  
  
  
  ?>