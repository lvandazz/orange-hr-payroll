<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */

class PayrollResult {

	private $payrollId;
	private $payDate;
	private $empNumber;
	private $employeeName;
	private $basicSalary;
	private $payeAmount;
	private $payePercent;
	private $pensionName;
	private $pensionEmployee;
	private $pensionEmployer;
	private $pensionId;
	private $taxRateId;
	private $paye;
	private $pension;
	private $takeHome;
	private $loanTitle;
	private $loanDeductionAmount;
	private $loanBalance;
    private $totalDeduction = 0;
	private $otherDeductions = array();
	
	function getPayRollId() {
		return $this -> payrollId;
	}

	/*
	 *
	 */
	function getPayDate( $pretty = TRUE ) {
		if( $pretty == TRUE ){
			$date = new DateTime( $this -> payDate ); 
			return $date -> format( 'F Y');
		}
		return $this -> getPayDate;
	}

	/*
	 *
	 */
	function getEmpNumber() {
		return $this -> empNumber;
	}

	/*
	 *
	 */
	function getEmployeeName() {
		return $this -> employeeName;
	}

	/*
	 *
	 */
	function getBasicSalary( $raw = TRUE ) {
		if ($raw == TRUE) {
			$nfcf = new NumberFormatCellFilter();
			return number_format($this -> basicSalary );
		}
		return $this -> basicSalary;
	}

	/*
	 *
	 */
	function getPayeAmount( $raw = TRUE ) {
		if ($raw == TRUE) {
			$nfcf = new NumberFormatCellFilter();
			return number_format($this -> payeAmount );
		}
		return $this -> payeAmount;
	}

	/*
	 *
	 */
	function getPayePercent() {
		return $this -> payePercent;
	}

	/*
	 *
	 */
	function getPensionName() {
		return $this -> pensionName;
	}

	/*
	 *
	 */
	function getPensionEmployee() {
		return $this -> pensionEmployee;
	}

	/*
	 *
	 */
	function getPensionEmployer() {
		return $this -> pensionEmployer;
	}

	/*
	 *
	 */
	function getPensionId() {
		return $this -> pensionId;
	}

	/*
	 *
	 */
	function getTaxRateId() {
		return $this -> taxRateId;
	}

	/*
	 *
	 */
	function setPayrollId($payrollId) {
		$this -> payrollId = $payrollId;
	}

	/*
	 *
	 */
	function setPayDate($payDate) {
		$this -> payDate = $payDate;
	}

	/*
	 *
	 */
	function setEmpNumber($empNumber) {
		$this -> empNumber = $empNumber;
	}

	/*
	 *
	 */
	function setEmployeeName($empName) {
		$this -> employeeName = $empName;
	}

	/*
	 *
	 */
	function setBasicSalary($basic) {
		$this -> basicSalary = $basic;
	}

	/*
	 *
	 */
	function setPayeAmount($amount) {
		$this -> payeAmount = $amount;
	}

	/*
	 *
	 */
	function setPayePercent($amount) {
		$this -> payePercent = $amount;
	}

	/*
	 *
	 */
	function setPensionId($id) {
		$this -> pensionId = $id;
	}

	/*
	 *
	 */
	function setTaxRateId($id) {
		$this -> taxRateId = $id;
	}

	/*
	 *
	 */
	function getPaye( $raw = TRUE ) {
		if ($raw == TRUE) {

			$nfcf = new NumberFormatCellFilter();
			return number_format($this -> paye );
		}
		return $this -> paye;
	}

	/*
	 *
	 */
	function setPaye($paye) {
		$this -> paye = $paye;
	}

	/*
	 *
	 */
	function getPension( $raw = TRUE ) {
		if ($raw == TRUE) {

			$nfcf = new NumberFormatCellFilter();
			return number_format($this -> pension );
		}
		return $this -> pension;
	}

	/*
	 *
	 */
	function setPension($pension) {
		$this -> pension = $pension;
	}

	/*
	 *
	 */
	function setPensionName($name) {
		$this -> pensionName = $name;
	}

	/*
	 *
	 */
	function getTakeHome($raw = TRUE) {
		if ($raw == TRUE) {

			$nfcf = new NumberFormatCellFilter();
			return number_format($this -> takeHome );
		}

		return $this -> takeHome;
	}

	/*
	 *
	 */
	function setTakeHome($takeHome) {
		$this -> takeHome = $takeHome;
	}
	/*
	 * 
	 */
	 function getLoanTitle(){
	 	return $this -> loanTitle;
	 }
	 /*
	  * 
	  */
	 function setLoanTitle($title){
	  	$this -> loanTitle = $title;
	  }
	 /*
	  * 
	  */
	  function getLoanDeductionAmount( $raw = false){
	  	if( $raw == true )
	  		return $this -> loanDeductionAmount;
		else {
			return number_format( $this -> loanDeductionAmount );
		}
	  }
	 /*
	  * 
	  */
	 function setLoanDeductionAmount( $amount ){
	 	$this -> loanDeductionAmount = $amount;
	 }
	/*
	 * 
	 */
	 function getLoanBalance( $raw = false ){
	 	if( $raw == true ){
	 		return $this -> loanBalance;
	 	}
		else{
			return number_format( $this -> loanBalance );
		}
	 }
	 /*
	  * 
	  */
	  function setLoanBalance( $balance ){
	  	$this -> loanBalance = $balance;
	  }
	  /*
	   * 
	   */
	   function addDeduction( $deduction ){
	   	 $this -> totalDeduction = $this -> totalDeduction +  $deduction;
	   }
	   /*
	    * 
	    */
	    function getTotalDeduction( $raw = true ){
	    	if( $raw == treu )
	    		return $this -> totalDeduction;
			else {
				return number_format( $this -> totalDeduction );
			}
	    }
		/*
		 * 
		 */
		function addOtherDeduction( Deduction $deduction ){
			
			if( $deduction  -> getIsRecurring() == 1 ){
				$this -> addDeduction( $deduction -> getRepaymentAmount());
			}
			else{
				
				$this -> addDeduction($deduction -> getAmount());
			}
			
			$this -> otherDeductions[ $deduction -> getDeductionId() ] = $deduction;
		}
		/*
		 * 
		 */
		function getOtherDeductions( $deductionId = null ){
			if( !is_null( $deductionId ) ){
				return $this -> otherDeductions[ $deductionId ];
			}
			return $this -> otherDeductions;
		}
		
}
?>