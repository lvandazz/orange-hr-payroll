<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
class PayslipResult extends  PayrollResult {
	private $payslipId;
	private $committed;
	private $dateCommited;
	private $jobTitle;
	
	function getPayslipId() {
		return $this -> payslipId;
	}
	/*
	 *
	 */
	function dateCommited() {
		return $this -> dateCommited;
	}

	/*
	 *
	 */
	function getCommitment() {
		return $this -> committed;
	}

	/*
	 *
	 */
	function setPayslipId($payslipId) {
		$this -> payslipId = $payslipId;
	}
	/*
	 *
	 */  
	function setPayrollPeriod($payrollPeriod) {
		$this -> payrollPeriod = $payrollPeriod;
	}
	/*
	 * 
	 */
	function setCommitment( $commitment ){
		if( $commitment == 0 ){
			$this -> committed = 'Pending';
		}
		if( $commitment == 1 ){
			$this -> committed = 'Approved';
		}
	}
	/*
	 * 
	 */
	 function getJobTitle(){
	 	return $this -> jobTitle;
	 }
	 /*
	  * 
	  */
	  function setJobTitle( $jobTitle ){
	  	$this -> jobTitle = $jobTitle;
	  }

}
