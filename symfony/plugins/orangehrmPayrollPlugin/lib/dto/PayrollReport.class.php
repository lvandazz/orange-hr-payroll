<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
class PayrollReport extends  PayrollResult{
	
	private $totalTax;
	private $totalPension;
	private $totalLoans;
	private $totalTakeHome;
	private $totalSalary;
	private $totalDeductions;
	
	function setTotalTax( $total ){
		$this -> totalTax = $total;
	}
	/*
	 * 
	 */
	function setTotalPension( $pension ){
		$this -> totalPension = $pension;
	}
	/*
	 * 
	 */
	function setTotalLoans( $loan ){
		$this -> totalLoans = $loan;
	}
	/*
	 * 
	 */
	 function setTotalTakeHome( $takehome ){
	 	$this -> totalTakeHome = $takehome;
	 }
	 /*
	  *  getters
	  */
	  function getTotalTax( $raw = true ){
	  	if( $raw == true ){
	  		return $this -> totalTax;
	  	}
		return number_format( $this -> totalTax);
	  }
	  /*
	   * 
	   */
	  function getTotalPension( $raw = true ){
	  	if( $raw == true ){
	  		return $this -> totalPension;
	  	}
		return number_format( $this -> totalPension );
	  }
	  /*
	   * 
	   */
	  function getTotalLoans( $raw = true ){
	  	if( $raw == true ){
	  		return $this -> totalLoans;
	  	}
		return number_format( $this -> totalLoans );
	  }
	  /*
	   * 
	   */
	  function getTotalTakeHome( $raw = true ){
	  	if( $raw == true ){
	  		return $this -> totalTakeHome;
	  	}
		return number_format( $this -> totalTakeHome );
	  }
	  /*
	   * 
	   */
	  function setTotalSalary( $total ){
	  	$this -> totalSalary = $total;
	  }
	  /*
	   * 
	   */
	  function getTotalSalary( $raw = true ){
	  	if( $raw == true ){
	  		return $this -> totalSalary;
	  	}
		return number_format( $this -> totalSalary );
	  }
	  /*
	   * 
	   */
	  function setTotalDeductions( $total ){
	  	$this -> totalDeductions = $total;
	  }
	  /*
	   * 
	   */
	  function getTotalDeductions( $raw = true ){
	  	$otherD = $this -> getOtherDeductions();
		
	  	if( $raw == true ){
	  		return $this -> totalDeductions;
	  	}
		return number_format( $this -> totalDeductions );
	  }
}
