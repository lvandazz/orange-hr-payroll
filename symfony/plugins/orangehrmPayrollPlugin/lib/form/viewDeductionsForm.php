<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class viewDeductionsForm extends BaseForm{
    	
		protected $deductionService;
		
		function getDeductionService(){
			if( is_null( $this -> deductionService ) ) {
				$this -> deductionService = new DeductionService();
			}
			return $this -> deductionService;
		}
		/*
		 * 
		 */
		function configure(){
			
			// get pay periods
			$resultEmployees = $this -> getDeductionService() -> getDeductions();
			$employees = array( null => 'All');
			$employeeService = new EmployeeService();
			
			if( $resultEmployees != null ){
				foreach( $resultEmployees as $employeeDeduction ){
					$employee = $employeeService -> getEmployee($employeeDeduction -> getEmpNumber());
					if( ! isset( $employees[ $employee -> getEmpNumber() ] ) ){
						$employees[ $employee -> getEmpNumber()] = $employee -> getFullName();	
						
					}
				}
			}
	 
			$status = array('' => 'All', 0 => 'Created' , 1 => 'Returning', 2 => 'Completed' );
			// create widgets 
			$widgets = array(
				'employeeD' => new sfWidgetFormSelect(array( 'choices' => $employees ),array( 'id' => 'employee' ) ),
				'statusD' => new sfWidgetFormSelect(array( 'choices' => $status ),array( 'id' => 'status' ) ),
				'isRecurring' => new sfWidgetFormSelect(array( 'choices' => array( '' => 'All', 1 => 'Yes', 0 => 'No' ) ),array( 'id' => 'isRecurring' ) )
			);
			
			// set validators
			$validators = array(
				'employeeD' => new sfValidatorString(array( 'required' => false ) ),
				'statusD' => new sfValidatorString(array( 'required' => false ) ),
				'isRecurring' => new sfValidatorString(array( 'required' => false ) ) 
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('viewDeductions[%s]');
			
			$employee = $this -> getOption('employee' );
			$status = $this -> getOption( 'status' );
			$isRecurring = $this -> getOption( 'isRecurring' );
			 
			if( ! is_null( $employee ) ){
				$this -> setDefault('employeeD' , $employee );
			}
			if( ! is_null( $status ) ){
				$this -> setDefault('statusD' , $status );
			}
			if( ! is_null( $isRecurring ) ){
				$this -> setDefault('isRecurring' , $isRecurring );
			}
			
		}
		
	/*
	 * 
	 */
	 
    }
?>