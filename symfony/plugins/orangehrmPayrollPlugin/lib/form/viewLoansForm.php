<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class viewLoansForm extends BaseForm{
    	
		protected $loanService;
		
		function getLoanService(){
			if( is_null( $this -> loanService ) ) {
				$this -> loanService = new LoanService();
			}
			return $this -> loanService;
		}
		/*
		 * 
		 */
		function configure(){
			
			$orgsService = new FinancialOrganizationService();
			$resultOrganizations = $orgsService -> getFinancialOrganizations();
			$organizations = array( null => 'All');
			
			
			if( $resultOrganizations != null ){
				foreach( $resultOrganizations as $organization ){
					$organizations[ $organization -> getOrganizationId() ] = $organization -> getName();
				}
			}
	 
			
			// create widgets 
			$widgets = array(
				'organization' => new sfWidgetFormSelect(array( 'choices' => $organizations ),array( 'id' => 'organization' ) )
			);
			
			// set validators
			$validators = array(
				'organization' => new sfValidatorString(array( 'required' => false ) )
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this-> widgetSchema -> setNameFormat('viewLoans[%s]');
			
			$organization = $this -> getOption('organization' );
			 
			if( ! is_null( $organization ) ){
				$this -> setDefault('organization' , $organization );
			}
			
		}
		
	/*
	 * 
	 */
	 
    }
?>