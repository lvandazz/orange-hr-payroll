<?php
/*
 * 
 * @author Joseph Luvanda - lvandazz@gmail.com
 * 
 */
 class AddAllowanceForm extends BaseForm{
 	private $allowanceService;
	private $allowanceId;
	/*
	 * 
	 * 
	 */
	function getAllowanceService()
	{
		if( is_null( $this -> allowanceService ) ){
			$this -> allowanceService = new AllowanceService();
			$this -> allowanceService -> setAllowanceDao( new AllowanceDao());
		}
		return $this -> allowanceService;
	}
	/*
	 * 
	 * 
	 */
	 function setAllowanceService( AllowanceService $allowanceService ){
	 	$this -> allowanceService = $allowanceService;
	 }
	/*
	 * 
	 * 
	 */
	 function configure()
	 {
	 	//No pre-loaded data
	 	$this -> allowanceId = $this -> getOption('allowanceId' );
		if( isset( $this -> allowanceId ) ){
			$allowance = $this -> getAllowanceDetails( $this -> allowanceId );
		}
	 	// create widgets
	 	$widgets = array(
			'allowanceTitle' =>  new sfWidgetFormInputText(array(), array( 'id' => 'allowanceTitle' )),
			'allowanceAmount' => new sfWidgetFormInputText(array(), array( 'id' => 'allowanceAmount'   )),
			'allowancePercentage' => new sfWidgetFormInputText(array( ) , array( 'id' => 'allowancePercentage')),
		);
		
		// set Validators
		$validators = array(
			'allowanceTitle' => new sfValidatorString(array( 'required' => false )),
			'allowanceAmount' => new sfValidatorNumber(array( 'required' => false , 'min' => 0 )),
			'allowancePercentage' => new sfValidatorNumber(array( 'required' => false , 'min' => 0 ))
		);
		
		$this -> setWidgets( $widgets );
		$this -> setValidators( $validators );
		$this->widgetSchema->setNameFormat('addAllowance[%s]');
		
		//
		if( isset( $allowance ) && $allowance != null  ){
			
			$this -> setDefault('allowanceTitle', $allowance -> getTitle());
			$this -> setDefault( 'allowanceAmount', $allowance -> getAllowanceAmount(true));
			$this -> setDefault('allowancePercentage', $allowance -> getPercentageBasic());
		}
		else{
			$this -> setDefault('allowanceAmount', 0 );
			$this -> setDefault( 'allowancePercentage' , 0 );
		}
			
	 }
	 /*
	  * 
	  * 
	  */
	  function save()
	  {
	  	
		if( empty( $this -> allowanceId ) ){
			$allowance = new Allowance();
				
		}else{
			
			$allowance = $this -> getAllowanceDetails($this -> allowanceId );
			
		}
	  	
		$allowance -> title = $this -> getValue( 'allowanceTitle' );
		$allowance -> amount = $this -> getValue( 'allowanceAmount' );
		$allowance -> perce_basic = $this -> getValue( 'allowancePercentage' );
	 	
		$this -> getAllowanceService() -> saveAllowance( $allowance );
		  
		return $allowance -> getID();
	  }
	  /*
	   * 
	   * 
	   */
	   function getAllowanceDetails( $allowanceId ){
	   	
			return $this -> getAllowanceService()-> getAllowanceById($allowanceId);
	   }
 
 }

