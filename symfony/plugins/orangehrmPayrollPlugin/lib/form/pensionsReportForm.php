<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class pensionsReportForm extends BaseForm{
    	
		protected $employeeService;
		protected $pimService;
		protected $reportService;
		protected $pensionService;
		protected $pensEmpService;
					
		/*
		 * 
		 */
		function configure(){
			$this -> employeeService = new EmployeeService();
			$this -> orgService = new Subunit();
			$this -> reportService = new ReportService();
			$this -> pensionService = new PensionService();
			$this -> pensEmpService = new PensionEmployeeService();
			
			$resultEmployees = $this -> employeeService -> getEmployeeList();
			
			$employees = array( null => 'All');
			$employeeService = new EmployeeService();
			
			if( $resultEmployees != null ){
				foreach( $resultEmployees as $employee ){
					///$employee = $employeeService -> getEmployee($employee -> getEmpNumber());
					if( ! isset( $employees[ $employee -> getEmpNumber() ] ) ){
						$employees[ $employee -> getEmpNumber()] = $employee -> getFullName();	
						
					}
				}
			}
	 
			$pensions = array('' => 'All');
			$resultPensions = $this -> pensionService -> getPensions();
			if( count( $resultPensions) > 0) {
				foreach( $resultPensions as $pension ){
					$pensions[ $pension -> getPensionId() ] = $pension -> getPensionName();
				}
			} 
			// create widgets 
			$widgets = array(
				'employeeD' => new sfWidgetFormSelect(array( 'choices' => $employees ),array( 'id' => 'employee' ) ),
				'pension' => new sfWidgetFormSelect(array( 'choices' => $pensions ),array( 'id' => 'pension' ) ),
				'fromDate' => new ohrmWidgetDatePicker(array(),array( 'id' => 'fromDate') ),
				'toDate' => new ohrmWidgetDatePicker(array(),array( 'id' => 'toDate' ) )
			);
			
			// set validators
			$validators = array(
				'employeeD' => new sfValidatorString(array( 'required' => false ) ),
				'pension' => new sfValidatorString(array( 'required' => false ) ),
				'fromDate' => new sfValidatorDate(array( 'required' => false ) ),
				'toDate' => new sfValidatorDate(array( 'required' => false ) )  
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('payrollReport[%s]');
			
			$employee = $this -> getOption('employee' );
			$pension = $this -> getOption( 'pension' );
			$fromDate = $this -> getOption( 'fromDate' );
			$toDate = $this -> getOption( 'toDate' );
			 
			if(  $employee == null  ){
				$this -> setDefault('employeeD' , $employee );
			}
			if( $pension == null  ){
				$this -> setDefault('pension' , $pension );
			}
			if( $fromDate  != null  ){
				$this -> setDefault('fromDate' , $fromDate );
			}
			else{
				$this -> setDefault('fromDate', date('Y-m-d'));
			}
			if( $toDate != null  ){
				$this -> setDefault('toDate' , $toDate );
			}
			else{
				$this -> setDefault('toDate', date('Y-m-d'));
			}
		}
		
	/*
	 * 
	 */
	 
    }
?>