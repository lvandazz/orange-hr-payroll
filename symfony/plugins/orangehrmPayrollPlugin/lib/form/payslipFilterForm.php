<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class payslipFilterForm extends BaseForm{
    	
		protected $employeeService;
		protected $pimService;
		protected $reportService;
					
		/*
		 * 
		 */
		function configure(){
			$this -> employeeService = new EmployeeService();
			$this -> orgService = new Subunit();
			$this -> reportService = new ReportService();
			 
			if( $this -> getOption( 'logged' ) > 0 ){
				$resultEmployees[]  = $this -> employeeService -> getEmployee( $this -> getOption( 'logged' ) );
				 
			}
			else{
				$employees = array( null => 'All');
				$resultEmployees = $this -> employeeService -> getEmployeeList();	
			}
			
			
			
			$employeeService = new EmployeeService();
			
			if( $resultEmployees != null ){
				foreach( $resultEmployees as $employee ){ 
					if( ! isset( $employees[ $employee -> getEmpNumber() ] ) ){
						$employees[ $employee -> getEmpNumber()] = $employee -> getFullName();	 
					}
				}
			}
	  		
			// create widgets 
			$widgets = array(
				'employeeD' => new sfWidgetFormSelect(array( 'choices' => $employees ),array( 'id' => 'employee' ) ),
				//'organization' => new sfWidgetFormSelect(array( 'choices' => $organizations ),array( 'id' => 'organization' ) ),
				'fromDate' => new ohrmWidgetDatePicker(array(),array( 'id' => 'fromDate') ),
				'toDate' => new ohrmWidgetDatePicker(array(),array( 'id' => 'toDate' ) )
			);
			
			// set validators
			$validators = array(
				'employeeD' => new sfValidatorString(array( 'required' => false ) ), 
				'fromDate' => new sfValidatorDate(array( 'required' => false ) ),
				'toDate' => new sfValidatorDate(array( 'required' => false ) )  
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('payrollReport[%s]'); 
			$employee = $this -> getOption('employee' ); 
			$fromDate = $this -> getOption( 'fromDate' );
			$toDate = $this -> getOption( 'toDate' );
			 
			 
			if(  $employee == null  ){
				$this -> setDefault('employeeD' , $employee );
			}
			 
			if( $fromDate  != null  ){
				$this -> setDefault('fromDate' , $fromDate );
			}
			else{
				$this -> setDefault('fromDate', date('Y-m-d'));
			}
			if( $toDate != null  ){
				$this -> setDefault('toDate' , $toDate );
			}
			else{
				$this -> setDefault('toDate', date('Y-m-d'));
			}
		}
		
	/*
	 * 
	 */
	 
    }
?>