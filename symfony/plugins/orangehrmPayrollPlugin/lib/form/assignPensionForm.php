<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class assignPensionForm extends BaseForm{
    	
		private $pensionService;
		private $pensionEmployeeService;
		private $penEmpId;
		
		function getPensionService(){
			if( is_null( $this -> pensionService ) ){
				$this -> pensionService = new PensionService();
			}
			return $this -> pensionService;
		}
		/*
		 * 
		 * 
		 */
		 function configure( ){
		 		
		 	$this -> penEmpId = $this -> getOption( 'penEmpId');
			if( isset( $this -> penEmpId  ) AND $this -> penEmpId != null ){
				$pensionEmployee = $this -> getPensionEmployeeService() -> getPenEmp( $this -> penEmpId );
			}
			
			// create widgets 
			$widgets = array(
				'employeeName' => new sfWidgetFormInputText( array(), array( 'id' => 'employeeName' , 'placeholder' => 'Type for hints...') ),
				'employeeNameId' => new sfWidgetFormInputHidden( array(), array( 'id' => 'employeeNameId') ),
				'pensionName' => new sfWidgetFormSelect(array( 'choices' => $this -> getPensionsList()), array( 'id' => 'pensionName' ) ),
				'dateJoin' => new ohrmWidgetDatePicker(array(), array('id' => 'dateJoin')),
			);
			
			// set validators
			$validators = array(
				'employeeName' => new sfValidatorString(array( 'required' => true ) ),
				'pensionName' => new sfValidatorString(array( 'required' => true ) ),
				'employeeNameId' => new sfValidatorNumber(array( 'required' => true ) ),
				'dateJoin' => new sfValidatorDate(array( 'required' => true ) ),
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('assignPension[%s]');
			
			if( isset( $pensionEmployee ) AND $pensionEmployee != null  ){
				
				$employeeService = new EmployeeService();
				 
				$employee = $employeeService -> getEmployee($pensionEmployee -> getEmpNumber());
				$pension = $this -> getPensionService() -> getPension( $pensionEmployee -> getPensionId());
				
				
				$this -> setDefault('employeeName' , $employee -> getFullName());
				$this -> setDefault( 'pensionName' , $pension -> getPensionId());
				$this -> setDefault( 'dateJoin' , $pensionEmployee -> getDateJoined());
				$this -> setDefault( 'employeeNameId', $employee -> getEmpNumber() );
			}
		 }	
		/*
		 * 
		 * 
		 */
		 function getPensionsList(){
		 	
			$pensions = $this -> getPensionService() -> getPensions();
			
	        $list = array("" => "-- " . __('Select') . " --");
	        foreach ($pensions as $pension) {
	            $list[$pension->getPensionID()] = strtoupper( $pension ->getPensionName() );
	        }
	        return $list;
		 }
		 /*
		  * 
		  * 
		  */
		  function getEmployeesAsJson(){
		  	
			$employeeService = new EmployeeService();
			$employeeService -> setEmployeeDao( new EmployeeDao() );
			
			$properties = array( 'empNumber' , 'firstName', 'middleName','lastName','termination_id' );
			$employeeList = $employeeService -> getEmployeePropertyList($properties, 'lastName', 'ASC', true );
			$jsonArray = array();
			foreach( $employeeList as $employee ){
				 $empNumber = $employee['empNumber'];
            	 $name = trim(trim($employee['firstName'] . ' ' . $employee['middleName'],' ') . ' ' . $employee['lastName']);
        
            	 $jsonArray[] = array('name' => $name, 'id' => $empNumber);
			}
			$jsonString = json_encode( $jsonArray );
		  	return $jsonString;
		  }
		  /*
		   * 
		   * 
		   */
		   function save(){
		   	
				if( empty( $this -> penEmpId ) ){
					$pensionEmployee = new PensionEmployee();
				}
				else{
					$pensionEmployee = $this -> getPensionEmployeeService() -> getPenEmp($this -> penEmpId );
				}
				
				$pensionEmployee -> emp_number = $this -> getValue( 'employeeNameId' );
				$pensionEmployee -> pension_id = $this -> getValue( 'pensionName' );
				$pensionEmployee -> date_joined = $this -> getValue( 'dateJoin' );
				
				return $this -> getPensionEmployeeService() -> savePensionEmployee($pensionEmployee);
		   }
		   /*
		    * 
		    * 
		    */
		    function getPensionEmployeeService(){
		    	if( is_null( $this -> pensionEmployeeService ) ){
		    		$this -> pensionEmployeeService = new PensionEmployeeService();
		    	}
				return $this -> pensionEmployeeService;
		    }
		  
    }
    
?>