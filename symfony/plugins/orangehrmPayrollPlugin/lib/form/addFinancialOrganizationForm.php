<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class addFinancialOrganizationForm extends BaseForm{
    	
		private $financialOrganizationService;
		private $organizationId;
		
		public function getFinancialOrganizationService(){
			if( is_null( $this -> financialOrganizationService ) ){
				$this -> financialOrganizationService = new FinancialOrganizationService();
			}
			return $this -> financialOrganizationService;
		}
		/*
		 * 
		 * 
		 */
		 function configure(){
		 	 
			$this -> organizationId = $this -> getOption( 'organizationId');
			if( isset( $this -> organizationId  ) AND $this -> organizationId != null ){
				$organization = $this -> getFinancialOrganizationService() -> getFinancialOrganizations( $this -> organizationId ) -> getFirst();
			}
			
			// create widgets 
			$widgets = array(
				'organizationName' => new sfWidgetFormInputText( array(), array( 'id' => 'organizationName') ),
				'organizationAddress' => new sfWidgetFormInputText(array(), array( 'id' => 'organizationAddress' ) ),
				'organizationContact' => new sfWidgetFormInputText( array(), array( 'id' => 'organizationContact') ),
				'organizationInfo' => new sfWidgetFormTextarea( array(), array( 'id' => 'organizationInfo'))
			);
			
			// set validators
			$validators = array(
				'organizationName' => new sfValidatorString(array( 'required' => true ) ),
				'organizationAddress' => new sfValidatorString(array( 'required' => true ) ),
				'organizationContact' => new sfValidatorString(array( 'required' => true ) ),
				'organizationInfo' => new sfValidatorString(array( 'required' => false ) ),
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('addFinancialOrganization[%s]');
			
			if( isset( $organization ) AND $organization != null  ){
				$this -> setDefault('organizationName' , $organization -> getName());
				$this -> setDefault( 'organizationAddress' , $organization -> getAddress());
				$this -> setDefault( 'organizationContact' , $organization -> getContactNumber());
				$this -> setDefault( 'organizationInfo' , $organization -> getExtraInfo());
			}
		 }
		 /*
		  * 
		  * 
		  */
		  function save(){
		  	
			if( empty( $this -> organizationId ) ){
				$organization = new FinancialOrganization();
			}else{
				$organization = $this -> getFinancialOrganizationService() -> getFinancialOrganizations($this -> organizationId ) -> getFirst();
			} 
			$organization -> name = $this -> getValue( 'organizationName' );
			$organization -> address = $this -> getValue( 'organizationAddress' );
			$organization -> contact_number = $this -> getValue( 'organizationContact' );
			$organization -> extra_info = $this -> getValue( 'organizationInfo' );
			
			return $this -> getFinancialOrganizationService() -> saveFinancialOrganization( $organization );
		  }
		/*
		 * 
		 */
		 function getOrganizationsAsJSON(){
		 	$organizations = $this -> getFinancialOrganizationService() -> getFinancialOrganizations();
			
			$jsonArray = array();
			if( count( $organizations ) ){
				foreach( $organizations as $organization ){
					$jsonArray[] = array( 'name' => $organization -> getName(), 'id' => $organization -> getOrganizationId() );
				}
			}
			
			return json_encode( $jsonArray);
		 }
    }
    
?>