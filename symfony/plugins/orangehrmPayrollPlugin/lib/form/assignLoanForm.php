<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class assignLoanForm extends BaseForm{
    	
		private $loanService; 
		private $empLoandId;
		
		function getLoanService(){
			if( is_null( $this -> loanService ) ){
				$this -> loanService = new LoanService();
			}
			return $this -> loanService;
		}
		/*
		 * 
		 * 
		 */
		 function configure( ){
		 		
		 	$this -> empLoandId = $this -> getOption( 'empLoanId');
			   
			// create widgets 
			$widgets = array(
				'employeeName' => new sfWidgetFormInputText( array(), array( 'id' => 'employeeName' , 'placeholder' => 'Type for hints...') ),
				'loanTitle' => new sfWidgetFormSelect(array( 'choices' => $this -> getLoansList()), array( 'id' => 'loanTitle' ) ),
				'empId' => new sfWidgetFormInputHidden( array(), array( 'id' => 'empId') ),
				'dateJoin' => new ohrmWidgetDatePicker(array(), array('id' => 'dateJoin')),
				'monthRepayment' => new sfWidgetFormInputText( array(), array( 'id' => 'monthRepayment')),
				'repaymentStart' => new ohrmWidgetDatePicker(array(), array('id' => 'repaymentStart')),
				'extraInfo' => new sfWidgetFormTextarea(array(),array( 'id' => 'extraInfo' ))
				
			);
			
			// set validators
			$validators = array(
				'employeeName' => new sfValidatorString(array( 'required' => true ) ),
				'loanTitle' => new sfValidatorString(array( 'required' => true ) ),
				'empId' => new sfValidatorNumber(array( 'required' => false ) ),
				'dateJoin' => new sfValidatorDate(array( 'required' => true ) ),
				'repaymentStart' => new sfValidatorDate(array( 'required' => true ) ),
				'monthRepayment' => new sfValidatorNumber(array( 'required' => true ) ),
				'extraInfo' => new sfValidatorString(array( 'required' => false ) ),
				
			);
			
			if( isset( $this -> empLoandId  ) AND $this -> empLoandId != null ){ 
				$employeeLoan = $this -> getLoanService() -> getEmployeesLoans( $this -> empLoandId ) -> getFirst();
				$widgets[ 'balance' ] = new sfWidgetFormInputText( array(), array( 'id' => 'balance','disabled' => 'disabled' ) );
				$validators['balance' ] = new sfValidatorNumber( array('required' => false ) );
			}
			
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('assignLoan[%s]');
			
			if( isset( $employeeLoan ) AND $employeeLoan != null  ){
				
				$employeeService = new EmployeeService();
				 
				$employee = $employeeService -> getEmployee($employeeLoan -> getEmpNumber());
				$loan = $this -> getLoanService() -> getLoans( $employeeLoan -> getLoanId()) -> getFirst();
				
				
				$this -> setDefault('employeeName' , $employee -> getFullName());
				$this -> setDefault( 'loanTitle' , $loan -> getId());
				$this -> setDefault( 'dateJoin' , $employeeLoan -> getDateJoined());
				$this -> setDefault( 'empId', $employeeLoan -> getEmpNumber() );
				$this -> setDefault( 'repaymentStart', $employeeLoan -> getRepaymentStart() );
				$this -> setDefault( 'monthRepayment', $employeeLoan -> getMonthRepayment() );
				$this -> setDefault( 'extraInfo', $employeeLoan -> getExtraInfo() );
				$this -> setDefault( 'balance', $employeeLoan -> getBalance() );
				
			}
		 }	
		/*
		 * 
		 * 
		 */
		 function getLoansList(){
		 	
			$loans = $this -> getLoanService() -> getLoans();
			
	        $list = array("" => "-- " . __('Select') . " --");
	        foreach ($loans as $loan) {
	            $list[$loan->getID()] = $loan ->getTitle().' - '.number_format( $loan -> getPrincipal( ) ).' - '. strtoupper( $loan -> getOrganization() );
	        }
	        return $list;
		 }
		 /*
		  * 
		  * 
		  */
		  function getEmployeesAsJson(){
		  	
			$employeeService = new EmployeeService();
			$employeeService -> setEmployeeDao( new EmployeeDao() );
			
			$properties = array( 'empNumber' , 'firstName', 'middleName','lastName','termination_id' );
			$employeeList = $employeeService -> getEmployeePropertyList($properties, 'lastName', 'ASC', true );
			$jsonArray = array();
			foreach( $employeeList as $employee ){
				 $empNumber = $employee['empNumber'];
            	 $name = trim(trim($employee['firstName'] . ' ' . $employee['middleName'],' ') . ' ' . $employee['lastName']);
        
            	 $jsonArray[] = array('name' => $name, 'id' => $empNumber);
			}
			$jsonString = json_encode( $jsonArray );
		  	return $jsonString;
		  }
		  /*
		   * 
		   * 
		   */
		   function save(){
		   	
				// check for pending loans
				if( $this -> getLoanService() -> employeeHasPendingLoans( $this -> getValue( 'empId' ) ) ){
					return -1; // Employee has pending loans
				}
				
				if( empty( $this -> empLoandId ) ){
					$employeeLoan = new EmployeeLoan();
				}
				else{
					$employeeLoan = $this -> getLoanService() -> getEmployeesLoans($this -> empLoandId );
				}
				 
				
				$employeeLoan -> emp_number = $this -> getValue( 'empId' );
				$employeeLoan -> loan_id = $this -> getValue( 'loanTitle' );
				$employeeLoan -> date_joined = $this -> getValue( 'dateJoin' );
				$employeeLoan -> repayment_start = $this -> getValue( 'repaymentStart' );
				$employeeLoan -> month_repayment = $this -> getValue( 'monthRepayment' );
				$employeeLoan -> extra_info = $this -> getValue( 'extraInfo' );  
				$loan = $this -> loanService -> getLoans( $this -> getValue( 'loanTitle' ) ) -> getFirst();
				$loanInterest = $loan -> getInterest();
				
				/*if( isset( $loanInterest ) && $loanInterest != null && $loanInterest  > 0 ){
					$a =  $this -> getPrincipal() + ( $this -> getPrincipal() * $this -> getInterest() * $this -> getTerm() /100 );
					$balance = $a;
				}else{
					$balance = $loan -> getPrincipal();
				}*/
				$balance = $loan -> getAccruedAmount( true );
				$employeeLoan -> balance = $balance;
 				return $this -> getLoanService() -> saveEmployeeLoan($employeeLoan);
		   }
		  /*
		   * 
		   */
		  function getLoanRepaymentsAsJson(){
		  	$loanService = new LoanService();
			$loans = $loanService -> getLoans();
			$repayments = array();
			foreach( $loans as $loan ){
				$repayment = $loan -> getMonthRepaymentX( true );
				$repayments[] = array( 'loanId' => $loan -> getId(), 'repayment' => $repayment );
			}
			return json_encode( $repayments );
		  }
    }
    
?>