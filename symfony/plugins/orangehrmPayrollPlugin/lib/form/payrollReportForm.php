<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class payrollReportForm extends BaseForm{
    	
		protected $employeeService;
		protected $pimService;
		protected $reportService;
					
		/*
		 * 
		 */
		function configure(){
			$this -> employeeService = new EmployeeService();
			$this -> orgService = new Subunit();
			$this -> reportService = new ReportService();
			
			$resultEmployees = $this -> employeeService -> getEmployeeList();
			
			$employees = array( null => 'All');
			$employeeService = new EmployeeService();
			
			if( $resultEmployees != null ){
				foreach( $resultEmployees as $employee ){
					///$employee = $employeeService -> getEmployee($employee -> getEmpNumber());
					if( ! isset( $employees[ $employee -> getEmpNumber() ] ) ){
						$employees[ $employee -> getEmpNumber()] = $employee -> getFullName();	
						
					}
				}
			}
	 
			$subunits = array('' => 'All');
			$resultSubunits = $this -> reportService -> getSubunits();
			if( count( $resultSubunits) > 0) {
				foreach( $resultSubunits as $subunit ){
					$subunits[ $subunit -> getId() ] = $subunit -> getName();
				}
			} 
			// create widgets 
			$widgets = array(
				'employeeD' => new sfWidgetFormSelect(array( 'choices' => $employees ),array( 'id' => 'employee' ) ),
				'subunit' => new sfWidgetFormSelect(array( 'choices' => $subunits ),array( 'id' => 'subunit' ) ),
				'fromDate' => new ohrmWidgetDatePicker(array(),array( 'id' => 'fromDate') ),
				'toDate' => new ohrmWidgetDatePicker(array(),array( 'id' => 'toDate' ) )
			);
			
			// set validators
			$validators = array(
				'employeeD' => new sfValidatorString(array( 'required' => false ) ),
				'subunit' => new sfValidatorString(array( 'required' => false ) ),
				'fromDate' => new sfValidatorDate(array( 'required' => false ) ),
				'toDate' => new sfValidatorDate(array( 'required' => false ) )  
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('payrollReport[%s]');
			
			$employee = $this -> getOption('employee' );
			$subunit = $this -> getOption( 'subunit' );
			$fromDate = $this -> getOption( 'fromDate' );
			$toDate = $this -> getOption( 'toDate' );
			 
			if(  $employee == null  ){
				$this -> setDefault('employeeD' , $employee );
			}
			if( $subunit == null  ){
				$this -> setDefault('subunit' , $subunit );
			}
			if( $fromDate  != null  ){
				$this -> setDefault('fromDate' , $fromDate );
			}
			else{
				$this -> setDefault('fromDate', date('Y-m-d'));
			}
			if( $toDate != null  ){
				$this -> setDefault('toDate' , $toDate );
			}
			else{
				$this -> setDefault('toDate', date('Y-m-d'));
			}
		}
		
	/*
	 * 
	 */
	 
    }
?>