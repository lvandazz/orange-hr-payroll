<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Term - lvandazz@gmail.com
 */

class addLoanForm extends BaseForm {

	private $loanService;
	private $loanId;

	public function getLoanService() {
		if (is_null($this -> loanService)) {
			$this -> loanService = new LoanService();
		}
		return $this -> loanService;
	}

	/*
	 *
	 *
	 */
	function configure() {

		$this -> loanId = $this -> getOption('loanId');
		if (isset($this -> loanId) AND $this -> loanId != null) {
			$loan = $this -> getloanService() -> getLoans($this -> loanId) -> getFirst();
		}

		// create widgets
		$widgets = array(
		'loanTitle' => new sfWidgetFormInputText( array(), array('id' => 'loanTitle')), 
		'loanPrincipal' => new sfWidgetFormInputText( array(), array('id' => 'loanPrincipal')), 
		'loanTerm' => new sfWidgetFormInputText( array(), array('id' => 'loanTerm')),
		'loanDescription' => new sfWidgetFormTextarea( array(), array('id' => 'loanDescription')),
		'loanOrganization' => new sfWidgetFormSelect( array('choices' => $this -> getOrganizationsAsArray()), array('id' => 'loanOrganization')),
		'loanInterest' => new sfWidgetFormInputText( array(), array('id' => 'loanInterest')));

		// set validators
		$validators = array('loanTitle' => new sfValidatorString( array('required' => true)), 'loanPrincipal' => new sfValidatorNumber( array('required' => true)), 'loanTerm' => new sfValidatorInteger( array('required' => true)), 'loanDescription' => new sfValidatorString( array('required' => false)), 'loanInterest' => new sfValidatorNumber( array('required' => true)), 'loanOrganization' => new sfValidatorInteger( array('required' => true)), );
		$this -> setWidgets($widgets);
		$this -> setValidators($validators);
		$this -> widgetSchema -> setNameFormat('addLoan[%s]');

		if (isset($loan) AND $loan != null) {
			$this -> setDefault('loanTitle', $loan -> getTitle());
			$this -> setDefault('loanPrincipal', $loan -> getPrincipal());
			$this -> setDefault('loanTerm', $loan -> getTerm());
			$this -> setDefault('loanInterest', $loan -> getInterest());
			$this -> setDefault('loanOrganization', $loan -> getOrganizationId());
			$this -> setDefault('loanDescription', $loan -> getDescription());

		}
	}

	/*
	 *
	 *
	 */
	function save() {
		$loanEmps = null;
		if (empty($this -> loanId)) {
			$loan = new Loan();
		} else {
			$loan = $this -> getLoanService() -> getLoans( $this -> loanId ) -> getFirst();
			//get loan employees
			$loanEmps = $this -> getLoanService() -> getEmployeesByLoan( $this -> loanId );

		}
		 
		if (!is_null($loanEmps)) {
			if (count($loanEmps) > 0) {
				return -1;
			}
		}

		$loan -> title = $this -> getValue('loanTitle');
		$loan -> principal = $this -> getValue('loanPrincipal');
		$loan -> term = $this -> getValue('loanTerm');
		$loan -> interest = $this -> getValue('loanInterest');
		$loan -> description = $this -> getValue('loanDescription');
		$loan -> organization_id = $this -> getValue('loanOrganization');

		return $this -> getLoanService() -> saveLoan($loan);
	}

	/*
	 *
	 */
	function getOrganizationsAsArray() {
		$organizationService = new FinancialOrganizationService();
		$organizations = $organizationService -> getFinancialOrganizations();

		$OrgsArray = array();
		if (count($organizations)) {
			foreach ($organizations as $loan) {
				$OrgsArray[$loan -> getOrganizationId()] = $loan -> getName();
			}
		}

		return $OrgsArray;
	}

}
?>