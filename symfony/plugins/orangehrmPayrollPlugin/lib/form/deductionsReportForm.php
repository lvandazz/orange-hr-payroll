<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class deductionsReportForm extends BaseForm{
    	
		protected $employeeService;
		protected $pimService;
		protected $reportService;
					
		/*
		 * 
		 */
		function configure(){
			$this -> employeeService = new EmployeeService();
			
			$resultEmployees = $this -> employeeService -> getEmployeeList();
			
			$employees = array( null => 'All');
			$employeeService = new EmployeeService();
			
			if( $resultEmployees != null ){
				foreach( $resultEmployees as $employee ){
					///$employee = $employeeService -> getEmployee($employee -> getEmpNumber());
					if( ! isset( $employees[ $employee -> getEmpNumber() ] ) ){
						$employees[ $employee -> getEmpNumber()] = $employee -> getFullName();	
						
					}
				}
			}
			$recurrance = array( '' => 'All', 0 => 'Non-recurring', 1 => 'Recurring' );
			$status = array( '' => 'All', 0 => 'Pending', 1 => 'Returning', 2 => 'Completed' );
	 
			// create widgets 
			$widgets = array(
				'employeeD' => new sfWidgetFormSelect(array( 'choices' => $employees ),array( 'id' => 'employee' ) ),
				'isRecurring' => new sfWidgetFormSelect(array( 'choices' => $recurrance ),array( 'id' => 'recurrance' ) ),
				'status' => new sfWidgetFormSelect(array( 'choices' => $status ),array( 'id' => 'status' ) ),
				'fromDate' => new ohrmWidgetDatePicker(array(),array( 'id' => 'fromDate') ),
				'toDate' => new ohrmWidgetDatePicker(array(),array( 'id' => 'toDate' ) )
			);
			
			// set validators
			$validators = array(
				'employeeD' => new sfValidatorString(array( 'required' => false ) ),
				'isRecurring' => new sfValidatorString(array( 'required' => false ) ),
				'fromDate' => new sfValidatorDate(array( 'required' => false ) ),
				'status' => new sfValidatorString(array( 'required' => false ) ),
				'toDate' => new sfValidatorDate(array( 'required' => false ) )  
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('payrollReport[%s]');
			
			$employee = $this -> getOption('employee' );
			$fromDate = $this -> getOption( 'fromDate' );
			$toDate = $this -> getOption( 'toDate' );
			$status = $this -> getOption( 'status' );
			$isRecurring = $this -> getOption( 'isRecurring' );
			 
			 
			if(  $employee != null  ){
				$this -> setDefault('employeeD' , $employee );
			}
			
			if( $fromDate  != null  ){
				$this -> setDefault('fromDate' , $fromDate );
			}
			else{
				$this -> setDefault('fromDate', date('Y-m-d'));
			}
			if( $toDate != null  ){
				$this -> setDefault('toDate' , $toDate );
			}
			else{
				$this -> setDefault('toDate', date('Y-m-d'));
			}
			if( $isRecurring != null ){
				$this -> setDefault('isRecurring', $isRecurring);
			}
			if( $status != null ){
				$this -> setDefault('status' , $status );
			}
		}
		
	/*
	 * 
	 */
	 
    }
?>