<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class loansReportForm extends BaseForm{
    	
		protected $employeeService;
		protected $pimService;
		protected $reportService;
					
		/*
		 * 
		 */
		function configure(){
			$this -> employeeService = new EmployeeService();
			$this -> orgService = new Subunit();
			$this -> reportService = new ReportService();
			$financialOrgService  = new FinancialOrganizationService();
			
			$resultEmployees = $this -> employeeService -> getEmployeeList();
			
			$employees = array( null => 'All');
			$employeeService = new EmployeeService();
			
			if( $resultEmployees != null ){
				foreach( $resultEmployees as $employee ){
					///$employee = $employeeService -> getEmployee($employee -> getEmpNumber());
					if( ! isset( $employees[ $employee -> getEmpNumber() ] ) ){
						$employees[ $employee -> getEmpNumber()] = $employee -> getFullName();	
						
					}
				}
			}
	 
			$organizations = array('' => 'All');
			$resultOrgs = $financialOrgService -> getFinancialOrganizations();
			if( count( $resultOrgs) > 0) {
				foreach( $resultOrgs as $organization ){
					$organizations[ $organization -> getOrganizationId() ] = $organization -> getName();
				}
			} 
			// create widgets 
			$widgets = array(
				'employeeD' => new sfWidgetFormSelect(array( 'choices' => $employees ),array( 'id' => 'employee' ) ),
				'organization' => new sfWidgetFormSelect(array( 'choices' => $organizations ),array( 'id' => 'organization' ) ),
				'fromDate' => new ohrmWidgetDatePicker(array(),array( 'id' => 'fromDate') ),
				'toDate' => new ohrmWidgetDatePicker(array(),array( 'id' => 'toDate' ) )
			);
			
			// set validators
			$validators = array(
				'employeeD' => new sfValidatorString(array( 'required' => false ) ),
				'organization' => new sfValidatorString(array( 'required' => false ) ),
				'fromDate' => new sfValidatorDate(array( 'required' => false ) ),
				'toDate' => new sfValidatorDate(array( 'required' => false ) )  
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('payrollReport[%s]');
			
			$employee = $this -> getOption('employee' );
			$organization = $this -> getOption( 'organization' );
			$fromDate = $this -> getOption( 'fromDate' );
			$toDate = $this -> getOption( 'toDate' );
			 
			 
			if(  $employee == null  ){
				$this -> setDefault('employeeD' , $employee );
			}
			if( $organization == null  ){
				$this -> setDefault('organization' , $organization );
			}
			if( $fromDate  != null  ){
				$this -> setDefault('fromDate' , $fromDate );
			}
			else{
				$this -> setDefault('fromDate', date('Y-m-d'));
			}
			if( $toDate != null  ){
				$this -> setDefault('toDate' , $toDate );
			}
			else{
				$this -> setDefault('toDate', date('Y-m-d'));
			}
		}
		
	/*
	 * 
	 */
	 
    }
?>