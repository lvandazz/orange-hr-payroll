<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class runPayrollForm extends BaseForm{
    	
		protected $payrollService;
		
		function getPayrollService(){
			if( is_null( $this -> payrollService ) ) {
				$this -> payrollService = new PayrollService();
			}
			
			return $this -> payrollService;
		}
		/*
		 * 
		 */
		function configure(){
			
			// get pay periods
			$payperiods = $this -> getPayrollService() -> getPayPeriods();
			// create widgets 
			$widgets = array(
				'payDate' => new sfWidgetFormSelect(array( 'choices' => $payperiods ), 
				array( 'id' => 'payDate' ) )
				
			);
			
			// set validators
			$validators = array(
				'payDate' => new sfValidatorString(array( 'required' => true ) )
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('runPayroll[%s]');
			
			$month = $this -> getOption('selectedMonth' );
			  
			if( ! is_null( $month ) ){
				$this -> setDefault('payDate' , $month );
			}
			
		}
    }
?>