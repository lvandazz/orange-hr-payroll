<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class addPensionForm extends BaseForm{
    	
		private $pensionService;
		private $pensionId;
		
		public function getPensionService(){
			if( is_null( $this -> pensionService ) ){
				$this -> pensionService = new PensionService();
			}
			return $this -> pensionService;
		}
		/*
		 * 
		 * 
		 */
		 function configure(){
		 	 
			$this -> pensionId = $this -> getOption( 'pensionId');
			if( isset( $this -> pensionId  ) AND $this -> pensionId != null ){
				$pension = $this -> getPensionService() -> getPension( $this -> pensionId );
			}
			
			// create widgets 
			$widgets = array(
				'pensionName' => new sfWidgetFormInputText( array(), array( 'id' => 'pensionName') ),
				'percentEmployer' => new sfWidgetFormInputText(array(), array( 'id' => 'percentEmployer' ) ),
				'percentEmployee' => new sfWidgetFormInputText( array(), array( 'id' => 'percentEmployee') )
			);
			
			// set validators
			$validators = array(
				'pensionName' => new sfValidatorString(array( 'required' => true ) ),
				'percentEmployer' => new sfValidatorNumber(array( 'required' => true ) ),
				'percentEmployee' => new sfValidatorNumber(array( 'required' => true ) ),
			);
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('payrollReport[%s]');
			
			if( isset( $pension ) AND $pension != null  ){
				$this -> setDefault('pensionName' , $pension -> getPensionName());
				$this -> setDefault( 'percentEmployer' , $pension -> getPercentEmployer());
				$this -> setDefault( 'percentEmployee' , $pension -> getPercentEmployee());
			}
		 }
		 /*
		  * 
		  * 
		  */
		  function save(){
		  	
			if( empty( $this -> pensionId ) ){
				$pension = new Pension();
			}else{
				$pension = $this -> getPensionService() -> getPension($this -> pensionId );
			}
			if( $this -> getValue( 'percentEmployer' ) + $this -> getValue( 'percentEmployee' ) != 20 ){
				return -20;
			}
			$pension -> pension_name = $this -> getValue( 'pensionName' );
			$pension -> percent_employer = $this -> getValue( 'percentEmployer' );
			$pension -> percent_employee = $this -> getValue( 'percentEmployee' );
			
			return $this -> getPensionService() -> savePension( $pension );
			
			
		  }
    }
    
?>