<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class addDeductionForm extends BaseForm{
    	
		private $deductionService; 
		private $deductionId;
		
		function getDeductionService(){
			if( is_null( $this -> deductionService ) ){
				$this -> deductionService = new DeductionService();
			}
			return $this -> deductionService;
		}
		/*
		 * 
		 * 
		 */
		 function configure( ){
		 		
		 	$this -> deductionId = $this -> getOption( 'deductionId');
			   
			// create widgets 
			$widgets = array(
				'employeeName' => new sfWidgetFormInputText( array(), array( 'id' => 'employeeName' , 'placeholder' => 'Type for hints...') ),
				'deductionTitle' => new sfWidgetFormInputText(array(),array( 'id' => 'deductionTitle' ) ),
				'empId' => new sfWidgetFormInputHidden( array(), array( 'id' => 'empId') ),
				'amount' => new sfWidgetFormInputText( array(), array( 'id' => 'amount')),
				'repaymentAmount' => new sfWidgetFormInputText( array(), array( 'id' => 'repaymentAmount', 'disabled' => 'disabled')),
				'paymentStart' => new ohrmWidgetDatePicker(array(), array('id' => 'paymentStart')),
				'isRecurring' => new sfWidgetFormInputCheckbox( array(), array('id' => 'isRecurring' )),
				'extraInfo' => new sfWidgetFormTextarea(array(),array( 'id' => 'extraInfo' ))
				
			);
			
			// set validators
			$validators = array(
				'employeeName' => new sfValidatorString(array( 'required' => true ) ),
				'deductionTitle' => new sfValidatorString(array( 'required' => true ) ),
				'empId' => new sfValidatorNumber(array( 'required' => false ) ), 
				'amount' => new sfValidatorNumber(array( 'required' => true ) ),
				'extraInfo' => new sfValidatorString(array( 'required' => false ) ),
				'repaymentAmount' => new sfValidatorNumber(array( 'required' => false )),
				'isRecurring' => new sfValidatorNumber( array( 'required' => false ) ),
				'paymentStart' => new sfValidatorDate( array( 'required' => false )),
			);
			
			if( isset( $this -> deductionId  ) AND $this -> deductionId != null ){ 
				$employeeDeduction = $this -> getDeductionService() -> getDeductions( $this -> deductionId ) -> getFirst();
				$widgets[ 'balance' ] = new sfWidgetFormInputText( array(), array( 'id' => 'balance','disabled' => 'disabled' ) );
				$validators['balance' ] = new sfValidatorNumber( array('required' => false ) );
				$widgets[ 'dateCreated' ] = new sfWidgetFormInputText( array(), array( 'id' => 'date_created' ) );
				$validators[ 'dateCreated' ] = new sfValidatorDate(array( 'required' => false ) );
			}
			
			$this -> setWidgets( $widgets );
			$this -> setValidators( $validators );
			$this->widgetSchema->setNameFormat('addDeduction[%s]');
			
			if( isset( $employeeDeduction ) AND $employeeDeduction != null  ){
				
				$employeeService = new EmployeeService();
				 
				$employee = $employeeService -> getEmployee($employeeDeduction -> getEmpNumber());
				//$deduction = $this -> getDeductionService() -> getDeductions( $employeeDeduction -> getDeductions()) -> getFirst();
				
				$this -> setDefault('employeeName' , $employee -> getFullName());
				$this -> setDefault( 'deductionTitle' , $employeeDeduction -> getTitle());
				$this -> setDefault( 'dateCreated' , $employeeDeduction -> getDateCreated());
				$this -> setDefault( 'balance', number_format($employeeDeduction -> getBalance() ));
				$this -> setDefault( 'paymentStart', $employeeDeduction -> getPaymentStart() );
				$this -> setDefault( 'amount', $employeeDeduction -> getAmount() );
				$this -> setDefault( 'extraInfo', $employeeDeduction -> getDescription() );
				$this -> setDefault( 'empId' , $employee -> getEmpNumber());
				if( $employeeDeduction -> getIsRecurring() == 1 )
					$this -> setDefault( 'isRecurring' , $employeeDeduction -> getIsRecurring());
				
				$this -> setDefault( 'repaymentAmount' , $employeeDeduction -> getRepaymentAmount());
				
			}
			
		 }	
		
		 /*
		  * 
		  * 
		  */
		  function getEmployeesAsJson(){
		  	
			$employeeService = new EmployeeService();
			$employeeService -> setEmployeeDao( new EmployeeDao() );
			
			$properties = array( 'empNumber' , 'firstName', 'middleName','lastName','termination_id' );
			$employeeList = $employeeService -> getEmployeePropertyList($properties, 'lastName', 'ASC', true );
			$jsonArray = array();
			foreach( $employeeList as $employee ){
				 $empNumber = $employee['empNumber'];
            	 $name = trim(trim($employee['firstName'] . ' ' . $employee['middleName'],' ') . ' ' . $employee['lastName']);
        
            	 $jsonArray[] = array('name' => $name, 'id' => $empNumber);
			}
			$jsonString = json_encode( $jsonArray );
		  	return $jsonString;
		  }
		  /*
		   * 
		   * 
		   */
		   function save(){
		   	
				$balance = 0;
				
				if( empty( $this -> deductionId ) ){
					$employeeDeduction = new Deduction();
					
				}
				else{
					
					$employeeDeduction = $this -> getDeductionService() -> getDeductions( $this -> deductionId ) -> getFirst();
					 
				}
				 
				$employeeDeduction -> title = $this -> getValue( 'deductionTitle' );
				$employeeDeduction -> emp_number = $this -> getValue( 'empId' );
				$employeeDeduction -> amount = $this -> getValue( 'amount' );
				$employeeDeduction -> description = $this -> getValue( 'description' );
				$employeeDeduction -> payment_start = $this -> getValue( 'paymentStart' );
				$employeeDeduction -> date_created =  date( 'Y-m-d' );
				$employeeDeduction -> balance  = $this -> getValue( 'amount' );
				$employeeDeduction -> description = $this -> getValue( 'extraInfo' );  
				$employeeDeduction -> is_recurring = ( ( $this -> getValue( 'isRecurring' ) == null || $this -> getValue( 'isRecurring' ) == 0 ) ? 0 : 1 );
				$employeeDeduction -> status = 0 ;
				
				if( $this -> getValue( 'isRecurring' ) == null  || $this -> getValue( 'isRecurring' ) == 0 ){
					$employeeDeduction -> repayment_amount = null;
				}else{
					$employeeDeduction -> repayment_amount = $this -> getValue( 'repaymentAmount' );
				}
				
		 
 				return $this -> getDeductionService() -> saveEmployeeDeduction($employeeDeduction);
		   }
		  /*
		   * 
		   */
		  function getLoanRepaymentsAsJson(){
		  	$loanService = new LoanService();
			$loans = $loanService -> getLoans();
			$repayments = array();
			foreach( $loans as $loan ){
				$repayment = $loan -> getMonthRepaymentX( true );
				$repayments[] = array( 'loanId' => $loan -> getId(), 'repayment' => $repayment );
			}
			return json_encode( $repayments );
		  }
    }
    
?>