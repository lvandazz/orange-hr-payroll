<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
 class PensionDeductionDao extends  BaseDao{
 	
	function savePensionDeduction( PensionDeduction $pensionDeduction ){
		
		try{
			if ($pensionDeduction -> getDeductionId() == '') {
				$idGenService = new IDGeneratorService();
				$idGenService -> setEntity($pensionDeduction);
				$pensionDeduction -> setDeductionId(NULL);
			}
			
			$pensionDeduction -> save();
			return $pensionDeduction -> getDeductionId();
			 
		}catch( Exception $e ){
			throw new DaoException( $e -> getMessage());
		}
	}
 }
