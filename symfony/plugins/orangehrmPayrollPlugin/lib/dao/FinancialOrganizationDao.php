<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
 	
 class FinancialOrganizationDao extends  BaseDao{
 	
	function getFinancialOrganizations( $organizationId = null ){
		
		try{
			$query = Doctrine_Query :: create()
					-> from( 'FinancialOrganization' )
					-> select('*');
					
			if( $organizationId != null ){
				$query -> addWhere( 'organization_id =?', $organizationId );
			}
			return $query -> execute();
			
		}catch( Exception $e ){
			throw new DaoException( $e -> getMessage());
		}
	}
	/*
	 * 
	 */
	 function saveFinancialOrganization( FinancialOrganization $organization ){
	 	try{
	 		if ($organization -> getOrganizationId() == '') {
			$idGenService = new IDGeneratorService();
			$idGenService -> setEntity($payroll);
			//$payroll -> setPayrollId($idGenService -> getNextID());
			//print_r( $payroll );
		}

		$organization -> save();
		return $organization -> getOrganizationId();
	 	}catch( Exception $e ){
	 		throw new DaoException( $e -> getMessage() );
	 	}
	 }
	  
	  /*
	   * 
	   */
	   function deleteOrganization( $organizationIds ){
		 	
			
			try{
			
				$qr = Doctrine_Query :: create()
							-> from( 'FinancialOrganization' )
							-> whereIn( 'organization_id' , $organizationIds )
							-> delete();
				
				$noOfAffectedRows = $qr -> execute();
				if( $noOfAffectedRows > 0 ){
					return true;
				}
				return false;
								
				}catch( Exception $e ){
			 		throw new DaoException( $e -> getMessage());
			 }
			 
		 }
	   /*
	    * 
	    */
	   function getOrganizationLoans( $OrganizationId ){
	   	try{
	   		$q  = Doctrine_Query :: create()
				-> from( 'Loan' )
				-> where( 'organization_id = ?' , $OrganizationId );
				 
			return $q -> execute();
	   	}catch( Exception $e ){
	   		throw new DaoException( $e -> getMessage());
	   	}
	   }
 }
 ?>
