<?php

class PayrollDao extends BaseDao {

	function getMonthPayroll($currentDate) {

		try {
			$dateObj = new DateTime($currentDate);
			$dateParts = explode('-', $currentDate);
			$dateParts[2] = 31;
			$lastDate = implode('-', $dateParts);

			$query = Doctrine_Query::create() -> from('Payroll') -> addWhere('pay_date >= ?', $currentDate) -> addWhere('pay_date <= ?', $lastDate);

			return $query -> execute();
		} catch( Exception $e ) {
			throw new DaoException($e -> getMessage());
		}

	}

	/*
	 *
	 *
	 */
	function savePayroll(Payroll $payroll) {

		if ($payroll -> getPayrollId() == '') {
			$idGenService = new IDGeneratorService();
			$idGenService -> setEntity($payroll);
			//$payroll -> setPayrollId($idGenService -> getNextID());
			//print_r( $payroll );
		}

		$payroll -> save();
		return $payroll -> getPayrollId();
	}

	/*
	 *
	 */
	function getPayroll($payrollId) {
		try {
			return Doctrine::getTable('Payroll') -> find($payrollId);

		} catch( Exception $e ) {
			throw new DaoException($e -> getMessage());
		}

	}
	/*
	 * 
	 */
	 function getPayrolls( ){
	 	try{
	 		
	 		$q = Doctrine_Query :: create() -> from( 'Payroll' );
			return $q -> execute();
			
	 	}catch( Exception $e ){
	 		throw new DaoException( $e -> getMessage( ) );
	 	}
	 }

}
