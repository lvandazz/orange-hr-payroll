<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class  ReportDao extends  BaseDao{
    	
		function getSubunits(){
			try{
				$q = Doctrine_Query :: create()
						-> from( 'Subunit' );
			    return $q -> execute();
			}catch( Exception $e ){
				throw new DaoException( $e -> getMessage( ));
			}
		}
		/*
		 * 
		 */
		function getPayrollsByDates( $from, $to ){
			try{
				$q = Doctrine_Query :: create()
						-> from( 'Payroll' )
						-> where( 'pay_date >= ? AND pay_date <= ?', array( $from, $to ))
						-> orderBy( 'payroll_id DESC');
						
				return $q -> execute();	
				
			}catch( Exception $e ){
				throw new DaoException( $e -> getMessage());
			}
		}
		/*
		 * 
		 */
		 function getSubunitEmployees( $subunitId ){
		 	try{
		 		$q = Doctrine_Query :: create()
						 -> from( 'Employee' )
						 -> where( 'work_station =?' , $subunitId );
						 
				return $q -> execute();
				
		 	}catch( Exception $e ){
		 		throw new DaoException( $e -> getMessage());
		 	}
		 }
    }
    
?>