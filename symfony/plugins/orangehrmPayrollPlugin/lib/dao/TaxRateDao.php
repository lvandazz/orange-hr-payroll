<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class TaxRateDao extends  BaseDao{
    	
		/*
		 * 
		 * 
		 */
		 function getTaxRates(){
		 	try{
		 		$query = Doctrine_Query :: create()
						-> from( 'TaxRate' )
						-> select( '*' );
						
				return $query ->  execute();
				
		 	}catch( Exception $e ){
		 		throw new DaoException( $e -> getMessage( ));
		 	}
		 }
		 /*
		  * 
		  * 
		  */
		  function getTaxRate( $salary , $rateId = null){
		  	try{
		  		if( $rateId != null ){
		  			$q = Doctrine_Query :: create()
						-> from( 'TaxRate')
						-> where( 'rate_id =?' , $rateId );
						
					return $q ->execute();
		  		}
		  		$query = Doctrine_Query :: create()
						-> from('TaxRate tr' )
						-> addWhere( 'tr.salary_from <= ?',$salary)
						-> addWhere( 'tr.salary_to >= ?', $salary )
						-> select('*' );
						
						
				$result =  $query -> execute();
				if( count( $result ) != 0 ){
					return $result;
				}
				$query = Doctrine_Query :: create()
						-> from( 'TaxRate tr' ) 
						-> where( 'tr.salary_from >=?', $salary )
						-> where( 'tr.salary_to = ?', '>' );
				return $query -> execute();
				
		  	}catch( Exception $e ){
		  		throw new DaoException( $e -> getMessage());
		  	}
		  }
    }
?>