<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */

class PaySlipDao extends BaseDao {

	function getEmployeePaySlips($empNumber, $payroll = NULL) {
		try {
			$query = Doctrine_Query::create() -> from('PaySlip p') -> addWhere('p.emp_number = ?', $empNumber);
			if ($payroll != NULL) {
				$query -> addWhere('p.payroll_id = ?', $payroll);
			}
			return $query -> execute();

		} catch( Exception $e ) {
			throw new DaoException($e -> getMessage());
		}
	}

	/*
	 *
	 */
	function savePayslip(Payslip $payslip) {

		try {

			if ($payslip -> getPayslipId() == '') {
				$idGenService = new IDGeneratorService();
				$idGenService -> setEntity($payslip);
				$payslip -> setPayslipId(NULL);
			}

			$payslip -> save();
			return $payslip -> getPayslipId();

		} catch( Exception $e ) {
			throw new DaoException($e -> getMessage());
		}
	}
	/*
	 * 
	 */
	 function getPayslips( $payroll = null, $empNumber =null){
	 	try{
	 		$query = Doctrine_Query :: create()
					-> from( 'PaySlip p' )
					-> orderBy('p.payslip_id DESC'  );
					
	 		if( $payroll != null ){
	 			$query ->  addWhere( 'p.payroll_id =?' ,$payroll );
	 		}
			if( $empNumber != null ){
				$query -> addWhere( 'p.emp_number =?', $empNumber );
			}
			return $query -> execute();
					
	 	}catch( Exception $e ){
	 		throw new DaoException( $e -> getMessage());
	 	}
	 }
	 /*
	  * 
	  */
	  function getPayslip( $payslipId ){
	  	try{
	  		$query = Doctrine_Query :: create()
					-> from( 'Payslip p' )
					-> addWhere( 'p.payslip_id =?' , $payslipId )
					-> select( '*' )
					-> limit( 1 );
	  		return $query -> execute();
	  	}catch( Exception $e ){
	  		throw new DaoException( $e -> getMessage());
	  	}
	  }
}
