<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
  class PensionEmployeeDao extends BaseDao{
  	
	function savePensionEmployee( PensionEmployee $pensionEmployee ){
		
			try {

	            if ($pensionEmployee ->getPenEmpID() == '') {
	                $idGenService = new IDGeneratorService();
	                $idGenService->setEntity($pensionEmployee);
	                $pensionEmployee->setPenEmpID($idGenService->getNextID());	
			   }
			    
	           $pensionEmployee -> save();
			   
	           return $pensionEmployee -> getPenEmpID();
			   
	        } catch (Exception $e) {
	            throw new DaoException($e->getMessage());
        		}
	}
	/*
	 * 
	 * 
	 */
	 function getPenEmp( $penEmpId = NULL, $empNumber = NULL){
	 	
	 		try{
	 			if( $empNumber == NULL )
					return Doctrine :: getTable('PensionEmployee' ) -> findBy( 'pen_emp_id' , $penEmpId );
				if( $penEmpId == NULL ){
					return Doctrine :: getTable('PensionEmployee' ) -> findBy( 'emp_number' , $empNumber );
				}	
				return null;
	 		}catch( Exception $e ){
	 			throw new DaoException( $e -> getMessage( ));
	 		}
	 }
	 /*
	  * 
	  * 
	  */
	  function getPensionsEmployees( $pensionId=null ){
	  	try{
	  		$query =Doctrine_Query::create();
	  		if( $pensionId == null )
	  			$query 
						///-> select( 'pe.date_joined,e.firstName,e.lastName,p.pension_name')
                        ->from('PensionEmployee pe')
                        ->leftJoin('pe.Employee e')
                        ->leftJoin('pe.Pension p');
			else{
				$query  -> from( 'PensionEmployee' ) -> where('pension_id = ?', $pensionId );
			}
			return $query -> execute( );
			
	  	}catch( Exception $e ){
	  		throw new DaoException( $e-> getMessage());
	  	}
	  }
	  /*
	   * 
	   */
	function getPensionDeduction( $payrollId, $empNumber, $pensionId = null ){
		try{
			if( $pensionId != null ){
				$q = Doctrine_Query :: create()
					-> from('PensionDeduction' )
					-> where( 'pension_id = ?', $pensionId );
				return $q -> execute();	
			}
			
			$q = Doctrine_Query :: create()
				-> from( 'PensionDeduction' )
				-> where( 'payroll_id =? AND emp_number =?', array( $payrollId , $empNumber ) ); 
			return $q -> execute();
			
		}catch( Exception $e ){
			throw new DaoException($e -> getMessage());
		}
	}
	/*
	 * 
	 */
	    
  } 