<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
 class TaxDeductionDao extends BaseDao{
 	
	function saveTaxDeduction( TaxDeduction $taxDeduction ){
		
		try{
			
			if ($taxDeduction -> getDeductionId() == '') {
				$idGenService = new IDGeneratorService();
				$idGenService -> setEntity($taxDeduction);
				$taxDeduction -> setDeductionId(NULL);
			}
			
			$taxDeduction -> save();
			return $taxDeduction -> getDeductionId();
			
		}catch( Exception $e ){
			throw new DaoException ( $e -> getMessage( ));
		}
	}
	/*
	 * 
	 */
	function getTaxDeduction( $payrollId , $empNumber ){
		try{
			$q = Doctrine_Query :: create()
				 -> from( 'TaxDeduction' )
				 -> where( 'payroll_id =? AND emp_number =?', array( $payrollId, $empNumber ) );
			 
			return $q -> execute();
				 
		}catch( Exception $e ){
			throw new DaoException( $e -> getMessage( ));
		}
	}
 }
