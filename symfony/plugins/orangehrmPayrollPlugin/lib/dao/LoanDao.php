<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */

class LoanDao extends  BaseDao {

	function getLoans($loanId = null,$organization = null ) {
		try {
		 
				$query = Doctrine_Query::create() -> from('Loan');
				
				if( $organization != null ){ 
					$query -> addWhere( 'organization_id =?', $organization );
			 		}
				
				if( $loanId != null ){
					$query -> AddWhere( 'id =?', $loanId ); 
					}
				 
				 
				if( $loanId != null ){
					#return $query -> execute() -> getFirst();
				}
				
			return $query -> execute();

		} catch( Exception $e ) {
			throw new DaoException($e -> getMessage());
		}
	}

	/*
	 *
	 */
	function saveLoan(Loan $loan) {
		try {
			
			 if ($loan -> getId() == '') {
				$idGenService = new IDGeneratorService();
				$idGenService -> setEntity($loan);
				$loan -> setId( $idGenService -> getNextID());
			} 
			$loan -> save();
			return $loan -> getId();
			
		} catch( Exception $e ) {
			throw new DaoException($e -> getMessage());
		}
	}
	/*
	 * 
	 */
	 function getEmployeesByLoan( $loanId ){
	 	try{
	 		 
	 		return Doctrine :: getTable('EmployeeLoan') -> findBy('loan_id',$loanId);
	 	}catch( Exception $e ){
	 		throw new DaoException( $e -> getMessage( ));
	 	}
	 }
	 /*
	  * 
	  * 
	  */
	  function deleteLoans( $loanIds ){
	  	try{
				$affectedRows = 0;
				foreach( $loanIds as $loanId ){
					$loanEmps = Doctrine_Query :: create()
							-> from( 'EmployeeLoan' )
							-> where( 'loan_id =?', $loanId ) -> execute();
							
					if( count( $loanEmps ) > 0  ){
						return -1; // loan has employees
					}			
				}
				
				foreach ($loanIds as $loanId ) {
					   $result  = Doctrine_Query :: create()
							-> from( 'Loan l' ) 
							-> delete()
							-> where( 'l.id = ?',  $loanId  ); 
					   $affectedRows += $result -> execute(); 
				}
				
			if( $affectedRows == count( $loanIds )){
				return true;
			}
			return false;
							
			}catch( Exception $e ){
		 		throw new DaoException( $e -> getMessage());
			 }
	  }
	  /*
	   * 
	   */
	function getEmployeesLoans( $empLoanId  = null, $empId = null ,$loanId=null){
		try{
			
			
			$q = Doctrine_Query :: create()
				-> from( 'EmployeeLoan' );
				
			if( !is_null( $empLoanId )){
				$q -> where( 'employee_loan_id =?', $empLoanId );
			}
			if(  $empId  != null ) {
				$q -> where( 'emp_number =?' , $empId );
			}
			if( $loanId != null ){
				$q -> addWhere( 'loan_id =?', $loanId );
			}
			$q = $q -> execute();
			
		return $q;
		
		}catch( Exception $e ){
			throw new DaoException( $e -> getMessage());
		}
	}
	/*
	 * 
	 * 
	 */
	 function saveEmployeeLoan( EmployeeLoan $employeeLoan ){
	 	try{
	 		 if ($employeeLoan -> getEmployeeLoanId() == '') {
				$idGenService = new IDGeneratorService();
				$idGenService -> setEntity($employeeLoan);
				$employeeLoan -> setEmployeeLoanId( $idGenService -> getNextID());
			} 
			
	 		$employeeLoan -> save();
			return $employeeLoan -> getEmployeeLoanId();
			
	 	}catch( Exception $e ){
	 		throw new DaoException( $e -> getMessage( ) );
	 	}
	 }
	 /*
	  * 
	  */
	 function getEmployeePendingLoans( $empNumber ){
	 	try{
	 		
			$q = Doctrine_Query :: create()
				-> from( 'EmployeeLoan el' )
				-> where( 'el.emp_number =?',$empNumber )
				-> addWhere( 'el.status !=?', 2);
			return $q -> execute();
			
	 	}catch( Exception $e ){
	 		throw new DaoException( $e -> getMessage( ) );
	 	}
		
	 }
	 /*
	  * 
	  */
	  function saveLoanDeduction( LoanDeduction $loanDeduction ){
	  	
	  		try{
		 		 if ($loanDeduction -> getLoanDeductionId() == '') {
					$idGenService = new IDGeneratorService();
					$idGenService -> setEntity($loanDeduction);
					$loanDeduction -> setLoanDeductionId( $idGenService -> getNextID());
				} 
		 		$loanDeduction -> save();
				return $loanDeduction -> getLoanDeductionId();
				
		 		}catch( Exception $e ){
		 			throw new DaoException( $e -> getMessage( ) );
		 		} 
		}
	  /*
	   * 
	   */
	   function getLoanDeduction( $loanDeductionId = null, $payrollId = null , $empNumber = null ){
	   	
		try{
			$q = Doctrine_Query :: create()
					-> from( 'LoanDeduction' );
					
			if( isset( $loanDeductionId )){
				$q -> addWhere( 'loan_deduction_id =?' , $loanDeductionId );	
			}
			if( isset( $payrollId ) && isset( $empNumber) ){
				$q -> addWhere( 'payroll_id=? AND emp_number=?', array( $payrollId,$empNumber ));
			}
			
			return $q -> execute();
			
		}catch( Exception $e ){
			throw  new DaoException( $e -> getMessage() );
		}
	   }
	  /*
	   * 
	   */
	   function deleteEmployeesLoans( $empLoanIds ){
	   
	   	try{
	   		
		   		$affectedRows = 0;
				
				foreach( $empLoanIds as $empLoanId ){
						
						$empLoans = Doctrine_Query :: create()
								-> from( 'EmployeeLoan' )
								-> where( 'loan_id =?', $loanId ) -> execute();
								
						if( count( $loanEmps ) > 0  ){
							return -1; // loan has employees
						}			
					}
					
					foreach ($empLoanIds as $loanId ) {
						   $result  = Doctrine_Query :: create()
								-> from( 'Loan l' ) 
								-> delete()
								-> where( 'l.id = ?',  $loanId  ); 
						   $affectedRows += $result -> execute(); 
					}
					
				if( $affectedRows == count( $empLoanIds )){
					return true;
				}
				return false;
								
				}catch( Exception $e ){
			 		throw new DaoException( $e -> getMessage());
				 }
	   }
}
?>