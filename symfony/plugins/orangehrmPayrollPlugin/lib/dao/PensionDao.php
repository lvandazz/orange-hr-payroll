<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */

class PensionDao extends  BaseDao {

	/*
	 *
	 *
	 */
	function getPensions() {
		try {
			$query = Doctrine_query::create() -> from('Pension');
			return $query -> execute();
		} catch( exception $e ) {
			throw new DaoException($e -> getMessage());
		}
	}

	/*
	 *
	 */
	function getPension($pensionId) {
		try {
			
			return Doctrine::getTable('Pension') -> find($pensionId);
		} catch( Exception $e ) {
			throw new DaoException($e -> getMessage());
		}
	}

	/*
	 *
	 *
	 */
	function savePension(Pension $pension) {
		try {

			if ($pension -> getPensionID() == '') {
				$idGenService = new IDGeneratorService();
				$idGenService -> setEntity($pension);
				$pension -> setPensionID($idGenService -> getNextID());
			}

			$pension -> save();

			return $pension -> getPensionID();
		} catch (Exception $e) {
			throw new DaoException($e -> getMessage());
		}
	}
	
	/*
	 * 
	 */
	function deletePension( $pensionId ){
		try{
			
			$q = Doctrine_Query :: create()
					-> from( 'Pension' )
					-> delete( )
					-> where('pension_id =?',$pensionId);
					
			 if( $q -> execute() > 0 ){
			 	return true;
			 }
			 return false;
					
		}catch( Exception $e ){
			throw new DaoException($e -> getMessage());
		}
		
	}	
}
?>