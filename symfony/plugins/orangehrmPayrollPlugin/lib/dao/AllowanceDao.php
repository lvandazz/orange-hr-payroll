<?php

/*
 * 
 * 
 * 
 */
 class AllowanceDao extends BaseDao
 {
 	
	
	function getAllowanceList( )
	{
		try{
			$query = Doctrine_Query :: create()
					-> select( 'allowance_id', 'title', 'amount', 'percentage_basic')
					-> from ( 'allowance' );
					
					
			return $query-> execute();
		}
		catch( Exception $e )
		{
			throw new DaoException( $e -> getMessage());
		}
	}
	/*
	 * 
	 * 
	 */
	 function saveAllowance( Allowance $allowance ){
	 	 try {

            if ($allowance ->getID() == '') {
                $idGenService = new IDGeneratorService();
                $idGenService->setEntity($allowance);
                $allowance->setID($idGenService->getNextID());
			
            ////	   if(   $allowance -> getID()  == ''  ){
		   	$allowance -> setID( NULL );	
		   }
		   
		   //die( $allowance -> getID().' '.$allowance -> getAllowanceAmount().' '.$allowance->getPercentageBasic().' '.$allowance->getTitle());
		  
           $allowance -> save();
		   
            return true;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
	 	
	 }
	 /*
	  * 
	  * 
	  */
	  function getAllowanceById( $allowanceId ){
	  	try{
	  		return Doctrine :: getTable('Allowance' ) -> find( $allowanceId );
	  	}catch( Exception $e ){
	  		throw new DaoException( $e -> getMessage ());
	  	}
				
	  }
	 	/*
		 * 
		 * 
		 * 
		 */
		 function deleteAllowances( $allowanceIds ){
		 	
			
			try{
				
				$query = Doctrine_Query :: create()
						-> from( 'allowance' )
						-> whereIn( 'allowance_id' , $allowanceIds)
						-> select( '*' );
				
				
				foreach( $results as $result ){
					
					$result -> delete( );
				}
				
			$qr = Doctrine_Query :: create()
					-> from( 'allowance' )
					-> delete( )
					-> whereIn( 'allowance_id' , $allowanceIds );
			
			$noOfAffectedRows = $qr -> execute();
			if( $noOfAffectedRows > 0 ){
				return true;
			}
			return false;
							
			}catch( Exception $e ){
		 		throw new DaoException( $e -> getMessage());
			 }
			 
		 }
 }
