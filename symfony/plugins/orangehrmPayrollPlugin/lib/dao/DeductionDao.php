<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */

 class DeductionDao extends BaseDao{
 	
	/*
	 * 
	 */
	 function getDeductions( $deductionId = null, $empNumber = null , $recurring = null, $status = null ){
	 	
		try{
			$q = Doctrine_Query :: create() -> from( 'Deduction' );
			if( ! is_null( $status ) ){
				
				if( is_array( $status ) ){ 
					$q -> whereIn( 'status',$status ); 
				}
				else{
					$q -> addWhere( 'status =?', $status );	
				}
				
			}
			if( ! is_null( $deductionId ) ){
				$q -> addWhere('deduction_id = ?', $deductionId );
			}
			if( $empNumber != null  ){
				$q -> addWhere( 'emp_number =?', $empNumber );
			}
			if(  $recurring != null ) {
				$q -> addWhere( 'is_recurring =?' , $recurring );
			} 
			 
			return $q -> execute();
			
		}catch( Exception $e ){
			throw new DaoException( $e -> getMessage());
		}
	 }
	 /*
	  * 
	  */
	 function saveDeduction( Deduction $deduction ){
	 	
	 	try{
	 		if ($deduction -> getDeductionId() == '') {
				$idGenService = new IDGeneratorService();
				$idGenService -> setEntity($deduction);
				$deduction -> setDeductionId( $idGenService -> getNextID());
			} 
			$deduction -> save();
			return $deduction -> getDeductionId();
			
	 	}catch( Exception $e ){
	 		throw new DaoException( $e -> getMessage( ) );
	 	}
		
	 }
	 
	/*
	 * 
	 */
	function getDeductionLines( $deductionId = null, $payrollId = null, $empNumber = null ){
		try{
			$q = Doctrine_Query :: create() -> from( 'DeductionLine' );
			if( $deductionId != null  ) {
				$q -> addWhere( 'deduction_id =?', $deductionId );
			}
			if( $payrollId != null ){
				$q -> addWhere( 'payroll_id = ?', $payrollId );
			}
			if(  $empNumber != null ){
				$q -> addWhere( 'emp_number =?', $empNumber );
			} 
			return $q -> execute();
		}catch( Exception $e ){
			throw new DaoException( $e -> getMessage());
		}
		
	}
	/*
	 * 
	 */
	 function saveDeductionLine( DeductionLine $deductionLine ){
	 	try{
	 		 if ($deductionLine -> getLineId() == '') {
				$idGenService = new IDGeneratorService();
				$idGenService -> setEntity($deductionLine);
				$deductionLine -> setLineId( $idGenService -> getNextID());
			}
			$deductionLine -> save();
			return $deductionLine -> getLineId();	
					 
	 	}catch( Exception $e ){
	 		throw new DaoException( $e -> getMessage());
	 	}
	 }
 }
