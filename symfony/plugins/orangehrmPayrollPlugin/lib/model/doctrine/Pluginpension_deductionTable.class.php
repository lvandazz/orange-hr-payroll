<?php

/**
 * Pluginpension_deductionTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Pluginpension_deductionTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object Pluginpension_deductionTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Pluginpension_deduction');
    }
}