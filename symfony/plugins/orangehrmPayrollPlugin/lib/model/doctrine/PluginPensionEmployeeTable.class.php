<?php

/**
 * PluginPensionEmployeeTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginPensionEmployeeTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginPensionEmployeeTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginPensionEmployee');
    }
}