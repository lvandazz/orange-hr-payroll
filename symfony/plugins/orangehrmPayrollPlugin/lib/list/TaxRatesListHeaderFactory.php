<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class TaxRatesListHeaderFactory extends  ohrmListConfigurationFactory{
    	/*
		 * 
		 * 
		 */	
    	public function init(){
    		$header1 = new ListHeader();
			$header2 = new ListHeader();
			$header3 = new ListHeader();
			$header4 = new ListHeader();
			$header5 = new ListHeader();
			$header6 = new ListHeader();
			
			$header1-> populateFromArray(array(
			    'name' => 'Salary From ',
			    'width' => '20%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getSalaryF'),
			));
			$header2-> populateFromArray(array(
			    'name' => 'Salary To ',
			    'width' => '20%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getSalaryT'),
			));
			$header3-> populateFromArray(array(
			    'name' => 'Rate Amount',
			    'width' => '20%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getRateAmnt'),
			));
			$header4-> populateFromArray(array(
			    'name' => 'Rate Percentage',
			    'width' => '20%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getRatePerce'),
			));
			$header5-> populateFromArray(array(
			    'name' => 'As Of',
			    'width' => '20%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getAs'),
			));
			
			$this-> headers = array( $header1, $header2,$header3,$header4,$header5 );
    	}
		/*
		 * 
		 * 
		 */
		 public function getClassName() {
			return 'TaxRates';
	}
    }
?>