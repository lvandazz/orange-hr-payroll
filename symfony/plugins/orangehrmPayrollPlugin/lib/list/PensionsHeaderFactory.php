<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class PensionsHeaderFactory extends  ohrmListConfigurationFactory{
    	/*
		 * 
		 * 
		 */	
    	public function init(){
    		$header1 = new ListHeader();
			$header2 = new ListHeader();
			$header3 = new ListHeader();
			$header4 = new ListHeader();
		 
			$header1->populateFromArray(array(
		    'name' => 'Pension',
		    'width' => '50%',
		    'isSortable' => false,
		   // 'sortField' => 'v.name',
		    'elementType' => 'link',
		    'elementProperty' => array(
			'labelGetter' => 'getPensionName',
			'placeholderGetters' => array('pensionId' => 'getPensionID'),
			'urlPattern' => 'addPension?pensionId={pensionId}'),
		));
			$header2-> populateFromArray(array(
			    'name' => 'Percent Employer',
			    'width' => '25%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getPercentEy'),
			));
			$header3-> populateFromArray(array(
			    'name' => 'Percent Employee',
			    'width' => '25%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getPercentEe'),
			));
		 
			
			$this-> headers = array( $header1, $header2,$header3  );
    	}
		/*
		 * 
		 * 
		 */
		 public function getClassName() {
			return 'Pensions';
	}
    }
?>