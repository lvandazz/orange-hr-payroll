<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    class PayrollResultsHeaderFactory extends  ohrmListConfigurationFactory{
    	public function init(){
    		$header1 = new ListHeader();
			$header2 = new ListHeader();
			$header3 = new ListHeader();
			$header4 = new ListHeader();
			$header5 = new ListHeader();
			$header6 = new ListHeader();
			$header1->populateFromArray(array(
		    'name' => 'Employee',
		    'width' => '20%',
		    'isSortable' => false,
		   // 'sortField' => 'v.name',
		    'elementType' => 'link',
		    'elementProperty' => array(
			'labelGetter' => 'getEmployeeName',
			'placeholderGetters' => array('payrollId' => 'getPayrollId', 'empNumber' => 'getEmpNumber'),
			'urlPattern' => 'viewEmployeePayroll?payrollId={payrollId}&empNumber={empNumber}'),
		));
			$header2-> populateFromArray(array(
			    'name' => 'Salary',
			    'width' => '20%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getBasicSalary'),
			));
			$header3-> populateFromArray(array(
			    'name' => 'PAYE',
			    'width' => '15%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getPaye'),
			));
		 	$header4-> populateFromArray(array(
			    'name' => 'Pension',
			    'width' => '15%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getPensionName'),
			));
			$header5-> populateFromArray(array(
			    'name' => 'Pension Amount',
			    'width' => '20%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getPension'),
			));
			$header6-> populateFromArray(array(
			    'name' => 'Take Home',
			    'width' => '20%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getTakeHome'),
			));
			
			$this-> headers = array( $header1, $header2,$header3,$header4,$header5,$header6  );
    	}
		/*
		 * 
		 * 
		 */
		 public function getClassName() {
			return  'PayrollResults';
	}
    }
