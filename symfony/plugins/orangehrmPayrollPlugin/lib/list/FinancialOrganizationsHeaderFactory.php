<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class FinancialOrganizationsHeaderFactory extends  ohrmListConfigurationFactory{
    	/*
		 * 
		 * 
		 */	
    	public function init(){
    		$header1 = new ListHeader();
			$header2 = new ListHeader();
			$header3 = new ListHeader();
			$header4 = new ListHeader();
		 
			$header1->populateFromArray(array(
		    'name' => 'Name',
		    'width' => '50%',
		    'isSortable' => false,
		   // 'sortField' => 'v.name',
		    'elementType' => 'link',
		    'elementProperty' => array(
			'labelGetter' => 'getName',
			'placeholderGetters' => array('organizationId' => 'getOrganizationId'),
			'urlPattern' => 'addFinancialOrganization?organizationId={organizationId}'),
		));
			$header2-> populateFromArray(array(
			    'name' => 'Address',
			    'width' => '25%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getAddress'),
			));
			$header3-> populateFromArray(array(
			    'name' => 'Contact',
			    'width' => '25%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getContactNumber'),
			));
		 
			
			$this-> headers = array( $header1, $header2,$header3  );
    	}
		/*
		 * 
		 * 
		 */
		 public function getClassName() {
			return 'FinancialOrgs';
	}
    }
?>