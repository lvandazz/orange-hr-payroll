<?php


class AllowanceListHeaderFactory extends  ohrmListConfigurationFactory{
	
	public function init(){
		$header1 = new ListHeader();
		$header2 = new ListHeader();
		$header3 = new ListHeader();
		$header4 = new ListHeader();
		$header1->populateFromArray(array(
		    'name' => 'Allowance Title',
		    'width' => '46%',
		    'isSortable' => false,
		   // 'sortField' => 'v.name',
		    'elementType' => 'link',
		    'elementProperty' => array(
			'labelGetter' => 'getAllowanceTitle',
			'placeholderGetters' => array('allowance_id' => 'getID'),
			'urlPattern' => 'addAllowance?allowanceId={allowance_id}'),
		));
 
		$header2->populateFromArray(array(
		    'name' => 'Amount',
		    'width' => '37%',
		    'isSortable' => FALSE,
		    //'sortField' => 'e.emp_firstname',
		    'elementType' => 'label',
		    'elementProperty' => array('getter' => 'getAllowanceAmount'),
		));

		$header3->populateFromArray(array(
		    'name' => 'Percentage Basic',
		    'width' => '17%',
		    'isSortable' => FALSE,
		    ///'sortField' => 'v.status',
		    'elementType' => 'label',
		    'elementProperty' => array('getter' => 'getPercentageBasic'),
		));

		$this->headers = array($header1, $header2, $header3);
	}
	/*
	 * 
	 * 
	 * 
	 */
	public function getClassName() {
		return 'AllowanceList';
	}
}
