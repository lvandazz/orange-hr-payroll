<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    class EmployeePayrollHeaderFactory extends  ohrmListConfigurationFactory{
    	public function init(){
    		$header1 = new ListHeader();
			$header2 = new ListHeader();
			$header3 = new ListHeader();
			$header4 = new ListHeader();
			$header5 = new ListHeader();
			$header6 = new ListHeader();
			$header7 = new ListHeader();
			$header8 = new ListHeader();
			 
			$header1-> populateFromArray(array(
			    'name' => 'Salary',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getBasicSalary'),
			));
			$header2-> populateFromArray(array(
			    'name' => 'PAYE',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getPaye'),
			));
		 	$header3-> populateFromArray(array(
			    'name' => 'Pension',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getPensionName'),
			));
			$header4-> populateFromArray(array(
			    'name' => 'Pension Amount',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getPension'),
			));
			$header5-> populateFromArray(array(
			    'name' => 'Loan',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getLoanTitle'),
			));
			$header6-> populateFromArray(array(
			    'name' => 'Loan Deduction',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getLoanDeductionAmount'),
			));
			$header7-> populateFromArray(array(
			    'name' => 'Loan Balance',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getLoanBalance'),
			));
			$header8-> populateFromArray(array(
			    'name' => 'Take Home',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getTakeHome'),
			));
			
			$this-> headers = array( $header1, $header2,$header3,$header4,$header5,$header6,$header7,$header8  );
    	}
		/*
		 * 
		 * 
		 */
		 public function getClassName() {
			return  'EmployeePayroll';
	}
    }
