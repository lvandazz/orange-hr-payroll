<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class DeductionsHeaderFactory extends  ohrmListConfigurationFactory{
    	/*
		 * 
		 * 
		 */	
    	public function init(){
    		$header1 = new ListHeader();
			$header2 = new ListHeader();
			$header3 = new ListHeader();
			$header4 = new ListHeader();
			$header5 = new ListHeader();
			$header6 = new ListHeader();
			$header7 = new ListHeader();
			$header8 = new ListHeader();
			
			$header1->populateFromArray(array(
			    'name' => 'Employee',
			    'width' => '25%',
			    'isSortable' => false,
			  	 // 'sortField' => 'v.name',
			    'elementType' => 'link',
			    'elementProperty' => array(
				'labelGetter' => 'getEmployee',
				'placeholderGetters' => array('deductionId' => 'getDeductionId'),
				'urlPattern' => 'addDeduction?deductionId={deductionId}'),
			));
			$header2-> populateFromArray(array(
			    'name' => 'Title',
			    'width' => '25%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getTitle'),
			));
			$header3-> populateFromArray(array(
			    'name' => 'Amount',
			    'width' => '12.5%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getAmountX'),
			));
		 	$header4 -> populateFromArray(array(
			    'name' => 'Status',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getStatusX'),
			));
			
			$header5 -> populateFromArray(array(
			    'name' => 'Date Created',
			    'width' => '15%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getDateCreated'),
			));
			$header6 -> populateFromArray(array(
			    'name' => 'Recurring ?',
			    'width' => '15%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getRecurringX'),
			));
			
			$this-> headers = array( $header1, $header2,$header3, $header4 , $header5,$header6);
    	}
		/*
		 * 
		 * 
		 */
		 public function getClassName() {
			return 'DeductionsList';
		}
    }
?>