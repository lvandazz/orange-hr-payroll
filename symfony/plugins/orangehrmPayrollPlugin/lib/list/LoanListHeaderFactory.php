<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class LoanListHeaderFactory extends  ohrmListConfigurationFactory{
    	/*
		 * 
		 * 
		 */	
    	public function init(){
    		$header1 = new ListHeader();
			$header2 = new ListHeader();
			$header3 = new ListHeader();
			$header4 = new ListHeader();
			$header5 = new ListHeader();
			$header6 = new ListHeader();
			$header7 = new ListHeader();
			$header8 = new ListHeader();
			
			$header1->populateFromArray(array(
			    'name' => 'Title',
			    'width' => '17.5%',
			    'isSortable' => false,
			  	 // 'sortField' => 'v.name',
			    'elementType' => 'link',
			    'elementProperty' => array(
				'labelGetter' => 'getTitle',
				'placeholderGetters' => array('loanId' => 'getId'),
				'urlPattern' => 'addLoan?loanId={loanId}'),
			));
			$header2-> populateFromArray(array(
			    'name' => 'Principal',
			    'width' => '15%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getPrincipalX'),
			));
			$header3-> populateFromArray(array(
			    'name' => 'Payment Term',
			    'width' => '12.5%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getTerm'),
			));
		 	$header4 -> populateFromArray(array(
			    'name' => 'Interest',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getInterest'),
			));
			
			$header7-> populateFromArray(array(
			    'name' => 'Accrued Amount',
			    'width' => '12.5%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getAccruedAmount'),
			));
			$header6-> populateFromArray(array(
			    'name' => 'Month Repayment',
			    'width' => '15%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getMonthRepaymentX'),
			));
			$header8-> populateFromArray(array(
			    'name' => 'Organization',
			    'width' => '17.5%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getOrganization'),
			));
			$this-> headers = array( $header1, $header2,$header3, $header4,$header6,$header7,$header8);
    	}
		/*
		 * 
		 * 
		 */
		 public function getClassName() {
			return 'LoanList';
	}
    }
?>