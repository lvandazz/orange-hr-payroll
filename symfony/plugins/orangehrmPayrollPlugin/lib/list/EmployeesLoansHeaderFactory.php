<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class EmployeesLoansHeaderFactory extends  ohrmListConfigurationFactory{
    	/*
		 * 
		 * 
		 */	
    	public function init(){
    		$header1 = new ListHeader();
			$header2 = new ListHeader();
			$header3 = new ListHeader();
			$header4 = new ListHeader();
			$header5 = new ListHeader();
			$header6 = new ListHeader();
			$header7 = new ListHeader();
			$header8 = new ListHeader();
			 
			$header1->populateFromArray(array(
		    'name' => 'Employee',
		    'width' => '15%',
		    'isSortable' => false,
		   // 'sortField' => 'v.name',
		    'elementType' => 'link',
		    'elementProperty' => array(
			'labelGetter' => 'getEmpName',
			'placeholderGetters' => array('empLoanId' => 'getEmployeeLoanId'),
			'urlPattern' => 'assignLoan?empLoanId={empLoanId}'),
		));
			$header2-> populateFromArray(array(
			    'name' => 'Loan',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getLoanTitle'),
			));
			$header3-> populateFromArray(array(
			    'name' => 'Accrued Amount',
			    'width' => '15%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getAccruedAmount'),
			));
			$header4 -> populateFromArray(array(
			    'name' => 'Balance',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getBalanceX'),
			));
			
		 	$header5 -> populateFromArray(array(
			    'name' => 'Date Join',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getDateJoined'),
			));
			
			$header6-> populateFromArray(array(
			    'name' => 'Status',
			    'width' => '15%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getStatusX'),
			));
			$header7 -> populateFromArray(array(
			    'name' => 'Month Repayment',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getRepaymentX'),
			));
			$header8 -> populateFromArray(array(
			    'name' => 'Organization',
			    'width' => '10%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getOrganization'),
			));
			$this-> headers = array( $header1, $header2,$header3, $header4,$header5,$header6 ,$header7,$header8);
    	}
		/*
		 * 
		 * 
		 */
		 public function getClassName() {
			return 'EmployeeLoan';
	}
    }
?>