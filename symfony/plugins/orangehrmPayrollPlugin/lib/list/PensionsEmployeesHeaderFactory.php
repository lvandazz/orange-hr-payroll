<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class PensionsEmployeesHeaderFactory extends  ohrmListConfigurationFactory{
    	/*
		 * 
		 * 
		 */	
    	public function init(){
    		$header1 = new ListHeader();
			$header2 = new ListHeader();
			$header3 = new ListHeader();
			$header4 = new ListHeader();
		 
			$header1->populateFromArray(array(
		    'name' => 'Employee Name',
		    'width' => '40%',
		    'isSortable' => false,
		   // 'sortField' => 'v.name',
		    'elementType' => 'link',
		    'elementProperty' => array(
			'labelGetter' => 'getFullName',
			'placeholderGetters' => array('penEmpId' => 'getPenEmpId'),
			'urlPattern' => 'assignPension?penEmpId={penEmpId}'),
		));
			$header2-> populateFromArray(array(
			    'name' => 'Pension',
			    'width' => '30%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getPensionName'),
			));
			$header3-> populateFromArray(array(
			    'name' => 'Date Join',
			    'width' => '30%',
			    'isSortable' => FALSE,
			    //'sortField' => 'e.emp_firstname',
			    'elementType' => 'label',
			    'elementProperty' => array('getter' => 'getDateJoined'),
			));
		 
			
			$this-> headers = array( $header1, $header2,$header3  );
    	}
		/*
		 * 
		 * 
		 */
		 public function getClassName() {
			return 'PensionsEmployees';
	}
    }
?>