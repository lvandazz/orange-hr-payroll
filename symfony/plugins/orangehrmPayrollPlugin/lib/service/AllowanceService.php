<?php
/*
 * 
 * 
 * 
 * 
 */
 class AllowanceService extends  BaseService{
 	private $allowanceDao ;
	
	public function __construct()
	{
		$this -> allowanceDao = new AllowanceDao();
	}
	
	public function getAllowanceDao( )
	{
		return $this -> allowanceDao;
	}
	public function setAllowanceDao( AllowanceDao $allowanceDao )
	{
		$this -> allowanceDao = $allowanceDao;
	}
	
	public function getAllowanceList()
	{
		return $this -> allowanceDao ->getAllowanceList();
	}
	/*
	 * 
	 * 
	 */
	 function saveAllowance( Allowance $allowance ){
	 	$this -> allowanceDao -> saveAllowance( $allowance );
	 }
	 /*
	  * 
	  * 
	  */
	  function getAllowanceById( $allowanceId ){
	  	return $this -> allowanceDao -> getAllowanceById($allowanceId);
	  }
	  /*
	   * 
	   * 
	   * 
	   */
	   function deleteAllowance( $allowanceIds ){
	   	
			 
			if( count( $allowanceIds ) > 0 ){
				
				$isDeletionSuccess = $this -> allowanceDao -> deleteAllowances( $allowanceIds );
				return $isDeletionSuccess;
			}
			return false;
	   }
 }
