<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
 class PensionDeductionService extends BaseService{
 	
	private $pensionDeductionDao;
	
	function __construct(){
		$this -> pensionDeductionDao = new PensionDeductionDao();
	}
	/*
	 * 
	 */
	function savePensionDeduction( PensionDeduction $pensionDeduction ){
		
		return $this -> pensionDeductionDao -> savePensionDeduction( $pensionDeduction );
	}
 }
