<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class TaxRateService extends BaseService{
    	
		private $taxRateDao;
		/*
		 * 
		 * 
		 */
		function getTaxRateDao(){
			if( is_null( $this -> taxRateDao ) ){
				$this -> taxRateDao = new TaxRateDao();
			}
			return $this -> taxRateDao;
		}
		/*
		 * 
		 * 
		 */
		function getTaxRates(){
			return $this -> taxRateDao -> getTaxRates();
		}
		/*
		 * 
		 */
		 function setTaxRateDao( TaxRateDao $taxRateDao ){
		 	$this -> taxRateDao = $taxRateDao;
		 }
		 /*
		  * 
		  */
		  function __construct(){
		  	$this -> taxRateDao = new TaxRateDao();
		  }
		  /*
		   * 
		   */
		   function getTaxRate( $salary, $rateId=null ){
		   	 return $this -> taxRateDao -> getTaxRate($salary, $rateId);
		   }
    }
?>