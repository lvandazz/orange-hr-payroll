<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
 class TaxDeductionService extends BaseService{
 	
	private $taxDeductionDao;
	
	function __construct(){
		$this -> taxDeductionDao = new TaxDeductionDao();
	}
	/*
	 * 
	 */
	function saveTaxDeduction( TaxDeduction $taxDeduction ){
		return $this -> taxDeductionDao -> saveTaxDeduction( $taxDeduction );
	}
	/*
	 * 
	 */
	 function getTaxDeduction( $payrollId, $empNumber ){
	 	return $this -> taxDeductionDao -> getTaxDeduction($payrollId, $empNumber) -> getFirst();
	 }
 }
