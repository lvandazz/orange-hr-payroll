<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
 class FinancialOrganizationService extends  BaseService{
 	private $financialOrganizationDao;
	
	function __construct(){
		$this -> financialOrganizationDao = new FinancialOrganizationDao();
	}
	
	function getFinancialOrganizations( $organizationId = null){
		return $this -> financialOrganizationDao -> getFinancialOrganizations( $organizationId );
	}
	/*
	 * 
	 */
	function saveFinancialOrganization( FinancialOrganization $organization ){
		return $this -> financialOrganizationDao -> saveFinancialOrganization($organization);
	}
	/*
	 * 
	 */
	function deleteOrganization( $organizationId ){
		return $this -> financialOrganizationDao -> deleteOrganization($organizationId);
	}
	/*
	 * 
	 */
	 function getOrganizationLoans( $organizationId ){
	 	$result =  $this -> financialOrganizationDao -> getOrganizationLoans($organizationId);
		
		if( count( $result ) == 0 || $result == null ){
			return null;
		}
		return $result;
	 }
 }
 ?>
