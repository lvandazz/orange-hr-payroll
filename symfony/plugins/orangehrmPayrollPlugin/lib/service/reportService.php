<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class ReportService extends  BaseService{
    	private $reportDao ;
		/*
		 *
		 */  
		function __construct(){
			$this -> reportDao = new ReportDao();
		}
		/*
		 * 
		 */
		 
		function getSubunits(){
			return $this -> reportDao -> getSubunits();
		}
		/*
		 * 
		 */
		function getPayrollsByDates( $from , $to ){
			$from = new DateTime( $from );
			$to = new DateTime( $to );
			
			$from = $from -> format( 'Y-m-01' );
			$to = $to -> format( 'Y-m-31' );
			
			$result = $this -> reportDao -> getPayrollsByDates($from, $to);
			 
			if( count( $result ) == 0 ){
				return null;
			}else{
				
				return $result ;
			}
		}
		/*
		 * 
		 */
		function getFullPayroll($from = null, $to = null, $empNumber = null , $subunit = null  ){
			$employeeService = new EmployeeService();
			$payrollService = new PayrollService();
			$fullPayroll = array();
			$pensionService = new PensionService();
			$pensionEmpService = new PensionEmployeeService();
			$taxService = new TaxRateService();
			$taxDeduService = new TaxDeductionService();
			$loanService = new LoanService();
			$deductionService = new DeductionService();
									 
			if( $from != null AND $to != null )
				$payrolls = $this -> getPayrollsByDates($from, $to);
			else{
				return null;
			}
		
			$employees = null;
			
			if( $empNumber != null ){
				$employees[] = $employeeService -> getEmployee($empNumber);
			}else{
				
				$employees = $employeeService -> getEmployeeList();
					
				$i = 0;
				foreach( $employees as $e ){
					if( $subunit != null )
						if( $e -> getWorkStation() != $subunit ){
							unset( $employees[ $i ] );
						}
					
				$i = $i +1;	
				
				}					
			}
			
			$totalTax = 0;
			$totalLoans = 0;
			$totalPension = 0;
			$totalTakeHome = 0;
			$totalSalary = 0;
			$totalDeductions = 0;
			
			foreach( $payrolls as $payroll ){
				
				$payrollId = $payroll -> getPayrollId();
				
				foreach( $employees as $employee ){
					//instance report
					$payrollReport = new PayrollReport();
					//salary 
					$salaries = $employeeService -> getEmployeeSalaries( $employee -> getEmpNumber() );
					$salary = 0;
					
					if( $salaries == null ){
						return null;
					}
					
					foreach( $salaries as $s ){
						$salary += $s -> getAmount(); 
					}				
					$gross = $salary;
					$totalSalary += $gross;
					//pension 
					$pension  = $pensionService -> getPension( $pensionEmpService -> getPenEmp(null, $employee -> getEmpNumber()) -> getPensionId());
					
					$pensionAmount = ($salary * $pension -> getPercentEmployee() )/100;
					$totalPension += $pensionAmount;
					$salary -= $pensionAmount;
					
					 
					$empNumber = $employee -> getEmpNumber();
					
					// tax deduction
					$taxDeduction = $taxDeduService -> getTaxDeduction($payrollId, $empNumber);  
					
					if(  $taxDeduction  != null AND count( $taxDeduction ) > 0 ) {
							
						$taxRate = $taxService -> getTaxRate(null,$taxDeduction -> getTaxRateId());
						$taxRate = $taxRate -> getFirst();
						$excessAmount = (($salary - $taxRate -> getSalaryFrom()) * ($taxRate -> getRatePercent() / 100));
						$paye = $excessAmount + $taxRate -> getRateAmount();
						$salary -= $paye;
						$totalTax += $paye;
						//loan Deduction 
						$loanDeduction = $loanService -> getLoanDeduction( null, $payrollId, $empNumber );
						
						if( count( $loanDeduction ) != 0  ){
							$loanDeduction = $loanDeduction -> getFirst();
							$empLoan = $loanService -> getEmployeesLoans(null, $empNumber, $loanDeduction -> getLoanId() );
							$empLoan = $empLoan -> getFirst();
							$salary -= $empLoan -> getMonthRepayment();
							$payrollReport -> setLoanDeductionAmount( $empLoan -> getMonthRepayment());
							$totalLoans += $empLoan -> getMonthRepayment();
						}
						
						// deductions
						$deductionLine = $deductionService -> getDeductionLines(null,$payrollId,$empNumber);
						if( $deductionLine != null ){
							$deductionLine = $deductionLine -> getFirst();
							
							$deduction = $deductionService -> getDeductions( $deductionLine -> getDeductionId());
							
							$deduction = $deduction -> getFirst();
							
							if( $deduction -> getIsRecurring() == 1 ){
								$salary -= $deduction -> getRepaymentAmount();
								$totalDeductions += $deduction -> getRepaymentAmount(); 
							}
							else{
								$salary -= $deduction -> getAmount();
								$totalDeductions += $deduction -> getAmount();
							}
							$payrollReport -> addOtherDeduction( $deduction );
						}
						
						
							
					}
					
					  $totalTakeHome += $salary;
					  $payDate = new DateTime( $payroll -> getPayDate());
					  $payDate = $payDate -> format( 'F Y' ); 
					  $payrollReport -> setEmpNumber($empNumber);
					  $payrollReport -> setEmployeeName( $employee -> getFullName());
					  $payrollReport -> setBasicSalary( $gross );
					  $payrollReport -> setPayDate($payDate);
					  $payrollReport -> setPension($pensionAmount);
					  $payrollReport -> setPensionName($pension -> getPensionName());
					  $payrollReport -> setPaye($paye);
					  $payrollReport -> setTakeHome( $salary );
					  
					  $fullPayroll[ ] = $payrollReport;
					  
 				}
				
				
			}
			
			$total = new PayrollReport();
			$total -> setTotalTax($totalTax	);
			$total -> setTotalTakeHome($totalTakeHome);
			$total -> setTotalLoans( $totalLoans );
			$total -> setTotalSalary($totalSalary );
			$total -> setTotalPension( $totalPension );
			$total -> setTotalDeductions( $totalDeductions );
				
			$fullPayroll[] = $total;
			return $fullPayroll;
		}
	
		/*
		 * 
		 * 
		 */
		 function getFullPensions($from, $to ,  $empNumber = null, $pension = null ){
		 	$employeeService = new EmployeeService();
			$pensionService = new PayrollService();
			$fullPensions = array();
			$pensionService = new PensionService();
			$pensionEmpService = new PensionEmployeeService(); 
			$payrolls = null;
			
			if( $from != null AND $to != null )
				$payrolls = $this -> getPayrollsByDates($from, $to);
			else{
				return null;
			}
			
			
			$employees = null;
			
			if( $empNumber != null ){
				$employees[] = $employeeService -> getEmployee($empNumber);
			}else{
				
				$employees = $employeeService -> getEmployeeList();
			}
			
			if( $pension != null ){
				$penEmps = $pensionEmpService -> getPensionsEmployees( $pension );
				if( $penEmps != null  AND count( $penEmps ) > 0 ){
					$employees = array();
					foreach( $penEmps as $penEmp ){
						$employees[ ] = $employeeService -> getEmployee($penEmp -> getEmpNumber() );
					}
				}
				
			}
			
			$totalEmpShare = 0;
			$totalEmplyrShare = 0;
			$totalPension = 0;
			
			foreach( $payrolls as $payroll ){
				$payrollId = $payroll -> getPayrollId();
				
				foreach( $employees as $employee ){
					$pensionReport = new PensionReport();
					
					$pensionDeduction = $pensionEmpService -> getPensionDeduction($payrollId, $employee -> getEmpNumber());
				 	if( $pensionDeduction != null ){
						$empPension = $pensionService -> getPension( $pensionDeduction -> getPensionId());
					
					$salaries  = $employeeService -> getEmployeeSalaries( $employee -> getEmpNumber());
					$salary = 0;
					foreach ( $salaries as $s ){
						$salary += $s -> getAmount();
					}
					$pensionAmountEmployee  = ($salary * $empPension -> getPercentEmployee() )/100;
					$pensionAmountEmployer = ($salary * $empPension -> getPercentEmployer() )/100;		
					$pensionReport -> setPayDate( $payroll -> getPayDate());
					$pensionReport -> setEmployeeName( $employee -> getFullName());
					$pensionReport -> setPensionName( $empPension -> getPensionName());
					$pensionReport -> setPension($pensionAmountEmployee);
					$pensionReport -> setEmployerShare( $pensionAmountEmployer );
					$pensionReport -> setTotLinePension( $pensionAmountEmployee + $pensionAmountEmployer );
					$totalEmpShare += $pensionAmountEmployee;
					$totalEmplyrShare += $pensionAmountEmployer;
					$totalPension += $pensionAmountEmployee + $pensionAmountEmployer;
					
					$fullPensions[ ] = $pensionReport;	
					}
				 	
				} 	
			}
			    $reportTotal = new PensionReport();
				$reportTotal -> setTotEmpPension( $totalEmpShare );
				$reportTotal -> setTotEmplyrPension( $totalEmplyrShare );
				$reportTotal -> setTotalPension($totalPension);
				
				$fullPensions[] = $reportTotal;
				return $fullPensions;
		 } 	

		/*
		 * 
		 * 
		 */
		function getFullLoans( $from , $to , $empNumber = null , $organization = null ){
			$employeeService = new EmployeeService();
			$loanService = new LoanService();
			$fullLoans = array();
			$payrolls = null;
			
			if(  $from != null AND $to != null ){
				$payrolls = $this -> getPayrollsByDates($from, $to);
				
			}
			if( $payrolls == null )	{
					return null;
				}
			
			$employees = null;
			if( $empNumber != null ){
				$employees[ ] = $employeeService -> getEmployee( $empNumber );
			}else{
				$employees = $employeeService -> getEmployeeList();
			}
			
			$totalP = 0;
			$totalA = 0;
			$totalD = 0;
			$totalB = 0;
			$totals = array();
			
			foreach( $payrolls as $payroll ){
				$payrollId = $payroll -> getPayrollId(); 
				
				foreach( $employees as $employee ){
					$loanReport = new LoanReport();
					$loanDeduction = $loanService -> getLoanDeduction(null, $payrollId,$employee -> getEmpNumber());
					
					if( $loanDeduction != null AND count( $loanDeduction ) > 0  ){
						$loanDeduction = $loanDeduction -> getFirst();
						
						$employeeLoan = $loanService -> getEmployeesLoans(null, $employee -> getEmpNumber(), $loanDeduction -> getLoanId()) -> getFirst();
						
						if( $organization != null ){
							$loan = $loanService -> getLoans( $loanDeduction -> getLoanId(), $organization ) -> getFirst();
							 
						}else{
							$loan = $loanService -> getLoans($loanDeduction -> getLoanId()) -> getFirst();	
						}
						
						if( $loan != null  ){
						$loanReport -> setEmployeeName( $employee -> getFullName());
						$loanReport -> setPayDate( $payroll -> getPayDate());
						$loanReport -> setLoanTitle( $loan -> getTitle());
						
						$loanReport -> setLoanDeductionAmount( $employeeLoan -> getMonthRepayment());
						$loanReport -> setLoanBalance( $loanDeduction -> getBalance());
						
						$fullLoans[ ] = $loanReport;
						
						if( ! isset( $totals[ $loan -> getId() ] ) ){
							
							if( ! isset( $totals[ $loan -> getId()][ $employee -> getEmpNumber() ] ) ){
								$totals[ $loan -> getId()][ $employee -> getEmpNumber() ] = $employee -> getEmpNumber();
								$totalP += $loan -> getPrincipal();
								$totalA += $loan -> getAccruedAmount(true);
								$totalB += $loanDeduction -> getBalance();	
								$loanReport -> setLoanPrincipal( $loan -> getPrincipal() );
								$loanReport -> setLoanAccAmount( $loan -> getAccruedAmount(true));
								
							}
						}else{
								
							if( ! isset( $totals[ $loan -> getId()][ $employee -> getEmpNumber() ] ) ){
								$totals[ $loan -> getId()][ $employee -> getEmpNumber() ] = $employee -> getEmpNumber();
								$totalP += $loan -> getPrincipal();
								$totalA += $loan -> getAccruedAmount(true);
								$totalB += $loanDeduction -> getBalance();	
								$loanReport -> setLoanPrincipal( $loan -> getPrincipal() );
								$loanReport -> setLoanAccAmount( $loan -> getAccruedAmount(true));
								
							}
						}
						
						$totalD += $employeeLoan -> getMonthRepayment();			
						
						}
					}
					
				}
				
			}
			$loanReport = new LoanReport();
			$loanReport -> setTotalAccAmount( $totalA );
			$loanReport -> setTotalLoanDeduction( $totalD );
			$loanReport -> setTotalPrincipal( $totalP );
			$loanReport -> setTotalBalance($totalB);
			
		 
			$fullLoans[ ] = $loanReport ;
			return $fullLoans;
		}
		/*
		 * 
		 */
		function getFullDeductions( $from , $to , $employee = null, $status = null, $recurrance = null){
			$employeeService = new EmployeeService();
			$deductionService = new DeductionService();
			$fullDeductions = array();
			$payrolls = null;
			$totals = array();
			
			if(  $from != null AND $to != null ){
				$payrolls = $this -> getPayrollsByDates($from, $to);
				
			}
			if( $payrolls == null )	{
					return null;
				}
			 
			$employees = null;
			if( $empNumber != null ){
				$employees[ ] = $employeeService -> getEmployee( $empNumber );
			}else{
				$employees = $employeeService -> getEmployeeList();
			}
			
			if( $recurrance === null || $recurrance === '' ){
				$recurrance = null;
			}
			if( $status  === null || $status === '' ){
				$status = null;
			}
			
			$totalB = 0;
			$totalA = 0;
			$totalDl = 0;
			
			foreach( $payrolls as $payroll ){
				$payrollId = $payroll -> getPayrollId();
				foreach( $employees as $employee ){
					$deductionLines = $deductionService -> getDeductionLines( null, $payrollId, $employee -> getEmpNumber());
					 
					foreach( $deductionLines as $deductionLine ){
						
						$deductionReport = new DeductionReport();
						$deduction = $deductionService -> getDeductions( $deductionLine -> getDeductionId(),$employee -> getEmpNumber(), $recurrance, $status ) -> getFirst();
						 
						$deductionReport -> setPayDate( $payroll -> getPayDate());
						$deductionReport -> setTitle( $deduction -> getTitle());
						$deductionReport -> setBalance( $deductionLine -> getBalance());
						
						if( $deduction -> getIsRecurring() == 1 ){
							$deductionReport -> setDeductionAmount( $deduction -> getRepaymentAmount());
						}
						else{
							$deductionReport -> setDeductionAmount( $deduction -> getAmount());
						}
						
						if( ! isset( $totals[ $deduction -> getDeductionId() ] ) ){
							
							if( ! isset( $totals[ $deduction -> getDeductionId() ][ $employee -> getEmpNumber() ] ) ){
								$totals[ $deduction -> getDeductionId() ][ $employee -> getEmpNumber() ] = $employee -> getEmpNumber();
								$totalA += $deduction -> getAmount();
								$totalB += $deductionLine -> getBalance();
							}
						}else{
								
							if( ! isset( $totals[ $deduction -> getDeductionId() ][ $employee -> getEmpNumber() ] ) ){
								$totals[ $deduction -> getDeductionId() ][ $employee -> getEmpNumber() ] = $employee -> getEmpNumber();
								$totalA += $deduction -> getAmount();
								$totalB += $deductionLine -> getBalance();
							}
						}
						
						$deductionReport -> setAmount( $deduction -> getAmount());
						$deductionReport -> setEmployeeName( $employee -> getFullName());
						
						$fullDeductions[] = $deductionReport;
					}
				}
				
			}
		$total = new DeductionReport();
		$total -> setTotalAmount( $totalA );
		$total -> setTotalBalance( $totalB );
		$fullDeductions[] = $total;
		return $fullDeductions;
		}
    } 
	
?>