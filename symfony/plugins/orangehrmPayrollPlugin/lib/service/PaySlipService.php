<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
 class PaySlipService extends  BaseService{
 	
	private $paySlipDao;
	
	function __construct(){
		$this -> paySlipDao = new PaySlipDao();
	}
	/*
	 * 
	 */
	function getEmployeePaySlips($empNumber,$payroll = NULL ){
		
		$payslips = $this -> paySlipDao -> getEmployeePaySlips($empNumber,$payroll);
		return $payslips -> getFirst();
	}
	/*
	 * 
	 */
	 function savePayslip( Payslip $payslip ){
	 	return $this -> paySlipDao -> savePayslip($payslip);
	 }
	 /*
	  * 
	  */
	 function getPayslips( $employee = null, $fromDate = null, $toDate = null ){
	 	
		$payrolls =  null;
		$reportSevice = new ReportService();
		$employeeService = new EmployeeService();
		$payrollService = new PayrollService();
		$pensionService = new PensionService();
		$taxRateService = new TaxRateService();
		$pensionEmpService = new PensionEmployeeService();
		$payslips = array();
		$resultPayslips = array();
		
		if( $fromDate != null AND $toDate != null ){
			$payrolls = $reportSevice -> getPayrollsByDates($fromDate, $toDate);
			
			if( $payrolls == null ){
				return array();
			}
		}
		
		if( $payrolls != null  ){
			
			foreach( $payrolls as $payroll ){
				
				$result = $this -> paySlipDao -> getPayslips($payroll -> getPayrollId(), $employee ) -> getFirst();
				$resultPayslips[ ] = $result;
			}	
		}
		else{
			$resultPayslips = $this -> paySlipDao -> getPayslips(null, $employee );	
		}
	 	
		
		if(  count ( $resultPayslips ) > 0 ){
		 
			foreach( $resultPayslips as $payslip ){
				
				if( get_class( $payslip ) == 'Payslip' ){
					$procdPayslip = $payrollService -> processPayslip($payslip,
					$employeeService, $pensionEmpService, $pensionService, $taxRateService); 
					$payslips[ ] = $procdPayslip;	
				}
					
				 
			}	
					
		}
		 
		return $payslips;
	 }
	 /*
	  * 
	  */
	  function getPayslip( $payslipId ){
	  	return $this -> paySlipDao -> getPayslip($payslipId) -> getFirst() ;
	  }
 }
