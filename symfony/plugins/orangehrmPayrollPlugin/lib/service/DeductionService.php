<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
class DeductionService extends BaseService{
	
	private $deductionDao;
	
	/*
	 * 
	 */
	 function __construct(){
	 	$this -> deductionDao = new DeductionDao();
	 }
	 /*
	  * 
	  */
	 function getDeductions( $deductionId = null , $empNumber = null , $recurring = false, $status = null){
	 	
	 	$result =  $this -> deductionDao -> getDeductions( $deductionId, $empNumber , $recurring, $status );
		if( is_null( $result ) || count( $result ) == 0 ){
			return null;
		}
		
		return $result; 
	 }
	 /*
	  * 
	  */
	 function saveEmployeeDeduction( Deduction $employeeDeduction ){
	 	return $this -> deductionDao -> saveDeduction( $employeeDeduction);
	 }
	 /*
	  * 
	  */
	  function getDeductionLines( $deductionId = null, $payrollId = null, $empNumber = null )
	  {
	  	
		$result = $this -> deductionDao -> getDeductionLines($deductionId , $payrollId, $empNumber );
		if( is_null( $result ) || count( $result ) == 0 ){
			return null;
		}
		
		return $result; 
	  }
	  /*
	   * 
	   */
	   function saveDeductionLine( DeductionLine $deductionLine ){
	   	return $this -> deductionDao -> saveDeductionLine($deductionLine);
	   }
	  
}
