<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */

class PayrollService extends  BaseService {
	private $payrollDao;

	function __construct() {
		$this -> payrollDao = new PayrollDao();
	}

	/*
	 *
	 *
	 */
	function getMonthPayroll($month = null) {

		$currentMonth = $date = implode('-', array(date('Y'), $month == null ? date('m') : $month, 1));
		$monthPayroll = $this -> payrollDao -> getMonthPayroll($currentMonth) -> getFirst();

		if ($monthPayroll == false) {
			#no payroll runned
			$monthPayroll = new PayRoll();
			$monthPayroll -> setPayDate(implode('-', array(date('Y'), $month == null ? date('m') : $month, date('d'))));
			$monthPayroll -> setPayrollId($this -> payrollDao -> savePayroll($monthPayroll));
		}

		return $monthPayroll;
	}

	/*
	 *
	 */
	function runPayroll($month = null, $payrollId = null, $empNumber = null ) {
	 
		$employeeService = new EmployeeService();
		if( $payrollId == null && $empNumber == null )
			$employeeList = $employeeService -> getEmployeeList();
		else{
			$employeeList[] = $employeeService -> getEmployee($empNumber);
		}
		
		$pensionEmpService = new PensionEmployeeService();
		$pensionService = new PensionService();
		$taxRateService = new TaxRateService();
		$payRollResults = array();
		$paySlipService = new PaySlipService();
		$deductionService = new DeductionService();
		
		if( $payrollId == null && $empNumber == null )
			$payroll = $this -> getMonthPayroll($month);
		else{
			$payroll = $this -> payrollDao -> getPayroll($payrollId);
		}
			
		$loanService = new LoanService();
		
		foreach ($employeeList as $employee) {

			//check employee pay slip
			$payslip = $paySlipService -> getEmployeePaySlips($employee -> getEmpNumber(), $payroll -> getPayrollId());

			if ($payslip == NULL OR isset( $payrollId )) :

				$result = new PayrollResult();

				//continue get employee pension and calculate tax rate
				$empPension = $pensionEmpService -> getPenEmp(NULL, $employee -> getEmpNumber());
				
				if( $empPension  == null ){
					return -322;
				}
				
				$pension = $pensionService -> getPension($empPension -> getPensionId());
				$salaries = $employeeService -> getEmployeeSalaries($employee -> getEmpNumber());
				$totalSalary = 0;
				foreach( $salaries as $s ){
					$totalSalary += $s -> getAmount( );
				}
				$salaries = $totalSalary;
				//deduct pension
				$pensionAmount = ($salaries * ( $pension -> getPercentEmployee() / 100));
				$salary = ($salaries - $pensionAmount);
				
				//TRA Tax Favor
			    $taxRate = $taxRateService -> getTaxRate($salary) -> getFirst();
				
				// calcute paye
				$excessAmount = (($salary - $taxRate -> getSalaryFrom()) * ($taxRate -> getRatePercent() / 100));
				$paye = $excessAmount + $taxRate -> getRateAmount();
				$takehome = $salary - $paye;
				#check for loan 
				if( $loanService -> employeeHasPendingLoans( $employee -> getEmpNumber()) == true ){
					# get employee loan
					$employeeLoan = $loanService -> getEmployeePendingLoans( $employee -> getEmpNumber()) -> getFirst();
					if( count( $employeeLoan ) > 0 ){
						$loan = $loanService -> getLoans( $employeeLoan  -> getLoanId()) -> getFirst();
						// check repayment dates 
						$payrollDate = new DateTime(  $payroll -> getPayDate());
						$repaymentDate = new DateTime( $employeeLoan -> getRepaymentStart());  
						$diffDate = $repaymentDate -> diff( $payrollDate ); 
						
						if( ($diffDate -> days > 0 AND $diffDate -> invert == 0 )  ){ 
							$takehome = $takehome -  $employeeLoan -> getMonthRepayment();	 
							$result -> setLoanTitle( $loan -> getTitle());
							$result -> setLoanDeductionAmount( $employeeLoan -> getMonthRepayment());
							$result -> setLoanBalance( $employeeLoan -> getBalance() ); 
						}
					}  
				}
				
				#other deductions 
				$otherDeductions = $deductionService -> getDeductions(null,$employee -> getEmpNumber(),null,array(0,1));
				
				if( ! is_null( $otherDeductions ) ){
					
					$total = 0; 
					foreach( $otherDeductions as $odeduction ){
						
						//calculate amount to deduct
						//check dates
						$payrollDate = new DateTime( $payroll -> getPayDate());
						$paymentDate = new DateTime( $odeduction -> getPaymentStart());
						$diffDate = $paymentDate-> diff( $payrollDate  );
						 
						if( $diffDate -> days > 0 AND $diffDate -> invert == 0){
							
							if( $odeduction -> getIsRecurring() == 1 ){
								$total += $odeduction -> getRepaymentAmount();
							}
							else{
								$total += $odeduction -> getAmount();
							}
							$result -> addOtherDeduction($odeduction );
								
						}
					}
					$takehome -= $total;	
				}
				//set result data
				$result -> setPayrollId($payroll -> getPayrollId());
				$result -> setEmpNumber($employee -> getEmpNumber());
				$result -> setEmployeeName($employee -> getFullName());
				$result -> setBasicSalary($salaries);
				$result -> setPaye($paye);
				$result -> setPension($pensionAmount);
				$result -> setPensionName($pension -> getPensionName());
				$result -> setTakeHome($takehome);
				
				$payRollResults[] = $result;

			endif;
		}
		return $payRollResults;
	}

	/*
	 *
	 */
	function procPayroll($employeeIds, Payroll $payroll) {

		$taxRatesService = new TaxRateService();
		$employeeService = new EmployeeService();
		$pensionEmployeeService = new PensionEmployeeService();
		$taxDeductionService = new TaxDeductionService();
		$pensionDeductionService = new PensionDeductionService();
		$taxDeductionService = new TaxDeductionService();
		$payslipService = new PaySlipService();
		$loanService = new LoanService();
		
		$pensionService = new PensionService();
		foreach ($employeeIds as $employeeId) {
			// Employee record
			$employee = $employeeService -> getEmployee($employeeId);
			$salaries = $employeeService -> getEmployeeSalaries($employee -> getEmpNumber());
			$totalSalary = 0;
			foreach( $salaries as $salary ){
				$totalSalary += $salary -> getAmount( );
			}
			$salaries = $totalSalary;
			 
			$pensionEmployee = $pensionEmployeeService -> getPenEmp(null, $employee -> getEmpNumber());
			$pension = $pensionService -> getPension( $pensionEmployee -> getPensionId());
	 		$pensionAmount = ( $salaries * ($pension -> getPercentEmployee() / 100));
			//TRA Tax Favor
			$salaries = $salaries - $pensionAmount;
			$taxRate = $taxRatesService -> getTaxRate($salaries) -> getFirst();

			//  TaxDeduction
			$taxDeduction = new TaxDeduction();
			$taxDeduction -> setTaxRateId($taxRate -> getRateId());
			$taxDeduction -> setEmpNumber($employee -> getEmpNumber());
			$taxDeduction -> setPayrollId($payroll -> getPayrollId());
			$taxDeductionService -> saveTaxDeduction($taxDeduction);

			// Pension Deduction
			$pensionDeduction = new PensionDeduction();
			$pensionEmployee = $pensionEmployeeService -> getPenEmp(NULL, $employee -> getEmpNumber());
			$pensionDeduction -> setEmpNumber($employee -> getEmpNumber());
			$pensionDeduction -> setPayrollId($payroll -> getPayrollId());
			$pensionDeduction -> setPensionId( $pensionEmployee -> getPensionId());
			$pensionDeductionService -> savePensionDeduction($pensionDeduction);
			
			#check for loan 
			if( $loanService -> employeeHasPendingLoans( $employee -> getEmpNumber()) == true ){
				
				# get employee loan
				$employeeLoan = $loanService -> getEmployeePendingLoans( $employee -> getEmpNumber()) -> getFirst();
				if( count( $employeeLoan ) > 0 ){
					$loan = $loanService -> getLoans( $employeeLoan  -> getLoanId()) -> getFirst();
					// check repayment dates 
					$payrollDate = new DateTime(  $payroll -> getPayDate());
					$repaymentDate = new DateTime( $employeeLoan -> getRepaymentStart()); 
					$diffDate = $repaymentDate -> diff( $payrollDate );
					
					if( $diffDate -> days > 0  AND $diffDate -> invert == 0 ){
						#loan deduction	 
					    $loanDeduction = new LoanDeduction();
						$loanDeduction -> setEmpNumber( $employee -> getEmpNumber());
						$loanDeduction -> setPayrollId( $payroll -> getPayrollId() );
						$loanDeduction -> setAmount( $employeeLoan -> getMonthRepayment());
						$loanDeduction -> setLoanId( $employeeLoan -> getLoanId());
						
						
						#employee Loan update
						$currentBalance = $employeeLoan -> getBalance();
						$newBalance = $currentBalance - $employeeLoan -> getMonthRepayment();
						if( $newBalance == 0 ){
							$employeeLoan -> setStatus( 2 );// completed
						} 
						else{
							$employeeLoan -> setStatus( 1 );
						}
						$employeeLoan -> setBalance( $newBalance );
						$loanDeduction -> setBalance( $newBalance );
						$loanService -> saveLoanDeduction($loanDeduction);
						
						$loanService -> saveEmployeeLoan( $employeeLoan );
													
						}
					}  
				}
			
			#other deductions 
				$deductionService  = new DeductionService();
				$otherDeductions = $deductionService -> getDeductions(null,$employee -> getEmpNumber(),null,array(0,1));
				
				if( ! is_null( $otherDeductions ) ){
					$deductionAmount = 0; 
					foreach( $otherDeductions as $odeduction ){ 
						//calculate amount to deduct
						//check dates
						$payrollDate = new DateTime( $payroll -> getPayDate());
						$paymentDate = new DateTime( $odeduction -> getPaymentStart());
						$diffDate = $payrollDate -> diff( $paymentDate );
						
						if( $diffDate -> days > 0 AND $diffDate -> invert == 1 ){
							
							$currentBalance = $odeduction -> getBalance();
							$deductionLine = new DeductionLine();
							$deductionLine -> setEmpNumber( $employee -> getEmpNumber() );
							$deductionLine -> setPayrollId( $payroll -> getPayrollId());
							$deductionLine -> setDeductionId( $odeduction -> getDeductionId( ) );
							$deductionLine -> setDeductionDate( $payroll -> getPayDate());
							
							if( $odeduction -> getIsRecurring() == 1 ){
								
								if( $currentBalance - $odeduction -> getRepaymentAmount() <= 0 ){
									
									$odeduction -> setStatus(2);
									$odeduction -> setCompletedDate( $payroll -> getPayDate());
									$odeduction -> setBalance( 0 ); 
									$deductionLine -> setBalance( 0 );
								}
								else{
									$odeduction -> setStatus(1);
									$odeduction -> setBalance( $currentBalance - $odeduction -> getRepaymentAmount());
									$deductionLine -> setBalance( $currentBalance - $odeduction -> getRepaymentAmount());	
								} 
								
								$deductionLine -> setAmount(  $odeduction -> getRepaymentAmount() );
								
							}
							else{
								 $odeduction -> setStatus(2);
								$odeduction -> setCompletedDate( $payroll -> getPayDate());
								 $odeduction -> setBalance( $currentBalance - $odeduction -> getAmount() );
								 $deductionLine -> setBalance( $currentBalance - $odeduction -> getAmount() );
								 $deductionLine -> setAmount( $odeduction -> getAmount());
							}
							
							$deductionService -> saveEmployeeDeduction( $odeduction );	
							$deductionService -> saveDeductionLine($deductionLine);
							
						}
					}
					
				}
			// Payslip
			$payslip = new Payslip();
			$payslip -> setEmpNumber($employee -> getEmpNumber());
			$payslip -> setDateCommited(date('Y-m-d'));
			$payslip -> setPayrollId($payroll -> getPayrollId());
			$payslip -> setIsCommitted(1);
			$payslipService -> savePayslip($payslip);

		}
		return TRUE;
	}

	/*
	 *
	 */
	function getPayPeriods() {
		
		 
		$periods = array( date( 'm' ) => date('F'));
		
		return $periods;
	}

	/*
	 *
	 */
	function processPayslip(Payslip $payslip,EmployeeService $employeeService,PensionEmployeeService $pensionEmpService, PensionService $pensionService,TaxRateService $taxRateService ) {
			
		$payroll = $this -> payrollDao -> getPayroll( $payslip -> getPayrollId()); 
		$employee = $employeeService -> getEmployee( $payslip -> getEmpNumber());	 
		//get employee pension and calculate tax rate
		$empPension = $pensionEmpService -> getPenEmp(NULL, $employee -> getEmpNumber());
		$pension = $pensionService -> getPension($empPension -> getPensionId());
		$salaries = $employeeService -> getEmployeeSalaries($employee -> getEmpNumber());
		$totalSalary = 0;
		$deductionService = new DeductionService();
		
		foreach( $salaries as $salary ){
			$totalSalary += $salary -> getAmount( );
		}
		
		$salaries = $totalSalary;
		
		//deduct pension
		$pensionAmount = ($salaries * ($pension -> getPercentEmployee() / 100));
		// calcute paye
		$salary = ($salaries - $pensionAmount);
		
		
		$taxRate = $taxRateService -> getTaxRate($salary) -> getFirst();
		
		$excessAmount = (($salary - $taxRate -> getSalaryFrom()) * ($taxRate -> getRatePercent() / 100));
		$paye = $excessAmount + $taxRate -> getRateAmount();
		$takehome = $salary - $paye;

		//check for loan information 
		$loanService = new LoanService();
		$loanDeduction = $loanService -> getLoanDeduction(null,$payslip -> getPayrollId(),$employee -> getEmpNumber());
		$loanDeduction = $loanDeduction -> getFirst();
		
		#other deductions
		$otherDeductions = $deductionService -> getDeductionLines( null, $payslip -> getPayrollId(), $payslip -> getEmpNumber());
		
		
		//set result data
		$result = new PayslipResult();
		$result -> setPayrollId($payroll -> getPayrollId());
		$result -> setEmpNumber($employee -> getEmpNumber());
		$result -> setEmployeeName($employee -> getFullName());
		$result -> setBasicSalary($salaries);
		$result -> setPaye($paye);
		$result -> addDeduction( $paye );
		$result -> setPension($pensionAmount);
		$result -> addDeduction($pensionAmount );
		$result -> setPensionName($pension -> getPensionName());
		$result -> setTakeHome($takehome);
		$result -> setPayDate($payroll -> getPayDate());
		$result -> setPayslipId($payslip -> getPayslipId());
		$result -> setCommitment( $payslip -> getIsCommitted());
		$result -> setJobTitle( $employee -> getJobTitleName());
		
		
		if( ! empty( $loanDeduction ) ){
			$loan = $loanService -> getLoans( $loanDeduction -> getLoanId()) -> getFirst();
			$loanOrgS = new FinancialOrganizationService();
			$loanOrg = $loanOrgS -> getFinancialOrganizations( $loan -> getOrganizationId()) -> getFirst(); 
			$result -> setLoanTitle( $loan -> getTitle().' - '.$loanOrg -> getName());
			$result -> setTakeHome( $result -> getTakeHome(false) - $loanDeduction -> getAmount());
			$result -> setLoanDeductionAmount( $loanDeduction -> getAmount()); 
			$result -> addDeduction( $loanDeduction -> getAmount());
		}
		#other Deductions
		if( ! is_null( $otherDeductions )) { 
			foreach( $otherDeductions as $deduction ) { 
				$deductionObj = $deductionService -> getDeductions( $deduction -> getDeductionId()) -> getFirst();
				$result -> addOtherDeduction($deductionObj);
				
				if( $deductionObj -> getIsRecurring() == 1 ){
					$result -> setTakeHome( ($result -> getTakeHome(false) - $deductionObj -> getRepaymentAmount()) );	
				}else{
					$result -> setTakeHome( ($result -> getTakeHome(false) - $deductionObj -> getAmount() ));
				}
					
			}	
		}
		return $result;
		
	}
	/*
	 * 
	 */
	 function getPayroll( $payrollId ){
	 	return $this -> payrollDao -> getPayroll( $payrollId );
	 }
}
?>
