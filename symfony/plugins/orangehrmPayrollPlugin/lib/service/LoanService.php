<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class LoanService extends  BaseService{
    	private $loanDao;
		
		function __construct(){
			$this ->loanDao = new LoanDao();
		}
		
		function getLoans( $loanId = null , $organization_id = null){
			
			return $this -> loanDao -> getLoans( $loanId , $organization_id);
		}
		/*
		 * 
		 */
		function saveLoan( Loan $loan ){
			return $this -> loanDao -> saveLoan($loan);
		}
		/*
		 * 
		 */
		function getEmployeesByLoan( $loanId ){  
			return $this -> loanDao -> getEmployeesByLoan($loanId);
		}
		/*
		 * 
		 * 
		 */
		 function deleteLoans( $loanIds ){
	   	
			 
			if( count( $loanIds ) > 0 ){ 				
				$isDeletionSuccess = $this -> loanDao -> deleteLoans( $loanIds );
				
				return $isDeletionSuccess;
			}
			return false;
	   }
		 /*
		  * 
		  */
		 function getEmployeesLoans( $empLoanId  = null , $empId = null ,$loanId=null){
		 	return $this ->loanDao -> getEmployeesLoans( $empLoanId ,$empId,$loanId);
		 }
		 /*
		  * 
		  */
		  function saveEmployeeLoan( EmployeeLoan  $employeeLoan ){
		  	
		  	return $this -> loanDao -> saveEmployeeLoan( $employeeLoan );
		  }
		  /*
		   * 
		   */
		   function employeeHasPendingLoans( $empNumber ){
		   	
			$employeeLoans = $this -> loanDao -> getEmployeesLoans(null,$empNumber );
			
			if( count( $employeeLoans ) ==  0 ){
				
				return false;
			}
			else{
				 
				foreach( $employeeLoans as $employeeLoan ){
 					if( ! $employeeLoan -> getStatus() === 2 ){
						return false;
					}
				}
				
				return true;
			}
			
		   }
		   /*
		    * 
		    */
		    function getEmployeePendingLoans( $empNumber ){
		    	return $this -> loanDao -> getEmployeePendingLoans($empNumber);
		    }
			/*
			 * 
			 */
			 function saveLoanDeduction( LoanDeduction $loanDeduction ){
			 	return $this -> loanDao -> saveLoanDeduction($loanDeduction);
			 }
			 /*
			  * 
			  */
			  function getLoanDeduction( $loanDeductionId = null , $payrollId = null, $empNumber = null ){
			  	
				return $this -> loanDao -> getLoanDeduction($loanDeductionId,$payrollId,$empNumber);
			  }
		/*
		 * 
		 */
		 function deleteEmployeeLoan( $empLoanId ){
		 	
		 }
		 
		 /*
		  * 
		  */
		  
    }
?>