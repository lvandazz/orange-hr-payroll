<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */

class PensionService extends  BaseService {
	protected $pensionDao;
	/*
	 *
	 *
	 */
	function getPensionDao() {
		if (is_null($this -> pensionDao)) {
			$this -> pensionDao = new PensionDao();
		}
		return $this -> pensionDao;
	}

	/*
	 *
	 *
	 */
	function setPensionDao(PensionDao $pensionDao) {
		$this -> pensionDao = $pensionDao;
	}

	/*
	 *
	 *
	 */
	function __construct() {
		$this -> pensionDao = new PensionDao();
	}

	/*
	 * 		    *
	 */

	function getPensions() {
		return $this -> pensionDao -> getPensions();
	}

	/*
	 *
	 *
	 */
	function getPension($pensionId) {
		return $this -> pensionDao -> getPension($pensionId);
	}

	/*
	 *
	 *
	 */
	function savePension(Pension $pension) {
		return $this -> pensionDao -> savePension($pension);
	}
	/*
	 * 
	 * 
	 */
	 function deletePensions($pensionIds ){
	 		
	 	$employeePensionService = new PensionEmployeeService();
		
		foreach( $pensionIds as $pensionId ){
			if( count( $employeePensionService -> getPensionsEmployees( $pensionId )) === 0 ){
				if( $this -> pensionDao -> deletePension($pensionId) == false ){
					return false;
				} 
			}
			else
			return false;
		}
		return true;
	 }
	
}
?>