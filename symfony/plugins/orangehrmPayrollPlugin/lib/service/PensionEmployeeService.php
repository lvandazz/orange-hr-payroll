<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
  class PensionEmployeeService extends  BaseService{
  	
	
	private $pensionEmployeeDao;
	
	function __construct(){
		$this -> pensionEmployeeDao = new PensionEmployeeDao();
	}
	/*
	 * 
	 * 
	 */
	 function savePensionEmployee( PensionEmployee $pensionEmployee ){
	 	return $this -> pensionEmployeeDao -> savePensionEmployee($pensionEmployee);
	 }
	 /*
	  * 
	  * 
	  */
	  function getPenEmp( $penEmpId = NULL , $empNumber = NULL ){
	  	if( $penEmpId != NULL )
	  		return $this -> pensionEmployeeDao -> getPenEmp($penEmpId) -> getFirst();
		if( $empNumber != NULL )
			return $this -> pensionEmployeeDao -> getPenEmp(NULL, $empNumber ) -> getFirst();
		
		return null;
	  }
	  /*
	   * 
	   * 
	   */
	   function getPensionsEmployees($pensionId=null){
	   		return $this -> pensionEmployeeDao -> getPensionsEmployees($pensionId);
	   }
	   /*
	    * 
	    * 
	    */
	    function getEmployeePension( $empNumber ){
	    	
	    }
		/*
		 * 
		 */
		 function getPensionDeduction( $payrollId , $empNumber , $pensionId=null){
		 	
			$result = $this -> pensionEmployeeDao -> getPensionDeduction($payrollId, $empNumber, $pensionId);
			return $result -> getFirst();
		 }
		
  } 