<html>
	<head>
		<title>Pensions Report</title>
		<style>
			.report, td {
				padding: 1px;
			}
			.report {
				margin: 0 auto;
			}
			table {
				width: 100%;
				margin: 0 auto;
			}
			body {
				margin: 0 auto;
				font-family: 'Helvetica';
			}
			h3, h4 {
				text-align: center
			}
			
			.table-header, .td-data {
				background: #CCC;
				border: 1px solid #CCC;
				font-weight: bold;
			}
			.title {
				font-weight: bold;
			}
			.bottom-line td {
				border-bottom: 1px solid #CCC;
			}
			table{ 
				border: 0px solid #444;
			}
			td{
				border: 1px solid #444;
			}
	</style>
  				
	</head>
	<body>
		<div class = 'report'>
					<h3>
						ASYX Group Company Ltd
					</h3>
					<h3>
						 P.O.Box 123,Dar es Salaam 
					</h3>
					<h4>
						 Pensions Report
					</h4>
					<br/>
					<table class = 'table hover' >
		
				<tr class = 'table-header'>
					<td>#</td><td>Payroll Month</td><td>Employee Name</td> 
					<td>Pension</td><td>Employee's Share</td><td>Employer's Share</td><td>Total</td>
				</tr>
			</thead>
			<tbody>
				<?php
					if( isset( $fullPensions ) AND count( $fullPensions ) > 0 ){
						$i = 1;	
						
						foreach( $fullPensions as $line ){
				?>
				<tr>
					<td><?php echo $i ++; ?></td>
					<td><?php echo $line -> getPayDate(); ?></td>
					<td><?php echo $line -> getEmployeeName(); ?></td>
					<td><?php echo $line -> getPensionName(); ?></td>
					<td><?php echo $line -> getPension(true); ?></td>
					<td><?php echo $line -> getEmployerShare(false); ?></td> 
					<td><?php echo $line -> getTotLinePension(false); ?>
				</tr>
				<?php }
					?>
				
						<tr class = 'table-header'>
							<th colspan = '4' >Total<td> <?php echo $totals -> getTotEmpPension(false); ?></td>
							<td><?php echo $totals -> getTotEmplyrPension( false ); ?></td>
							<td><?php echo $totals -> getTotalPension(false );?></td> 
						</tr>
			</thead>
					<?php } ?>
				
			</tbody>
		</table>
		
					<p class="P3">
						<span class="T4">Date:</span><span class="T10"> ______________________</span>
					</p> 
					<p class="No_20_Spacing">
						<span class="T4">Director:</span><span class="title"> ____________________________</span>
					</p>
	</div>
	</body>
</html>

				
