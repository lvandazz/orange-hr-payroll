<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    
?>
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/addPensionSuccess'));
  
?>

 <div class="box" id="addPension">

    <div class="head">
        <h1><?php echo isset($pensionId) ? __('Edit Pension') : __('Add Pension '); ?></h1>
    </div>

    <div class="inner">
        <?php include_partial('global/flash_messages'); ?>
        <form name="frmAddPension" id="frmAddPension" method="post">
		 
            <?php echo $form['_csrf_token'];  ?>
            <fieldset>
                <ol>
                    <li>
                        <?php echo $form['pensionName']->renderLabel(__('Pension Name') . ' <em>*</em>'); ?>
                        <?php echo $form['pensionName']->render(array("maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['percentEmployer']->renderLabel(__('Percent by Employer'). ' <em>*</em>' ); ?>
                        <?php echo $form['percentEmployer']->render(array("maxlength" => 5)); ?>
                    </li>
                    <li>
                        <?php echo $form['percentEmployee']->renderLabel(__('Percent Employee'). ' <em>*</em>'); ?>
                        <?php echo $form['percentEmployee']->render(array("maxlength" => 5 )); ?>
                    </li>
                    
                    <li class="required">
                        <em>*</em> <?php echo __(CommonMessages::REQUIRED_FIELD); ?>
                    </li>
                    
                </ol>
                <p>
                    <?php  
                    if (isset($pensionId )) { ?>
                        <?php if($pensionPermissions->canUpdate() && isset($pensionId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave" value="<?php echo __("Edit"); ?>"/>
                        <?php }?>
                        <input type="button" class="cancel" name="btnBack" id="btnBack" value="<?php echo __("Back"); ?>"/>
                    <?php } else { ?>
                        <?php if($pensionPermissions->canCreate() && empty($pensionId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave"value="<?php echo __("Save"); ?>"/>
                    <?php }} ?>
                </p>
            </fieldset>
        </form>
    </div>
     
</div>
<script language="JavaScript">
	var lang_edit = "<?php echo __("Edit"); ?>";
    var lang_save = "<?php echo __("Save"); ?>";
    var lang_cancel = "<?php echo __("Cancel"); ?>";
    var lang_back = "<?php echo __("Back"); ?>";
    var linkForAddPension = "<?php echo url_for('payroll/addPension'); ?>";
    var backBtnUrl = "<?php echo url_for( 'payroll/viewPensions' ); ?>";
    var cancelBtnUrl = "<?php echo url_for( 'payroll/addPension' ); ?>";
    <?php if( isset( $pensionId ) ){?>
    	var pensionId = '<?php echo $pensionId; ?>';
    <?php }else{?>
    	var pensionId = '';
    <?php } ?>
     
</script>