 
		<style>
			 
			.payslip, td {
				padding: 1px;
			}
			.payslip { 
				margin: 0 auto;
			}
			table {
				width: 100%;
				margin: 0 auto; 
				padding-top: 11px; 
			}
			.body{
				padding-top: 10px;
				padding-bottom: 20px;
				margin: 0 auto;
				font-family: 'Helvetica';
				width:80%;
			}
			h3,h4{
				text-align: center;
				font-weight: bold;
				font-size: 1.1em;
			}
			
			.table-header, .th-data{
				background : #CCC;
				border: 1px solid #CCC;
				font-weight: bold;
			}
			.content{
				 
			}
			.title{
				font-weight : bold; 
			}
			.bottom-line td{
				border-bottom: 1px solid #CCC;
			}
			.back{
				width: 66px;
				height: 33px;
				background:#33ac3f;
				margin-right: 20px;
				margin-top: 13px;
				float: right;
				clear:both;
				border: 1px solid #33ac3f;
				border-radius: 6px;
			}
			.back label{
				padding-bottom: 4px;
				color: #fff;
			}
			
	</style> 
	<div class='innner'>
	<button class='back' onclick= "goBack()"><label>Back</label></button>
	</div>
	
	<div class = 'body'>
		<div class = 'payslip'>
					<h3>
						Asyx Group Ltd
					</h3>
					<h3>
						 P.O.Box 123,Dar es Salaam 
					</h3>
					<h4>
						 Salary Slip 
					</h4>
					<br/>
						<span class="title">Employee Name:</span>
						<span class="content"> <?php echo $content -> getEmployeeName(); ?></span>
					<br/>
						<span class="title">Designation:</span>
						<span class="content"><?php echo $content -> getJobTitle();?> </span>
					<br/>
						<span class="title">Payroll Period:</span>
						<span class="content"> <?php echo $content -> getPayDate(); ?></span>
					</p> 
					<br>
					<table>
					 
						<tr class="table-header">
							<td  class="th-data">  Earnings  
							</td>
							<td  class="th-data">  
							</td>
							<td  class="th-data"> Deductions 
							</td>
							<td class="th-data" > 	 
							
							</td>
						</tr>
						<tr class="Table21">
							<td>
								<span class="content">Salary</span>
							</td><td>
							
								<span class="content"><?php echo $content -> getBasicSalary(); ?></span>
							</td><td>
							
								<span class="content"><?php echo $content -> getPensionName(); ?></span>
							</td><td>
							
								<span class="content"><?php echo $content -> getPension(); ?></span>
							</td>
						</tr>
						<tr class="Table21">
							<td>
							
								<span class="title"> </span>
							</td><td>
							
								<span class="title"> </span>
							</td><td>
							
								<span class="content">PAYE</span>
							</td><td>
							
								<span class="content"><?php echo $content -> getPaye(); ?></span>
							</td>
						</tr>
						<tr class="Table21">
							<td>
							
								<span class="title"> </span>
							</td><td>
							
								<span class="title"> </span>
							</td><td>
							
								<span class="content"><?php echo ( is_null($content -> getLoanTitle()) ? '' : $content -> getLoanTitle() );?></span>
							</td><td>
							
								<span class="content"><?php echo ( is_null($content -> getLoanTitle()) ? '-' : $content -> getLoanDeductionAmount() );?></span>
							</td>
						</tr>
						<tr class="Table21">
							<td>
								 
							</td><td>
								 
							</td><td>
							
								<span class="title"> </span>
							</td><td>
							
								<span class="title"> </span>
							</td>
						</tr>
						<tr class="Table21">
							<td>
								 
							</td><td>
								 
							</td><td>
							
								<span class="title"></span>
							</td><td>
							
								<span class="title"> </span>
							</td>
						</tr>
						<tr class="Table21">
							<td>
								 
							</td><td>
								 
							</td><td>
								 
							</td><td>
								 
							</td>
						</tr>
						<tr class="bottom-line">
							<td> 
								<span class="title">Total Earnings</span>
							</td><td>
							
								<span class="content"><?php echo $content -> getBasicSalary(); ?></span>
							</td><td>
							
								<span class="title">Total Deductions</span>
							</td><td>
							<span class="content"><?php echo number_format($content -> getTotalDeduction()); ?></span>
							</td>
						</tr>
						<tr class="bottom-line">
							<td>
								 
							</td><td>
							</td><td class = 'title'> NET Salary </span>
							</td><td>
							
								<span class="content"><?php echo $content -> getTakeHome(); ?></span>
							</td>
						</tr>
						<tr class="Table21">
							<td>
								 
							</td><td>
								 
							</td><td>
								 
							</td><td>
								 
							</td>
						</tr>
					</table>
					<p class="P3">
						 
					</p>
					 
					<p class="No_20_Spacing">
						<span class="T4">Cheque No:</span><span class="T10"> _________________     </span><span class="T4">Name of Bank:</span><span class="T10"> __________________________</span>
					</p>
					<p>
						<span class="T4">Date:</span><span class="T10"> ______________________</span>
					</p>
					<p class="P3">
						 
					</p>
					<p class="No_20_Spacing">
						<span class="content">Signature of the Employee: ________________ </span><span class="T4">Director:</span><span class="title"> ____________________________</span>
					</p>

	</div>
	<script>
		function goBack(){
			location.href='<?php echo url_for('payroll/viewPayslips' ); ?>'
		}
	</script>


				