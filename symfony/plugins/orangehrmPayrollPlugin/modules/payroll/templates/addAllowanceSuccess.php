<?php
/*
 *@author Joseph Luvanda 
 *
 * 
 */
 ?>
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/addAllowanceSuccess'));
  
?>

 <div class="box" id="addAllowance">

    <div class="head">
        <h1><?php echo isset($allowanceId) ? __('Edit Allowance') : __('Add Allowance '); ?></h1>
    </div>

    <div class="inner">
        <?php include_partial('global/flash_messages'); ?>
        <form name="frmAddAllowance" id="frmAddAllowance" method="post">
		 
            <?php echo $form['_csrf_token'];  ?>
            <fieldset>
                <ol>
                    <li>
                        <?php echo $form['allowanceTitle']->renderLabel(__('Title') . ' <em>*</em>'); ?>
                        <?php echo $form['allowanceTitle']->render(array("maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['allowanceAmount']->renderLabel(__('Amount') ); ?>
                        <?php echo $form['allowanceAmount']->render(array("maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['allowancePercentage']->renderLabel(__('Percentage Basic')); ?>
                        <?php echo $form['allowancePercentage']->render(array("maxlength" => 100)); ?>
                    </li>
                    
                    <li class="required">
                        <em>*</em> <?php echo __(CommonMessages::REQUIRED_FIELD); ?>
                    </li>
                    
                </ol>
                <p>
                    <?php  
                    if (isset($allowanceId )) { ?>
                        <?php if($allowancePermissions->canUpdate() && isset($allowanceId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave" value="<?php echo __("Edit"); ?>"/>
                        <?php }?>
                        <input type="button" class="cancel" name="btnBack" id="btnBack" value="<?php echo __("Back"); ?>"/>
                    <?php } else { ?>
                        <?php if($allowancePermissions->canCreate() && empty($allowanceId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave"value="<?php echo __("Save"); ?>"/>
                    <?php }} ?>
                </p>
            </fieldset>
        </form>
    </div>
     
</div>
<script language="JavaScript">
	var lang_edit = "<?php echo __("Edit"); ?>";
    var lang_save = "<?php echo __("Save"); ?>";
    var lang_cancel = "<?php echo __("Cancel"); ?>";
    var lang_back = "<?php echo __("Back"); ?>";
    var linkForAddAllowance = "<?php echo url_for('payroll/addAllowance'); ?>";
    var backBtnUrl = "<?php echo url_for( 'payroll/viewAllowanceList' ); ?>";
    var cancelBtnUrl = "<?php echo url_for( 'payroll/addAllowance' ); ?>";
    <?php if( isset( $allowanceId ) ){?>
    	var allowanceId = '<?php echo $allowanceId; ?>';
    <?php }else{?>
    	var allowanceId = '';
    <?php } ?>
     
</script>