<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    
?>
<?php if($payrollPermissions->canRead()){?>
<div class="box searchForm toggableForm" id="srchPayroll">
	<div class="head">
        	<h1><?php echo __('Employee Payroll - '. $payroll); ?></h1>
  		</div>
	<div class="inner"> 
	<p>
       <span style='font-weight: bold;'><?php echo __(' Employee Name '); ?>:</span>
       <?php echo $employee['fullName' ]; ?>
       </p>
       <p>
       <span style='font-weight: bold'><?php echo __(' Designation '); ?>:</span>
       <?php echo $employee['title' ]; ?> 
       </p>
       <p>
       <span style='font-weight: bold'><?php echo __(' Salary '); ?>:</span>
       <?php echo $employee['salary' ]; ?>
       </p>
       <br>
       <p>
       	<a target= '__blank' href = '<?php echo url_for( 'pim/viewEmployee/').'/empNumber/'.$employee[ 'empNumber' ]; ?>' class='add' > View Profile </a> &nbsp;&nbsp;
       	<a href = '<?php echo url_for( 'payroll/runPayroll'); ?>' class='add' > Back  </a>
       </p>  
    </div>
    <a href="#" class="toggle tiptip" title="<?php echo __(CommonMessages::TOGGABLE_DEFAULT_MESSAGE); ?>">&gt;</a>
</div>

<?php include_component('core', 'ohrmList', $parmetersForListCompoment); ?>
<div id=" " class="box">

	<div class="head">
		<h1>Other Deductions</h1>
	</div>

	<div class="inner">	
			<table class="table hover" id="resultTable">
				<thead>
					<tr>
						
						<th>Title</th>
						<th>Amount</th>
						<th>Date Created</th>
						<th>Balance</th>
						<th>Status</th>
						<th>Recurring ?</th>
					</tr>
				</thead>
				<tbody>
					<?php
				 
						if( count( $otherDeductions ) > 0  ){
							foreach( $otherDeductions as $deduction ){							
							?>
							<tr>
								<td><a target = "_blank" href = '<?php echo url_for( 'payroll/addDeduction?deductionId='.$deduction -> getDeductionId()); ?>'><?php echo $deduction -> getTitle (); ?></a></td>
								<td><?php echo $deduction -> getAmountX(); ?></td>
								<td><?php echo $deduction -> getDateCreated(); ?></td>
								<td><?php echo $deduction -> getBalanceX(); ?></td>
								<td><?php echo $deduction -> getStatusX(); ?></td>
								<td><?php echo $deduction -> getRecurringX(); ?></td>
							</tr>
						<?php
							}
						}
						else{
						?>
					<tr>
						<td class="check" style="display: none;"></td>
						<td>No Records Found</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
	
	</div>

</div>
<form name="frmHiddenParam" id="frmHiddenParam" method="post" action="<?php echo url_for('payroll/viewPayroll'); ?>">
    <input type="hidden" name="pageNo" id="pageNo" value="<?php //echo $form->pageNo;         ?>" />
    <input type="hidden" name="hdnAction" id="hdnAction" value="search" />
</form>
<?php } ?>
<script type="text/javascript">

    function submitPage(pageNo) {

        document.frmHiddenParam.pageNo.value = pageNo;
        document.frmHiddenParam.hdnAction.value = 'paging';
        document.getElementById('frmHiddenParam').submit();

    }
    //<![CDATA[
     var addPensionUrl = '<?php echo url_for('payroll/addPension'); ?>';
    var lang_all = '<?php echo __("All") ?>';
    //]]>
</script>