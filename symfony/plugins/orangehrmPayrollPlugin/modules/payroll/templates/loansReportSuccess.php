<?php

/*
 * 
 * 
 * 
 */ 

?>
<?php if($reportPermissions->canRead()){?> 
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/loansReportSuccess'));
?>
<div class="box searchForm toggableForm" id="srchPayroll">
	<div class="head">
        	<h1><?php echo __('Report Filter'); ?></h1>
  		</div>
	<div class="inner">
        <form name="frmSrch" id="frmSrch" method="post" action="<?php echo url_for('payroll/loansReport'); ?>">
            <fieldset>
                <?php echo $form['_csrf_token']; ?>
                <ol>
                    <li>
                        <?php echo $form['employeeD']->renderLabel(__('Employee'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['employeeD']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li> 
                    <li>
                        <?php echo $form['organization']->renderLabel(__('Organization'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['organization']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li> 
                     <li>
                        <?php echo $form['fromDate']->renderLabel(__('From'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['fromDate']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['toDate']->renderLabel(__('To'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['toDate']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li>  
                </ol>

                <p>
                    <input type="button" id="btnSrch" value="<?php echo __("Search") ?>" name="btnSrch" />
                    <?php 
                    
                    	if( isset( $fullLoans ) AND count( $fullLoans ) > 0 ){
                    		?>
                    	<input type="button" id="btnPrint" value="<?php echo __("Export to PDF") ?>" name="btnPrint" />		
                    		<?php	
                    	} 
					?>   
                </p>
            </fieldset>            
        </form>
    </div>
    <a href="#" class="toggle tiptip" title="<?php echo __(CommonMessages::TOGGABLE_DEFAULT_MESSAGE); ?>">&gt;</a>
</div>
<div class = 'box'>
	<div class = 'head'>
			<h1>Loans Report</h1>
	</div>
<div class = 'inner' >
		<table class = 'table hover' >
			<thead>
				<tr>
					<th>#</th><th>Payroll Month</th><th>Employee Name</th> 
					<th>Loan</th><th>Principal</th><th>Deduction</th><th>Balance</th><th>Accrued Amount</th>
				</tr>
			</thead>
			<tbody>
				<?php
					if( isset( $fullLoans ) AND count( $fullLoans ) > 0 ){
						$i = 1;	
						
						foreach( $fullLoans as $line ){
				?>
				<tr>
					<td><?php echo $i ++; ?></td>
					<td><?php echo $line -> getPayDate(); ?></td>
					<td><?php echo $line -> getEmployeeName(); ?></td>
					<td><?php echo $line -> getLoanTitle(); ?></td>
					<td><?php echo $line -> getLoanPrincipal(false) == 0 ? '"' :$line -> getLoanPrincipal(false); ?></td>
					<td><?php echo $line -> getLoanDeductionAmount(false); ?></td> 
					<td><?php echo $line -> getLoanBalance(false) == 0 ?  '"' :$line -> getLoanBalance(false); ; ?><br />
						<td><?php echo $line -> getLoanAccAmount(false) == 0 ?  '"' :$line -> getLoanAccAmount(false) ; ?>
				</tr>
				<?php }
					?>
					<thead>
						<tr>
							<th colspan = '4' >Total<th> <?php echo $totals -> getTotalPrincipal(false); ?></th>
							<th><?php echo $totals -> getTotalLoanDeduction( false ); ?></th>
							<th><?php echo $totals -> getTotalBalance(false); ?></th>
							<th><?php echo $totals -> getTotalAcc(false );?></th> 
						</tr>
			</thead>
					<?php } ?>
				
			</tbody>
		</table>
	</div>
</div>
<?php //include_component('core', 'ohrmList', $parmetersForListCompoment); ?>

<!-- Confirmation box HTML: Begins -->
<div class="modal hide" id="deleteConfirmation">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3><?php echo __('OrangeHRM - Confirmation Required'); ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo __(CommonMessages::DELETE_CONFIRMATION); ?></p>
    </div>
    <div class="modal-footer">
        <input type="button" class="btn" data-dismiss="modal" id="dialogDeleteBtn" value="<?php echo __('Ok'); ?>" />
        <input type="button" class="btn reset" data-dismiss="modal" value="<?php echo __('Cancel'); ?>" />
    </div>
</div>
<!-- Confirmation box HTML: Ends -->

<form name="frmHiddenParam" id="frmHiddenParam" method="post" action="<?php echo url_for('payroll/payrollReport'); ?>">
    <input type="hidden" name="pageNo" id="pageNo" value="<?php echo $form->pageNo;         ?>" />
    <input type="hidden" name="hdnAction" id="hdnAction" value="search" />
</form>
<?php } ?>
<script type="text/javascript">

    function submitPage(pageNo) {

        document.frmHiddenParam.pageNo.value = pageNo;
        document.frmHiddenParam.hdnAction.value = 'paging';
        document.getElementById('frmHiddenParam').submit();

    }
    //<![CDATA[
    var addDeductionUrl = '<?php echo url_for('payroll/addDeduction'); ?>';
    
    var lang_all = '<?php echo __("All") ?>';
    //]]>
</script>