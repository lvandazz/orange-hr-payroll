<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    
?>
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/addLoanSuccess'));
  
?>

 <div class="box" id="addLoan">

    <div class="head">
        <h1><?php echo isset($loanId) ? __('Edit Loan') : __('Add Loan'); ?></h1>
    </div>

    <div class="inner">
        <?php include_partial('global/flash_messages'); ?>
        <form name="frmAddLoan" id="frmAddLoan" method="post">
		 
            <?php echo $form['_csrf_token'];  ?>
            <?php //echo $form[ '' ] -> render(); ?>
            <fieldset>
                <ol>
                    <li>
                        <?php echo $form['loanTitle']->renderLabel(__('Title') . ' <em>*</em>'); ?>
                        <?php echo $form['loanTitle']->render(array("maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['loanOrganization']->renderLabel(__('Organization'). ' <em>*</em>'); ?>
                        <?php echo $form['loanOrganization']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['loanPrincipal']->renderLabel(__('Amount'). ' <em>*</em>' ); ?>
                        <?php echo $form['loanPrincipal']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['loanTerm']->renderLabel(__('Term'). ' <em>*</em>'); ?>
                        <?php echo $form['loanTerm']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['loanInterest']->renderLabel(__('Interest'). ' <em>*</em>'); ?>
                        <?php echo $form['loanInterest']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['loanDescription']->renderLabel(__('Description')); ?>
                        <?php echo $form['loanDescription']->render(); ?>
                    </li>
                    <li class="required">
                        <em>*</em> <?php echo __(CommonMessages::REQUIRED_FIELD); ?>
                    </li>
                    
                </ol>
                <p>
                    <?php  
                    if (isset($loanId )) { ?>
                        <?php if($loansPermissions->canUpdate() && isset($loanId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave" value="<?php echo __("Edit"); ?>"/>
                        <?php }?>
                        <input type="button" class="cancel" name="btnBack" id="btnBack" value="<?php echo __("Back"); ?>"/>
                    <?php } else { ?>
                        <?php if($loansPermissions->canCreate() && empty($loanId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave"value="<?php echo __("Save"); ?>"/>
                    <?php }} ?>
                </p>
            </fieldset>
        </form>
    </div>
     
</div>
<script language="JavaScript">
	var lang_edit = "<?php echo __("Edit"); ?>";
    var lang_save = "<?php echo __("Save"); ?>";
    var lang_cancel = "<?php echo __("Cancel"); ?>";
    var lang_back = "<?php echo __("Back"); ?>";
    var linkForAddLoan = "<?php echo url_for('payroll/addLoan'); ?>";
    var backBtnUrl = "<?php echo url_for( 'payroll/viewLoans' ); ?>";
    var cancelBtnUrl = "<?php echo url_for( 'payroll/addLoan' ); ?>";
    <?php if( isset( $loanId ) ){?>
    	var loanId = '<?php echo $loanId; ?>';
    <?php }else{?>
    	var loanId = '';
    <?php } ?>
     
</script>