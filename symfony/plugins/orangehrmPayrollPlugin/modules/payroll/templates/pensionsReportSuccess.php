<?php

/*
 * 
 * 
 * 
 */ 

?>
<?php if($reportPermissions->canRead()){?> 
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/pensionsReportSuccess'));
?>
<div class="box searchForm toggableForm" id="srchPayroll">
	<div class="head">
        	<h1><?php echo __('Report Filter'); ?></h1>
  		</div>
	<div class="inner">
        <form name="frmSrch" id="frmSrch" method="post" action="<?php echo url_for('payroll/pensionsReport'); ?>">
            <fieldset>
                <?php echo $form['_csrf_token']; ?>
                <ol>
                    <li>
                        <?php echo $form['employeeD']->renderLabel(__('Employee'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['employeeD']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li> 
                    <li>
                        <?php echo $form['pension']->renderLabel(__('Pension'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['pension']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li> 
                     <li>
                        <?php echo $form['fromDate']->renderLabel(__('From'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['fromDate']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['toDate']->renderLabel(__('To'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['toDate']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li>  
                </ol>

                <p>
                    <input type="button" id="btnSrch" value="<?php echo __("Search") ?>" name="btnSrch" />
                    <?php
                    	if( isset( $fullPensions ) AND count( $fullPensions ) > 0 ){
                    		?>
                    		<input type="button" id="btnPrint" value="<?php echo __("Export to PDF") ?>" name="btnPrint" />
                    	<?php		
                    	
							}    
						?>
                </p>
            </fieldset>            
        </form>
    </div>
    <a href="#" class="toggle tiptip" title="<?php echo __(CommonMessages::TOGGABLE_DEFAULT_MESSAGE); ?>">&gt;</a>
</div>
<div class = 'box'>
	<div class = 'head'>
			<h1>Pensions Report</h1>
	</div>
<div class = 'inner' >
		<table class = 'table hover' >
			<thead>
				<tr>
					<th>#</th><th>Payroll Month</th><th>Employee Name</th> 
					<th>Pension</th><th>Employee's Share</th><th>Employer's Share</th><th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php
					if( isset( $fullPensions ) AND count( $fullPensions ) > 0 ){
						$i = 1;	
						
						foreach( $fullPensions as $line ){
				?>
				<tr>
					<td><?php echo $i ++; ?></td>
					<td><?php echo $line -> getPayDate(); ?></td>
					<td><?php echo $line -> getEmployeeName(); ?></td>
					<td><?php echo $line -> getPensionName(); ?></td>
					<td><?php echo $line -> getPension(true); ?></td>
					<td><?php echo $line -> getEmployerShare(false); ?></td> 
					<td><?php echo $line -> getTotLinePension(false); ?>
				</tr>
				<?php }
					?>
					<thead>
						<tr>
							<th colspan = '4' >Total<th> <?php echo $totals -> getTotEmpPension(false); ?></th>
							<th><?php echo $totals -> getTotEmplyrPension( false ); ?></th>
							<th><?php echo $totals -> getTotalPension(false );?></th> 
						</tr>
			</thead>
					<?php } ?>
				
			</tbody>
		</table>
	</div>
</div>
<?php //include_component('core', 'ohrmList', $parmetersForListCompoment); ?>

<!-- Confirmation box HTML: Begins -->
<div class="modal hide" id="deleteConfirmation">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3><?php echo __('OrangeHRM - Confirmation Required'); ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo __(CommonMessages::DELETE_CONFIRMATION); ?></p>
    </div>
    <div class="modal-footer">
        <input type="button" class="btn" data-dismiss="modal" id="dialogDeleteBtn" value="<?php echo __('Ok'); ?>" />
        <input type="button" class="btn reset" data-dismiss="modal" value="<?php echo __('Cancel'); ?>" />
    </div>
</div>
<!-- Confirmation box HTML: Ends -->

<form name="frmHiddenParam" id="frmHiddenParam" method="post" action="<?php echo url_for('payroll/payrollReport'); ?>">
    <input type="hidden" name="pageNo" id="pageNo" value="<?php echo $form->pageNo;         ?>" />
    <input type="hidden" name="hdnAction" id="hdnAction" value="search" />
</form>
<?php } ?>
<script type="text/javascript">

    function submitPage(pageNo) {

        document.frmHiddenParam.pageNo.value = pageNo;
        document.frmHiddenParam.hdnAction.value = 'paging';
        document.getElementById('frmHiddenParam').submit();

    }
    //<![CDATA[
    var addDeductionUrl = '<?php echo url_for('payroll/addDeduction'); ?>';
    
    var lang_all = '<?php echo __("All") ?>';
    //]]>
</script>