<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    
?>
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/assignPensionSuccess'));
  
?>

 <div class="box" id="assignPension">

    <div class="head">
        <h1><?php echo isset($penEmpId) ? __('Edit Employee Pension') : __('Assign Employee Pension '); ?></h1>
    </div>

    <div class="inner">
        <?php include_partial('global/flash_messages'); ?>
        <form name="frmAssignPension" id="frmAssignPension" method="post">
		 
            <?php echo $form['_csrf_token'];  ?>
            <?php echo $form[ 'employeeNameId' ] -> render(); ?>
            <fieldset>
                <ol>
                    <li>
                        <?php echo $form['employeeName']->renderLabel(__('Employee Name') . ' <em>*</em>'); ?>
                        <?php echo $form['employeeName']->render(array("maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['pensionName']->renderLabel(__('Pension Name'). ' <em>*</em>' ); ?>
                        <?php echo $form['pensionName']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['dateJoin']->renderLabel(__('Date Join'). ' <em>*</em>'); ?>
                        <?php echo $form['dateJoin']->render(); ?>
                    </li>
                    
                    <li class="required">
                        <em>*</em> <?php echo __(CommonMessages::REQUIRED_FIELD); ?>
                    </li>
                    
                </ol>
                <p>
                    <?php  
                    if (isset($penEmpId )) { ?>
                        <?php if($pensionPermissions->canUpdate() && isset($penEmpId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave" value="<?php echo __("Edit"); ?>"/>
                        <?php }?>
                        <input type="button" class="cancel" name="btnBack" id="btnBack" value="<?php echo __("Back"); ?>"/>
                    <?php } else { ?>
                        <?php if($pensionPermissions->canCreate() && empty($penEmpId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave"value="<?php echo __("Save"); ?>"/>
                    <?php }} ?>
                </p>
            </fieldset>
        </form>
    </div>
     
</div>
<script language="JavaScript">
	var lang_edit = "<?php echo __("Edit"); ?>";
    var lang_save = "<?php echo __("Save"); ?>";
    var lang_cancel = "<?php echo __("Cancel"); ?>";
    var lang_back = "<?php echo __("Back"); ?>";
    var linkForAssignPension = "<?php echo url_for('payroll/assignPension'); ?>";
    var backBtnUrl = "<?php echo url_for( 'payroll/viewEmployeesPensions' ); ?>";
    var cancelBtnUrl = "<?php echo url_for( 'payroll/assignPension' ); ?>";
    <?php if( isset( $penEmpId ) ){?>
    	var penEmpId = '<?php echo $penEmpId; ?>';
    <?php }else{?>
    	var penEmpId = '';
    <?php } ?>
     var employeeList =  <?php echo str_replace('&#039;', "'", $form->getEmployeesAsJson()) ?> ;
</script>