<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    
?>
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/viewPayslipsSuccess'));
?>
<?php if($payrollPermissions->canRead()){?>
	
<div class="box searchForm toggableForm" id="srchPayroll">
	<div class="head">
        	<h1><?php echo __('Payslip Filter'); ?></h1>
  		</div>
	<div class="inner">
        <form name="frmSrch" id="frmSrch" method="post" action="<?php echo url_for('payroll/viewPayslips'); ?>">
            <fieldset>
                <?php echo $form['_csrf_token']; ?>
                 <ol>
                    <li>
                        <?php echo $form['employeeD']->renderLabel(__('Employee'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['employeeD']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li> 
                  
                     <li>
                        <?php echo $form['fromDate']->renderLabel(__('From'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['fromDate']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['toDate']->renderLabel(__('To'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['toDate']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li>  
                </ol>


                <p>
                    <input type="button" id="btnSrch" value="<?php echo __("Search") ?>" name="btnSrch" />    
                </p>
            </fieldset>            
        </form>
    </div>
    <a href="#" class="toggle tiptip" title="<?php echo __(CommonMessages::TOGGABLE_DEFAULT_MESSAGE); ?>">&gt;</a>
</div> 
<?php include_component('core', 'ohrmList', $parmetersForListCompoment); ?>

<!-- Confirmation box HTML: Begins -->
<div class="modal hide" id="printConfirmation">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3><?php echo __('OrangeHRM - Confirmation Required'); ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo __(CommonMessages::CONFIRM_PROCESS); ?></p>
    </div>
    <div class="modal-footer">
        <input type="button" class="btn" data-dismiss="modal" id="dialogConfirmPrint" value="<?php echo __('Ok'); ?>" />
        <input type="button" class="btn reset" data-dismiss="modal" value="<?php echo __('Cancel'); ?>" />
    </div>
</div>
<div class="modal hide" id="previewConfirmation">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3><?php echo __('OrangeHRM - Confirmation Required'); ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo __(CommonMessages::CONFIRM_PROCESS); ?></p>
    </div>
    <div class="modal-footer">
        <input type="button" class="btn" data-dismiss="modal" id="dialogConfirmPreview" value="<?php echo __('Ok'); ?>" />
        <input type="button" class="btn reset" data-dismiss="modal" value="<?php echo __('Cancel'); ?>" />
    </div>
</div>
<!-- Confirmation box HTML: Ends -->

<form name="frmHiddenParam" id="frmHiddenParam" method="post" action="<?php echo url_for('payroll/viewPayroll'); ?>">
    <input type="hidden" name="pageNo" id="pageNo" value="<?php //echo $form->pageNo;         ?>" />
    <input type="hidden" name="hdnAction" id="hdnAction" value="search" />
</form>
<?php } ?>
<script type="text/javascript">

    function submitPage(pageNo) {

        document.frmHiddenParam.pageNo.value = pageNo;
        document.frmHiddenParam.hdnAction.value = 'paging';
        document.getElementById('frmHiddenParam').submit();

    }
    //<![CDATA[
     var addPensionUrl = '<?php echo url_for('payroll/addPension'); ?>';
    var lang_all = '<?php echo __("All") ?>';
    var printPayslipUrl = '<?php echo url_for('payroll/printPayslips' ); ?>';
    //]]>
</script>