<?php

/*
 * 
 * 
 * 
 */ 

?>
<?php if($loansPermissions->canRead()){?>
<div class="box searchForm toggableForm" id="srchPayroll">
	<div class="head">
        	<h1><?php echo __('Loans'); ?></h1>
  		</div>
	<div class="inner">
        <form name="frmSrch" id="frmSrch" method="post" action="<?php echo url_for('payroll/viewLoans'); ?>">
            <fieldset>
                <?php echo $form['_csrf_token']; ?>
                <ol>
                    <li>
                        <?php echo $form['organization']->renderLabel(__('Organization'), array("class" => "Label")); ?>
                        <?php echo $form['organization']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li>
                </ol>

                <p>
                    <input type="button" id="btnSrch" value="<?php echo __("Search") ?>" name="btnSrch" />    
                </p>
            </fieldset>            
        </form>
    </div>
    <a href="#" class="toggle tiptip" title="<?php echo __(CommonMessages::TOGGABLE_DEFAULT_MESSAGE); ?>">&gt;</a>
</div>

<?php include_component('core', 'ohrmList', $parmetersForListCompoment); ?>

<!-- Confirmation box HTML: Begins -->
<div class="modal hide" id="deleteConfirmation">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3><?php echo __('OrangeHRM - Confirmation Required'); ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo __(CommonMessages::DELETE_CONFIRMATION); ?></p>
    </div>
    <div class="modal-footer">
        <input type="button" class="btn" data-dismiss="modal" id="dialogDeleteBtn" value="<?php echo __('Ok'); ?>" />
        <input type="button" class="btn reset" data-dismiss="modal" value="<?php echo __('Cancel'); ?>" />
    </div>
</div>
<!-- Confirmation box HTML: Ends -->

<form name="frmHiddenParam" id="frmHiddenParam" method="post" action="<?php echo url_for('payroll/viewLoans'); ?>">
    <input type="hidden" name="pageNo" id="pageNo" value="<?php echo $form->pageNo;         ?>" />
    <input type="hidden" name="hdnAction" id="hdnAction" value="search" />
</form>
<?php } ?>
<script type="text/javascript">

    function submitPage(pageNo) {

        document.frmHiddenParam.pageNo.value = pageNo;
        document.frmHiddenParam.hdnAction.value = 'paging';
        document.getElementById('frmHiddenParam').submit();

    }
    //<![CDATA[
    var addLoanUrl = '<?php echo url_for('payroll/addLoan'); ?>';
    
    var lang_all = '<?php echo __("All") ?>';
    //]]>
</script>