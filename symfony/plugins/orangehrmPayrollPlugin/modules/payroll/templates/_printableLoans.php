<html>
	<head>
		<title>Loans Report</title>
		<style>
			.report, td {
				padding: 1px;
			}
			.report {
				margin: 0 auto;
			}
			table {
				width: 100%;
				margin: 0 auto;
			}
			body {
				margin: 0 auto;
				font-family: 'Helvetica';
			}
			h3, h4 {
				text-align: center
			}
			
			.table-header, .td-data {
				background: #CCC;
				border: 1px solid #CCC;
				font-weight: bold;
			}
			.title {
				font-weight: bold;
			}
			.bottom-line td {
				border-bottom: 1px solid #CCC;
			}
			table{ 
				border: 0px solid #444;
			}
			td{
				border: 1px solid #444;
			}
	</style>
  				
	</head>
	<body>
		<div class = 'report'>
					<h3>
						ASYX Group Company Ltd
					</h3>
					<h3>
						 P.O.Box 123,Dar es Salaam 
					</h3>
					<h4>
						 Loans Report
					</h4>
					<br/>
					<table class = 'table hover' >
			
				<tr class = 'table-header'>
					<td>#</td><td>Payroll Month</td><td>Employee Name</td> 
					<td>Loan</td><td>Principal</td><td>Deduction</td><td>Balance</td><td>Accrued Amount</td>
				</tr>
			
			<tbody>
				<?php
					if( isset( $fullLoans ) AND count( $fullLoans ) > 0 ){
						$i = 1;	
						
						foreach( $fullLoans as $line ){
				?>
				<tr>
					<td><?php echo $i ++; ?></td>
					<td><?php echo $line -> getPayDate(); ?></td>
					<td><?php echo $line -> getEmployeeName(); ?></td>
					<td><?php echo $line -> getLoanTitle(); ?></td>
					<td><?php echo $line -> getLoanPrincipal(false) == 0 ? '"' :$line -> getLoanPrincipal(false); ?></td>
					<td><?php echo $line -> getLoanDeductionAmount(false); ?></td> 
					<td><?php echo $line -> getLoanBalance(false) == 0 ?  '"' :$line -> getLoanBalance(false); ; ?><br />
						<td><?php echo $line -> getLoanAccAmount(false) == 0 ?  '"' :$line -> getLoanAccAmount(false) ; ?>
				</tr>
				<?php }
					?>
					
						<tr>
							<td colspan = '4' >Total<td> <?php echo $totals -> getTotalPrincipal(false); ?></td>
							<td><?php echo $totals -> getTotalLoanDeduction( false ); ?></td>
							<td><?php echo $totals -> getTotalBalance(false); ?></td>
							<td><?php echo $totals -> getTotalAcc(false );?></td> 
						</tr>
			
					<?php } ?>
				
			</tbody>
		</table>
		
					<p class="P3">
						<span class="T4">Date:</span><span class="T10"> ______________________</span>
					</p> 
					<p class="No_20_Spacing">
						<span class="T4">Director:</span><span class="title"> ____________________________</span>
					</p>
	</div>
	</body>
</html>

				
