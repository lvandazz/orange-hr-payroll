<?php

/*
 * 
 * 
 * 
 */ 

?>
<?php if($reportPermissions->canRead()){?> 
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/payeReportSuccess'));
?>
<div class="box searchForm toggableForm" id="srchPayroll">
	<div class="head">
        	<h1><?php echo __('Report Filter'); ?></h1>
  		</div>
	<div class="inner">
        <form name="frmSrch" id="frmSrch" method="post" action="<?php echo url_for('payroll/payrollReport'); ?>">
            <fieldset>
                <?php echo $form['_csrf_token']; ?>
                <ol>
                    <li>
                        <?php echo $form['employeeD']->renderLabel(__('Employee'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['employeeD']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li> 
                    <li>
                        <?php echo $form['subunit']->renderLabel(__('Subunit'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['subunit']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li> 
                     <li>
                        <?php echo $form['fromDate']->renderLabel(__('From'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['fromDate']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['toDate']->renderLabel(__('To'), array("class" => "employeeLabel")); ?>
                        <?php echo $form['toDate']->render(array("class" => "drpDown", "maxlength" => 50)); ?>
                    </li>  
                </ol>

                <p>
                    <input type="button" id="btnSrch" value="<?php echo __("Search") ?>" name="btnSrch" />    
                </p>
            </fieldset>            
        </form>
    </div>
    <a href="#" class="toggle tiptip" title="<?php echo __(CommonMessages::TOGGABLE_DEFAULT_MESSAGE); ?>">&gt;</a>
</div>
<div class = 'box'>
	<div class = 'head'>
			<h1>Payroll Report</h1>
	</div>
<div class = 'inner' >
		<table class = 'table hover' >
			<thead>
				<tr>
					<th>#</th><th>Payroll Month</th><th>Employee Name</th><th>Gross Salary</th>
					<th>Pension</th><th>Tax</th><th>Loans</th><th>Deductions</th><th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php
					if( isset( $fullPayroll ) AND count( $fullPayroll ) > 0 ){
						$i = 1;	
						
						foreach( $fullPayroll as $line ){
				?>
				<tr>
					<td><?php echo $i ++; ?></td>
					<td><?php echo $line -> getPayDate(); ?></td>
					<td><?php echo $line -> getEmployeeName(); ?></td>
					<td><?php echo $line -> getBasicSalary(); ?></td>
					<td><?php echo '-'.$line -> getPension(); ?></td>
					<td><?php echo ' -'.$line -> getPaye(); ?></td>
					<td><?php
							
							$loan = $line -> getLoanDeductionAmount() ;
							if( isset($loan ) AND $loan != 0 ){
								echo ' - '.$line -> getLoanDeductionAmount();
							}else{
								echo '-';
							}
					 ?></td>
					<td><?php 
							$others =  $line -> getOtherDeductions();
						 	if( isset( $others )  AND $others != 0 ){
						 		$total = 0;
						 		foreach( $line -> getOtherDeductions() as $dedu){
						 			if( $dedu -> getIsRecurring() == 1 ){
						 				$total += $dedu -> getRepaymentAmount();
						 			}else{
						 				$total += $dedu -> getAmount();
						 			}
						 		}
							
								echo '-'.($total == 0 ? '' : number_format($total) );
						 	}else{
						 		echo '-';
						 	}
					 ?></td>
					 <td><?php echo $line -> getTakeHome(); ?>
				</tr>
				<?php }
					?>
					<thead>
						<tr>
							<th colspan = '3' >Total<th> <?php echo $totals -> getTotalSalary (false); ?></th>
							<th><?php echo '-'.$totals -> getTotalPension( false ); ?></th>
							<th><?php echo '-'.$totals -> getTotalTax(false );?></th>
							<th><?php echo '-'.$totals -> getTotalLoans(false );?></th>
							<th><?php echo '-'.$totals -> getTotalDeductions( false ); ?></th>
							<th><?php echo $totals -> getTotalTakeHome(false); ?></th>
						</tr>
			</thead>
					<?php } ?>
				
			</tbody>
		</table>
	</div>
</div>
<?php //include_component('core', 'ohrmList', $parmetersForListCompoment); ?>

<!-- Confirmation box HTML: Begins -->
<div class="modal hide" id="deleteConfirmation">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3><?php echo __('OrangeHRM - Confirmation Required'); ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo __(CommonMessages::DELETE_CONFIRMATION); ?></p>
    </div>
    <div class="modal-footer">
        <input type="button" class="btn" data-dismiss="modal" id="dialogDeleteBtn" value="<?php echo __('Ok'); ?>" />
        <input type="button" class="btn reset" data-dismiss="modal" value="<?php echo __('Cancel'); ?>" />
    </div>
</div>
<!-- Confirmation box HTML: Ends -->

<form name="frmHiddenParam" id="frmHiddenParam" method="post" action="<?php echo url_for('payroll/payrollReport'); ?>">
    <input type="hidden" name="pageNo" id="pageNo" value="<?php echo $form->pageNo;         ?>" />
    <input type="hidden" name="hdnAction" id="hdnAction" value="search" />
</form>
<?php } ?>
<script type="text/javascript">

    function submitPage(pageNo) {

        document.frmHiddenParam.pageNo.value = pageNo;
        document.frmHiddenParam.hdnAction.value = 'paging';
        document.getElementById('frmHiddenParam').submit();

    }
    //<![CDATA[
    var addDeductionUrl = '<?php echo url_for('payroll/addDeduction'); ?>';
    
    var lang_all = '<?php echo __("All") ?>';
    //]]>
</script>