<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    
?>
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/addFinancialOrganizationSuccess'));
  
?>

 <div class="box" id="assignPension">

    <div class="head">
        <h1><?php echo isset($organizationId) ? __('Edit Organization') : __('Add Organization '); ?></h1>
    </div>

    <div class="inner">
        <?php include_partial('global/flash_messages'); ?>
        <form name="frmAddFinancialOrganization" id="frmAddFinancialOrganization" method="post">
		 
            <?php echo $form['_csrf_token'];  ?>
            <?php //echo $form[ 'organizationId' ] -> render(); ?>
            <fieldset>
                <ol>
                    <li>
                        <?php echo $form['organizationName']->renderLabel(__('Name') . ' <em>*</em>'); ?>
                        <?php echo $form['organizationName']->render(array("maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['organizationAddress']->renderLabel(__('Address '). ' <em>*</em>' ); ?>
                        <?php echo $form['organizationAddress']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['organizationContact']->renderLabel(__('Contact'). ' <em>*</em>'); ?>
                        <?php echo $form['organizationContact']->render(); ?>
                    </li>
                    <li>
                    	<?php echo $form['organizationInfo']->renderLabel(__('Extra Info')); ?>
                        <?php echo $form['organizationInfo']->render(); ?>
                    </li>
                    <li class="required">
                        <em>*</em> <?php echo __(CommonMessages::REQUIRED_FIELD); ?>
                    </li>
                    
                </ol>
                <p>
                    <?php  
                    if (isset($organizationId )) { ?>
                        <?php if($financialOrgsPermissions->canUpdate() && isset($organizationId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave" value="<?php echo __("Edit"); ?>"/>
                        <?php }?>
                        <input type="button" class="cancel" name="btnBack" id="btnBack" value="<?php echo __("Back"); ?>"/>
                    <?php } else { ?>
                        <?php if($financialOrgsPermissions->canCreate() && empty($organizationId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave"value="<?php echo __("Save"); ?>"/>
                        <input type="button" class="cancel" name="btnBack" id="btnBack" value="<?php echo __("Back"); ?>"/>
                    <?php }} ?>
                </p>
            </fieldset>
        </form>
    </div>
     
</div>
<script language="JavaScript">
	var lang_edit = "<?php echo __("Edit"); ?>";
    var lang_save = "<?php echo __("Save"); ?>";
    var lang_cancel = "<?php echo __("Cancel"); ?>";
    var lang_back = "<?php echo __("Back"); ?>";
    var linkForaddFinancialOrganization = "<?php echo url_for('payroll/addFinancialOrganization'); ?>";
    var backBtnUrl = "<?php echo url_for( 'payroll/viewFinancialOrganizations' ); ?>";
    var cancelBtnUrl = "<?php echo url_for( 'payroll/addFinancialOrganization' ); ?>";
    <?php if( isset( $organizationId ) ){?>
    	var organizationId = '<?php echo $organizationId; ?>';
    <?php }else{?>
    	var organizationId = '';
    <?php } ?>
    var organizationList =  <?php echo str_replace('&#039;', "'", $form->getOrganizationsAsJSON()) ?> ;
</script>