<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    
?>
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/assignLoanSuccess'));
  
?>

 <div class="box" id="assignLoan">

    <div class="head">
        <h1><?php echo isset($empLoanId) ? __('Edit Employee Loan') : __('Assign Employee Loan '); ?></h1>
    </div>

    <div class="inner">
        <?php include_partial('global/flash_messages'); ?>
        <form name="frmAssignLoan" id="frmAssignLoan" method="post">
		 
            <?php echo $form['_csrf_token'];  ?>
            <?php echo $form[ 'empId' ] -> render(); ?>
            <fieldset>
                <ol>
                    <li>
                        <?php echo $form['employeeName']->renderLabel(__('Employee Name') . ' <em>*</em>'); ?>
                        <?php echo $form['employeeName']->render(array("maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['loanTitle']->renderLabel(__('Loan'). ' <em>*</em>' ); ?>
                        <?php echo $form['loanTitle']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['dateJoin']->renderLabel(__('Date Join'). ' <em>*</em>'); ?>
                        <?php echo $form['dateJoin']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['repaymentStart']->renderLabel(__('Repayment Start Date'). ' <em>*</em>'); ?>
                        <?php echo $form['repaymentStart']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['monthRepayment']->renderLabel(__('Month Repayment'). ' <em>*</em>'); ?>
                        <?php echo $form['monthRepayment']->render(); ?>
                    </li>
                    <?php
                    if( isset( $form[ 'balance' ] ) ):
					?>
                     <li>
                        <?php echo $form['balance']->renderLabel(__('Balance')); ?>
                        <?php echo $form['balance']->render(); ?>
                    </li>
                    <?php endif; ?>
                   <li>
                        <?php echo $form['extraInfo']->renderLabel(__('Extra Information')); ?>
                        <?php echo $form['extraInfo']->render(); ?>
                    </li>
                    <li class="required">
                        <em>*</em> <?php echo __(CommonMessages::REQUIRED_FIELD); ?>
                    </li>
                    
                </ol>
                <p>
                    <?php  
                    if (isset($empLoanId )) { ?>
                        <?php if($empLoanPermissions->canUpdate() && isset($empLoanId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave" value="<?php echo __("Edit"); ?>"/>
                        <?php }?>
                        <input type="button" class="cancel" name="btnBack" id="btnBack" value="<?php echo __("Back"); ?>"/>
                    <?php } else { ?>
                        <?php if($empLoanPermissions->canCreate() && empty($empLoanId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave"value="<?php echo __("Save"); ?>"/>
                    <?php }} ?>
                </p>
            </fieldset>
        </form>
    </div>
     
</div>
<script language="JavaScript">
	var lang_edit = "<?php echo __("Edit"); ?>";
    var lang_save = "<?php echo __("Save"); ?>";
    var lang_cancel = "<?php echo __("Cancel"); ?>";
    var lang_back = "<?php echo __("Back"); ?>";
    var linkForAssignLoan = "<?php echo url_for('payroll/assignLoan'); ?>";
    var backBtnUrl = "<?php echo url_for( 'payroll/viewEmployeesLoans' ); ?>";
    var cancelBtnUrl = "<?php echo url_for( 'payroll/assignLoan' ); ?>";
    <?php if( isset( $empLoanId ) ){?>
    	var empLoanId = '<?php echo $empLoanId; ?>';
    <?php }else{?>
    	var empLoanId = '';
    <?php } ?>
     var employeeList =  <?php echo str_replace('&#039;', "'", $form->getEmployeesAsJson()) ?> ;
     var loanRepayments = <?php echo str_replace('&#039;', "'", $form->getLoanRepaymentsAsJson()) ?> ;
</script>