<html>
	<head>
		<title>Payroll Report</title>
		<style>
			.report, td {
				padding: 1px;
			}
			.report {
				margin: 0 auto;
			}
			table {
				width: 100%;
				margin: 0 auto;
			}
			body {
				margin: 0 auto;
				font-family: 'Helvetica';
			}
			h3, h4 {
				text-align: center
			}
			
			.table-header, .td-data {
				background: #CCC;
				border: 1px solid #CCC;
				font-weight: bold;
			}
			.title {
				font-weight: bold;
			}
			.bottom-line td {
				border-bottom: 1px solid #CCC;
			}
			table{ 
				border: 0px solid #444;
			}
			td{
				border: 1px solid #444;
			}
	</style>
  				
	</head>
	<body>
		<div class = 'report'>
					<h3>
						ASYX Group Company Ltd
					</h3>
					<h3>
						 P.O.Box 123,Dar es Salaam 
					</h3>
					<h4>
						 Payroll Report
					</h4>
					<br/>
					<table class = 'table hover' >
					
						<tr class = 'table-header'>
							<td>#</td><td>Payroll Month</td><td>Employee Name</td><td>Gross Salary</td>
							<td>Pension</td><td>Tax</td><td>Loans</td><td>Deductions</td><td>Total</td>
						</tr>
					
					<tbody>
						<?php
							if( isset( $fullPayroll ) AND count( $fullPayroll ) > 0 ){
								$i = 1;	
								
								foreach( $fullPayroll as $line ){
						?>
						<tr>
							<td><?php echo $i ++; ?></td>
							<td><?php echo $line -> getPayDate(); ?></td>
							<td><?php echo $line -> getEmployeeName(); ?></td>
							<td><?php echo $line -> getBasicSalary(); ?></td>
							<td><?php echo '-'.$line -> getPension(); ?></td>
							<td><?php echo ' -'.$line -> getPaye(); ?></td>
							<td><?php
									
									$loan = $line -> getLoanDeductionAmount() ;
									if( isset($loan ) AND $loan != 0 ){
										echo ' - '.$line -> getLoanDeductionAmount();
									}else{
										echo '-';
									}
							 ?></td>
							<td><?php 
									$others =  $line -> getOtherDeductions();
								 	if( isset( $others )  AND $others != 0 ){
								 		$total = 0;
								 		foreach( $line -> getOtherDeductions() as $dedu){
								 			if( $dedu -> getIsRecurring() == 1 ){
								 				$total += $dedu -> getRepaymentAmount();
								 			}else{
								 				$total += $dedu -> getAmount();
								 			}
								 		}
									
										echo '-'.($total == 0 ? '' : number_format($total) );
								 	}else{
								 		echo '-';
								 	}
							 ?></td>
							 <td><?php echo $line -> getTakeHome(); ?>
						</tr>
						<?php }
							?>
							
								<tr class = 'table-header'>
									<th colspan = '3' >Total<td> <?php echo $totals -> getTotalSalary (false); ?></td>
									<td><?php echo '-'.$totals -> getTotalPension( false ); ?></td>
									<td><?php echo '-'.$totals -> getTotalTax(false );?></td>
									<td><?php echo '-'.$totals -> getTotalLoans(false );?></td>
									<td><?php echo '-'.$totals -> getTotalDeductions( false ); ?></td>
									<td><?php echo $totals -> getTotalTakeHome(false); ?></td>
								</tr>
					
							<?php } ?>
						
					</tbody>
					</table>
		
					<p class="P3">
						<span class="T4">Date:</span><span class="T10"> ______________________</span>
					</p> 
					<p class="No_20_Spacing">
						<span class="T4">Director:</span><span class="title"> ____________________________</span>
					</p>
	</div>
	</body>
</html>

				
