<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    
?>
<?php
 use_javascript(plugin_web_path('orangehrmPayrollPlugin', 'js/addDeductionSuccess'));
  
?>

 <div class="box" id="addDeduction">

    <div class="head">
        <h1><?php echo isset($deductionId) ? __('Edit Employee Deduction') : __('Add Employee Deduction '); ?></h1>
    </div>

    <div class="inner">
        <?php include_partial('global/flash_messages'); ?>
        <form name="formAddDeduction" id="formAddDeduction" method="post">
		 
            <?php echo $form['_csrf_token'];  ?>
            <?php echo $form[ 'empId' ] -> render(); ?>
            <fieldset>
                <ol>
                    <li>
                        <?php echo $form['employeeName']->renderLabel(__('Employee Name') . ' <em>*</em>'); ?>
                        <?php echo $form['employeeName']->render(array("maxlength" => 50)); ?>
                    </li>
                    <li>
                        <?php echo $form['deductionTitle']->renderLabel(__('Title'). ' <em>*</em>' ); ?>
                        <?php echo $form['deductionTitle']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['amount']->renderLabel(__('Amount'). ' <em>*</em>'); ?>
                        <?php echo $form['amount']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['paymentStart']->renderLabel(__('Payment Start Date'). ' <em>*</em>'); ?>
                        <?php echo $form['paymentStart']->render(); ?>
                    </li>
                    <li>
                        <?php echo $form['isRecurring']->renderLabel(__('Is Recurring' ). ' <em>*</em>'); ?>
                        <?php echo $form['isRecurring']->render(); ?>
                    </li>
                    <li>
                    	<?php echo $form['repaymentAmount']->renderLabel(__('Repayment Amount' ). ' <em>*</em>'); ?>
                        <?php echo $form['repaymentAmount']->render(); ?>
                    </li>
                    <?php
                    if( isset( $form[ 'balance' ] ) ):
					?>
                     <li>
                        <?php echo $form['balance']->renderLabel(__('Balance')); ?>
                        <?php echo $form['balance']->render(); ?>
                    </li>
                    <?php endif; ?>
                   <li>
                        <?php echo $form['extraInfo']->renderLabel(__('Deduction Description')); ?>
                        <?php echo $form['extraInfo']->render(); ?>
                    </li>
                    <li class="required">
                        <em>*</em> <?php echo __(CommonMessages::REQUIRED_FIELD); ?>
                    </li>
                    
                </ol>
                <p>
                    <?php  
                    if (isset($deductionId )) { ?>
                        <?php if($deductionPermissions->canUpdate() && isset($deductionId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave" value="<?php echo __("Edit"); ?>"/>
                        <?php }?>
                        <input type="button" class="cancel" name="btnBack" id="btnBack" value="<?php echo __("Back"); ?>"/>
                    <?php } else { ?>
                        <?php if($deductionPermissions->canCreate() && empty($deductionId)){?>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave"value="<?php echo __("Save"); ?>"/>
                    <?php }} ?>
                </p>
            </fieldset>
        </form>
    </div>
     
</div>
<script language="JavaScript">
	var lang_edit = "<?php echo __("Edit"); ?>";
    var lang_save = "<?php echo __("Save"); ?>";
    var lang_cancel = "<?php echo __("Cancel"); ?>";
    var lang_back = "<?php echo __("Back"); ?>";
    var linkForaddDeduction = "<?php echo url_for('payroll/addDeduction'); ?>";
    var backBtnUrl = "<?php echo url_for( 'payroll/viewDeductions' ); ?>";
    var cancelBtnUrl = "<?php echo url_for( 'payroll/addDeduction' ); ?>";
    <?php if( isset( $deductionId ) ){?>
    	var deductionId = '<?php echo $deductionId; ?>';
    <?php }else{?>
    	var deductionId = '';
    <?php } ?>
     var employeeList =  <?php echo str_replace('&#039;', "'", $form->getEmployeesAsJson()) ?> ;
     
</script>