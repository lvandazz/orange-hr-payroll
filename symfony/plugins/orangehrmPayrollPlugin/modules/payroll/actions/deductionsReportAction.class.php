<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class deductionsReportAction extends  baseAction{
    	protected $payrollService;
		/*
		 * 
		 * 
		 */
		 function getReportService(){
		  
		 	if( is_null( $this -> payrollService ) ){
		 		$this -> payrollService = new ReportService(); 
		 	}
			
			return $this -> payrollService;
		 }
		 
		 /*
		  * 
		  * 
		  */
		  function execute( $request ){
		  	//data group permissions
		  	$this -> reportPermissions = $this -> getDataGroupPermissions( 'pension' );
			
			$frmSearch = $request -> getParameter('payrollReport') ;
			 
			$employee = $frmSearch['employeeD'];
			$fromDate = $frmSearch['fromDate'];
			$toDate = $frmSearch[ 'toDate' ];
			$isRecurring = $frmSearch[ 'isRecurring'];
			$status = $frmSearch[ 'status' ];
			
			$print = $request -> getParameter( 'print' );
			
			
			if( $fromDate > $toDate ){
				$this -> getUser() -> setFlash( 'error', 'Invalid dates' );
				$this -> redirect( 'payroll/deductionsReport');
			} 
			$frmParams = array( 'employee' => $employee , 'organization' => $organization , 'fromDate' => $fromDate ,'toDate' => $toDate, 'isRecurring' => $isRecurring, 'status' => $status );
			$this -> form = new deductionsReportForm(array(),$frmParams);
			 
			// 
			$fullDeductions = $this -> getReportService() -> getFullDeductions($fromDate, $toDate,$employee,$status,$isRecurring);
			
			if( count( $fullDeductions  ) > 0 ){
				$totals = $fullDeductions[ count ( $fullDeductions ) - 1];
				$this -> totals = $totals; 
				unset( $fullDeductions[ count ( $fullDeductions ) - 1]);
				$this -> fullDeductions = $fullDeductions;
				
				if( $print != null ){
					$this -> getContext() -> getConfiguration() -> loadHelpers( 'Pdf' );
					$html = $this -> getPartial( 'payroll/printableDeductions',array( 'fullDeductions' => $fullDeductions, 'totals' => $totals ));
					pdf_create( $html,'DeductionsReport-'.date('F,d Y'));
				}	
			} 
			
			 
		  }
		
    }
?>