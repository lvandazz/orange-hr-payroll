<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class assignLoanAction extends baseAction{
    	private $loanService;
		
		public function getLoanService(){
			if( is_null( $this -> loanService ) ){
				$this -> loanService = new loanService();
			}
			return $this -> loanService;
		}
		/*
		 * 
		 * 
		 */
		 function execute( $request ){
		 	$request -> setParameter( 'initialActionName' , 'viewLoans' );
			// data group permissions
			$this -> empLoanPermissions = $this -> getDataGroupPermissions( 'loans' );
			$this -> empLoanId = $request -> getParameter( 'empLoanId' );
			if( $this -> empLoanPermissions -> canCreate() == 0 ){
				$this -> redirect( 'payroll/viewEmployeesLoans' );
			}
			$values = array( 'empLoanId' => $this -> empLoanId, 'empLoanPermissions' => $this -> empLoanPermissions );
			$this -> setForm( new assignLoanForm(array(), $values )); 
			if( $request -> isMethod( 'post' ) ){
					$this -> form -> bind( $request -> getParameter( $this -> form -> getName()));
					if( $this -> form -> isValid()){ 
						$result = $this -> form -> save();
						if( $result == -1 ){
							$this -> getUser() -> setFlash( 'warning' , __( TopLevelMessages :: EMP_PENDING_LOAN ) );
							$this -> redirect( 'payroll/assignLoan' );
							return;
						} 
						$this -> empLoanId = $result; 
						$this -> getUser() -> setFlash( 'success' , __( TopLevelMessages :: SAVE_SUCCESS ) );
						$this -> redirect( 'payroll/assignLoan?empLoanId='.$this -> empLoanId );
					}else{
						Logger::getLogger('payroll.assignLoan')->error($this->form);
                		$this->getUser()->setFlash('warning', __(TopLevelMessages::SAVE_FAILURE), false);
					}
				}
		 }
		 /*
		  * 
		  * 
		  */
		  function setForm( sfForm $form ){
		  	$this -> form = $form;
		  }
		  /*
		   * 
		   * 
		   */
		   function getForm( ){
		   	return $this -> form;
		   }
    }
?>