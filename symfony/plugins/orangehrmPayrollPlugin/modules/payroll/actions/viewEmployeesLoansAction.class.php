<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */

class viewEmployeesLoansAction extends  baseAction {

	private $loanService;

	function getloanService() {
		if (is_null($this -> loanService)) {
			$this -> loanService = new loanService();
		}
		return $this -> loanService;
	}

	/*
	 *
	 *
	 */
	function execute($request) {
		//data group permissions
		$this -> employeeLoansPermissions = $this -> getDataGroupPermissions('loans'); 
		
				 
		$this -> $parmetersForListCompoment = array();
		
		$employee = $this -> getUser() -> getEmployeeNumber();
		$employee = $employee == 0 ? null : $employee;
		$loans = $this -> getloanService() -> getEmployeesLoans(null,$employee,null);
		 
		$srchParams = array();
		$params = array();
		$this -> parmetersForListCompoment = $params;
		$this -> __setListComponent($loans, 5, $srchParams, 1, $this -> employeeLoansPermissions);
	}

	/*
	 *
	 *
	 */
	private function __setListComponent($data, $noOfRecords, $srchParams, $pageNumber, $permissions) {
		$runtimeDefinitions = array();
		$buttons = array();

		if ($permissions -> canCreate()) {
			$buttons['Add'] = array('label' => 'Assign Loan', 'function' => 'assignLoan');
		}

		if (!$permissions -> canDelete()) {
			$runtimeDefinitions['hasSelectableRows'] = false;
		} else if ($permissions -> canDelete()) {
			$buttons['Delete'] = array('label' => 'Delete', 'type' => 'submit', 'data-toggle' => 'modal', 'data-target' => '#deleteConfirmation', 'class' => 'delete');
		}

		$runtimeDefinitions['buttons'] = $buttons;

		$configurationFactory = new EmployeesLoansHeaderFactory();
		$configurationFactory -> setRuntimeDefinitions($runtimeDefinitions);

		ohrmListComponent::setPageNumber($pageNumber);
		ohrmListComponent::setConfigurationFactory($configurationFactory);
		ohrmListComponent::setListData($data);
		ohrmListComponent::setItemsPerPage($noOfRecords);
		ohrmListComponent::setNumberOfRecords(count( $data ));
	}

}
?>