<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class viewPensionsAction extends  baseAction{
    	protected $pensionsService;
		/*
		 * 
		 * 
		 */
		 function getPensionsService(){
		  
		 	if( is_null( $this -> pensionsService ) ){
		 		$this -> pensionsService = new PensionService();
				$this -> pensionsService -> setPensionDao(new PensionDao()); 
		 	}
			
			return $this -> pensionsService;
		 }
		 
		 /*
		  * 
		  * 
		  */
		  function execute( $request ){
		  	//data group permissions
		  	$this -> pensionsPermissions = $this -> getDataGroupPermissions( 'pension' );
			
			// pensions
			$pensions = $this -> getPensionsService() -> getPensions();
			
			$srchParams = array();
			$params = array();
			$this -> parmetersForListCompoment = $params;
			$this->__setListComponent($pensions, 5, $srchParams, 1, $this->pensionsPermissions);
		  }
		  /*
		   * 
		   * 
		   */
		    private function __setListComponent( $pensions, $noOfRecords, $srchParams, $pageNumber, $permissions) {
			 	$runtimeDefinitions = array();
		        $buttons = array();
		
		        if ($permissions->canCreate()) {
		            $buttons['Add'] = array('label' => 'Add',
		                'function' => 'addPension');
		        }
		
		        if (!$permissions->canDelete()) {
		            $runtimeDefinitions['hasSelectableRows'] = false;
		        } else if ($permissions->canDelete()) {
		            $buttons['Delete'] = array('label' => 'Delete',
		                'type' => 'submit',
		                'data-toggle' => 'modal',
		                'data-target' => '#deleteConfirmation',
		                'class' => 'delete');
		        }
		
		        $runtimeDefinitions['buttons'] = $buttons;
		        
		        $configurationFactory = new PensionsHeaderFactory();
		        $configurationFactory->setRuntimeDefinitions($runtimeDefinitions);
		        
		        ohrmListComponent::setPageNumber($pageNumber);
		        ohrmListComponent::setConfigurationFactory($configurationFactory);
		        ohrmListComponent::setListData($pensions);
		        ohrmListComponent::setItemsPerPage($noOfRecords);
		        ohrmListComponent::setNumberOfRecords( count( $pensions) );
	 }
    }
?>