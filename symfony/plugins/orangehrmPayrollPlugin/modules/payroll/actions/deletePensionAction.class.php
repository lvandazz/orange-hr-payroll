<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class deletePensionAction extends  baseAction{
    	
		private $pensionService;
		
		public function getPensionService( ){
			if( is_null( $this -> pensionService  ) ){
				$this -> pensionService = new PensionService();
			}
			return $this -> pensionService;
		}
		
		function execute( $request ){
			// get permissions
			$pensionService = $this -> getDataGroupPermissions( 'allowance_list' );
			
			if( $pensionService -> canDelete() ){
				$form = new DefaultListForm();
				$pensionIds = $request -> getParameter( 'chkSelectRow' );
				if( $this -> getPensionService() -> deletePensions($pensionIds) === true ){
					$this -> getUser() -> setFlash( 'success', __( TopLevelMessages :: DELETE_SUCCESS ) );	
				}else{
					$this -> getUser() -> setFlash( 'warning', __( TopLevelMessages :: DELETE_FAILURE ) );
				}
				
			}else{
				$this -> getUser() -> setFlash( 'warning' , __( TopLevelMessages:: ACCESS_DENIED ));
			}
			$this -> redirect( 'payroll/viewPensions' );
		}
    }
?>