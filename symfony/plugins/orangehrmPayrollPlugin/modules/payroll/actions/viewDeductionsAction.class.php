<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class viewDeductionsAction extends  baseAction{
    	protected $deductionService;
		/*
		 * 
		 * 
		 */
		 function getDeductionService(){
		  
		 	if( is_null( $this -> deductionService ) ){
		 		$this -> deductionService = new DeductionService(); 
		 	}
			
			return $this -> deductionService;
		 }
		 
		 /*
		  * 
		  * 
		  */
		  function execute( $request ){
		  	//data group permissions
		  	$this -> deductionPermissions = $this -> getDataGroupPermissions( 'pension' );
			
			$frmSearch = $request -> getParameter( 'viewDeductions' );
			 
			$employee = $frmSearch['employeeD'];
			$status = $frmSearch['statusD'];
			$isRecurring = $frmSearch['isRecurring'];// === null ? false : true;
			
			$frmParams = array( 'employee' => $employee , 'status' => $status , 'isRecurring' => $isRecurring ); 
			$this -> form = new viewDeductionsForm(array(),$frmParams);
			 
			// deductions
			$deductions = $this -> getDeductionService() -> getDeductions(null,$employee,$isRecurring, $status );
			
			$srchParams = array();
			$params = array();
			$this -> parmetersForListCompoment = $params;
			$this->__setListComponent($deductions, 5, $srchParams, 1, $this->deductionPermissions);
		  }
		  /*
		   * 
		   * 
		   */
		    private function __setListComponent( $deductions, $noOfRecords, $srchParams, $pageNumber, $permissions) {
			 	$runtimeDefinitions = array();
		        $buttons = array();
		
		        if ($permissions->canCreate()) {
		            $buttons['Add'] = array('label' => 'Add',
		                'function' => 'addDeduction');
		        }
		
		        if (!$permissions->canDelete()) {
		            $runtimeDefinitions['hasSelectableRows'] = false;
		        } else if ($permissions->canDelete()) {
		            $buttons['Delete'] = array('label' => 'Delete',
		                'type' => 'submit',
		                'data-toggle' => 'modal',
		                'data-target' => '#deleteConfirmation',
		                'class' => 'delete');
		        }
		
		        $runtimeDefinitions['buttons'] = $buttons;
		        
		        $configurationFactory = new DeductionsHeaderFactory();
		        $configurationFactory->setRuntimeDefinitions($runtimeDefinitions);
		        
		        ohrmListComponent::setPageNumber($pageNumber);
		        ohrmListComponent::setConfigurationFactory($configurationFactory);
		        ohrmListComponent::setListData($deductions);
		        ohrmListComponent::setItemsPerPage($noOfRecords);
		        ohrmListComponent::setNumberOfRecords( count( $deductions) );
	 }
    }
?>