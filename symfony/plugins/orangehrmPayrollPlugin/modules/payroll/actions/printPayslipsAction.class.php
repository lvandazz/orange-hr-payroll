<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class printPayslipsAction extends baseAction{
    	private $payslipService; 
		function getPayslipService(){
			if(  is_null( $this -> payslipService ) ){
				$this -> payslipService = new PaySlipService();
			}
			return $this -> payslipService;
		}
		 
		/*
		 * 
		 */
		function execute( $request ){
			
			
			$employeeService = new EmployeeService();
			$payrollService = new PayrollService();
			$pensionEmpService = new PensionEmployeeService();
			$taxRateService = new TaxRateService();
			$pensionService = new PensionService();
			
			$request->setParameter('initialActionName', 'viewPayslips');  
			
			$payslips = $request -> getParameter( 'chkSelectRow' );
			
			if( empty( $payslips ) ){
				$this -> redirect( 'payroll/viewPayslips' );
			}
			$payslip = $this -> getPayslipService() -> getPayslip($payslips[ 0 ]);
			
			$payslip = $payrollService -> processPayslip($payslip, $employeeService, $pensionEmpService, $pensionService, $taxRateService);
			
			
			$print = $request -> getParameter( 'print' );
			$preview = $request -> getParameter( 'preview' );
			 
			
			if( $print != null  ){
				$this -> getContext() -> getConfiguration() -> loadHelpers( 'Pdf' );
				$html = $this -> getPartial( 'payroll/printablePayslip',array( 'content' => $payslip )); 
				pdf_create( $html,'payslip');
				$this -> redirect( 'payroll/viewPayslips' );	
			}
			if( $preview != null ){
				$this -> content = $payslip;
			}
		}
    }
