<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
class viewFinancialOrganizationsAction extends  baseAction {
	private $financialOrganizationService;

	function getFinancialService() {
		if (is_null($this -> financialOrganizationService)) {
			$this -> financialOrganizationService = new FinancialOrganizationService();
		}
		return $this -> financialOrganizationService;

	}

	function execute($request) {

		$organizations = $this -> getFinancialService() -> getFinancialOrganizations();
		//data group permissions
		$this -> financialOrgsPermission = $this -> getDataGroupPermissions('pension');

		$srchParams = array();
		$params = array();
		$this -> parmetersForListCompoment = $params;
		$this -> __setListComponent($organizations, 5, $srchParams, 1, $this -> financialOrgsPermission);
	}

	/*
	 *
	 */
	private function __setListComponent($data, $noOfRecords, $srchParams, $pageNumber, $permissions) {
		$runtimeDefinitions = array();
		$buttons = array();

		if ($permissions -> canCreate()) {
			$buttons['Add'] = array('label' => 'Add', 'function' => 'addFinancialOrganization');
		}

		if (!$permissions -> canDelete()) {
			$runtimeDefinitions['hasSelectableRows'] = false;
		} else if ($permissions -> canDelete()) {
			$buttons['Delete'] = array('label' => 'Delete', 'type' => 'submit', 'data-toggle' => 'modal', 'data-target' => '#deleteConfirmation', 'class' => 'delete');
		}

		$runtimeDefinitions['buttons'] = $buttons;

		$configurationFactory = new FinancialOrganizationsHeaderFactory();
		$configurationFactory -> setRuntimeDefinitions($runtimeDefinitions);

		ohrmListComponent::setPageNumber($pageNumber);
		ohrmListComponent::setConfigurationFactory($configurationFactory);
		ohrmListComponent::setListData($data);
		ohrmListComponent::setItemsPerPage($noOfRecords);
		ohrmListComponent::setNumberOfRecords(count($data));
	}

}
 ?>
