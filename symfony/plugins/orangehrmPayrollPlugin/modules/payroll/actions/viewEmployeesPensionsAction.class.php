<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */

class viewEmployeesPensionsAction extends  baseAction {

	private $pensionEmployeeService;

	function getPensionEmployeeService() {
		if (is_null($this -> pensionEmployeeService)) {
			$this -> pensionEmployeeService = new PensionEmployeeService();
		}
		return $this -> pensionEmployeeService;
	}

	/*
	 *
	 *
	 */
	function execute($request) {
		//data group permissions
		$this -> pensionsPermissions = $this -> getDataGroupPermissions('pension');

		$this -> $parmetersForListCompoment = array();
		$pensionsEmployees = $this -> getPensionEmployeeService() -> getPensionsEmployees();
		 
		$srchParams = array();
		$params = array();
		$this -> parmetersForListCompoment = $params;
		$this -> __setListComponent($pensionsEmployees, 5, $srchParams, 1, $this -> pensionsPermissions);
	}

	/*
	 *
	 *
	 */
	private function __setListComponent($pensionsEmployees, $noOfRecords, $srchParams, $pageNumber, $permissions) {
		$runtimeDefinitions = array();
		$buttons = array();

		if ($permissions -> canCreate()) {
			$buttons['Add'] = array('label' => 'Add', 'function' => 'assignPension');
		}

		if (!$permissions -> canDelete()) {
			$runtimeDefinitions['hasSelectableRows'] = false;
		} else if ($permissions -> canDelete()) {
			$buttons['Delete'] = array('label' => 'Delete', 'type' => 'submit', 'data-toggle' => 'modal', 'data-target' => '#deleteConfirmation', 'class' => 'delete');
		}

		$runtimeDefinitions['buttons'] = $buttons;

		$configurationFactory = new PensionsEmployeesHeaderFactory();
		$configurationFactory -> setRuntimeDefinitions($runtimeDefinitions);

		ohrmListComponent::setPageNumber($pageNumber);
		ohrmListComponent::setConfigurationFactory($configurationFactory);
		ohrmListComponent::setListData($pensionsEmployees);
		ohrmListComponent::setItemsPerPage($noOfRecords);
		ohrmListComponent::setNumberOfRecords(count( $pensionsEmployees));
	}

}
?>