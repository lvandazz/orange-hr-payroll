<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class viewTaxRatesAction extends baseAction{
    	private $taxRateService;
		
		
		/*
		 * 
		 */
		 function execute( $request ){
		 	
			$taxRates = $this -> getTaxRateService( ) -> getTaxRates();
			//print_r( $taxRates );
			$this -> taxRatePermissions = $this -> getDataGroupPermissions('tax_rate' ); 
			$srchParams = array();
			$params = array();
			$this -> parmetersForListCompoment = $params;
			$this->__setListComponent($taxRates, 5, $srchParams, 1, $this->taxRatePermissions);
		 }
		 /*
		  * 
		  */
		  function getTaxRateService(){
		  	if( is_null( $this -> taxRateService ) ){
		  		$this -> taxRateService = new TaxRateService();
				$this -> taxRateService -> setTaxRateDao( new TaxRateDao() );
		  	}
			return $this -> taxRateService;
		  }
		  /*
		   * 
		   * 
		   */
		  function __setListComponent( $taxRates,$noOfRecords,$srchParams, $pageNumber, $taxRatePermissions ){
		  		$runtimeDefinitions = array();
		        
		        $configurationFactory = new TaxRatesListHeaderFactory();
		        $configurationFactory->setRuntimeDefinitions($runtimeDefinitions);
		        
		        ohrmListComponent::setPageNumber($pageNumber);
		        ohrmListComponent::setConfigurationFactory($configurationFactory);
		        ohrmListComponent::setListData($taxRates);
		        ohrmListComponent::setItemsPerPage($noOfRecords);
		        ohrmListComponent::setNumberOfRecords( count($taxRates) );
		  }
    }
?>