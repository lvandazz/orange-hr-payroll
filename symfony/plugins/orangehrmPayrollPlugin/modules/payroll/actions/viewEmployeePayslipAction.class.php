<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */

    class viewEmployeePayslipAction extends baseAction{
    	private $payrollService; 
		private $employeeService;
		function getPayrollService(){
			if( is_null( $this -> payrollService ) ){
				$this -> payrollService = new PayrollService();
			}
			return $this -> payrollService;
		}
		/*
		 * 
		 * 
		 */
		 function getEmployeeService(){
		 	if( is_null( $this -> employeeService ) ){
		 		$this -> employeeService = new EmployeeService();
		 	}
			return $this -> employeeService;
		 }
		 /*
		  * 
		  * 
		  */
		 function execute( $request ){
			$request->setParameter('initialActionName', 'runPayroll');
		 	$this -> payrollPermissions = $this -> getDataGroupPermissions( 'pension' );
			
			$payrollId = $request -> getParameter( 'payrollId' );
			$empNumber = $request -> getParameter( 'empNumber' );
			$payrollResults = $this -> getPayrollService() -> runPayroll(null,$payrollId,$empNumber ); 
			//die( print_r( $payrollResults ) );
			$employeeService  = new EmployeeService();
			$employee  = $employeeService -> getEmployee($empNumber);
			$salary = 0;
			if( count( $employeeService -> getEmployeeSalaries($empNumber) ) > 0 ){
				foreach( $employeeService -> getEmployeeSalaries($empNumber) as $s ){
					$salary += $s -> getAmount();
				}
			}
			
			$employee = array( 'fullName' => $employee -> getFullName(), 'title' => $employee -> getJobTitleName(),
			'salary' => number_format( $salary ),
			'empNumber' => $empNumber
			); 
			$this -> employee = $employee;
			$this -> empNumber = $empNumber ;
			$this -> payroll = $this -> getPayrollService( ) -> getPayroll($payrollId);
			$this -> payroll = new DateTime( $this -> payroll -> getPayDate());
			$this -> payroll = $this -> payroll -> format('F Y');
			$this -> getUser()-> setAttribute('payrollMonth', $month );
			$this -> form = new runPayrollForm(array(),array( 'selectedMonth' => $month ));
			
			$srchParams = array();
			$params = array();
			$this -> parmetersForListCompoment = $params;
			$this->__setListComponent($payrollResults, 5, $srchParams, 1, $this-> payrollPermissions);
			
			if( isset( $payrollResults[0] ) )
				$this -> otherDeductions = $payrollResults[0] -> getOtherDeductions();
		 }
		 /*
		  * 
		  */
		 private function __setListComponent( $payrollResults, $noOfRecords, $srchParams, $pageNumber, $permissions) {
			 	$runtimeDefinitions = array();
		        $buttons = array();
		
		        if ($permissions->canCreate()) {
		            $buttons['Process'] = array('label' => 'Commit Payroll',
		                'type' => 'submit',
		                'data-toggle' => 'modal',
		                'data-target' => '#processConfirmation',
		                'class' => 'blue');
		        }
		 
		        $runtimeDefinitions['buttons'] = $buttons;
		        
		        $configurationFactory = new EmployeePayrollHeaderFactory();
		        //$configurationFactory->setRuntimeDefinitions($runtimeDefinitions); 
		        ohrmListComponent::setPageNumber($pageNumber);
		        ohrmListComponent::setConfigurationFactory($configurationFactory);
		        ohrmListComponent::setListData($payrollResults);
		        ohrmListComponent::setItemsPerPage($noOfRecords);
		        ohrmListComponent::setNumberOfRecords( count($payrollResults) );
    }
}
?> 