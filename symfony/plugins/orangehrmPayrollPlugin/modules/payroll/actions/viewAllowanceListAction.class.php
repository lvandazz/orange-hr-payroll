<?php

/*
 * 
 * 
 * 
 */
class viewAllowanceListAction extends  baseAction{
 	
	private $allowanceService;
	public function execute( $request )
	{
		$usrObj = $this->getUser()->getAttribute('user');
		$allowanceList = $this -> getAllowanceService() -> getAllowanceList();
		$this -> allowancePermissions = $this->  getDataGroupPermissions( 'allowance_list' );
		 
		$srchParams = array( );
        $srchParams['noOfRecords'] = 10;
        $srchParams['offset'] = 0;
		$params = array();
        $this->parmetersForListCompoment = $params;
		//print_r( $allowanceList );
		$this->__setListComponent($allowanceList, 10, $srchParams, 0, $this->allowancePermissions);
	}
	
	/*
	 * 
	 * 
	 * 
	 */
	 function getAllowanceService()
	 {
	 	if( is_null( $this -> allowanceService ) )
		{
			$this -> allowanceService = new AllowanceService();
			$this -> allowanceService -> setAllowanceDao( new AllowanceDao());
		}
		return $this -> allowanceService;
	 }
	 /*
	  * 
	  * 
	  * 
	  */
	  function setForm( sfForm $form )
	  {
	  	if( is_null( $this -> form ) ){
	  		$this -> form  = $form;
	  	}
	  }
	  /*
	   * 
	   * 
	   * 
	   */
	 private function __setListComponent( $allowanceList, $noOfRecords, $srchParams, $pageNumber, $permissions)
	 {
	 	$runtimeDefinitions = array();
        $buttons = array();

        if ($permissions->canCreate()) {
            $buttons['Add'] = array('label' => 'Add',
                'function' => 'addAllowance');
        }

        if (!$permissions->canDelete()) {
            $runtimeDefinitions['hasSelectableRows'] = false;
        } else if ($permissions->canDelete()) {
            $buttons['Delete'] = array('label' => 'Delete',
                'type' => 'submit',
                'data-toggle' => 'modal',
                'data-target' => '#deleteConfirmation',
                'class' => 'delete');
        }

        $runtimeDefinitions['buttons'] = $buttons;
        
        $configurationFactory = new AllowanceListHeaderFactory();
        $configurationFactory->setRuntimeDefinitions($runtimeDefinitions);
        
        ohrmListComponent::setPageNumber($pageNumber);
        ohrmListComponent::setConfigurationFactory($configurationFactory);
        ohrmListComponent::setListData($allowanceList);
        ohrmListComponent::setItemsPerPage($noOfRecords);
        ohrmListComponent::setNumberOfRecords($this->getAllowanceService()->getAllowanceList());
	 }
 }
 
