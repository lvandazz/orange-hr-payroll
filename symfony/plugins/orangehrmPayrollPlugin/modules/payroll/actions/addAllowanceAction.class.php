<?php
/*
 * 
 * @author Joseph Luvanda
 * @file addAllowanceAction.class.php
 * @documentation create form for adding new allowances
 */
 
 class addAllowanceAction extends baseAction{
 	
	private $allowanceService;
	
	/*
	 * 
	 * 
	 */
	function getAllowanceService()
	{
		if( is_null( $this -> allowanceService ) ){
			$this -> allowanceService = new AllowanceService();
			$this -> allowanceService -> setAllowanceDao( new AllowanceDao());
		}
		return $this -> allowanceService;
	}
	/*
	 * 
	 * 
	 */
	 function setAllowanceService( AllowanceService $allowanceService ){
	 	$this -> allowanceService = $allowanceService;
	 }
	/*
	 * 
	 * 
	 */
	 function setForm( sfForm $form ){
	 	if( is_null( $this -> form ) ){
	 		$this -> form = $form;
	 	}
	 }
	 /*
	  * 
	  * 
	  */
	  function getForm( )
	  {
	  	return $this -> form;
	  }
	  /*
	   * 
	   * 
	   */
	   public function execute( $request ){
	   	//For highlighting corresponding menu item
	   	$request -> setParameter( 'initialActionName' , 'viewAllowanceList' );
		$this -> allowancePermissions = $this -> getDataGroupPermissions('allowance_list' );
		$this -> allowanceId = $request -> getParameter( 'allowanceId' );
		
		
		$values = array( 'allowanceId' => $this -> allowanceId, 'allowancePermissions' => $this -> allowancePermissions);
		
		$this -> setForm(new AddAllowanceForm( array() , $values ));
		
		
		if( $request ->isMethod( 'post' ) )
		{
			
			$this -> form -> bind( $request -> getParameter( $this ->form -> getName()));
			if( $this -> form -> isValid() ){
				$this -> allowanceId = $this -> form -> save();
				$this -> getUser() -> setFlash( 'success',__(TopLevelMessages::SAVE_SUCCESS));
				$this -> redirect( 'payroll/addAllowance?allowanceId='.$this-> allowanceId );
				
			}
			else{
				Logger::getLogger('payroll.addAllowance')->error($this->form);
                $this->getUser()->setFlash('warning', __(TopLevelMessages::SAVE_FAILURE), false);
			}
			
			
			
		}
	   }
 }
