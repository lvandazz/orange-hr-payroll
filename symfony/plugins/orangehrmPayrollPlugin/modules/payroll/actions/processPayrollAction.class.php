<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
	class processPayrollAction extends baseAction{
		
		private $payrollService;
		
		/*
		 * 
		 */
		 function getPayrollService(){
		 	if( is_null( $this -> payrollService ) ){
		 		$this -> payrollService = new PayrollService();
		 	}
			return $this -> payrollService;
		 }
		 /*
		  * 
		  */
		  function execute( $request ){
 	  	
			if( is_null( $this -> getUser() -> getAttribute( 'payrollMonth' ) ) ){
				$this -> getUser() -> setFlash( 'error', __( TopLevelMessages :: NO_PAYROLL_MONTH ) );
				$this -> redirect( 'payroll/runPayroll' );
			}
			
			$employeeIds = $request -> getParameter('chkSelectRow' );
			$payroll = $this -> getPayrollService() -> getMonthPayroll( $this -> getUser() -> getAttribute('payrollMonth' ));
		 
		    if( is_array( $employeeIds ) AND count( $employeeIds ) > 0 ){	
				 $this -> getPayrollService() -> procPayroll( $employeeIds, $payroll );
		    }	
			
			$this -> getUser() -> setFlash( 'success', __( TopLevelMessages :: PROCESS_COMPLETE ) );
			$this -> redirect( 'payroll/runPayroll' );
		  }
	}
