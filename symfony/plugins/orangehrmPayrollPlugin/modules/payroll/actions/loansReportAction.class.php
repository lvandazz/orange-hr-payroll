<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class loansReportAction extends  baseAction{
    	protected $payrollService;
		/*
		 * 
		 * 
		 */
		 function getReportService(){
		  
		 	if( is_null( $this -> payrollService ) ){
		 		$this -> payrollService = new ReportService(); 
		 	}
			
			return $this -> payrollService;
		 }
		 
		 /*
		  * 
		  * 
		  */
		  function execute( $request ){
		  	//data group permissions
		  	$this -> reportPermissions = $this -> getDataGroupPermissions( 'pension' );
			
			$frmSearch = $request -> getParameter('payrollReport') ;
			 
			$employee = $frmSearch['employeeD'];
			$organization = $frmSearch['organization'];
			$fromDate = $frmSearch['fromDate'];
			$toDate = $frmSearch[ 'toDate' ];
			
			
			
			if( $fromDate > $toDate ){
				$this -> getUser() -> setFlash( 'error', 'Invalid dates' );
				$this -> redirect( 'payroll/loansReport');
			}
			 
			 
			$frmParams = array( 'employee' => $employee , 'organization' => $organization , 'fromDate' => $fromDate, 'toDate' => $toDate ); 
			
			$this -> form = new loansReportForm(array(),$frmParams);
			 
			// 
			$fullLoans = $this -> getReportService() -> getFullLoans($fromDate, $toDate, $employee, $organization );
			$totals = $fullLoans[ count ( $fullLoans ) - 1];
			$this -> totals = $totals;  
			unset( $fullLoans[ count ( $fullLoans ) - 1]);
			$this -> fullLoans = $fullLoans;
			
			if( $request -> getParameter( 'print' ) != null ){
				$this -> getContext() -> getConfiguration() -> loadHelpers( 'Pdf' );
				$html = $this -> getPartial( 'payroll/printableLoans',array( 'fullLoans' => $fullLoans, 'totals' => $totals ));
				pdf_create( $html,'LoansReport-'.date('F,d Y'),TRUE,'landscape' );
			}
			 
		  }
		
    }
?>