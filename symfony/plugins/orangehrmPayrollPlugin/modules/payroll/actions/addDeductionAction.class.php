<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class addDeductionAction extends baseAction{
    	private $deductionService;
		
		public function getDeductionService(){
			if( is_null( $this -> deductionService ) ){
				$this -> deductionService = new DeductionService();
			}
			return $this -> deductionService;
		}
		/*
		 * 
		 * 
		 */
		 function execute( $request ){
		 	$request -> setParameter( 'initialActionName' , 'viewDeductions' );
			// data group permissions
			$this -> deductionPermissions = $this -> getDataGroupPermissions( 'pension' );
			$this -> deductionId = $request -> getParameter( 'deductionId' );
			$values = array( 'deductionId' => $this -> deductionId, 'deductionService' , $this -> deductionService );
			$this -> setForm( new addDeductionForm(array(), $values ));
			
			
			if( $request -> isMethod( 'post' ) ){
					$this -> form -> bind( $request -> getParameter( $this -> form -> getName()));
					if( $this -> form -> isValid()){
						
						
						if(! is_null( $this -> deductionId  ) ){
							$deduction = $this -> getDeductionService() -> getDeductionLines( $this -> deductionId );
							
							if(! is_null( $deduction ) ){
								$this -> getUser() -> setFlash( 'success' , __( TopLevelMessages :: CHANGE_RESTRICTED ) );
								$this -> redirect( 'payroll/addDeduction?deductionId='.$this -> deductionId );
								return;
							}
						}
						
						
						
						$result = $this -> form -> save();
						 
						if( $result == -1 ){
							$this -> getUser() -> setFlash( 'success' , __( TopLevelMessages :: SAVE_FAILURE ) );
							$this -> deductionId = $request -> getParameter( 'deductionId' );	
						}
						else{
							$this -> deductionId = $result; 
							$this -> getUser() -> setFlash( 'success' , __( TopLevelMessages :: SAVE_SUCCESS ) );
						}
						$this -> redirect( 'payroll/addDeduction?deductionId='.$this -> deductionId );
					}else{
						Logger::getLogger('payroll.addDeduction')->error($this->form);
                		$this->getUser()->setFlash('warning', __(TopLevelMessages::SAVE_FAILURE), false);
					}
				}
		 }
		 /*
		  * 
		  * 
		  */
		  function setForm( sfForm $form ){
		  	$this -> form = $form;
		  }
		  /*
		   * 
		   * 
		   */
		   function getForm( ){
		   	return $this -> form;
		   }
    }
?>