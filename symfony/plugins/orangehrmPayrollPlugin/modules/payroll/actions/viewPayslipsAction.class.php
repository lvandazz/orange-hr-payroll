<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */
class viewPayslipsAction extends baseAction {

	private $payslipService;

	function getPayslipService() {
		if (is_null($this -> payslipService)) {
			$this -> payslipService = new PaySlipService();
		}
		return $this -> payslipService;
	}

	/*
	 *
	 */
	function execute($request) {
		$this -> payrollPermissions = $this -> getDataGroupPermissions('pension');
		
		
		$frmSearch = $request -> getParameter('payrollReport') ;
			 
		$employee = $frmSearch['employeeD'];
		$fromDate = $frmSearch['fromDate'];
		$toDate = $frmSearch[ 'toDate' ];
			
		if( $fromDate > $toDate ){
			$this -> getUser() -> setFlash( 'error', 'Invalid dates' );
			$this -> redirect( 'payroll/payrollReport');
		}
		
		if( $this -> getUser() -> getEmployeeNumber() > 0 ){
			$logged  = $employee = $this -> getUser() -> getEmployeeNumber();
			
		}else{
			$logged = 0;
		}
		
		$frmParams = array( 'employee' => $employee ,'fromDate' => $fromDate, 'toDate' => $toDate, 'logged' => $logged );  
		$this -> form = new payslipFilterForm(array(),$frmParams); 
		
		$payslips = $this -> getPayslipService() -> getPayslips( $employee, $fromDate, $toDate );
		 
		
		$srchParams = array();
		$params = array();
		$this -> parmetersForListCompoment = $params;
		$this -> __setListComponent($payslips, 5, $srchParams, 1, $this -> payrollPermissions);

	}

	/*
	 *
	 */
	private function __setListComponent($payslips, $noOfRecords, $srchParams, $pageNumber, $permissions) {
		$runtimeDefinitions = array();
		$buttons = array();

		$buttons['Print'] = array('label' => 'Print', 'type' => 'submit', 'data-toggle' => 'modal', 'data-target' => '#printConfirmation', 'class' => 'blue');
		//$buttons['Preview'] = array('label' => 'Preview', 'type' => 'submit', 'data-toggle' => 'modal', 'data-target' => '#previewConfirmation', 'class' => 'info');
		$runtimeDefinitions['buttons'] = $buttons;

		$configurationFactory = new PayslipsResultsHeaderFactory();
		$configurationFactory -> setRuntimeDefinitions($runtimeDefinitions);

		ohrmListComponent::setPageNumber($pageNumber);
		ohrmListComponent::setConfigurationFactory($configurationFactory);
		ohrmListComponent::setListData($payslips);
		ohrmListComponent::setItemsPerPage($noOfRecords);
		ohrmListComponent::setNumberOfRecords(count($payslips));
	}

}
