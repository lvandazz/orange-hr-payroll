<?php
/*
 * @author - Joseph Luvanda
 * @Package - Payroll plugin for orangeHRM
 * @Contact - lvandazz@gmail.com
 */

class viewLoansAction extends  baseAction {

	private $loanService;

	function getLoanService() {
		if (is_null($this -> loanService)) {
			$this -> loanService = new loanService();
		}
		return $this -> loanService;
	}

	/*
	 *
	 *
	 */
	function execute($request) {
		//data group permissions
		$this -> loansPermissions = $this -> getDataGroupPermissions('pension'); 		 
		$frmSearch = $request -> getParameter( 'viewLoans' );
		$organization = $frmSearch[ 'organization' ];
		 
		$frmParams = array( 'organization' => $organization); 
		$this -> form = new viewLoansForm(array(),$frmParams);
		$this -> $parmetersForListCompoment = array();
		
		$loans = $this -> getLoanService() -> getLoans(null,$organization); 
		
		$srchParams = array();
		$params = array();
		$this -> parmetersForListCompoment = $params;
		$this -> __setListComponent($loans, 5, $srchParams, 1, $this -> loansPermissions);
	}

	/*
	 *
	 *
	 */
	private function __setListComponent($data, $noOfRecords, $srchParams, $pageNumber, $permissions) {
		$runtimeDefinitions = array();
		$buttons = array();

		if ($permissions -> canCreate()) {
			$buttons['Add'] = array('label' => 'Add', 'function' => 'addLoan');
		}

		if (!$permissions -> canDelete()) {
			$runtimeDefinitions['hasSelectableRows'] = false;
		} else if ($permissions -> canDelete()) {
			$buttons['Delete'] = array('label' => 'Delete', 'type' => 'submit', 'data-toggle' => 'modal', 'data-target' => '#deleteConfirmation', 'class' => 'delete');
		}

		$runtimeDefinitions['buttons'] = $buttons;

		$configurationFactory = new LoanListHeaderFactory();
		$configurationFactory -> setRuntimeDefinitions($runtimeDefinitions);

		ohrmListComponent::setPageNumber($pageNumber);
		ohrmListComponent::setConfigurationFactory($configurationFactory);
		ohrmListComponent::setListData($data);
		ohrmListComponent::setItemsPerPage($noOfRecords);
		ohrmListComponent::setNumberOfRecords(count( $data ));
	}

}
?>