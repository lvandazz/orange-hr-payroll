<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class deleteLoanAction extends  baseAction{
    	
		private $loanService;
		
		public function getLoanService( ){
			if( is_null( $this -> allowanceService  ) ){
				$this -> loanService = new LoanService();
			}
			return $this -> loanService;
		}
		
		function execute( $request ){
			// get permissions
			$loanPermissions = $this -> getDataGroupPermissions( 'allowance_list' );
			
			if( $loanPermissions -> canDelete() ){
				$form = new DefaultListForm();
				$loanIds = $request -> getParameter( 'chkSelectRow' );
				if( $this -> getLoanService() -> deleteLoans($loanIds) === true ){
					$this -> getUser() -> setFlash( 'success', __( TopLevelMessages :: DELETE_SUCCESS ) );	
				}else{
					$this -> getUser() -> setFlash( 'warning', __( TopLevelMessages :: DELETE_FAILURE ) );
				}
				
			}else{
				$this -> getUser() -> setFlash( 'warning' , __( TopLevelMessages:: ACCESS_DENIED ));
			}
			$this -> redirect( 'payroll/viewLoans' );
		}
    }
?>