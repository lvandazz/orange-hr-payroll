<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class deleteOrganizationAction extends  baseAction{
    	
		private $organizationService;
		
		public function getOrganizationService( ){
			
			if( is_null( $this -> organizationService  ) ){
				$this -> organizationService = new FinancialOrganizationService();
			}
			return $this -> organizationService;
		}
		
		function execute( $request ){
			// get permissions
			$organizationPermissions = $this -> getDataGroupPermissions( 'allowance_list' );
			
			if( $organizationPermissions -> canDelete() ){
				$form = new DefaultListForm();	
				 
				$orgIds = $request -> getParameter( 'chkSelectRow' );
				 
				foreach( $orgIds as $orgId ){
					$loans = $this -> getOrganizationService() -> getOrganizationLoans($orgId);
					 
					if( is_null( $loans) ){
						
						if( $this -> getOrganizationService() -> deleteOrganization( $orgId ) == true ){
							$this -> getUser() -> setFlash( 'success', __( TopLevelMessages :: DELETE_SUCCESS ) );			
						}else{
							$this -> getUser() -> setFlash( 'warning', __( TopLevelMessages :: DELETE_FAILURE ) );
						}
					}else{
						$this -> getUser() -> setFlash( 'warning', __( TopLevelMessages :: ORG_HAS_LOANS ) );
					}
				}
				
			}else{
				$this -> getUser() -> setFlash( 'failure' , __( TopLevelMessages:: ACCESS_DENIED ));
			}
			$this -> redirect( 'payroll/viewFinancialOrganizations' );
		}
    }
?>