<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class pensionsReportAction extends  baseAction{
    	protected $payrollService;
		/*
		 * 
		 * 
		 */
		 function getReportService(){
		  
		 	if( is_null( $this -> payrollService ) ){
		 		$this -> payrollService = new ReportService(); 
		 	}
			
			return $this -> payrollService;
		 }
		 
		 /*
		  * 
		  * 
		  */
		  function execute( $request ){
		  	//data group permissions
		  	$this -> reportPermissions = $this -> getDataGroupPermissions( 'pension' );
			
			$frmSearch = $request -> getParameter('payrollReport') ;
			 
			$employee = $frmSearch['employeeD'];
			$pension = $frmSearch['pension'];
			$fromDate = $frmSearch['fromDate'];
			$toDate = $frmSearch[ 'toDate' ];
			
			if( $fromDate > $toDate ){
				$this -> getUser() -> setFlash( 'error', 'Invalid dates' );
				$this -> redirect( 'payroll/pensionsReport');
			}
			
			 
			$frmParams = array( 'employee' => $employee , 'pension' => $pension , 'fromDate' => $fromDate , 'toDate' => $toDate); 
			$this -> form = new pensionsReportForm(array(),$frmParams);
			 
			// 
			$fullPensions = $this -> getReportService() -> getFullPensions($fromDate, $toDate, $employee, $pension );
			
			$this -> totals = $totals = $fullPensions[ count ( $fullPensions ) - 1]; 
			unset( $fullPensions[ count ( $fullPensions ) - 1]);
			$this -> fullPensions = $fullPensions;
			
			if( $request -> getParameter( 'print' ) != null ){
				$this -> getContext() -> getConfiguration() -> loadHelpers( 'Pdf' );
				$html = $this -> getPartial( 'payroll/printablePensions',array( 'fullPensions' => $fullPensions, 'totals' => $totals ));
				pdf_create( $html,'PensionsReport-'.date('F,d Y'),TRUE,'landscape' );
			}
			
			
		  }
		
    }
?>