<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class addLoanAction extends baseAction{
    	private $loanService;
		
		public function getLoanService(){
			if( is_null( $this -> loanService ) ){
				$this -> loanService = new loanService();
			}
			return $this -> loanService;
		}
		/*
		 * 
		 * 
		 */
		 function execute( $request ){
		 	$request -> setParameter( 'initialActionName' , 'viewLoans' );
			// data group permissions
			$this -> loansPermissions = $this -> getDataGroupPermissions( 'loans' );
			
			if( $this -> loansPermissions -> canCreate() == 0 ){
				$this -> redirect( 'payroll/viewLoans' );
			}
			$this -> loanId = $request -> getParameter( 'loanId' );
			$values = array( 'loanId' => $this -> loanId, 'loansPermissions' , $this -> loansPermissions );
			$this -> setForm( new addLoanForm(array(), $values ));
			
			
			if( $request -> isMethod( 'post' ) ){
					$this -> form -> bind( $request -> getParameter( $this -> form -> getName()));
					if( $this -> form -> isValid()){
						
						$result = $this -> form -> save();
						 
						if( $result == -1 ){
							$this -> getUser() -> setFlash( 'success' , __( TopLevelMessages :: LOAN_HAS_EMPLOYEE ) );
							$this -> loanId = $request -> getParameter( 'loanId' );	
						}
						else{
							$this -> loanId = $result; 
						}
						$this -> redirect( 'payroll/addLoan?loanId='.$this -> loanId );
					}else{
						Logger::getLogger('payroll.addLoan')->error($this->form);
                		$this->getUser()->setFlash('warning', __(TopLevelMessages::SAVE_FAILURE), false);
					}
				}
		 }
		 /*
		  * 
		  * 
		  */
		  function setForm( sfForm $form ){
		  	$this -> form = $form;
		  }
		  /*
		   * 
		   * 
		   */
		   function getForm( ){
		   	return $this -> form;
		   }
    }
?>