<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class addFinancialOrganizationAction extends baseAction{
    	private $financialOrganizationService ;
		
		function getFinancialOrganizationService(){
			if( is_null( $this -> financialOrganizationService ) ){
				$this -> financialOrganizationService = new FinancialOrganizationService();
			}
			return $this -> financialOrganizationService;
		}
		/*
		 * 
		 */
		function execute( $request ){
			$request->setParameter('initialActionName', 'viewFinancialOrganizations');  
			$this -> financialOrgsPermissions = $this -> getDataGroupPermissions( 'pension' );
			
			$values = array( 'organizationId' => $request -> getParameter( 'organizationId' ), 'financialOrgsPermissions' => $this -> financialOrgsPermissions );
			
			$this -> form = new addFinancialOrganizationForm(array(), $values );
			$this -> organizationId = $request -> getParameter( 'organizationId' );
			
			if( $request -> isMethod( 'post' ) ){
					$this -> form -> bind( $request -> getParameter( $this -> form -> getName()));
					if( $this -> form -> isValid()){
						$this -> organizationId = $this -> form -> save();
						$this -> getUser() -> setFlash( 'success' , __( TopLevelMessages :: SAVE_SUCCESS ) );
						$this -> redirect( 'payroll/addFinancialOrganization?organizationId='.$this -> organizationId );
					}else{
						Logger::getLogger('payroll.addFinancialOrganization')->error($this->form);
                		$this->getUser()->setFlash('warning', __(TopLevelMessages::SAVE_FAILURE), false);
					}
				}
		}
    }
?>