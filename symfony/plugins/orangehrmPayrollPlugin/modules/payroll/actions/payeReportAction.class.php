<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class payeReportAction extends  baseAction{
    	protected $payrollService;
		/*
		 * 
		 * 
		 */
		 function getReportService(){
		  
		 	if( is_null( $this -> payrollService ) ){
		 		$this -> payrollService = new ReportService(); 
		 	}
			
			return $this -> payrollService;
		 }
		 
		 /*
		  * 
		  * 
		  */
		  function execute( $request ){
		  	//data group permissions
		  	$this -> reportPermissions = $this -> getDataGroupPermissions( 'pension' );
			
			$frmSearch = $request -> getParameter('payrollReport') ;
			 
			$employee = $frmSearch['employeeD'];
			$subunit = $frmSearch['subunit'];
			$fromDate = $frmSearch['fromDate'];
			$toDate = $frmSearch[ 'toDate' ];
			
			if( $fromDate > $toDate ){
				$this -> getUser() -> setFlash( 'error', 'Invalid dates' );
				$this -> redirect( 'payroll/payrollReport');
			}
			
			 
			$frmParams = array( 'employee' => $employee , 'subunit' => $subunit , 'fromDate' => $fromDate, 'toDate' => $toDate ); 
			$this -> form = new payrollReportForm(array(),$frmParams);
			 
			// 
			$fullPayroll = $this -> getReportService() -> getFullPayroll($fromDate, $toDate, $employee, $subunit );
			
			$this -> totals = $fullPayroll[ count ( $fullPayroll ) - 1]; 
			unset( $fullPayroll[ count ( $fullPayroll ) - 1]);
			$this -> fullPayroll = $fullPayroll;
			
		  }
		
    }
?>