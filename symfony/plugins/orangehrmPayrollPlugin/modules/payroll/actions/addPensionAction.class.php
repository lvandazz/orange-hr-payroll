<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class AddPensionAction extends baseAction{
    	
		protected $pensionService;
		
		public function getPensionService(){
			if( is_null( $this -> pensionService ) ){
				$this -> pensionService = new PensionService();
			}
			return $this -> pensionService;
		}
		/*
		 * 
		 * 
		 */
		 function setForm( sfForm $form ){
		 	$this -> form = $form;
		 }
		 /*
		  * 
		  * 
		  */
		  function getForm( ){
		  	return $this -> form;
		  }
		  /*
		   * 
		   * 
		   */
		   function execute( $request ){
		   	
		   		$request -> setParameter( 'initialActionName' , 'viewPensions' );
				$this -> pensionPermissions = $this -> getDataGroupPermissions( 'pension' );
				$this -> pensionId = $request -> getParameter( 'pensionId' );
				if( $this -> pensionPermissions -> canCreate() == 0 ){
					$this -> redirect( 'payroll/viewPensions' );
				}
				$values = array( 'pensionId' => $this -> pensionId, 'pensionPermissions'  => $this -> pensionPermissions );
				
				$this -> setForm( new addPensionForm( array(), $values ));
				
				if( $request -> isMethod( 'post' ) ){
					
					$this -> form -> bind( $request -> getParameter( $this -> form -> getName()));
					if( $this -> form -> isValid()){
										 
						$result = $this -> form -> save();
						if( $result === -20 ){
							$this -> getUser() -> setFlash( 'warning' , __( TopLevelMessages :: PENSION_EXCEEDED ) );
							$this -> redirect( 'payroll/addPension');
						}
						else{
							
							$this -> pensionId = $result;
							$this -> getUser() -> setFlash( 'success' , __( TopLevelMessages :: SAVE_SUCCESS ) );
							$this -> redirect( 'payroll/addPension?pensionId='.$this -> pensionId );
						}
						
					}else{
						Logger::getLogger('payroll.addPension')->error($this->form);
                		$this->getUser()->setFlash('warning', __(TopLevelMessages::SAVE_FAILURE), false);
					}
				}
		   }
    }
?>