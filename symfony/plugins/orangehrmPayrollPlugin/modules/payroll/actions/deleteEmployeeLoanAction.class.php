<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class deleteEmployeeLoanAction extends  baseAction{
    	
		private $LoanService;
		
		public function getLoanService( ){
			
			if( is_null( $this -> LoanService  ) ){
				$this -> LoanService = new LoanService();
			}
			return $this -> LoanService;
		}
		
		function execute( $request ){
			// get permissions
			$organizationPermissions = $this -> getDataGroupPermissions( 'allowance_list' );
			
			if( $organizationPermissions -> canDelete() ){
				$form = new DefaultListForm();	
				 
				$orgIds = $request -> getParameter( 'chkSelectRow' );
				 
				foreach( $orgIds as $orgId ){
					$loans = $this -> getLoanService() -> getOrganizationLoans($orgId);
					 
					if( is_null( $loans) ){
						
						if( $this -> getLoanService() -> deleteOrganization( $orgId ) == true ){
							$this -> getUser() -> setFlash( 'success', __( TopLevelMessages :: DELETE_SUCCESS ) );			
						}else{
							$this -> getUser() -> setFlash( 'warning', __( TopLevelMessages :: DELETE_FAILURE ) );
						}
					}else{
						$this -> getUser() -> setFlash( 'warning', __( TopLevelMessages :: ORG_HAS_LOANS ) );
					}
				}
				
			}else{
				$this -> getUser() -> setFlash( 'failure' , __( TopLevelMessages:: ACCESS_DENIED ));
			}
			$this -> redirect( 'payroll/viewEmployeesLoans' );
		}
    }
?>