<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */

    class runPayrollAction extends baseAction{
    	private $payrollService; 
		private $employeeService;
		function getPayrollService(){
			if( is_null( $this -> payrollService ) ){
				$this -> payrollService = new PayrollService();
			}
			return $this -> payrollService;
		}
		/*
		 * 
		 * 
		 */
		 function getEmployeeService(){
		 	if( is_null( $this -> employeeService ) ){
		 		$this -> employeeService = new EmployeeService();
		 	}
			return $this -> employeeService;
		 }
		 /*
		  * 
		  * 
		  */
		 function execute( $request ){

		 	$this -> payrollPermissions = $this -> getDataGroupPermissions( 'pension' );
			
			
			 
			$month = $request -> getParameter('runPayroll');
			if( isset( $month ) AND $month != null ){
				$month = $month[ 'payDate' ];
			}else{
				$month = null;
			}
			$payrollResults = $this -> getPayrollService() -> runPayroll( $month );
			
			if( $payrollResults == -322 ){
				$payrollResults = array();
				$this -> getUser() -> setFlash( 'error', __( TopLevelMessages :: PENSION_NOT_DEFINED ) );
			}
			$this -> getUser()-> setAttribute('payrollMonth', $month );
			$this -> form = new runPayrollForm(array(),array( 'selectedMonth' => $month ));
			
			$srchParams = array();
			$params = array();
			$this -> parmetersForListCompoment = $params;
			$this->__setListComponent($payrollResults, 5, $srchParams, 1, $this-> payrollPermissions);
			
			
		 }
		 /*
		  * 
		  */
		 private function __setListComponent( $payrollResults, $noOfRecords, $srchParams, $pageNumber, $permissions) {
			 	$runtimeDefinitions = array();
		        $buttons = array();
		
		        if ($permissions->canCreate()) {
		            $buttons['Process'] = array('label' => 'Commit ',
		                'type' => 'submit',
		                'data-toggle' => 'modal',
		                'data-target' => '#processConfirmation',
		                'class' => 'blue');
		        }
					
		        $runtimeDefinitions['buttons'] = $buttons;
		        
		        $configurationFactory = new PayrollResultsHeaderFactory();
		        $configurationFactory->setRuntimeDefinitions($runtimeDefinitions);
		        
		        ohrmListComponent::setPageNumber($pageNumber);
		        ohrmListComponent::setConfigurationFactory($configurationFactory);
		        ohrmListComponent::setListData($payrollResults);
		        ohrmListComponent::setItemsPerPage($noOfRecords);
		        ohrmListComponent::setNumberOfRecords( count($payrollResults) );
    }
}
?> 