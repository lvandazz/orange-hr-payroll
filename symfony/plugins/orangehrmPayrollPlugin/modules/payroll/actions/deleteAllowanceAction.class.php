<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class deleteAllowanceAction extends  baseAction{
    	
		private $allowanceService;
		
		public function getAllowanceService( ){
			if( is_null( $this -> allowanceService  ) ){
				$this -> allowanceService = new AllowanceService();
				$this -> allowanceService -> setAllowanceDao(new AllowanceDao() );
			}
			return $this -> allowanceService;
		}
		
		function execute( $request ){
			// get permissions
			$allowancePermissions = $this -> getDataGroupPermissions( 'allowance_list' );
			
			if( $allowancePermissions -> canDelete() ){
				$form = new DefaultListForm();	
				 
				$allowanceIds = $request -> getParameter( 'chkSelectRow' );
				$this -> getAllowanceService() -> deleteAllowance($allowanceIds);
				$this -> getUser() -> setFlash( 'success', __( TopLevelMessages :: DELETE_SUCCESS ) );
			}else{
				$this -> getUser() -> setFlash( 'failure' , __( TopLevelMessages:: ACCESS_DENIED ));
			}
			$this -> redirect( 'payroll/viewAllowanceList' );
		}
    }
?>