<?php
   /*
    * @author - Joseph Luvanda
    * @Package - Payroll plugin for orangeHRM
    * @Contact - lvandazz@gmail.com
    */
    
    class assignPensionAction extends baseAction{
    	private $pensionService;
		
		public function getPensionService(){
			if( is_null( $this -> pensionService ) ){
				$this -> pensionService = new PensionService();
			}
			return $this -> pensionService;
		}
		/*
		 * 
		 * 
		 */
		 function execute( $request ){
		 	$request -> setParameter( 'initialActionName' , 'viewPensions' );
			// data group permissions
			$this -> pensionPermissions = $this -> getDataGroupPermissions( 'pension' );
			$this -> penEmpId = $request -> getParameter( 'penEmpId' );
			$values = array( 'penEmpId' => $this -> penEmpId, 'pensionPermissions' , $this -> pensionPermissions );
			$this -> setForm( new assignPensionForm(array(), $values ));
			
			
			if( $request -> isMethod( 'post' ) ){
					$this -> form -> bind( $request -> getParameter( $this -> form -> getName()));
					if( $this -> form -> isValid()){
						$this -> penEmpId = $this -> form -> save();
						$this -> getUser() -> setFlash( 'success' , __( TopLevelMessages :: SAVE_SUCCESS ) );
						$this -> redirect( 'payroll/assignPension?penEmpId='.$this -> penEmpId );
					}else{
						Logger::getLogger('payroll.assignPension')->error($this->form);
                		$this->getUser()->setFlash('warning', __(TopLevelMessages::SAVE_FAILURE), false);
					}
				}
		 }
		 /*
		  * 
		  * 
		  */
		  function setForm( sfForm $form ){
		  	$this -> form = $form;
		  }
		  /*
		   * 
		   * 
		   */
		   function getForm( ){
		   	return $this -> form;
		   }
    }
?>