/**
 * @author Joseph Luvanda - lvandazz@gmail.com
 */

$(document).ready(function() {

	if ($('#btnSave').attr('value') == lang_edit) {

		$('#organizationName').attr('disabled', 'disable');
		$('#organizationAddress').attr('disabled', 'disable');
		$('#organizationContact').attr('disabled', 'disable');
		$('#organizationInfo').attr('disabled', 'disable');
	}

	/*//Auto complete
	$("#organizationName").autocomplete(organizationList, {
	formatItem: function(item) {
	return $('<div/>').text(item.name).html();
	},
	formatResult: function(item) {
	return item.name
	},
	matchContains:true
	}).result(function(event, item) {
	//$("#candidateSearch_selectedCandidate").val(item.id);
	//$("label.error").hide();
	});
	*/

	// save

	$('#btnSave').click(function() {

		if ($('#btnSave').attr('value') == lang_edit) {
			$('#organizationName').removeAttr('disabled');
			$('#organizationAddress').removeAttr('disabled');
			$('#organizationContact').removeAttr('disabled');
			$('#organizationInfo').removeAttr('disabled');
			
			$('#btnSave').attr('value', lang_save);
			$('#btnBack').attr('value', lang_cancel);

			return;
		}

		if (isValidForm()) {
			$('form#frmAddFinancialOrganization').attr({
				action : linkForaddFinancialOrganization + '?organizationId=' + organizationId
			});
			$('#frmAddFinancialOrganization').submit();
		}

	});

	$('#btnBack').click(function() {

		if ($('#btnBack').attr('value') == lang_back) {
			window.location.replace(backBtnUrl);
		}

		if ($('#btnBack').attr('value') == lang_cancel) {
			window.location.replace(cancelBtnUrl + '?organizationId=' + organizationId);
		}
	});

});
function isValidForm() {

	$.validator.addMethod("organizationNameValidation", function(value, element, params) {
		var temp = true;
		var hmCount = organizationList.length;

		var i;
		for ( i = 0; i < hmCount; i++) {
			hmName = $.trim($('#organizationName').val()).toLowerCase();
			arrayName = organizationList[i].name.toLowerCase();
			if (hmName == arrayName && organizationList[i].id != organizationId) {
				$('#organizationNameId').val(organizationList[i].id);
				temp = false;
				break;
			}
		}
		return temp;
	});

	jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z\s\-]+$/i.test(value);
}, "Only alphabetical characters"); 

	var validator = $('#frmAddFinancialOrganization').validate({
		rules : {
			'addFinancialOrganization[organizationName]' : {
				required : true,
				organizationNameValidation : true,
				lettersonly : true
			},
			'addFinancialOrganization[organizationAddress]' : {
				required : true,

			},
			'addFinancialOrganization[organizationContact]' : {
				required : true
			},
			'addFinancialOrganization[organizationInfo]' : {
				required : false
			}
		},
		messages : {
			'addFinancialOrganization[organizationName]' : {
				organizationNameValidation : 'Organization name already exists'
			}
		}
	});

	return true;
}