/**
 * @author Joseph Luvanda - lvandazz@gmail.com
 */

$( document ).ready( function(){
	
	
	if( $( '#btnSave' ).attr('value' ) == lang_edit ){
		$('#pensionName' ).attr('disabled', 'disable' );
		$('#percentEmployer' ).attr('disabled', 'disable' );
		$('#percentEmployee' ).attr('disabled', 'disable' );
	}
	
	
	
	// save 
	
	$( '#btnSave' ).click( function (){
		 
		if( $( '#btnSave' ).attr( 'value' ) == lang_edit ){
			
			$( '#pensionName' ).removeAttr( 'disabled' );
			$( '#percentEmployer' ).removeAttr( 'disabled' );
			$( '#percentEmployee' ).removeAttr( 'disabled' );
			
			$('#btnSave').attr( 'value' , lang_save );
			$('#btnBack' ).attr( 'value' , lang_cancel );
			
			return;
		}
 
			
		if( isValidForm()){
			$( 'form#frmAddPension' ).attr({
				action : linkForAddPension + '?pensionId='+ pensionId
			} );
			$( '#frmAddPension' ).submit();	
		}
		
	});
	
	$( '#btnBack' ).click( function (){
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_back ){
			window.location.replace( backBtnUrl );
		}
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_cancel ){
			window.location.replace( cancelBtnUrl + '?pensionId='+ pensionId );
		}
	})
	 
});
function isValidForm(){
	
	
	$.validator.addMethod("alphabets", function(value, element, params) {
         
        return (value =="" ||(value == parseInt(value, 10)));
    });
   jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z\s\-]+$/i.test(value);
}, "Only alphabetical characters"); 
	var validator = $( '#frmAddPension' ).validate({
		rules : {
			'addPension[pensionName]' : {
				required : true,
				maxlength: 20,
				lettersonly: true
			},
			'addPension[percentEmployee]' : {
				number : true,
				max: 15,
				min: 1,
				required: true
				
			},
			'addPension[percentEmployer]' : {
				number: true,
				max: 15,
				min:1,
				required: true
			}
		},
		messages : {
			 'addPension[pensionName]' : {
                lettersonly: 'Enter a valid  Name'
            }
		}
	});
	
	return true;
}