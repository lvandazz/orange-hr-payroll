<?php

class TopLevelMessages {
    
    const SAVE_SUCCESS = "Successfully Saved";
    const ADD_SUCCESS = "Successfully Added";
    const UPDATE_SUCCESS = "Successfully Updated";
    const DELETE_SUCCESS = "Successfully Deleted";
    const SAVE_FAILURE = "Failed to Save";
    const DELETE_FAILURE = "Failed to Delete";
    const INVALID_FILE_TYPE = "Invalid File Type";
    const NO_RECORDS_FOUND = "No Records Found";
    const SELECT_RECORDS = "Select Records to Delete";
    const FILE_TYPE_SAVE_FAILURE = "Failed to Save: File Type Not Allowed";
    const FILE_SIZE_SAVE_FAILURE = "Failed to Save: File Size Exceeded";
    const ACCESS_DENIED = "Access Denied";
    const VALIDATION_FAILED = "Validation Failed";
	const PROCESS_COMPLETE = " Payroll Processing Complete";
	const NO_PAYROLL_MONTH = " No Payroll Selected";
	const LOAN_HAS_EMPLOYEE = "Selected Loan was assigned to Employee(s). Can not change";
	const EMP_PENDING_LOAN = 'The employee has not completed the previous loan ';
	const CHANGE_RESTRICTED = 'Changes are restricted to this record';
	const ORG_HAS_LOANS = 'Organization has Loan record(s)';
	const PENSION_HAS_EMPLOYEE = 'Pension has employee record(s) associated';
	const PENSION_EXCEEDED = 'Total pension should be 20%';
	const PENSION_NOT_DEFINED = 'Employee(s) have no  assigned pension. ';
	 
}

