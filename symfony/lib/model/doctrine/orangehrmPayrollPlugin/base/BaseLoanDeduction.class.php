<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('LoanDeduction', 'doctrine');

/**
 * BaseLoanDeduction
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $loan_deduction_id
 * @property integer $payroll_id
 * @property integer $emp_number
 * @property float $amount
 * @property integer $loan_id
 * @property float $balance
 * @property Doctrine_Collection $Payroll
 * @property Doctrine_Collection $Employee
 * @property Doctrine_Collection $Loan
 * 
 * @method integer             getLoanDeductionId()   Returns the current record's "loan_deduction_id" value
 * @method integer             getPayrollId()         Returns the current record's "payroll_id" value
 * @method integer             getEmpNumber()         Returns the current record's "emp_number" value
 * @method float               getAmount()            Returns the current record's "amount" value
 * @method integer             getLoanId()            Returns the current record's "loan_id" value
 * @method float               getBalance()           Returns the current record's "balance" value
 * @method Doctrine_Collection getPayroll()           Returns the current record's "Payroll" collection
 * @method Doctrine_Collection getEmployee()          Returns the current record's "Employee" collection
 * @method Doctrine_Collection getLoan()              Returns the current record's "Loan" collection
 * @method LoanDeduction       setLoanDeductionId()   Sets the current record's "loan_deduction_id" value
 * @method LoanDeduction       setPayrollId()         Sets the current record's "payroll_id" value
 * @method LoanDeduction       setEmpNumber()         Sets the current record's "emp_number" value
 * @method LoanDeduction       setAmount()            Sets the current record's "amount" value
 * @method LoanDeduction       setLoanId()            Sets the current record's "loan_id" value
 * @method LoanDeduction       setBalance()           Sets the current record's "balance" value
 * @method LoanDeduction       setPayroll()           Sets the current record's "Payroll" collection
 * @method LoanDeduction       setEmployee()          Sets the current record's "Employee" collection
 * @method LoanDeduction       setLoan()              Sets the current record's "Loan" collection
 * 
 * @package    orangehrm
 * @subpackage model\payroll\base
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseLoanDeduction extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('loan_deduction');
        $this->hasColumn('loan_deduction_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('payroll_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('emp_number', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('amount', 'float', null, array(
             'type' => 'float',
             ));
        $this->hasColumn('loan_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('balance', 'float', null, array(
             'type' => 'float',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('Payroll', array(
             'local' => 'payroll_id',
             'foreign' => 'payroll_id'));

        $this->hasMany('Employee', array(
             'local' => 'emp_number',
             'foreign' => 'empNumber'));

        $this->hasMany('Loan', array(
             'local' => 'loan_id',
             'foreign' => 'id'));
    }
}