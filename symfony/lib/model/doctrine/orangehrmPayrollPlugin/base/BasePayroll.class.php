<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Payroll', 'doctrine');

/**
 * BasePayroll
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $payroll_id
 * @property date $pay_date
 * @property TaxDeduction $TaxDeduction
 * @property PensionDeduction $PensionDeduction
 * @property Payslip $Payslip
 * @property LoanDeduction $LoanDeduction
 * @property DeductionLine $DeductionLine
 * 
 * @method integer          getPayrollId()        Returns the current record's "payroll_id" value
 * @method date             getPayDate()          Returns the current record's "pay_date" value
 * @method TaxDeduction     getTaxDeduction()     Returns the current record's "TaxDeduction" value
 * @method PensionDeduction getPensionDeduction() Returns the current record's "PensionDeduction" value
 * @method Payslip          getPayslip()          Returns the current record's "Payslip" value
 * @method LoanDeduction    getLoanDeduction()    Returns the current record's "LoanDeduction" value
 * @method DeductionLine    getDeductionLine()    Returns the current record's "DeductionLine" value
 * @method Payroll          setPayrollId()        Sets the current record's "payroll_id" value
 * @method Payroll          setPayDate()          Sets the current record's "pay_date" value
 * @method Payroll          setTaxDeduction()     Sets the current record's "TaxDeduction" value
 * @method Payroll          setPensionDeduction() Sets the current record's "PensionDeduction" value
 * @method Payroll          setPayslip()          Sets the current record's "Payslip" value
 * @method Payroll          setLoanDeduction()    Sets the current record's "LoanDeduction" value
 * @method Payroll          setDeductionLine()    Sets the current record's "DeductionLine" value
 * 
 * @package    orangehrm
 * @subpackage model\payroll\base
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePayroll extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('payroll');
        $this->hasColumn('payroll_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('pay_date', 'date', null, array(
             'type' => 'date',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('TaxDeduction', array(
             'local' => 'payroll_id',
             'foreign' => 'payroll_id'));

        $this->hasOne('PensionDeduction', array(
             'local' => 'payroll_id',
             'foreign' => 'payroll_id'));

        $this->hasOne('Payslip', array(
             'local' => 'payroll_id',
             'foreign' => 'payroll_id'));

        $this->hasOne('LoanDeduction', array(
             'local' => 'payroll_id',
             'foreign' => 'payroll_id'));

        $this->hasOne('DeductionLine', array(
             'local' => 'payroll_id',
             'foreign' => 'payroll_id'));
    }
}