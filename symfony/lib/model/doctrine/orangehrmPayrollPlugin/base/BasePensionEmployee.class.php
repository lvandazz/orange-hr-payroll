<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('PensionEmployee', 'doctrine');

/**
 * BasePensionEmployee
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $pension_id
 * @property integer $emp_number
 * @property date $date_created
 * @property date $date_joined
 * @property integer $pen_emp_id
 * @property Doctrine_Collection $Employee
 * @property Doctrine_Collection $Pension
 * 
 * @method integer             getPensionId()    Returns the current record's "pension_id" value
 * @method integer             getEmpNumber()    Returns the current record's "emp_number" value
 * @method date                getDateCreated()  Returns the current record's "date_created" value
 * @method date                getDateJoined()   Returns the current record's "date_joined" value
 * @method integer             getPenEmpId()     Returns the current record's "pen_emp_id" value
 * @method Doctrine_Collection getEmployee()     Returns the current record's "Employee" collection
 * @method Doctrine_Collection getPension()      Returns the current record's "Pension" collection
 * @method PensionEmployee     setPensionId()    Sets the current record's "pension_id" value
 * @method PensionEmployee     setEmpNumber()    Sets the current record's "emp_number" value
 * @method PensionEmployee     setDateCreated()  Sets the current record's "date_created" value
 * @method PensionEmployee     setDateJoined()   Sets the current record's "date_joined" value
 * @method PensionEmployee     setPenEmpId()     Sets the current record's "pen_emp_id" value
 * @method PensionEmployee     setEmployee()     Sets the current record's "Employee" collection
 * @method PensionEmployee     setPension()      Sets the current record's "Pension" collection
 * 
 * @package    orangehrm
 * @subpackage model\payroll\base
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePensionEmployee extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('pension_employee');
        $this->hasColumn('pension_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('emp_number', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('date_created', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('date_joined', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('pen_emp_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('Employee', array(
             'local' => 'emp_number',
             'foreign' => 'empNumber'));

        $this->hasMany('Pension', array(
             'local' => 'pension_id',
             'foreign' => 'pension_id'));
    }
}