<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('OprasForm', 'doctrine');

/**
 * BaseOprasForm
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $form_id
 * @property integer $appraisee
 * @property integer $supervisor
 * @property date $date_from
 * @property date $date_to
 * @property longvarchar $description
 * @property Doctrine_Collection $Employee
 * @property PerformanceAgreement $PerformanceAgreement
 * @property PaLine $PaLine
 * 
 * @method integer              getFormId()               Returns the current record's "form_id" value
 * @method integer              getAppraisee()            Returns the current record's "appraisee" value
 * @method integer              getSupervisor()           Returns the current record's "supervisor" value
 * @method date                 getDateFrom()             Returns the current record's "date_from" value
 * @method date                 getDateTo()               Returns the current record's "date_to" value
 * @method longvarchar          getDescription()          Returns the current record's "description" value
 * @method Doctrine_Collection  getEmployee()             Returns the current record's "Employee" collection
 * @method PerformanceAgreement getPerformanceAgreement() Returns the current record's "PerformanceAgreement" value
 * @method PaLine               getPaLine()               Returns the current record's "PaLine" value
 * @method OprasForm            setFormId()               Sets the current record's "form_id" value
 * @method OprasForm            setAppraisee()            Sets the current record's "appraisee" value
 * @method OprasForm            setSupervisor()           Sets the current record's "supervisor" value
 * @method OprasForm            setDateFrom()             Sets the current record's "date_from" value
 * @method OprasForm            setDateTo()               Sets the current record's "date_to" value
 * @method OprasForm            setDescription()          Sets the current record's "description" value
 * @method OprasForm            setEmployee()             Sets the current record's "Employee" collection
 * @method OprasForm            setPerformanceAgreement() Sets the current record's "PerformanceAgreement" value
 * @method OprasForm            setPaLine()               Sets the current record's "PaLine" value
 * 
 * @package    orangehrm
 * @subpackage model\payroll\base
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseOprasForm extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('opras_form');
        $this->hasColumn('form_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('appraisee', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('supervisor', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('date_from', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('date_to', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('description', 'longvarchar', null, array(
             'type' => 'longvarchar',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('Employee', array(
             'local' => 'supervisor',
             'foreign' => 'EmpNumber'));

        $this->hasOne('PerformanceAgreement', array(
             'local' => 'form_id',
             'foreign' => 'form_id'));

        $this->hasOne('PaLine', array(
             'local' => 'form_id',
             'foreign' => 'form_id'));
    }
}