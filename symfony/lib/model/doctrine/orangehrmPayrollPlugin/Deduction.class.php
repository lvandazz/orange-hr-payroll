<?php

/**
 * Deduction
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    orangehrm
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Deduction extends PluginDeduction
{
	/*
	 * 
	 */
	function getEmployee(){
		$employeeService = new EmployeeService();
		$employee = $employeeService -> getEmployee( $this -> getEmpNumber());
		
		return $employee -> getFullName();
	}
	/*
	 * 
	 */
	function getDateCreatedX(){
		
	}
	/*
	 * 
	 */
	function getAmountX( $raw = false ){
		if( $raw == false ){
			return number_format( $this -> getAmount() );
		}
		return $this -> getAmount();
	}
	/*
	 * 
	 */
	 function getStatusX(){
	 	if( $this -> getStatus() == 0 ){
	 		return 'Created';
	 	}
		if( $this -> getStatus() == 1 ){
			return 'Returning';
		}
		if( $this -> getStatus() == 2 ){
			return 'Completed';
		}
	 }
	 /*
	  * 
	  */
	 function getRecurringX( ){
	 	if( $this -> getIsRecurring() == 1){
	 		return 'Yes';
	 	}
		return  'No';
	 }
	 /*
	  * 
	  */
	  function getBalanceX( $raw = false ){
		if( $raw == false ){
			return number_format( $this -> getBalance() );
		}
		return $this -> getBalance();
	}
	 /*
	  * 
	  */
	  function getRepaymentAmountX( $raw = false ){
	  	
	  	if( $raw == false ){
	  		return number_format( $this -> getRepaymentAmount());
	  	}
		
		return $this -> getRepaymentAmount();
	  }
}
