<?php

/**
 * allowance
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    orangehrm
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class allowance extends Pluginallowance
{
	function getID()
	{
		 
		return $this -> allowance_id;
	}
	
	function getAllowanceTitle()
	{
		return $this -> title;
	}
	
	function getPercentageBasic()
	{
		
		
		return  $this -> perce_basic;
	}
	 function getAllowanceAmount( $raw = false )
	{
		if( $raw == true )
			return $this -> amount;
		
		$nfcf = new NumberFormatCellFilter();
		return $nfcf ->  filter( $this -> amount );
	}
	/*
	 * 
	 * 
	 */
	 function setID( $id ){
	 	 $this -> allowance_id = $id;
	 }
}
