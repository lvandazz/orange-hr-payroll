<?php

/**
 * TaxDeducationTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class TaxDeducationTable extends PluginTaxDeducationTable
{
    /**
     * Returns an instance of this class.
     *
     * @return object TaxDeducationTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TaxDeducation');
    }
}