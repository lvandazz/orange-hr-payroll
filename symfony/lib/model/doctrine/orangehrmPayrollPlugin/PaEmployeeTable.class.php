<?php

/**
 * PaEmployeeTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PaEmployeeTable extends PluginPaEmployeeTable
{
    /**
     * Returns an instance of this class.
     *
     * @return object PaEmployeeTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PaEmployee');
    }
}