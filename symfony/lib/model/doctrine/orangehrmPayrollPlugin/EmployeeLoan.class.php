<?php

/**
 * EmployeeLoan
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    orangehrm
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class EmployeeLoan extends PluginEmployeeLoan
{

		
	function getLoan(   ){
		$loanService = new LoanService();
		$loan = $loanService -> getLoans( $this -> getLoanId()) -> getFirst();
		return $loan;
	}
	function getEmpName(){
		$employeeService = new EmployeeService();
		$employee = $employeeService -> getEmployee( $this -> getEmpNumber() );
		return $employee -> getFullName();
	}
	/*
	 * 
	 */
	 function getLoanTitle(){
	 	$loan = $this -> getLoan(   );
		return $loan -> getTitle();
	 }
	 /*
	  * 
	  */
	  function getPrincipalX( $raw = false ){
	  	
	  	$loan = $this -> getLoan(   );
		if( $raw == false ){
	  		return number_format( $loan -> getPrincipal( ));	
	  	}
		return $loan -> getPrincipal();
	  }
	 /*
	  * 
	  */
	  function getOrganization(){
	  	$organizationService = new FinancialOrganizationService();
		$loan = $this -> getLoan(   );
		$org = $organizationService -> getFinancialOrganizations($loan -> getOrganizationId() );
		return $org -> getFirst() -> getName();
	  }
	  /*
	   * 
	   */
	   function getStatusX(){
	   	 if( $this -> getStatus() == 0 ){
	   	 	return 'Disbursed';
	   	 }
		 if( $this -> getStatus() == 1 ){
		 	return 'Returning';
		 }
		 if( $this -> getStatus() == 2 ){
		 	return 'Completed';
		 }
	   }
	   /*
	    * 
	    */
	    function getBalanceX( $raw = false ){
	    	
			if( $raw == false ){
				return number_format($this -> getBalance());
			}
			return $this -> getBalance();
			
	    }
		/*
		 * 
		 */
		 function getAccruedAmount( $raw = false ){
		 	$loan = $this -> getLoan();
			return $loan -> getAccruedAmount( $raw );
			
		 }
		 /*
		  * 
		  */
		 function  getRepaymentX( $raw = false){
		 	if( $raw == FALSE ){
		 		return number_format( $this -> getMonthRepayment(),2);
		 	}
			return $this -> getMonthRepayment();
		 }
}
