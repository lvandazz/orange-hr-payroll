/**
 * @author Joseph Luvanda - lvandazz@gmail.com
 */

$( document ).ready( function(){
	
	
	if( $( '#btnSave' ).attr('value' ) == lang_edit ){
		$('#pensionName' ).attr('disabled', 'disable' );
		$('#employeeName' ).attr('disabled', 'disable' );
		$('#dateJoin' ).attr('disabled', 'disable' );
	}
	
	
	    //Auto complete
		$("#employeeName").autocomplete(employeeList, {
		        formatItem: function(item) {
		            return $('<div/>').text(item.name).html();
		        },
		        formatResult: function(item) {
		            return item.name
		        },  
		        matchContains:true
		    }).result(function(event, item) {
		        //$("#candidateSearch_selectedCandidate").val(item.id);
		        //$("label.error").hide();
        });
		
	
	// save 
	
	$( '#btnSave' ).click( function (){
		 
		if( $( '#btnSave' ).attr( 'value' ) == lang_edit ){
			
			$( '#pensionName' ).removeAttr( 'disabled' );
			$( '#employeeName' ).removeAttr( 'disabled' );
			$( '#dateJoin' ).removeAttr( 'disabled' );
			
			$('#btnSave').attr( 'value' , lang_save );
			$('#btnBack' ).attr( 'value' , lang_cancel );
			
			return;
		}
 
		if( isValidForm()){
			$( 'form#frmAssignPension' ).attr({
				action : linkForAssignPension + '?penEmpId='+ penEmpId
			} ); 
			$( '#frmAssignPension' ).submit();	
		}
		
	});
	
	$( '#btnBack' ).click( function (){
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_back ){
			window.location.replace( backBtnUrl );
		}
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_cancel ){
			window.location.replace( cancelBtnUrl + '?pensionId='+ pensionId );
		}
	});
	 
});
function isValidForm(){
	
	 $.validator.addMethod("employeeNameValidation", function(value, element, params) {
        var temp = false;
        var hmCount = employeeList.length;

        var i;
        for (i=0; i < hmCount; i++) {
            hmName = $.trim($('#employeeName').val()).toLowerCase();
            arrayName = employeeList[i].name.toLowerCase();
            if (hmName == arrayName) {
                $('#employeeNameId').val(employeeList[i].id);
                temp = true
                break;
            }
        }
        return temp;
    });
    
	$.validator.addMethod("alphabets", function(value, element, params) {
         
        return (value =="" ||(value == parseInt(value, 10)));
    });
   
	var validator = $( '#frmAssignPension' ).validate({
		rules : {
			'assignPension[pensionName]' : {
				required : true,
				
			},
			'assignPension[employeeName]' : {
				required: true,
				employeeNameValidation : true
				
			},
			'assignPension[dateJoin]' : {
				required: true,
				date: true
			}
		},
		messages : {
			 'assignPension[employeeName]' : {
                employeeNameValidation: 'Enter a valid employee Name'
            }
		}
	});
	
	return true;
}