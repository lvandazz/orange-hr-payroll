/**
 * @author Joseph Luvanda - lvandazz@gmail.com
 */

$( document ).ready( function(){
	
	
	if( $( '#btnSave' ).attr('value' ) == lang_edit ){
		$('#allowanceAmount' ).attr('disabled', 'disable' );
		$('#allowancePercentage' ).attr('disabled', 'disable' );
		$('#allowanceTitle' ).attr('disabled', 'disable' );
	}
	
	
	
	// save 
	
	$( '#btnSave' ).click( function (){
		
		if( $( '#btnSave' ).attr( 'value' ) == lang_edit ){
			
			$( '#allowanceTitle' ).removeAttr( 'disabled' );
			$( '#allowanceAmount' ).removeAttr( 'disabled' );
			$( '#allowancePercentage' ).removeAttr( 'disabled' );
			
			$('#btnSave').attr( 'value' , lang_save );
			$('#btnBack' ).attr( 'value' , lang_cancel );
			
			return;
		}
		
		
		if(  ( $('#allowanceAmount').val() == 0 || isNaN( $('#allowanceAmount' ).val() ))   &&
			 ( $( '#allowancePercentage' ).val() == 0 || isNaN( $('#allowancePercentage' ).val() ) )){
	 	
				return false;
			}
			
		if( isValidForm()){
			$( 'form#frmAddAllowance' ).attr({
				action : linkForAddAllowance + '?allowanceId='+ allowanceId
			} );
			$( '#frmAddAllowance' ).submit();	
		}
		
	});
	
	$( '#btnBack' ).click( function (){
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_back ){
			window.location.replace( backBtnUrl );
		}
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_cancel ){
			window.location.replace( cancelBtnUrl + '?allowanceId='+ allowanceId );
		}
	})
	$( '#allowanceAmount' ).focus( function (){
		
		$('#allowancePercentage' ).val( 0 );
	});
	
	$( '#allowancePercentage' ).focus( function (){
		
		$('#allowanceAmount' ).val( 0 );
	});
});

function isValidForm(){
	
	
	$.validator.addMethod("alphabets", function(value, element, params) {
         
        return (value =="" ||(value == parseInt(value, 10)));
    });
	var validator = $( '#frmAddAllowance' ).validate({
		rules : {
			'addAllowance[allowanceTitle]' : {
				required : true,
				maxlength: 45
			},
			'addAllowance[allowanceAmount]' : {
				number : true,
				maxlength: 12
				
			},
			'addAllowance[allowancePercentage]' : {
				number: true,
				maxlength: 4
			}
		},
		messages : {
			
		}
	});
	
	return true;
}

