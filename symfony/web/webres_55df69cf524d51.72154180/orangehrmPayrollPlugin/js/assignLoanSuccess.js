/**
 * @author Joseph Luvanda - lvandazz@gmail.com
 */

$( document ).ready( function(){
	
	$('#monthRepayment' ).attr('disabled', 'disable' );
	if( $( '#btnSave' ).attr('value' ) == lang_edit ){
		$('#loanTitle' ).attr('disabled', 'disable' );
		$('#employeeName' ).attr('disabled', 'disable' );
		$('#dateJoin' ).attr('disabled', 'disable' );
		$('#repaymentStart' ).attr('disabled', 'disable' );
		$('#monthRepayment' ).attr('disabled', 'disable' );
		$('#extraInfo' ).attr('disabled', 'disable' );
	}
	
	
	    //Auto complete
		$("#employeeName").autocomplete(employeeList, {
		        formatItem: function(item) {
		            return $('<div/>').text(item.name).html();
		        },
		        formatResult: function(item) {
		            return item.name
		        },  
		        matchContains:true
		    }).result(function(event, item) {
		        //$("#candidateSearch_selectedCandidate").val(item.id);
		        //$("label.error").hide();
        });
		
	 //Auto load repayments
	 $( "#loanTitle" ).on('change' ,function (){
	 	loadRepayment();
	 });
	// save 
	
	$( '#btnSave' ).click( function (){
		 
		if( $( '#btnSave' ).attr( 'value' ) == lang_edit ){
			
			$( '#loanTitle' ).removeAttr( 'disabled' );
			$( '#employeeName' ).removeAttr( 'disabled' );
			$( '#dateJoin' ).removeAttr( 'disabled' );
			$( '#repaymentStart' ).removeAttr( 'disabled' );
			 
			$( '#extraInfo' ).removeAttr( 'disabled' );
			
			$('#btnSave').attr( 'value' , lang_save );
			$('#btnBack' ).attr( 'value' , lang_cancel );
			
			return;
		}
 
		if( isValidForm()){
			$( 'form#frmAssignLoan' ).attr({
				action : linkForAssignLoan + '?empLoanId='+ empLoanId
			} ); 
			$('#monthRepayment' ).removeAttr('disabled');
			$( '#frmAssignLoan' ).submit();	
		}
		
	});
	
	$( '#btnBack' ).click( function (){
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_back ){
			window.location.replace( backBtnUrl );
		}
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_cancel ){
			window.location.replace( cancelBtnUrl + '?empLoanId='+ empLoanId );
		}
	});
	 
});
function isValidForm(){
	
	 $.validator.addMethod("employeeNameValidation", function(value, element, params) {
        var temp = false;
        var hmCount = employeeList.length;

        var i;
        for (i=0; i < hmCount; i++) {
            hmName = $.trim($('#employeeName').val()).toLowerCase();
            arrayName = employeeList[i].name.toLowerCase();
            if (hmName == arrayName) {
                $('#empId').val(employeeList[i].id);
                temp = true
                break;
            }
        }
        return temp;
    });
    
	$.validator.addMethod("alphabets", function(value, element, params) {
         
        return (value =="" ||(value == parseInt(value, 10)));
    });
   
	var validator = $( '#frmAssignLoan' ).validate({
		rules : {
			'assignLoan[loanTitle]' : {
				required : true,
				
			},
			'assignLoan[employeeName]' : {
				required: true,
				employeeNameValidation : true
				
			},
			'assignLoan[dateJoin]' : {
				required: true,
				date: true
			},
			'repaymentStart' : {
				required: true
			},
			'monthRepayment' : {
				required : true
			}
			
		},
		messages : {
			 'assignLoan[employeeName]' : {
                employeeNameValidation: 'Enter a valid employee Name'
            }
		}
	});
	
	return true;
}

function loadRepayment( ){
	var loanId = $('#loanTitle' ).val();
	var i;
	var repayment = null;
	for( i = 0; i < loanRepayments.length; i++ ){
		if( loanId == loanRepayments[ i ].loanId ){
			repayment = loanRepayments[ i ].repayment;
			break;
		}
	}
	$( '#monthRepayment' ).val( repayment );
}
