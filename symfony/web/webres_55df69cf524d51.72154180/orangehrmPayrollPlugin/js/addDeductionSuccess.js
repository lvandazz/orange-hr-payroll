/**
 * @author Joseph Luvanda - lvandazz@gmail.com
 */

$( document ).ready( function(){
	
	
	if( $( '#btnSave' ).attr('value' ) == lang_edit ){
		$('#deductionTitle' ).attr('disabled', 'disable' );
		$('#employeeName' ).attr('disabled', 'disable' );
		$('#amount' ).attr('disabled', 'disable' );
		$('#paymentStart' ).attr('disabled', 'disable' );
		$('#isRecurring' ).attr('disabled', 'disable' );
		$('#extraInfo' ).attr('disabled', 'disable' );
	}
	
	
	    //Auto complete
		$("#employeeName").autocomplete(employeeList, {
		        formatItem: function(item) {
		            return $('<div/>').text(item.name).html();
		        },
		        formatResult: function(item) {
		            return item.name
		        },  
		        matchContains:true
		    }).result(function(event, item) {
		        //$("#candidateSearch_selectedCandidate").val(item.id);
		        //$("label.error").hide();
        });
		
	 //Auto load repayments
	 $( "#isRecurring" ).on('click' ,function (){
	 	
	 	
	 	if( $('#isRecurring').val() == 1){
	 		$('#isRecurring' ).val( 0 );
	 		$( '#repaymentAmount' ).attr('disabled','disabled'); 	
	 		
	 	}
	 	else{
	 		$( '#isRecurring' ).val(1);
	 		$( '#repaymentAmount' ).removeAttr('disabled')	
	 	} 
	 });
	// save 
	
	$( '#btnSave' ).click( function (){
		 
		if( $( '#btnSave' ).attr( 'value' ) == lang_edit ){
			
			$( '#deductionTitle' ).removeAttr( 'disabled' );
			$( '#employeeName' ).removeAttr( 'disabled' );
			$( '#amount' ).removeAttr( 'disabled' );
			$( '#paymentStart' ).removeAttr( 'disabled' );
			$( '#isRecurring' ).removeAttr( 'disabled' );
			
			if( $( '#isRecurring' ).attr( 'checked' ) != undefined ){
				$( '#isRecurring' ).val(1);
				$( '#repaymentAmount' ).removeAttr( 'disabled' );	
			}
			
			$( '#extraInfo' ).removeAttr( 'disabled' );
			
			$('#btnSave').attr( 'value' , lang_save );
			$('#btnBack' ).attr( 'value' , lang_cancel );
			
			return;
		}
 
		if( isValidForm()){
			$( 'form#formAddDeduction' ).attr({
				action : linkForaddDeduction + '?deductionId='+ deductionId
			} ); 
			
			$( '#formAddDeduction' ).submit();	
		}
		
	});
	
	$( '#btnBack' ).click( function (){
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_back ){
			window.location.replace( backBtnUrl );
		}
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_cancel ){
			window.location.replace( cancelBtnUrl + '?deductionId='+ deductionId );
		}
	});
	 
});
function isValidForm(){
	
	 $.validator.addMethod("employeeNameValidation", function(value, element, params) {
        var temp = false;
        var hmCount = employeeList.length;

        var i;
        for (i=0; i < hmCount; i++) {
            hmName = $.trim($('#employeeName').val()).toLowerCase();
            arrayName = employeeList[i].name.toLowerCase();
            if (hmName == arrayName) {
                $('#empId').val(employeeList[i].id);
                temp = true
                break;
            }
        }
        return temp;
    });
    
	$.validator.addMethod("alphabets", function(value, element, params) {
         
        return (value =="" ||(value == parseInt(value, 10)));
    });
   
    $.validator.addMethod( 'greaterThan', function( value, element, params ){ 
    	
    	var repayment = $( element ).val();
    	var amount = $( '#amount' ).val();
    	
    	repayment = repayment * 1;
    	amount = amount * 1;
    	
    	 if( repayment > amount ){
    	 	return false ;
    	 }
    	 return true;
    });
	var validator = $( '#formAddDeduction' ).validate({
		rules : {
			'addDeduction[deductionTitle]' : {
				required : true,
				
			},
			'addDeduction[employeeName]' : {
				required: true,
				employeeNameValidation : true
				
			},
			'addDeduction[amount]' : {
				required: true,
				number: true,
				min: 0
			},
			'addDeduction[repaymentStart]' : {
				required: true,
				date:true
			},
			'addDeduction[repaymentAmount]' : {
				required : true,
				number: true, 
				greaterThan: true,
				min: 0,
			}
			
			
		},
		messages : {
			 'addDeduction[employeeName]' : {
                employeeNameValidation: 'Enter a valid employee Name'
           },
           'addDeduction[repaymentAmount]' : {
           		greaterThan : 'Invalid repayment amount'
           }
		}
	});
	
	return true;
}
 