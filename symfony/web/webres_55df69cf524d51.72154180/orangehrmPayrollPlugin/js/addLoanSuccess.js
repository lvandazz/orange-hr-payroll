/**
 * @author Joseph Luvanda - lvandazz@gmail.com
 */

$( document ).ready( function(){
	
	
	if( $( '#btnSave' ).attr('value' ) == lang_edit ){
		$('#loanTitle' ).attr('disabled', 'disable' );
		$('#loanPrincipal' ).attr('disabled', 'disable' );
		$('#loanInterest' ).attr('disabled', 'disable' );
		$('#loanTerm' ).attr('disabled', 'disable' );
		$('#loanOrganization' ).attr('disabled', 'disable' );
		$('#loanDescription' ).attr('disabled', 'disable' );
	}
	

	
	// save 
	
	$( '#btnSave' ).click( function (){
		 
		if( $( '#btnSave' ).attr( 'value' ) == lang_edit ){
			
			$( '#loanTitle' ).removeAttr( 'disabled' );
			$( '#loanOrganization' ).removeAttr( 'disabled' );
			$( '#loanDescription' ).removeAttr( 'disabled' );
			$( '#loanPrincipal' ).removeAttr( 'disabled' );
			$( '#loanInterest' ).removeAttr( 'disabled' );
			$( '#loanTerm' ).removeAttr( 'disabled' );
			
			$('#btnSave').attr( 'value' , lang_save );
			$('#btnBack' ).attr( 'value' , lang_cancel );
			
			return;
		}
 
		if( isValidForm()){
			$( 'form#frmAddLoan' ).attr({
				action : linkForAddLoan + '?loanId='+ loanId
			} ); 
			$( '#frmAddLoan' ).submit();	
		}
		
	});
	
	$( '#btnBack' ).click( function (){
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_back ){
			window.location.replace( backBtnUrl );
		}
		
		if( $( '#btnBack' ).attr( 'value' ) == lang_cancel ){
			window.location.replace( cancelBtnUrl + '?loanId='+ loanId );
		}
	});
	 
});
function isValidForm(){
	
	 $.validator.addMethod("employeeNameValidation", function(value, element, params) {
        var temp = false;
        var hmCount = employeeList.length;

        var i;
        for (i=0; i < hmCount; i++) {
            hmName = $.trim($('#employeeName').val()).toLowerCase();
            arrayName = employeeList[i].name.toLowerCase();
            if (hmName == arrayName) {
                $('#employeeNameId').val(employeeList[i].id);
                temp = true
                break;
            }
        }
        return temp;
    });
    
	
	jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z\s\-]+$/i.test(value);
}, "Only alphabetical characters"); 
   
	var validator = $( '#frmAddLoan' ).validate({
		rules : {
			'addLoan[loanTitle]' : {
				required : true,
				lettersonly: true
			},
			'addLoan[loanPrincipal]' : {
				required: true,
				number : true,
				min: 0,
				maxlength: 11
			},
			'addLoan[loanTerm]' : {
				required: true,
				digits: true,
				min: 0,
				
			},
			'addLoan[loanInterest]' : {
				required : true,
				digits : true,
				min: 0,
				max: 50
			},
			
		},
		messages : {
			 'addLoan[employeeName]' : {
                employeeNameValidation: 'Enter a valid employee Name'
            }
		}
	});
	
	return true;
}