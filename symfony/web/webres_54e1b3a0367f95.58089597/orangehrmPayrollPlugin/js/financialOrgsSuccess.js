/**
 * @author Joseph Luvanda
 */
$(document).ready(function() {

    $('#frmList_ohrmListComponent').attr('name','frmList_ohrmListComponent');
    
    $('#btnDelete').attr('disabled','disabled');
      
    $("#ohrmList_chkSelectAll").click(function() {
        $('table.table input[id^="ohrmList_chkSelectRecord_"]').attr('checked', ($(this).attr('checked') == 'checked'));
        if($(":checkbox").length == 1) {
            $('#btnDelete').attr('disabled','disabled');
        }
        else {
            if($("#ohrmList_chkSelectAll").is(':checked')) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled','disabled');
            }
        }
    });
    
    
    $(':checkbox[name*="chkSelectRow[]"]').click(function() {
        if($(':checkbox[name*="chkSelectRow[]"]').is(':checked')) {
            $('#btnDelete').removeAttr('disabled');
        } else {
            $('#btnDelete').attr('disabled','disabled');
        }
    });
     

    $('#dialogDeleteBtn').click(function() {
        document.frmList_ohrmListComponent.submit();
    });
		
});
function addFinancialOrganization(){
	 window.location.replace(addFinancialOrganizationUrl);
}