/**
 * @author Joseph Luvanda
 */
$(document).ready(function() {

    $('#frmList_ohrmListComponent').attr('name','frmList_ohrmListComponent');
    
    $('#dialogConfirmBtn').attr('disabled','disabled');
      
    $("#ohrmList_chkSelectAll").click(function() {
        $('table.table input[id^="ohrmList_chkSelectRecord_"]').attr('checked', ($(this).attr('checked') == 'checked'));
        if($(":checkbox").length == 1) {
            $('#dialogConfirmBtn').attr('disabled','disabled');
        }
        else {
            if($("#ohrmList_chkSelectAll").is(':checked')) {
                $('#dialogConfirmBtn').removeAttr('disabled');
            } else {
                $('#dialogConfirmBtn').attr('disabled','disabled');
            }
        }
    });
    
    
    $(':checkbox[name*="chkSelectRow[]"]').click(function() {
        if($(':checkbox[name*="chkSelectRow[]"]').is(':checked')) {
            $('#dialogConfirmBtn').removeAttr('disabled');
        } else {
            $('#dialogConfirmBtn').attr('disabled','disabled');
        }
    });
     

    $('#dialogConfirmPrint').click(function() {
    	var actionLink = $( document.frmList_ohrmListComponent).attr('action' );
    	var form = $( document.frmList_ohrmListComponent);
    	form.attr( 'action' , printPayslipUrl + '/?print=1' );
    	form.submit(); 
        //document.frmList_ohrmListComponent.submit();
    });
    
    $('#dialogConfirmPreview').click(function() {
    	var actionLink = $( document.frmList_ohrmListComponent).attr('action' );
    	var form = $( document.frmList_ohrmListComponent);
    	form.attr( 'action' , printPayslipUrl + '/?preview=1' );
    	form.submit(); 
        //document.frmList_ohrmListComponent.submit();
    });
    
    
      $('#btnSrch').click(function() {
        $('#frmSrchPayroll').submit();
    });
		
});
function addAllowance(){
	 window.location.replace(addAllowanceUrl);
}
